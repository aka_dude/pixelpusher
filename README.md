# Pixelpusher

An online multiplayer action game with simple mechanics.

Control a small swarm of drones and fly them into your opponents while dodging
theirs. Game mode: Capture the flag.

Based on my favourite part of [diep.io](https://diep.io).

## Play

To play, download the latest client from
[Steam](https://store.steampowered.com/app/2495130/Pixelpusher/) or
[itch.io](https://aetup.itch.io/pixelpusher).
Alternatively, build the client from source using the build instructions below.

Run the client and provide the address of a pixelpusher game server.

### Controls

- WASD/Arrow keys: Move
- Left mouse button: Attract drones to cursor
- Right mouse button: Repel drones from cursor
- Left shift: Cast psi-storm at cursor
- Space: Dash drones in direction of attract/repel
- F: Drop flag
- Ctrl+H: Show help screen
- Ctrl+Q: Leave game

You *really* need a two button mouse. Keybindings can be customized.

## Build from source

Build the game using the [stack](https://docs.haskellstack.org) build tool:

### macOS

```
brew install openal-soft
stack run pixelpusher-client --extra-lib-dirs=/usr/local/opt/openal-soft/lib
```

### Ubuntu

```
sudo apt install libglfw3-dev libopenal-dev libopusfile-dev libxi-dev libxxf86vm-dev libxcursor-dev libxinerama-dev pkg-config
stack run pixelpusher-client
```

### NixOS

```
stack run pixelpusher-client
```

### Nix (not on NixOS)

You may need to use [nixGL](https://github.com/guibou/nixGL) to run the client.

```
stack --nix install
nixGL pixelpusher-client
```

## Host a server

The game server `pixelpusher-server` listens on port 30303 over both TCP and
UDP.
