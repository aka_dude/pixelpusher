
-- This module is empty in order to work around the fact that stack cannot load
-- more than one main module at a time.

module Main where

import Server.Main qualified

main :: IO ()
main = Server.Main.main
