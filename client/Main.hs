
-- This module is empty in order to work around the fact that stack cannot load
-- more than one main module at a time.

module Main where

import Client.Main qualified

main :: IO ()
main = Client.Main.main
