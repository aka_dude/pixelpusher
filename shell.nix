{ nixpkgs ?
    import (fetchTarball {
      name = "nixpkgs-unstable-2023-10-16";
      url = "https://github.com/nixos/nixpkgs/archive/46bd31a747326626f4e01e0f1f80446a4de6c026.tar.gz";
      # Hash obtained using `nix-prefetch-url --unpack <url>`
      sha256 = "0fw9cfp0nmdscvizwzcrwvilnhl86gm2w32lz9wpqpw47qbd5501";
    }) {}
}:

with nixpkgs;

let ghc = haskell.compiler.ghc928;
    hls = haskell-language-server.override {
      supportedGhcVersions = [ "928" ];
    };
in

haskell.lib.buildStackProject {
  inherit ghc;
  name = "pixelpusher";
  buildInputs = [
    glfw
    haskellPackages.fourmolu
    hls
    libGL
    openal
    opusfile
    pkg-config
    stack
    xorg.libX11
    xorg.libXcursor
    xorg.libXext
    xorg.libXi
    xorg.libXinerama
    xorg.libXrandr
    xorg.libXxf86vm
    zlib
  ];
}
