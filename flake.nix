{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    stacklock2nix.url = "github:cdepillabout/stacklock2nix";
    all-cabal-hashes = {
      url = "github:commercialhaskell/all-cabal-hashes/hackage";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, stacklock2nix, all-cabal-hashes }:
    let
      supportedSystems = [
        "x86_64-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "aarch64-darwin"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      pkgs = system: import nixpkgs {
        inherit system;
        overlays = [
          stacklock2nix.overlay
          self.overlays.default
        ];
      };
    in
    {
      overlays.default = final: prev: {
        inherit (final.pixelpusher-pkgSet.pkgSet)
          pixelpusher
          pixelpusher-client
          pixelpusher-server;
        pixelpusher-pkgSet = final.stacklock2nix {
          stackYaml = ./stack.yaml;
          stackYamlLock = ./stack.yaml.lock;
          baseHaskellPkgSet = final.haskell.packages.ghc928;
          additionalHaskellPkgSetOverrides = hfinal: hprev: {
            pixelpusher-client =
              final.haskell.lib.compose.overrideCabal
                {
                  pname = "pixelpusher-client";
                  buildTarget = "pixelpusher-client";
                }
                hfinal.pixelpusher;
            pixelpusher-server =
              final.haskell.lib.compose.overrideCabal
                {
                  pname = "pixelpusher-server";
                  buildTarget = "pixelpusher-server";
                }
                hfinal.pixelpusher;
            mkDerivation = a: hprev.mkDerivation (a // {
              doBenchmark = false;
              doCheck = false;
              doHaddock = false;
              doHoogle = false;
            });
          };
          cabal2nixArgsOverrides = default: default // {
            opusfile = _: {
              opusfile = final.opusfile;
            };
          };
          additionalDevShellNativeBuildInputs = stacklockHaskellPkgSet: [
            final.cabal-install
            final.haskell.packages.ghc928.haskell-language-server
          ];
          inherit all-cabal-hashes;
          # NOTE: by default localPkgFilter filters out all files starting with `dist`
          # which leads to `sounds/distant_crack*` missing
          # https://github.com/cdepillabout/stacklock2nix/blob/84694f48ddd8e49b96a02216ca2ab406fba25e65/nix/build-support/stacklock2nix/default.nix#L592
          localPkgFilter = defaultLocalPkgFilter: pkgName: path: type:
            final.lib.hasInfix "/sounds/" path || defaultLocalPkgFilter path type;
        };
      };
      packages = forAllSystems (system: {
        default = self.packages."${system}".pixelpusher-client;
        client = self.packages."${system}".pixelpusher-client;
        server = self.packages."${system}".pixelpusher-server;
        inherit (pkgs system)
          pixelpusher
          pixelpusher-client
          pixelpusher-server;
      });
      devShells = forAllSystems (system: {
        default = (pkgs system).pixelpusher-pkgSet.devShell.overrideAttrs (a: {
          shellHook =
            (a.shellHook or "") +
            ''
              export NIX_PATH=nixpkgs=${nixpkgs}
            '';
        });
        stack = (pkgs system).mkShell {
          packages = [ (pkgs system).stack ];
          shellHook =
            ''
              export NIX_PATH=nixpkgs=${nixpkgs}
            '';
        };
      });
      formatter = forAllSystems (system:
        let
          inherit (pkgs system) fd haskell nixpkgs-fmt writeScriptBin;
          inherit (haskell.packages.ghc928) fourmolu;
        in
        writeScriptBin "fourmolu-inline" ''
          set -ex
          ${fourmolu}/bin/fourmolu -i `${fd}/bin/fd -g *.hs src`
          ${nixpkgs-fmt}/bin/nixpkgs-fmt `${fd}/bin/fd -g *.nix .`
        '');
      legacyPackages = forAllSystems pkgs;
    };
}
