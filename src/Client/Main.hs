{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- The entry point of the client
module Client.Main (
  main,
) where

import Control.Logging qualified as Logging
import Control.Monad (unless)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.State.Class
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State.Strict hiding (get, gets, put)
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.List (intercalate)
import Data.String (IsString)
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (doesFileExist)
import System.IO
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Custom.Clock (threadDelayNSec)
import Pixelpusher.Custom.Float (Float4 (Float4))
import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerName

import Client.Audio.AL.Env
import Client.Config.CommandLineParameters
import Client.Config.Keybindings
import Client.Graphics.GL.Env
import Client.Graphics.Rendering.BasicMessage (renderBasicMessage)
import Client.Graphics.Rendering.Preload (preloadVisuals)
import Client.Scene.CustomMultiplayer
import Client.Scene.Keybindings
import Client.Scene.Multiplayer
import Client.Scene.NetworkedGame
import Client.Scene.Options hiding (optionsMenu)
import Client.Scene.Replay
import Client.Scene.Settings
import Client.Scene.SinglePlayer
import Client.Scene.SinglePlayerGame
import Client.Scene.Title

--------------------------------------------------------------------------------

main :: IO ()
main =
  Logging.withFileLogging "pixelpusher-client.log" $ do
    Logging.log "Starting pixelpusher-client."
    hSetBuffering stdout LineBuffering
    parseArgs >>= \case
      Replay replayFilePath -> playReplay replayFilePath
      RunClient cliParams -> runClient cliParams
    Logging.log "Exiting pixelpusher-client."

runClient :: CommandLineParams -> IO ()
runClient cliParams = do
  gameParams <- loadGameParameters
  -- TODO: Inform user if there are errors loading the client state
  (initialClientState, clientStateMessages) <- loadClientState
  let playerOptions = cs_options initialClientState
      initialAudioOptions =
        InitialAudioOptions
          { iao_masterGainPercent = po_masterVolume playerOptions
          , iao_musicGainPercent = po_musicVolume playerOptions
          , iao_effectsGainPercent = po_effectsVolume playerOptions
          }

  withALEnv initialAudioOptions $ \alEnv -> do
    withGLEnv $ \glEnv -> do
      setFullscreenMode glEnv $
        if po_enableFullscreen playerOptions
          then Fullscreen
          else Windowed
      preloadVisuals glEnv
      lastClientState <- do
        flip execStateT initialClientState $ do
          let env = Env (clp_port cliParams) glEnv alEnv gameParams
          flip runReaderT env $
            case clp_multiplayerOptions cliParams of
              Nothing ->
                titleMenu clientStateMessages
              Just multiplayerOpts -> do
                field @"cs_multiplayer" .= multiplayerOpts
                case parseMultiplayerOptions multiplayerOpts of
                  Left err ->
                    customMultiplayerMenu [err]
                  Right runOptions ->
                    runMultiplayer customMultiplayerMenu runOptions
      saveClientState lastClientState

--------------------------------------------------------------------------------
-- Client state

loadClientState :: IO (ClientState, [Text])
loadClientState = do
  exists <- doesFileExist clientStateFilePath
  if exists
    then do
      Logging.log $ "Reading previous settings from " <> clientStateFilePath
      parseResult <- Toml.decode <$> readFile' clientStateFilePath
      case parseResult of
        Toml.Failure errors -> do
          Logging.log $
            "Error reading previous settings: "
              <> Text.intercalate "; " (map Text.pack errors)
              <> ". Starting with default settings."
          pure (defaultClientState, [clientStateResetWarning])
        Toml.Success warnings clientState -> do
          unless (null warnings) $
            Logging.log $
              "Warnings encountered while reading last user parameters: "
                <> Text.intercalate "; " (map Text.pack warnings)
          pure (clientState, [])
    else do
      Logging.log $
        "File "
          <> clientStateFilePath
          <> " not found. Starting with default settings."
      pure (defaultClientState, [])

saveClientState :: ClientState -> IO ()
saveClientState clientState = do
  Logging.log "Saving current settings."
  withWriteFileViaTemp clientStateFilePath $ \handle ->
    Prettyprinter.renderIO handle $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode clientState

clientStateFilePath :: IsString a => a
clientStateFilePath = "pixelpusher-options.toml"

clientStateResetWarning :: Text
clientStateResetWarning = "Could not load settings; reverting to defaults."

--------------------------------------------------------------------------------
-- Game parameters

-- Note: We should never overwrite the game parameters file, if it exists.
loadGameParameters :: IO BaseGameParams
loadGameParameters =
  fmap makeBaseGameParams $
    doesFileExist gameParamsFilePath >>= \case
      False -> do
        Logging.log $
          Text.pack $
            concat
              [ "Could not find/open '"
              , gameParamsFilePath
              , "'. Writing default parameters to '"
              , gameParamsFilePath
              , "'."
              ]
        withWriteFileViaTemp gameParamsFilePath $ \h ->
          Prettyprinter.renderIO h $
            Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
              Toml.encode defaultTomlGameParams
        Logging.log $
          Text.pack $
            "Default game parameters written to " <> gameParamsFilePath
        pure defaultTomlGameParams
      True -> do
        parseResult <- Toml.decode <$> readFile' gameParamsFilePath
        case parseResult of
          Toml.Failure errors -> do
            Logging.log $
              Text.pack $
                "Error reading game parameters: " <> intercalate "; " errors
            Logging.log "Using default game parameters"
            pure defaultTomlGameParams
          Toml.Success warnings gameParams -> do
            unless (null warnings) $ do
              Logging.log $
                Text.pack $
                  "Warnings encountered while reading game parameters: "
                    <> intercalate "; " warnings
            pure gameParams

gameParamsFilePath :: FilePath
gameParamsFilePath = "pixelpusher-params.toml"

--------------------------------------------------------------------------------
-- State machine

type Scene a = ReaderT Env (StateT ClientState IO) a

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data ClientState = ClientState
  { cs_options :: PlayerOptions
  , cs_singlePlayer :: SinglePlayerOptions
  , cs_multiplayer :: MultiplayerOptions
  , cs_keybindings :: Keybindings
  }
  deriving (Generic)

defaultClientState :: ClientState
defaultClientState =
  ClientState
    { cs_options = defaultPlayerOptions
    , cs_singlePlayer = emptySinglePlayerOptions
    , cs_multiplayer = emptyCustomMultiplayerOptions
    , cs_keybindings = defaultKeybindings
    }

instance Toml.FromValue ClientState where
  fromValue =
    Toml.withTable
      "client options"
      (Toml.genericParseTableDefaults defaultClientState)

instance Toml.ToValue ClientState where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable ClientState where
  toTable = Toml.genericToTable

data Env = Env
  { env_port :: Int
  , env_glEnv :: GLEnv
  , env_alEnv :: ALEnv
  , env_gameParams :: BaseGameParams
  }

titleMenu :: [Text] -> Scene ()
titleMenu messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  resultMaybe <- liftIO (runTitle env_glEnv env_alEnv messages')
  for_ resultMaybe $ \case
    TitleResult_Multiplayer ->
      multiplayerMenu []
    TitleResult_CustomMultiplayer ->
      customMultiplayerMenu []
    TitleResult_SinglePlayer ->
      singlePlayerMenu []
    TitleResult_Settings ->
      settingsMenu []
    TitleResult_Quit ->
      pure ()

multiplayerMenu :: [Text] -> Scene ()
multiplayerMenu messages = do
  let messages' = map Text.unpack messages
  initialName <- gets (mo_playerName . cs_multiplayer)
  resultMaybe <- do
    Env{..} <- ask
    liftIO (runMultiplayerMenu env_glEnv env_alEnv initialName messages')
  for_ resultMaybe $ \(newName, result) -> do
    field @"cs_multiplayer" . field @"mo_playerName" .= newName
    case result of
      Multiplayer_Connect -> do
        let options =
              MultiplayerOptions
                { mo_serverAddress = "pixelpusher.one"
                , mo_playerName = newName
                }
        case parseMultiplayerOptions options of
          Left err ->
            multiplayerMenu [err]
          Right runOptions ->
            runMultiplayer multiplayerMenu runOptions
      Multiplayer_Back ->
        titleMenu []

customMultiplayerMenu :: [Text] -> Scene ()
customMultiplayerMenu messages = do
  let messages' = map Text.unpack messages
  initialOptions <- gets cs_multiplayer
  resultMaybe <- do
    Env{..} <- ask
    liftIO $
      runCustomMultiplayerMenu env_glEnv env_alEnv initialOptions messages'
  for_ resultMaybe $ \(newOptions, result) -> do
    field @"cs_multiplayer" .= newOptions
    case result of
      CustomMultiplayer_Connect -> do
        case parseMultiplayerOptions newOptions of
          Left err ->
            customMultiplayerMenu [err]
          Right runOptions ->
            runMultiplayer customMultiplayerMenu runOptions
      CustomMultiplayer_Back ->
        titleMenu []

singlePlayerMenu :: [Text] -> Scene ()
singlePlayerMenu messages = do
  Env{..} <- ask
  keybindings <- gets cs_keybindings
  let messages' = map Text.unpack messages
  initialOptions <- gets cs_singlePlayer
  resultMaybe <-
    liftIO (runSinglePlayerMenu env_glEnv env_alEnv initialOptions messages')
  for_ resultMaybe $ \(newOptions@SinglePlayerOptions{..}, result) -> do
    field @"cs_singlePlayer" .= newOptions
    case result of
      SinglePlayer_Start -> do
        case makePlayerNameText spo_playerName of
          Left err ->
            singlePlayerMenu [err]
          Right playerName -> do
            playerOptions <- gets cs_options
            newPlayerOptions <-
              liftIO $
                runSinglePlayerGame
                  env_glEnv
                  env_alEnv
                  env_gameParams
                  keybindings
                  playerOptions
                  playerName
                  spo_botDifficulty
                  spo_teamSize
            field @"cs_options" .= newPlayerOptions
            singlePlayerMenu []
      SinglePlayer_Back ->
        titleMenu []

settingsMenu :: [Text] -> Scene ()
settingsMenu messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  resultMaybe <- liftIO (runSettingsMenu env_glEnv env_alEnv messages')
  for_ resultMaybe $ \case
    Settings_Audiovisual -> optionsMenu []
    Settings_Keybindings -> keybindingsMenu []
    Settings_Back -> titleMenu []

optionsMenu :: [Text] -> Scene ()
optionsMenu messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  initialOptions <- do
    AudioLevels{..} <- liftIO $ getAudioLevels env_alEnv
    enableFullscreen <-
      liftIO $
        getFullscreenMode env_glEnv <&> \case
          Fullscreen -> True
          Windowed -> False
    gets cs_options <&> \playerOptions ->
      playerOptions
        { po_masterVolume = al_masterGainPercent
        , po_musicVolume = al_musicGainPercent
        , po_effectsVolume = al_effectsGainPercent
        , po_enableFullscreen = enableFullscreen
        }
  resultMaybe <-
    liftIO (runOptionsMenu env_glEnv env_alEnv initialOptions messages')
  for_ resultMaybe $ \newPlayerOptions -> do
    field @"cs_options" .= newPlayerOptions
    settingsMenu []

keybindingsMenu :: [Text] -> Scene ()
keybindingsMenu messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  initialKeybindings <- gets cs_keybindings
  resultMaybe <-
    liftIO (runKeybindingsMenu env_glEnv env_alEnv initialKeybindings messages')
  for_ resultMaybe $ \newPlayerOptions -> do
    field @"cs_keybindings" .= newPlayerOptions
    settingsMenu []


runMultiplayer ::
  ([Text] -> Scene ()) -> RunMultiplayerOptions -> Scene ()
runMultiplayer next options@RunMultiplayerOptions{..} = do
  Env{..} <- ask
  keybindings <- gets cs_keybindings
  liftIO $ renderBasicMessage env_glEnv (Float4 1 1 1 1) "Connecting ..."
  gameExitStatus <- do
    playerOptions <- gets cs_options
    liftIO $
      runMultiplayerGame
        env_glEnv
        env_alEnv
        rmo_serverAddress
        env_port
        rmo_playerName
        Nothing
        keybindings
        playerOptions
  case gameExitStatus of
    Exit_ConnectionTimeout -> do
      next ["Could not connect to server (timeout)"]
    Exit_PlayerQuit newPlayerOptions -> do
      field @"cs_options" .= newPlayerOptions
      next []
    Exit_Error errMsgs ->
      next errMsgs
    Exit_Rejoin msg ->
      rejoinMultiplayer next options (Text.unpack msg)

rejoinMultiplayer ::
  ([Text] -> Scene ()) -> RunMultiplayerOptions -> String -> Scene ()
rejoinMultiplayer next options msg = do
  glEnv <- asks env_glEnv
  liftIO $ do
    renderBasicMessage glEnv (Float4 1 1 1 1) (msg ++ "\n\nReconnecting ...")
    threadDelayNSec 1_500_000_000
  -- Don't provide a room. If we're being asked to rejoin, the room has
  -- probably been closed.
  runMultiplayer next options

--------------------------------------------------------------------------------
-- State machine helpers

data RunMultiplayerOptions = RunMultiplayerOptions
  { rmo_serverAddress :: String
  , rmo_playerName :: PlayerName
  }

-- Helper
parseMultiplayerOptions ::
  MultiplayerOptions -> Either Text RunMultiplayerOptions
parseMultiplayerOptions MultiplayerOptions{..} = do
  rmo_serverAddress <-
    if Text.null mo_serverAddress
      then Left "Please provide a server address"
      else Right $ Text.unpack mo_serverAddress
  rmo_playerName <- makePlayerNameText mo_playerName
  pure RunMultiplayerOptions{..}

-- -- Helper
-- parseRoomID :: Text -> Either Text (Maybe Int)
-- parseRoomID input =
--   if Text.null input
--     then Right Nothing
--     else case Text.decimal input of
--       Left _ ->
--         Left errMsg
--       Right (roomID, restOfInput) ->
--         if Text.null restOfInput then Right (Just roomID) else Left errMsg
--  where
--   errMsg = "Invalid room"
