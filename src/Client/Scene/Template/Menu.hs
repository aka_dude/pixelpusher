{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.Template.Menu (
  menuScene,
  customMenuScene,
  LoopContinue (..),
  drawMenu,
  uiSoundHigh,
  uiSoundLow,
) where

import Control.Monad.Morph (lift)
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Reader
import Data.Foldable (for_)
import Data.Functor.Compose
import Data.IORef
import Data.List.NonEmpty (NonEmpty (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Graphics.GL.Core33
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Clock
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters (defaultGameParams)
import Pixelpusher.Game.Parameters.Audio
import Pixelpusher.Game.Parameters.Base.Audio

import Client.Audio.AL.Env
import Client.GlobalKeyCallback (globalKeyCallback)
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped
import Client.Graphics.GLFW.KeyCallback (toGLFWKeyCallback)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.GameUI.DarkLayer (drawDarkLayer)
import Client.Graphics.Rendering.GameUI.HelpOverlay (drawHelpOverlay)
import Client.Graphics.Rendering.Text
import Client.Menu
import Client.Menu.GLFW qualified as Menu
import Client.Menu.Input (MenuInputQueue)
import Client.Menu.Input qualified as Menu

menuScene ::
  GLEnv ->
  ALEnv ->
  Text ->
  [String] ->
  NonEmpty (MenuItem widgets model) ->
  model ->
  (Maybe (model, MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  IO (Maybe result)
menuScene glEnv alEnv title messages menuSpec initialModel userHandleInput = do
  customMenuScene
    glEnv
    alEnv
    title
    messages
    menuSpec
    initialModel
    (const userHandleInput)

-- | Like 'menuScene', but allows for the event handler to make arbitrary
-- updates to the model.
customMenuScene ::
  GLEnv ->
  ALEnv ->
  Text ->
  [String] ->
  NonEmpty (MenuItem widgets model) ->
  model ->
  ((model -> IO ()) -> Maybe (model, MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  IO (Maybe result)
customMenuScene
  glEnv
  alEnv
  title
  messages
  menuSpec
  initialModel
  userHandleInput = do
    inputQueue <- Menu.newQueue
    stateRef <- newIORef $ makeMenuState title menuSpec initialModel

    withCharCallback glEnv (Menu.charCallback inputQueue) $ do
      let keyCallback =
            toGLFWKeyCallback $
              globalKeyCallback alEnv <> Menu.keyCallback inputQueue
      withKeyCallback glEnv keyCallback $
        evalContT $
          callCC $ \k ->
            runApproximateClock C.tickDelay_ns $ do
              loopContinue <-
                lift $
                  loopBody messages userHandleInput glEnv inputQueue stateRef
              case loopContinue of
                Nothing -> k Nothing
                Just (LoopFinished result) -> k (Just result)
                Just LoopContinue -> pure ()

data LoopContinue result = LoopContinue | LoopFinished result

loopBody ::
  [String] ->
  ((model -> IO ()) -> Maybe (model, MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  GLEnv ->
  MenuInputQueue ->
  IORef (MenuState widgets model) ->
  IO (Maybe (LoopContinue result))
loopBody messages userHandleInput glEnv inputQueue stateRef =
  GLFW.windowShouldClose (gle_window glEnv) >>= \case
    True ->
      pure Nothing
    False ->
      Just <$> loopBody' messages userHandleInput glEnv inputQueue stateRef

loopBody' ::
  [String] ->
  ((model -> IO ()) -> Maybe (model, MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  GLEnv ->
  MenuInputQueue ->
  IORef (MenuState widgets model) ->
  IO (LoopContinue result)
loopBody' messages userHandleInput glEnv inputQueue stateRef = do
  GLFW.pollEvents
  inputEvents <- Menu.flushQueue inputQueue

  displayHelp <- do
    let isPressed = fmap keyIsPressed . GLFW.getKey (gle_window glEnv)
    ctrl <-
      (||)
        <$> isPressed GLFW.Key'LeftControl
        <*> isPressed GLFW.Key'RightControl
    ctrlH <- (&& ctrl) <$> isPressed GLFW.Key'H
    pure $ if ctrlH then ShowHelp else HideHelp

  result <-
    evalContT . callCC $ \k -> do
      for_ inputEvents $ \inputEvent -> do
        state0 <- lift $ readIORef stateRef
        result <-
          case handleMenuInput inputEvent state0 of
            Nothing ->
              let writeModel =
                    writeIORef stateRef
                      . (\model -> set menuModel model state0)
              in  lift $ userHandleInput writeModel Nothing
            Just (state1, updateEvent) ->
              lift $ do
                writeIORef stateRef state1
                let writeModel =
                      writeIORef stateRef
                        . (\model -> set menuModel model state1)
                userHandleInput
                  writeModel
                  (Just (view menuModel state1, updateEvent))
        case result of
          LoopContinue -> pure ()
          finished@(LoopFinished _) -> k finished
      pure LoopContinue

  case result of
    LoopFinished _ ->
      -- Avoid flashing the menu when closng the window
      pure ()
    LoopContinue -> do
      -- Handle framebuffer size changes
      gle_pollFramebufferSizeUpdate glEnv >>= \case
        Nothing -> pure ()
        Just (w, h) -> setSquareViewport (fromIntegral w) (fromIntegral h)

      menuState <- readIORef stateRef

      windowSquareLength <-
        fmap (uncurry min) $ GLFW.getFramebufferSize $ gle_window glEnv
      glClearColor 0.0 0.0 0.0 1.0
      glClear GL_COLOR_BUFFER_BIT

      runGL . flip runReaderT (glEnv, windowSquareLength) $ do
        drawMenu messages menuState
        drawStatuses
        case displayHelp of
          HideHelp -> pure ()
          ShowHelp -> drawDarkLayer >> drawHelpOverlay defaultGameParams

      GLFW.swapBuffers $ gle_window glEnv

  pure result

data DisplayHelp = ShowHelp | HideHelp

drawMenu :: [String] -> MenuState widgets model -> ReaderT (GLEnv, Int) GL ()
drawMenu messages menuState =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let title = getMenuTitle menuState
        menuLines = renderMenu menuState

    let fontAtlasRegular = gle_fontAtlasRegular glEnv
        fontSizeRegular = fa_fontSize fontAtlasRegular
        lineSpacingRegular = fromIntegral $ (fontSizeRegular * 3) `div` 2
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        lineSpacingLarge = fromIntegral $ (fontSizeLarge * 3) `div` 2

    let x_center = lineSpacingRegular
        offWhite = Float4 0.9 0.9 0.9 1
        darkGrey = Float4 0.2 0.2 0.2 1

    -- Title lines
    let y_top =
          fromIntegral (windowSquareLength * 3 `div` 4)
            - fromIntegral fontSizeLarge
        (pos_menu, titleLines) =
          shadowText (0.5 * fromIntegral fontSizeLarge) darkGrey -- multiplier is a hack
            <$> stackedLines_topJustify
              CharAlignLeft
              (Float2 x_center y_top)
              lineSpacingLarge
              [(offWhite, Text.unpack title)]

    -- Menu lines
    -- TODO: cleanup
    let (messages_pos, preCharLines) =
          fmap getCompose $
            -- fmap (concatMap shadowText' . getCompose) $
            stackedLines_topJustify' CharAlignLeft pos_menu lineSpacingRegular $
              Compose $
                flip map menuLines $
                  \(selected, textFragments) ->
                    let textFragments' =
                          textFragments <&> \(visibility, text) ->
                            let color =
                                  case visibility of
                                    Invisible -> Float4 0 0 0 0
                                    Shadowed -> Float4 0 0 0 0
                                    Visible ->
                                      case selected of
                                        Selected -> Float4 0.96 0.69 0.14 1
                                        NotSelected -> offWhite
                            in  ((visibility, color), Text.unpack text)
                    in  (selected, textFragments')
        charLines =
          flip map preCharLines $ \(_selected, preCharLine) ->
            fmap (map (over _1 snd)) preCharLine
        shadowedCharLines =
          let shadowColor selected (visibility, _) =
                let color =
                      case selected of
                        NotSelected ->
                          darkGrey
                        Selected ->
                          Float4 (0.88 * 0.7) (0 * 0.7) (0.6 * 0.7) 1
                in  case visibility of
                      Visible -> color
                      Shadowed ->
                        let (rgb, a) = Util.toRGB_A color
                        in  Util.fromRGB_A (Float.map3 (* 0.7) rgb) a
                      Invisible -> Float4 0 0 0 0
          in  flip map preCharLines $ \(selected, preCharLine) ->
                shadowTextWith
                  (0.5 * fromIntegral fontSizeRegular) -- multiplier is a hack
                  (shadowColor selected)
                  preCharLine

    -- Status message lines
    let (_, messageLines) =
          shadowText (0.5 * fromIntegral fontSizeLarge) darkGrey -- multiplier is a hack
            <$> stackedLines_topJustify
              CharAlignLeft
              messages_pos
              lineSpacingRegular
              (map (offWhite,) ("" : messages)) -- empty line for spacing
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasLarge glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines = titleLines
        }
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasRegular glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines = shadowedCharLines ++ charLines
        }
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasRegular glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines = messageLines
        }

leftLines :: [(Float3, String)]
leftLines =
  reverse
    [ (grey, "Show controls :: hold Ctrl+H")
    ]

rightLines :: [(Float3, String)]
rightLines =
  reverse
    [ (grey, "v" <> C.versionString)
    ]

grey :: Float3
grey = let x = 0.5 in Float3 x x x

-- modified from `Client.Graphics.Rendering.GameUI.Bottom.drawStatuses`
drawStatuses ::
  ReaderT (GLEnv, Int) GL ()
drawStatuses =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let windowSquareLength' = fromIntegral windowSquareLength

    let fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall

        smallSpacing = fromIntegral $ fontSizeSmall `div` 2
        lineSpacing = fromIntegral $ 5 * fontSizeSmall `div` 4

    let makeDisplayLines ::
          CharAlignment ->
          [(Float3, String)] ->
          [(CharAlignment, Float2, [(Float4, String)])]
        makeDisplayLines align = zipWith (makeDisplayLine align) [0 ..]

        makeDisplayLine ::
          CharAlignment ->
          Int ->
          (Float3, String) ->
          (CharAlignment, Float2, [(Float4, String)])
        makeDisplayLine align lineNo (color, text) =
          ( align
          , Float2 pos_x (smallSpacing + fromIntegral lineNo * lineSpacing)
          , [(Util.fromRGB_A color 1, text)]
          )
         where
          pos_x = case align of
            CharAlignRight -> windowSquareLength' - smallSpacing
            CharAlignLeft -> smallSpacing
            CharAlignCenter -> 0.5 * windowSquareLength'
     in drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlasSmall
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                let displayLines =
                      makeDisplayLines CharAlignLeft leftLines
                        <> makeDisplayLines CharAlignRight rightLines
                in  shadowText
                      (fromIntegral (fa_fontSize fontAtlasSmall))
                      (Float4 0 0 0 1)
                      displayLines
            }

--------------------------------------------------------------------------------
-- Standard sounds

uiSoundHigh :: ALEnv -> IO ()
uiSoundHigh alEnv =
  playBonkMuffled alEnv defaultAudioParams' BonkHigh 0.40 (0, 0)

uiSoundLow :: ALEnv -> IO ()
uiSoundLow alEnv =
  playBonkMuffled alEnv defaultAudioParams' BonkLow 0.25 (0, 0)

defaultAudioParams' :: AudioParams
defaultAudioParams' =
  makeAudioParams $ fmap Fixed.fromDouble defaultAudioParams

--------------------------------------------------------------------------------
-- Helpers

keyIsPressed :: GLFW.KeyState -> Bool
keyIsPressed = (/=) GLFW.KeyState'Released
