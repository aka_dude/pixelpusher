{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.Settings (
  runSettingsMenu,
  SettingsResult (..),
) where

import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.AL.Env
import Client.Graphics.GL.Env hiding (FullscreenMode (..))
import Client.Menu
import Client.Scene.Template.Menu

runSettingsMenu :: GLEnv -> ALEnv -> [String] -> IO (Maybe SettingsResult)
runSettingsMenu glEnv alEnv messages =
  menuScene
    glEnv
    alEnv
    "settings"
    messages
    settingsMenu
    ()
    (handleInput alEnv)

data SettingsResult
  = Settings_Audiovisual
  | Settings_Keybindings
  | Settings_Back

type SettingsWidgets :: MenuItemType -> Type
data SettingsWidgets ty where
  Audiovisual :: SettingsWidgets 'Button
  Keybindings :: SettingsWidgets 'Button
  Back :: SettingsWidgets 'Button

settingsMenu :: NonEmpty (MenuItem SettingsWidgets ())
settingsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget Audiovisual ButtonConfig{bc_label = "audiovisual"}
    , ButtonWidget Keybindings ButtonConfig{bc_label = "keybindings"}
    , ButtonWidget Back ButtonConfig{bc_label = "back"}
    ]

handleInput ::
  ALEnv ->
  Maybe ((), MenuUpdateEvent SettingsWidgets) ->
  IO (LoopContinue SettingsResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just ((), menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished Settings_Back
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Audiovisual -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Audiovisual
      ButtonPressed Keybindings -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Keybindings
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished Settings_Back
