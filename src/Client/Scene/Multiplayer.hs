{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.Multiplayer (
  runMultiplayerMenu,
  MultiplayerResult (..),
) where

import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Data.Text (Text)

import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.AL.Env (ALEnv)
import Client.Graphics.GL.Env
import Client.Menu
import Client.Scene.Template.Menu

runMultiplayerMenu ::
  GLEnv ->
  ALEnv ->
  Text ->
  [String] ->
  IO (Maybe (Text, MultiplayerResult))
runMultiplayerMenu glEnv alEnv initialName messages =
  menuScene
    glEnv
    alEnv
    "multiplayer"
    messages
    multiplayerMenu
    initialName
    (handleInput alEnv)

data MultiplayerResult
  = Multiplayer_Connect
  | Multiplayer_Back

type MultiplayerWidgets :: MenuItemType -> Type
data MultiplayerWidgets ty where
  Name :: MultiplayerWidgets 'TextInput
  Connect :: MultiplayerWidgets 'Button
  Back :: MultiplayerWidgets 'Button

multiplayerMenu :: NonEmpty (MenuItem MultiplayerWidgets Text)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ TextInputWidget
        Name
        id
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , ButtonWidget
        Connect
        ButtonConfig
          { bc_label = "connect"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  ALEnv ->
  Maybe (Text, MenuUpdateEvent MultiplayerWidgets) ->
  IO (LoopContinue (Text, MultiplayerResult))
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (playerName, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (playerName, Multiplayer_Connect)
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished (playerName, Multiplayer_Back)
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Connect -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (playerName, Multiplayer_Connect)
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished (playerName, Multiplayer_Back)
