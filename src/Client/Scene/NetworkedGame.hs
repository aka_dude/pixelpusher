{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.NetworkedGame (
  GameExitStatus (..),
  runMultiplayerGame,
) where

import Control.Applicative ((<|>))
import Control.Concurrent.Async (ExceptionInLinkedThread (..), withAsync)
import Control.Concurrent.Async qualified as Async (link)
import Control.Concurrent.MVar
import Control.Exception
import Data.Functor (($>))
import Data.IORef
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Lens.Micro.Platform.Custom

import Network.Socket

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameProtocol
import Pixelpusher.Game.PlayerName
import Pixelpusher.Network.TCPProtocol

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.Graphics.GL.Env
import Client.Network qualified
import Client.Util.CancellableTimeout (cancellableTimeout)

import Client.Scene.Game.GameLoop
import Client.Scene.Game.GameStateBuffer
import Client.Scene.Options (PlayerOptions)

--------------------------------------------------------------------------------

-- | Connect to a server and run the game.
runMultiplayerGame ::
  GLEnv ->
  ALEnv ->
  HostName ->
  Int -> -- Port
  PlayerName ->
  Maybe Int -> -- Room
  Keybindings ->
  PlayerOptions ->
  IO GameExitStatus
runMultiplayerGame
  glEnv
  alEnv
  server
  port
  playerName
  mRoomID
  keybindings
  playerOptions =
    fmap (fromMaybe Exit_ConnectionTimeout) $
      cancellableTimeout C.connTimeLimit_ns $ \cancelTimeout ->
        fmap (either id Exit_PlayerQuit) $
          tryJust handler $
            Client.Network.connect server port playerName mRoomID $ \conn ->
              cancelTimeout
                >> startNetworkedGame glEnv alEnv conn keybindings playerOptions

data GameExitStatus
  = Exit_ConnectionTimeout
  | Exit_PlayerQuit PlayerOptions
  | Exit_Error [Text]
  | Exit_Rejoin Text

handler :: SomeException -> Maybe GameExitStatus
handler e = handleIO e <|> handleTCPClose e

handleIO :: SomeException -> Maybe GameExitStatus
handleIO e =
  fromException @IOException e $> Exit_Error ["Could not connect to server"]

handleTCPClose :: SomeException -> Maybe GameExitStatus
handleTCPClose e =
  fromException @TCPConnectionClosedException e <&> \case
    ClosedBecause reconnect reason -> case reconnect of
      Reconnect ->
        Exit_Rejoin reason
      DoNotReconnect ->
        Exit_Error ["Connection closed by server: ", reason]
    UnexpectedClose ->
      Exit_Error ["Connection unexpectedly closed by server"]

--------------------------------------------------------------------------------

startNetworkedGame ::
  GLEnv ->
  ALEnv ->
  Client.Network.Connection ->
  Keybindings ->
  PlayerOptions ->
  IO PlayerOptions
startNetworkedGame glEnv alEnv conn keybindings playerOptions = do
  gameStateBuffer <- newGameStateBuffer
  latencyRef <- newIORef @(Maybe Int) Nothing
  sendMsgHandlesVar <- newEmptyMVar

  let runNetworkClient =
        Client.Network.runClient conn bufferPutMsg sendMsgHandlesVar putLatency
       where
        bufferPutMsg = insertUpdate gameStateBuffer . unServerGameMsg
        putLatency = writeIORef latencyRef . Just
  withAsync runNetworkClient $ \networkClientAsync ->
    handle unwrapLinkedException $ do
      Async.link networkClientAsync
      sendMsgHandles <- takeMVar sendMsgHandlesVar
      runGameLoop glEnv alEnv keybindings playerOptions $
        networkedGameLoop sendMsgHandles gameStateBuffer latencyRef

unwrapLinkedException :: ExceptionInLinkedThread -> IO a
unwrapLinkedException (ExceptionInLinkedThread _ e) = throwIO e
