{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.Keybindings (
  runKeybindingsMenu,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Generics.Product.Fields
import Data.IORef
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.Graphics.GL.Env hiding (FullscreenMode (..))
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas (fa_fontSize)
import Client.Graphics.Rendering.GameUI.DarkLayer (drawDarkLayer)
import Client.Graphics.Rendering.Text
import Client.Menu
import Client.Scene.Template.Menu

runKeybindingsMenu ::
  GLEnv -> ALEnv -> Keybindings -> [String] -> IO (Maybe Keybindings)
runKeybindingsMenu glEnv alEnv initialKeybindings messages =
  customMenuScene
    glEnv
    alEnv
    "keybindings"
    messages
    keybindingsMenu
    initialKeybindings
    (handleInput glEnv alEnv)

type KeybindingsWidgets :: MenuItemType -> Type
data KeybindingsWidgets ty where
  OverseerUp :: KeybindingsWidgets 'Keybind
  OverseerLeft :: KeybindingsWidgets 'Keybind
  OverseerDown :: KeybindingsWidgets 'Keybind
  OverseerRight :: KeybindingsWidgets 'Keybind
  OverseerDash :: KeybindingsWidgets 'Keybind
  PsiStorm :: KeybindingsWidgets 'Keybind
  DropFlag :: KeybindingsWidgets 'Keybind
  SwapMouseButtons :: KeybindingsWidgets 'Toggle
  InvertLeftMouseButton :: KeybindingsWidgets 'Toggle
  Back :: KeybindingsWidgets 'Button

keybindingsMenu :: NonEmpty (MenuItem KeybindingsWidgets Keybindings)
keybindingsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ KeybindWidget
        OverseerUp
        kb_overseerUp
        KeybindConfig{kc_label = "up"}
    , KeybindWidget
        OverseerLeft
        kb_overseerLeft
        KeybindConfig{kc_label = "left"}
    , KeybindWidget
        OverseerDown
        kb_overseerDown
        KeybindConfig{kc_label = "down"}
    , KeybindWidget
        OverseerRight
        kb_overseerRight
        KeybindConfig{kc_label = "right"}
    , KeybindWidget
        OverseerDash
        kb_overseerDash
        KeybindConfig{kc_label = "dash"}
    , KeybindWidget
        PsiStorm
        kb_psiStorm
        KeybindConfig{kc_label = "psionic storm"}
    , KeybindWidget
        DropFlag
        kb_dropFlag
        KeybindConfig{kc_label = "drop flag"}
    , ToggleWidget
        SwapMouseButtons
        (field @"kb_swapMouseButtons")
        ToggleConfig{tc_label = "swap mouse buttons"}
    , ToggleWidget
        InvertLeftMouseButton
        (field @"kb_invertLeftMouseButton")
        ToggleConfig{tc_label = "invert left mouse button"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  GLEnv ->
  ALEnv ->
  (Keybindings -> IO ()) ->
  Maybe (Keybindings, MenuUpdateEvent KeybindingsWidgets) ->
  IO (LoopContinue Keybindings)
handleInput glEnv alEnv writeModel = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (keybindings, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished keybindings
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerUp -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "up" (field @"kb_overseerUp") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerLeft -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "left" (field @"kb_overseerLeft") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerDown -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "down" (field @"kb_overseerDown") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerRight -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "right" (field @"kb_overseerRight") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerDash -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "dash" (field @"kb_overseerDash") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered PsiStorm -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "psionic storm" (field @"kb_psiStorm") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered DropFlag -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "drop flag" (field @"kb_dropFlag") keybindings
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched SwapMouseButtons _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched InvertLeftMouseButton _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished keybindings
 where
  keybindTriggeredHandler ::
    String ->
    ASetter' Keybindings GLFW.Key ->
    Keybindings ->
    IO ()
  keybindTriggeredHandler label setter_ keybindings = do
    key <- getKeyPress glEnv label
    writeModel $ set setter_ key keybindings

getKeyPress :: GLEnv -> String -> IO GLFW.Key
getKeyPress glEnv label = do
  windowSquareLength <-
    fmap (uncurry min) $ GLFW.getFramebufferSize $ gle_window glEnv

  runGL . flip runReaderT (glEnv, windowSquareLength) $ do
    drawDarkLayer
    lift $ do
      let screenWidth = fromIntegral windowSquareLength
          x = screenWidth / 2
          y = screenWidth * 3 / 4
          fontAtlas = gle_fontAtlasLarge glEnv
          spacing = fromIntegral $ fa_fontSize fontAtlas
          msgLines = ["press key for [" ++ label ++ "]"]
          color = Float4 1 1 1 1

      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_fontAtlas = fontAtlas
          , cvp_screenWidth = fromIntegral windowSquareLength
          , cvp_lines =
              snd $
                stackedLines_topJustify CharAlignCenter (Float2 x y) spacing $
                  map (color,) msgLines
          }
  GLFW.swapBuffers $ gle_window glEnv

  keyRef <- newIORef Nothing
  let keyCallback _window key _scanCode keyState _modifiers =
        case keyState of
          GLFW.KeyState'Pressed -> writeIORef keyRef (Just key)
          GLFW.KeyState'Released -> pure ()
          GLFW.KeyState'Repeating -> pure ()
  let loop = do
        GLFW.pollEvents
        readIORef keyRef >>= \case
          Nothing -> loop
          Just key -> pure key
  withKeyCallback glEnv keyCallback loop
