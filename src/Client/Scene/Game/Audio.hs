{-# LANGUAGE ViewPatterns #-}

module Client.Scene.Game.Audio (
  runGameAudio,
) where

import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (toFloats)
import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents (FlagType (BaseFlagType))
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player (player_team)
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Time

import Client.Audio.AL.Env
import Client.Audio.Game (playSfx)
import Client.Constants qualified as CC

runGameAudio ::
  ALEnv ->
  GameEvents ->
  GameScene ->
  Float2 ->
  Maybe ActorID ->
  IO ()
runGameAudio alEnv fo_gameEvents fo_gameScene cameraPos mActorID = do
  let mPlayerStuff =
        flip WIM.lookup (scene_players fo_gameScene) =<< mActorID
      mPlayerView = snd <$> mPlayerStuff

  -- Play sound effects
  let audioParams = gp_audio $ scene_gameParams fo_gameScene
      (listenPos, listenRadius) =
        case mPlayerView of
          Nothing -> (cameraPos, CC.viewRadius)
          Just playerView ->
            case pv_overseerView playerView of
              OV_Dead OverseerViewDead{ovd_deathTime, ovd_lastPos} ->
                let ticksSinceDeath =
                      fromIntegral $
                        scene_time fo_gameScene `diffTime` ovd_deathTime
                    timeConstantTicks = fromIntegral C.tickRate_hz
                    radius =
                      CC.viewRadius
                        * exp (-ticksSinceDeath / timeConstantTicks)
                in  (toFloats ovd_lastPos, radius)
              OV_Alive OverseerViewAlive{ova_pos} ->
                (toFloats ova_pos, CC.viewRadius)
      listenTeam = pv_team <$> mPlayerView
  playSfx alEnv audioParams listenRadius listenPos listenTeam fo_gameEvents

  -- Adjust music track gains
  case mPlayerStuff of
    Nothing ->
      setTrackGains
        alEnv
        TrackGains
          { tg_slow = 0
          , tg_fast = 1
          , tg_slowFast = 1
          , tg_flag = 0
          }
        Nothing
    Just (player, playerView) ->
      case scene_gamePhase fo_gameScene of
        GamePhase_Intermission{} ->
          let trackGains =
                TrackGains
                  { tg_slow = 0
                  , tg_fast = 0
                  , tg_slowFast = 0
                  , tg_flag = 0
                  }
          in  -- hack: taking into account adaptive music decay factor and bgm bpm
              setTrackGains alEnv trackGains (Just 12)
        GamePhase_Game{} ->
          let renderingComps = scene_renderingComps fo_gameScene
              viewTeam = view player_team player
              isInView' = case pv_overseerView playerView of
                OV_Dead{} -> const False
                OV_Alive OverseerViewAlive{ova_pos} ->
                  isInView CC.viewRadius (toFloats ova_pos)
              numEnemyOverseersInView =
                length $
                  flip filter (rcs_overseers renderingComps) $ \rc ->
                    viewTeam /= or_team (rendering_entityRendering rc)
                      && isInView' rc
              numEnemyDronesInView =
                length $
                  flip filter (rcs_drones renderingComps) $ \rc ->
                    viewTeam /= dr_team (rendering_entityRendering rc)
                      && isInView' rc
              fastGain =
                min 1 $
                  fromIntegral numEnemyOverseersInView
                    + 0.125 * fromIntegral numEnemyDronesInView
              alliedBaseFlags =
                length $
                  flip filter (scene_flags fo_gameScene) $
                    \(team, _, isBaseFlag) ->
                      team == viewTeam && isBaseFlag == BaseFlagType
              flagGain = fromIntegral (1 - alliedBaseFlags)
              trackGains =
                TrackGains
                  { tg_slow = 1 - fastGain
                  , tg_fast = fastGain
                  , tg_slowFast = 1
                  , tg_flag = flagGain
                  }
          in  setTrackGains alEnv trackGains Nothing
