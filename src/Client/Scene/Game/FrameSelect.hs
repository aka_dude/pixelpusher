{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

-- | Logic for selecting which frame to display when the delivery of frames
-- from the server is unsteady.
module Client.Scene.Game.FrameSelect (
  FrameSelectState,
  initialFrameSelectState,
  getFrame,
) where

import Control.Monad (when)
import Control.Monad.IO.Class
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Data.Generics.Product.Fields (field)
import Data.Int (Int32)
import Data.IntMap.Strict qualified as IM
import Data.Maybe (fromMaybe)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Clock (getMonotonicTimeNSecInt)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Client.Scene.Game.GameStateBuffer

data FrameSelectState = FrameSelectState
  { fss_frameShiftState :: FrameShiftState
  , fss_frameTimingHistory :: FrameTimingHistory
  }
  deriving stock (Generic)

initialFrameSelectState :: FrameSelectState
initialFrameSelectState =
  FrameSelectState
    { fss_frameShiftState = emptyFrameShiftState
    , fss_frameTimingHistory = emptyFrameTimingHistory
    }

data FrameShiftState = FrameShiftState
  { fss_ticksAheadHistory :: TicksAheadHistory
  , fss_prevFrameTime :: Int
  }
  deriving stock (Generic)

emptyFrameShiftState :: FrameShiftState
emptyFrameShiftState =
  FrameShiftState
    { fss_ticksAheadHistory = emptyTicksAheadHistory
    , fss_prevFrameTime = minBound
    }

data TicksAheadHistory = TicksAheadHistory
  { fah_samples :: Int
  , fah_histogram :: IM.IntMap Int
  }
  deriving stock (Generic)

emptyTicksAheadHistory :: TicksAheadHistory
emptyTicksAheadHistory =
  TicksAheadHistory
    { fah_samples = 0
    , fah_histogram = IM.empty
    }

data FrameTimingHistory = FrameTimingHistory
  { fth_samples :: Int
  , fth_predicted :: Int
  , fth_minimum :: Int
  }
  deriving stock (Generic)

emptyFrameTimingHistory :: FrameTimingHistory
emptyFrameTimingHistory =
  FrameTimingHistory
    { fth_samples = 0
    , fth_predicted = 0
    , fth_minimum = maxBound
    }

getFrame ::
  (Int -> IO ()) ->
  GameStateBuffer ->
  Ticks -> -- the period between invocations of this function
  StateT FrameSelectState IO (Maybe FrameOutputs)
getFrame delayClock gameStateBuffer ticksPerFrame = do
  (frameOutputsMaybe, ticksAheadOfServer) <-
    zoom (field @"fss_frameShiftState") $ do
      -- Compute frame shift
      frameShift <-
        field @"fss_ticksAheadHistory" %%= \history ->
          if fromIntegral (fah_samples history) * ticksPerFrame < ticksAheadSamplingTicks
            then (0, history)
            else
              let rawShift = fromIntegral $ negate $ medianTicksAhead history
                  maxShift = 5 * C.tickRate_hz
                  frameShift = max (-maxShift) $ min maxShift rawShift
              in  (frameShift, emptyTicksAheadHistory)

      -- Get next frame; apply frame shift
      (frameOutputsMaybe, ticksAheadOfServer) <-
        liftIO $ getNextScene gameStateBuffer $ ticksPerFrame + Ticks frameShift

      -- Record frame shift sample, but only if the delay between renders is at
      -- least somewhat stable.
      isDelayStable <-
        zoom (field @"fss_prevFrameTime") $
          checkRenderPeriodStability ticksPerFrame
      when isDelayStable $
        field @"fss_ticksAheadHistory" %= addTicksAheadSample ticksAheadOfServer

      pure (frameOutputsMaybe, ticksAheadOfServer)

  -- Compute and apply render timing shift
  zoom (field @"fss_frameTimingHistory") $ do
    clockDelayMaybe <-
      -- Compute frame timing
      id %%= \history ->
        -- TODO: is there a way to do this without lens?
        if fromIntegral (fth_samples history) * ticksPerFrame < frameTimingSamplingTicks
          then -- Not enough samples: wait
            (Nothing, history)
          else -- Flush history

            (,emptyFrameTimingHistory) $
              if fromIntegral (fth_predicted history) * ticksPerFrame >= frameTimingSamplingTicks `div` 5
                then -- We cannot perfectly distinguish between frames that had to be predicted
                -- because of network issues or because the game state
                -- computation took too long. For now we don't try.
                case frameTimingMinimum history of
                  Nothing ->
                    -- All samples were predictions: do nothing and wait for
                    -- the frame shift logic to kick in
                    Nothing
                  Just minDelay_ns ->
                    -- Too much prediction: delay clock
                    if minDelay_ns + frameTimingDelayShift_ns < frameTimingMaxDelay_ns
                      then Just frameTimingDelayShift_ns
                      else Nothing
                else case frameTimingMinimum history of
                  Nothing ->
                    -- This shouldn't happen
                    Nothing
                  Just minDelay_ns ->
                    let excessDelay_ns = minDelay_ns - frameTimingMinDelay_ns
                    in  if excessDelay_ns > 0
                          then Just (-excessDelay_ns `div` 2)
                          else Just frameTimingDelayShift_ns

    -- Apply frame timing
    liftIO $ for_ clockDelayMaybe delayClock

    -- Record frame timing sample
    if
        | Just frameOutputs <- frameOutputsMaybe
        , Just frameMonotonicTime <- fo_monotonicTimeNSec frameOutputs ->
            when (ticksAheadOfServer == 0) $ do
              monotonicTime <- liftIO getMonotonicTimeNSecInt
              let frameDelay = monotonicTime - frameMonotonicTime
              modify' $ addFrameTimingSample (Just frameDelay)
        | ticksAheadOfServer > 0 ->
            modify' $ addFrameTimingSample Nothing
        | otherwise ->
            pure ()

  pure frameOutputsMaybe

ticksAheadSamplingTicks :: Ticks
ticksAheadSamplingTicks = 2 * fromIntegral C.tickRate_hz

frameTimingSamplingTicks :: Ticks
frameTimingSamplingTicks = 2 * fromIntegral C.tickRate_hz

frameTimingMinDelay_ns :: Int
frameTimingMinDelay_ns = 250_000 -- 0.25 ms

frameTimingMaxDelay_ns :: Int
frameTimingMaxDelay_ns = 2_000_000 -- 2 ms

frameTimingDelayShift_ns :: Int
frameTimingDelayShift_ns = 250_000 -- 0.25 ms

addTicksAheadSample :: Int32 -> TicksAheadHistory -> TicksAheadHistory
addTicksAheadSample ticksAhead history =
  history
    & field @"fah_samples" +~ 1
    & field @"fah_histogram" . at (fromIntegral ticksAhead)
      %~ Just . maybe 1 succ

addFrameTimingSample :: Maybe Int -> FrameTimingHistory -> FrameTimingHistory
addFrameTimingSample sample history =
  case sample of
    Nothing ->
      history
        & field @"fth_samples" +~ 1
        & field @"fth_predicted" +~ 1
    Just delay ->
      history
        & field @"fth_samples" +~ 1
        & field @"fth_minimum" %~ min delay

frameTimingMinimum :: FrameTimingHistory -> Maybe Int
frameTimingMinimum FrameTimingHistory{..} =
  if fth_samples == fth_predicted
    then Nothing
    else Just fth_minimum

-- Approximate
medianTicksAhead :: TicksAheadHistory -> Int
medianTicksAhead history =
  let nTake = (fah_samples history + 1) `div` 2
  in  fromMaybe 0 $ indexHistogram nTake $ IM.toAscList $ fah_histogram history

indexHistogram :: Int -> [(a, Int)] -> Maybe a
indexHistogram _ [] = Nothing
indexHistogram nTake ((val, count) : xs)
  | nTake <= count = Just val
  | otherwise = indexHistogram (nTake - count) xs

checkRenderPeriodStability :: Ticks -> StateT Int IO Bool
checkRenderPeriodStability ticksPerFrame = do
  time <- liftIO getMonotonicTimeNSecInt
  oldTime <- id <<.= time
  pure $
    let timeSinceLastRender_ns = time - oldTime
        tickDelay_ns = fromIntegral C.tickDelay_ns
        diffDelay_ns =
          timeSinceLastRender_ns
            - fromIntegral (getTicks ticksPerFrame) * tickDelay_ns
        isDelayStable =
          -tickDelay_ns < diffDelay_ns && diffDelay_ns < tickDelay_ns
    in  isDelayStable
