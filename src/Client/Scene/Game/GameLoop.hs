{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Scene.Game.GameLoop (
  GameLoop,
  runGameLoop,
  networkedGameLoop,
  singlePlayerGameLoop,
) where

import Control.Concurrent.Async
import Control.Exception (bracket_)
import Control.Monad (when)
import Control.Monad.IO.Class
import Control.Monad.Morph (hoist, lift)
import Control.Monad.ST (RealWorld, stToIO)
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Except
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields (field)
import Data.IORef
import Data.Int (Int32)
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom
import System.Mem (performMinorGC)

import Pixelpusher.Custom.Clock (runAdjustableClock, runClock)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.GameProtocol
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.Graphics.CameraInterpolation
import Client.Graphics.GL.Env
import Client.Graphics.Rendering.GameScene
import Client.Graphics.Rendering.SceneDecorations
import Client.Menu
import Client.Menu.Input (MenuInputQueue)
import Client.Menu.Input qualified as Menu
import Client.Network (ClientSendHandles (..))

import Client.Scene.Game.Audio (runGameAudio)
import Client.Scene.Game.FrameSelect
import Client.Scene.Game.GameStateBuffer
import Client.Scene.Game.Input
import Client.Scene.Options
import Client.Scene.Options qualified as OptionsMenu
import Client.Scene.Template.Menu

--------------------------------------------------------------------------------

data RenderLoopState = RenderLoopState
  { rls_renderTicks :: Int32
  , rls_cameraPosVel :: (Float2, Float2)
  , rls_frameSelectState :: FrameSelectState
  , rls_lastGameStateBufferOutputs :: Maybe FrameOutputs
  }
  deriving stock (Generic)

data InputLoopState = InputLoopState
  { ils_inputTicks :: Int
  , ils_lastInputUpdateTicks :: PreviousInputUpdateTicks
  , ils_lastInputUpdate :: PlayerControlsTransport
  , ils_showTutorial :: ShowTutorial
  , ils_showMenu :: ShowMenu
  , ils_menuState :: MenuState OptionsWidgets PlayerOptions
  }
  deriving stock (Generic)

data PreviousInputUpdateTicks = PreviousInputUpdateTicks
  { piut_older :: Int
  , piut_newer :: Int
  }

pushInputUpdateTick ::
  Int -> PreviousInputUpdateTicks -> PreviousInputUpdateTicks
pushInputUpdateTick newTick previousTicks =
  PreviousInputUpdateTicks
    { piut_older = piut_newer previousTicks
    , piut_newer = newTick
    }

data ShowTutorial = ShowTutorial | DoNotShowTutorial

data ShowMenu = ShowMenu | DoNotShowMenu

initialRenderLoopState :: RenderLoopState
initialRenderLoopState =
  RenderLoopState
    { rls_renderTicks = 0
    , rls_cameraPosVel = (Float2 0 0, Float2 0 0) -- arbitrary
    , rls_frameSelectState = initialFrameSelectState
    , rls_lastGameStateBufferOutputs = Nothing
    }

initialInputLoopState :: PlayerOptions -> InputLoopState
initialInputLoopState playerOptions =
  InputLoopState
    { ils_inputTicks = 0
    , ils_lastInputUpdateTicks = PreviousInputUpdateTicks 0 0
    , ils_lastInputUpdate = defaultControlsTransport
    , ils_showTutorial = ShowTutorial
    , ils_showMenu = DoNotShowMenu
    , ils_menuState =
        makeMenuState "audiovisual" optionsMenu playerOptions
    }

ils_showTutorial_showMenu ::
  Lens' InputLoopState (ShowTutorial, ShowMenu)
ils_showTutorial_showMenu f s =
  let showTutorial = ils_showTutorial s
      showMenu = ils_showMenu s
  in  f (showTutorial, showMenu)
        <&> \(showTutorial', showMenu') ->
          s
            & field @"ils_showTutorial" .~ showTutorial'
            & field @"ils_showMenu" .~ showMenu'

--------------------------------------------------------------------------------

data GameLoop = GameLoop
  { gl_inputLoop :: InputLoop
  , gl_renderLoop :: RenderLoop
  }

newtype InputLoop
  = InputLoop
      ( IORef UIView ->
        IORef CameraPos ->
        GLEnv ->
        ALEnv ->
        InputEventsHandle ->
        MenuInputQueue ->
        Keybindings ->
        StateT InputLoopState IO PlayerContinueStatus
      )

newtype RenderLoop
  = RenderLoop
      ( (Int -> IO ()) ->
        IORef UIView ->
        IORef CameraPos ->
        GLEnv ->
        ALEnv ->
        Keybindings ->
        StateT RenderLoopState IO ()
      )

type CameraPos = Float2

data UIView = UIView
  { uiv_showTutorial :: ShowTutorial
  , uiv_showMenu :: ShowMenu
  , uiv_menuState :: MenuState OptionsWidgets PlayerOptions
  , uiv_statsOverlay :: Maybe StatsOverlay
  , uiv_displayHelp :: DisplayHelp
  }

inputDelayTicks :: Int
inputDelayTicks = 8

inputLoopDelay_ns :: Int
inputLoopDelay_ns = C.tickDelay_ns `div` inputDelayTicks

-- | This function must be called from the main thread because it performs
-- calls to GLFW.
runGameLoop ::
  GLEnv ->
  ALEnv ->
  Keybindings ->
  PlayerOptions ->
  GameLoop ->
  IO PlayerOptions
runGameLoop glEnv alEnv keybindings playerOptions gameLoop = do
  withMusicGains alEnv $ do
    inputEvents <- newInputEventsHandle
    menuInputQueue <- Menu.newQueue
    withKeyCallback glEnv (gameKeyCallback alEnv inputEvents) $ do
      let initLoopState = initialInputLoopState playerOptions
          initUIView =
            UIView
              { uiv_showTutorial = ils_showTutorial initLoopState
              , uiv_showMenu = ils_showMenu initLoopState
              , uiv_menuState = ils_menuState initLoopState
              , uiv_statsOverlay = Nothing
              , uiv_displayHelp = HideHelp
              }
      uiViewRef <- newIORef initUIView
      cameraPosRef <- newIORef $ fst $ rls_cameraPosVel initialRenderLoopState

      let renderingThread =
            bracket_ (GLFW.makeContextCurrent (Just (gle_window glEnv))) (GLFW.makeContextCurrent Nothing) $
              flip execStateT initialRenderLoopState $
                runAdjustableClock C.tickDelay_ns $ \delayClock ->
                  let RenderLoop renderLoop = gl_renderLoop gameLoop
                  in  renderLoop
                        delayClock
                        uiViewRef
                        cameraPosRef
                        glEnv
                        alEnv
                        keybindings
      bracket_ (GLFW.makeContextCurrent Nothing) (GLFW.makeContextCurrent (Just (gle_window glEnv)))
        $ bracket_
          (GLFW.setCursorInputMode (gle_window glEnv) GLFW.CursorInputMode'Hidden)
          (GLFW.setCursorInputMode (gle_window glEnv) GLFW.CursorInputMode'Normal)
        $ withAsyncBound renderingThread
        $ \_ ->
          evalContT . callCC $ \k -> do
            -- provides a way to break out of the loop
            fmap (view menuModel . ils_menuState) $
              flip execStateT initLoopState $
                runClock inputLoopDelay_ns $ do
                  -- the loop
                  playerContinueStatus <-
                    hoist lift $ do
                      let InputLoop inputLoop = gl_inputLoop gameLoop
                      inputLoop
                        uiViewRef
                        cameraPosRef
                        glEnv
                        alEnv
                        inputEvents
                        menuInputQueue
                        keybindings
                  case playerContinueStatus of
                    PlayerQuit -> do
                      gets (view menuModel . ils_menuState) >>= lift . k
                    PlayerContinue ->
                      liftIO performMinorGC

-- | Set music gains when starting and stopping the game loop
withMusicGains :: ALEnv -> IO a -> IO a
withMusicGains alEnv =
  bracket_
    (setTrackGains alEnv initialGains Nothing)
    (setTrackGains alEnv zeroGains Nothing)
 where
  initialGains =
    TrackGains{tg_slow = 0, tg_fast = 1, tg_slowFast = 1, tg_flag = 0}
  zeroGains =
    TrackGains{tg_slow = 0, tg_fast = 0, tg_slowFast = 0, tg_flag = 0}

--------------------------------------------------------------------------------

networkedGameLoop ::
  ClientSendHandles ClientGameMsg ->
  GameStateBuffer ->
  IORef (Maybe Int) ->
  GameLoop
networkedGameLoop sendHandles gameStateBuffer latencyRef =
  GameLoop
    { gl_inputLoop = networkedInputLoop sendHandles
    , gl_renderLoop = networkedRenderLoop gameStateBuffer latencyRef
    }

networkedInputLoop :: ClientSendHandles ClientGameMsg -> InputLoop
networkedInputLoop sendHandles =
  InputLoop $ \uiViewRef cameraPosRef glEnv alEnv inputEvents menuInputQueue keybindings -> do
    inputTicks <- field @"ils_inputTicks" <%= (+) 1
    lastUpdateTicks <- gets ils_lastInputUpdateTicks

    -- Inputs
    (playerControls, playerContinueStatus, statsOverlay, showHelpScreen) <-
      sharedInputs glEnv alEnv inputEvents menuInputQueue keybindings cameraPosRef

    -- Send player controls to the server. Send updates regularly, but also
    -- send controls immediately when a button is pressed or released (up to a
    -- rate limit).
    let underRateLimit =
          inputTicks - piut_older lastUpdateTicks >= inputDelayTicks
    when underRateLimit $ do
      oldPlayerControlsButtons <-
        gets $ getControlsTransportButtons . ils_lastInputUpdate
      let newPlayerControlsButtons = getControlsTransportButtons playerControls
          shouldSendUpdate =
            newPlayerControlsButtons /= oldPlayerControlsButtons -- send if button state changes
              || inputTicks - piut_newer lastUpdateTicks >= inputDelayTicks -- or if we've reached a delay limit
      when shouldSendUpdate $ do
        liftIO $ client_sendUDP sendHandles (ClientControls playerControls)
        field @"ils_lastInputUpdate" .= playerControls
        field @"ils_lastInputUpdateTicks" %= pushInputUpdateTick inputTicks

    showTutorial <- gets ils_showTutorial
    showMenu <- gets ils_showMenu
    menuState <- gets ils_menuState
    let uiView =
          UIView
            { uiv_showTutorial = showTutorial
            , uiv_showMenu = showMenu
            , uiv_menuState = menuState
            , uiv_statsOverlay = statsOverlay
            , uiv_displayHelp = showHelpScreen
            }

    -- Update UI view
    liftIO $ atomicWriteIORef uiViewRef uiView

    pure playerContinueStatus

networkedRenderLoop :: GameStateBuffer -> IORef (Maybe Int) -> RenderLoop
networkedRenderLoop gameStateBuffer latencyRef =
  RenderLoop $ \delayClock uiViewRef cameraPosRef glEnv alEnv keybindings -> do
    uiView <- lift $ readIORef uiViewRef

    -- Display rate control
    let ticksPerFrame = getTicksPerFrame glEnv
    clientTicks <- field @"rls_renderTicks" <%= (+) 1
    when (clientTicks `rem` ticksPerFrame == 0) $ do
      -- Get next frame
      (frameMaybe, frameType) <- do
        frameMaybe <- do
          zoom (field @"rls_frameSelectState") $
            getFrame delayClock gameStateBuffer (Ticks ticksPerFrame)
        case frameMaybe of
          Nothing ->
            (,OldFrame) <$> gets rls_lastGameStateBufferOutputs
          Just gsbOutputs -> do
            field @"rls_lastGameStateBufferOutputs" .= Just gsbOutputs
            pure (Just gsbOutputs, NewFrame)

      -- Audiovisuals
      latencyMaybe <- liftIO $ readIORef latencyRef
      for_ frameMaybe $ \FrameOutputs{..} ->
        zoom (field @"rls_cameraPosVel") $
          sharedAudioVisuals
            frameType
            glEnv
            alEnv
            clientTicks
            latencyMaybe
            keybindings
            uiView
            fo_gameScene
            fo_gameEvents
            fo_isFramePredicted
            (Just fo_roomID)
            fo_playerID

    gets (fst . rls_cameraPosVel) >>= \cameraPos ->
      liftIO $ atomicWriteIORef cameraPosRef cameraPos

-- Note: The single player game loop has been awkwardly forced into the shape
-- of the networked game loop because I thought it might reduce the testing
-- burden if both the single player and networked game loops were run in the
-- same context. This very slightly increases the latency of the single player
-- game loop, but it is worth it if it helps the development of the networked
-- game loop.
singlePlayerGameLoop ::
  PlayerID ->
  GameState RealWorld ->
  IORef PlayerControls ->
  IORef [GameEvents] ->
  GameLoop
singlePlayerGameLoop playerID gameState playerControlsRef gameEventsRef =
  GameLoop
    { gl_inputLoop = singlePlayerInputLoop playerControlsRef
    , gl_renderLoop =
        singlePlayerRenderLoop
          playerID
          gameState
          playerControlsRef
          gameEventsRef
    }

singlePlayerInputLoop :: IORef PlayerControls -> InputLoop
singlePlayerInputLoop playerControlsRef =
  InputLoop $ \uiViewRef cameraPosRef glEnv alEnv inputEvents menuInputQueue keybindings -> do
    -- Inputs
    (playerControlsTransport, playerContinueStatus, statsOverlay, showHelpScreen) <-
      sharedInputs glEnv alEnv inputEvents menuInputQueue keybindings cameraPosRef

    -- Update player controls
    liftIO $
      atomicWriteIORef
        playerControlsRef
        (fromControlsTransport playerControlsTransport)

    showTutorial <- gets ils_showTutorial
    showMenu <- gets ils_showMenu
    menuState <- gets ils_menuState
    let uiView =
          UIView
            { uiv_showTutorial = showTutorial
            , uiv_showMenu = showMenu
            , uiv_menuState = menuState
            , uiv_statsOverlay = statsOverlay
            , uiv_displayHelp = showHelpScreen
            }

    -- Update UI view
    liftIO $ atomicWriteIORef uiViewRef uiView

    pure playerContinueStatus

singlePlayerRenderLoop ::
  PlayerID ->
  GameState RealWorld ->
  IORef PlayerControls ->
  IORef [GameEvents] ->
  RenderLoop
singlePlayerRenderLoop playerID gameState playerControlsRef gameEventsRef =
  RenderLoop $ \_ uiViewRef cameraPosRef glEnv alEnv keybindings -> do
    uiView <- lift $ readIORef uiViewRef
    playerControls <- lift $ readIORef playerControlsRef

    -- Step game state
    let controlsMap = WIM.singleton playerID playerControls
        gameCommands = []
    stepGameEvents <-
      case uiv_showMenu uiView of
        DoNotShowMenu ->
          lift $ stToIO $ integrateGameState controlsMap gameCommands gameState
        ShowMenu ->
          pure mempty

    -- Display rate control
    let ticksPerFrame = getTicksPerFrame glEnv
    clientTicks <- field @"rls_renderTicks" <%= (+) 1
    if clientTicks `rem` ticksPerFrame /= 0
      then do
        -- Record game events
        liftIO $ modifyIORef' gameEventsRef (stepGameEvents :)
      else do
        -- Collect all game events since last render
        gameEvents <-
          mconcat . reverse . (stepGameEvents :)
            <$> liftIO (readIORef gameEventsRef)
        liftIO $ writeIORef gameEventsRef []

        -- Audiovisuals
        gameScene <-
          lift $ stToIO $ getGameScene gameState $ Just (PlayerActor playerID)
        let latencyMaybe = Nothing
            roomIDMaybe = Nothing
        zoom (field @"rls_cameraPosVel") $
          sharedAudioVisuals
            NewFrame
            glEnv
            alEnv
            clientTicks
            latencyMaybe
            keybindings
            uiView
            gameScene
            gameEvents
            False
            roomIDMaybe
            playerID

    gets (fst . rls_cameraPosVel) >>= \cameraPos ->
      liftIO $ atomicWriteIORef cameraPosRef cameraPos

--------------------------------------------------------------------------------

-- | Input handling shared between the networked and single-player game loops.
sharedInputs ::
  GLEnv ->
  ALEnv ->
  InputEventsHandle ->
  MenuInputQueue ->
  Keybindings ->
  IORef Float2 ->
  StateT
    InputLoopState
    IO
    (PlayerControlsTransport, PlayerContinueStatus, Maybe StatsOverlay, DisplayHelp)
sharedInputs glEnv alEnv inputEvents menuInputQueue keybindings cameraPosRef = do
  cameraPos <- liftIO $ readIORef cameraPosRef

  FrameInputs{..} <- do
    let inputOptions = InputOptions $ kb_invertLeftMouseButton keybindings
    liftIO $
      pollInputs glEnv inputEvents menuInputQueue keybindings inputOptions cameraPos

  -- Handle UI events
  zoom ils_showTutorial_showMenu $
    runUIEvents glEnv alEnv menuInputQueue fi_uiEvents

  -- Handle menu events
  gets ils_showMenu >>= \case
    DoNotShowMenu -> pure ()
    ShowMenu -> do
      -- Synchronize audio levels
      audioLevels <- liftIO $ getAudioLevels alEnv
      enableFullscreen <-
        liftIO $
          getFullscreenMode glEnv <&> \case
            Fullscreen -> True
            Windowed -> False
      field @"ils_menuState" . menuModel %= \playerOptions ->
        playerOptions
          { po_masterVolume = al_masterGainPercent audioLevels
          , po_musicVolume = al_musicGainPercent audioLevels
          , po_effectsVolume = al_effectsGainPercent audioLevels
          , po_enableFullscreen = enableFullscreen
          }
      result <-
        runExceptT $
          for_ fi_menuEvents $ \inputEvent -> do
            state0 <- lift $ gets ils_menuState
            result <-
              case handleMenuInput inputEvent state0 of
                Nothing ->
                  lift $ lift $ OptionsMenu.handleInput glEnv alEnv Nothing
                Just (!state1, updateEvent) ->
                  lift $ do
                    field @"ils_menuState" .= state1
                    lift $
                      OptionsMenu.handleInput
                        glEnv
                        alEnv
                        (Just (view menuModel state1, updateEvent))
            case result of
              LoopContinue -> pure ()
              LoopFinished loopResult -> throwE loopResult
      case result of
        Left _playerOptions -> do
          field @"ils_showMenu" .= DoNotShowMenu
          liftIO $
            GLFW.setKeyCallback
              (gle_window glEnv)
              (Just (gameKeyCallback alEnv inputEvents))
        Right () ->
          pure ()

  pure (fi_controls, fi_continueStatus, fi_displayStats, fi_displayHelp)

data FrameType = NewFrame | OldFrame

-- | Audiovisuals shared between the networked and single-player game loops.
--
-- Calls `GLFW.swapBuffers`
sharedAudioVisuals ::
  FrameType ->
  GLEnv ->
  ALEnv ->
  Int32 ->
  Maybe Int ->
  Keybindings ->
  UIView ->
  GameScene ->
  GameEvents ->
  Bool ->
  Maybe Int ->
  PlayerID ->
  StateT
    (Float2, Float2) -- (camera pos, camera vel)
    IO
    ()
sharedAudioVisuals
  frameType
  glEnv
  alEnv
  clientTicks
  latencyMaybe
  keybindings
  uiView
  gameScene
  gameEvents
  isFramePredicted
  roomIDMaybe
  playerID = do
    -- Handle framebuffer size changes. Needs to be done here, on the rendering
    -- thread.
    liftIO $
      gle_pollFramebufferSizeUpdate glEnv >>= \case
        Nothing -> pure ()
        Just (w, h) -> setSquareViewport (fromIntegral w) (fromIntegral h)
    -- Visuals before audio
    renderVisuals
    -- Only play audio for new frames
    case frameType of
      OldFrame -> pure ()
      NewFrame -> do
        cameraPos <- gets fst
        let actorID = PlayerActor playerID
        liftIO $ runGameAudio alEnv gameEvents gameScene cameraPos (Just actorID)
   where
    renderVisuals = do
      let (player, playerView) =
            expectJust "renderVisuals: Could not find PlayerID" $
              WIM.lookup (PlayerActor playerID) (scene_players gameScene)

      let ticksPerFrame = getTicksPerFrame glEnv
      modify' $
        case frameType of
          OldFrame -> id
          NewFrame -> case pv_overseerView playerView of
            OV_Alive OverseerViewAlive{ova_pos, ova_vel} ->
              cameraInterpolation
                ticksPerFrame
                (Fixed.toFloats ova_pos)
                (Fixed.toFloats ova_vel)
            OV_Dead OverseerViewDead{ovd_lastPos} ->
              cameraInterpolation
                ticksPerFrame
                (Fixed.toFloats ovd_lastPos)
                0
      newCameraPos <- gets fst

      liftIO $ do
        renderGameScene glEnv gameScene newCameraPos keybindings $
          SceneDecorations
            { sceneDeco_perspective =
                PlayerPerspective $
                  PlayerInfo (PlayerActor playerID) player playerView
            , sceneDeco_renderStatsOverlay = uiv_statsOverlay uiView
            , sceneDeco_renderHelpOverlay = uiv_displayHelp uiView
            , sceneDeco_roomID = roomIDMaybe
            , sceneDeco_latency = latencyMaybe
            , sceneDeco_elaspedClientTime = Ticks clientTicks
            , sceneDeco_showTutorial =
                case uiv_showTutorial uiView of
                  ShowTutorial -> True
                  DoNotShowTutorial -> False
            , sceneDeco_showMenu =
                case uiv_showMenu uiView of
                  ShowMenu -> Just $ uiv_menuState uiView
                  DoNotShowMenu -> Nothing
            , sceneDeco_showOSCursor = ShowOSCursor
            , sceneDeco_isFramePredicted = isFramePredicted
            , sceneDeco_enableScreenShake =
                po_enableScreenShake $ view menuModel $ uiv_menuState uiView
            , sceneDeco_enableVisualStatic =
                po_enableVisualStatic $ view menuModel $ uiv_menuState uiView
            }
      liftIO $ GLFW.swapBuffers $ gle_window glEnv

--------------------------------------------------------------------------------
-- UI event handling

runUIEvents ::
  GLEnv ->
  ALEnv ->
  MenuInputQueue ->
  [UIEvent] ->
  StateT (ShowTutorial, ShowMenu) IO ()
runUIEvents glEnv alEnv menuInputs =
  traverse_ (runUIEvent glEnv alEnv menuInputs)

runUIEvent ::
  GLEnv ->
  ALEnv ->
  MenuInputQueue ->
  UIEvent ->
  StateT (ShowTutorial, ShowMenu) IO ()
runUIEvent glEnv alEnv menuInputs = \case
  EscapePressed ->
    use _1 >>= \case
      ShowTutorial ->
        _1 .= DoNotShowTutorial
      DoNotShowTutorial -> do
        _2 .= ShowMenu
        liftIO $
          GLFW.setKeyCallback (gle_window glEnv) $
            Just $
              optionsKeyCallback alEnv menuInputs
