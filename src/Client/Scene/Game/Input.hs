{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE MultiWayIf #-}

module Client.Scene.Game.Input (
  InputEventsHandle,
  newInputEventsHandle,
  gameKeyCallback,
  optionsKeyCallback,
  pollInputs,
  FrameInputs (..),
  InputOptions (..),
  PlayerContinueStatus (..),
  UIEvent (..),
) where

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TQueue
import Control.Monad.IO.Class
import GHC.Float
import GHC.Generics (Generic)
import Graphics.UI.GLFW hiding (KeyCallback)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.PlayerControls

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.GlobalKeyCallback (globalKeyCallback)
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GLFW.KeyCallback
import Client.Graphics.Rendering.SceneDecorations
import Client.Menu.GLFW qualified
import Client.Menu.Input (MenuInputEvent (..), MenuInputQueue)
import Client.Menu.Input qualified as Menu

--------------------------------------------------------------------------------
-- Input callbacks

{-
Quering with `GLFW.getKey` is not guaranteed to capture all keypresses (e.g. if
the key is pressed and released between calls to `GLFW.pollEvents`), so
key callbacks should be used for keypresses that must be captured.
-}

newtype InputEventsHandle = InputEventsHandle {inputEventsQueue :: TQueue UIEvent}

newInputEventsHandle :: IO InputEventsHandle
newInputEventsHandle = InputEventsHandle <$> newTQueueIO

data UIEvent
  = EscapePressed

gameKeyCallback' :: TQueue UIEvent -> KeyCallback
gameKeyCallback' eventQueue =
  KeyCallback $ \_window key _scanCode keySt _mods ->
    let firstPress = keySt == GLFW.KeyState'Pressed
    in  if
            | key == GLFW.Key'Escape && firstPress ->
                Just . atomically $
                  writeTQueue eventQueue EscapePressed
            | otherwise ->
                Nothing

gameKeyCallback ::
  ALEnv -> InputEventsHandle -> GLFW.KeyCallback
gameKeyCallback alEnv (InputEventsHandle eventQueue) =
  toGLFWKeyCallback $
    globalKeyCallback alEnv <> gameKeyCallback' eventQueue

optionsKeyCallback ::
  ALEnv -> MenuInputQueue -> GLFW.KeyCallback
optionsKeyCallback alEnv menuInputQueue =
  toGLFWKeyCallback $
    globalKeyCallback alEnv <> Client.Menu.GLFW.keyCallback menuInputQueue

--------------------------------------------------------------------------------
-- Input polling

newtype InputOptions = InputOptions
  { inputOpts_invertLeftMouseButton :: Bool
  }
  deriving stock (Generic)

data FrameInputs = FrameInputs
  { fi_controls :: PlayerControlsTransport
  , fi_continueStatus :: PlayerContinueStatus
  , fi_displayStats :: Maybe StatsOverlay
  , fi_displayHelp :: DisplayHelp
  , fi_uiEvents :: [UIEvent]
  , fi_menuEvents :: [MenuInputEvent]
  }

data PlayerContinueStatus
  = PlayerContinue
  | PlayerQuit

-- | Calls `GLFW.pollEvents`
pollInputs ::
  GLEnv ->
  InputEventsHandle ->
  MenuInputQueue ->
  Keybindings ->
  InputOptions ->
  Float2 ->
  IO FrameInputs
pollInputs glEnv inputEventsHandle menuInputQueue keybindings uiState cameraPos = do
  GLFW.pollEvents
  uiEvents <- atomically $ flushTQueue $ inputEventsQueue inputEventsHandle
  menuEvents <- Menu.flushQueue menuInputQueue
  playerControls <- do
    let window = gle_window glEnv
        invertLeftMouseButton =
          if inputOpts_invertLeftMouseButton uiState
            then InvertLeftMouseButton
            else NoInvertLeftMouseButton
    windowSize <- GLFW.getWindowSize window
    readPlayerControls
      (GLFW.getKey window)
      (GLFW.getMouseButton window)
      (GLFW.getCursorPos window)
      windowSize
      cameraPos
      invertLeftMouseButton
      keybindings
  (statsOverlay, helpScreen) <- checkHolds glEnv
  playerContinueStatus <- checkPlayerQuit glEnv
  pure
    FrameInputs
      { fi_controls = playerControls
      , fi_continueStatus = playerContinueStatus
      , fi_displayStats = statsOverlay
      , fi_displayHelp = helpScreen
      , fi_uiEvents = uiEvents
      , fi_menuEvents = menuEvents
      }

data InvertLeftMouseButton
  = InvertLeftMouseButton
  | NoInvertLeftMouseButton

readPlayerControls ::
  (Applicative f) =>
  (Key -> f KeyState) ->
  (MouseButton -> f MouseButtonState) ->
  f (Double, Double) ->
  (Int, Int) ->
  Float2 ->
  InvertLeftMouseButton ->
  Keybindings ->
  f PlayerControlsTransport
readPlayerControls
  glfwGetKey
  glfwGetMouseButton
  glfwGetCursorPos
  windowSize
  cameraPos
  invertLeftMouseButton
  keybindings = do
    let isPressed = fmap keyIsPressed . glfwGetKey
    up <- isPressed $ kb_overseerUp keybindings
    left <- isPressed $ kb_overseerLeft keybindings
    down <- isPressed $ kb_overseerDown keybindings
    right <- isPressed $ kb_overseerRight keybindings
    psiStorm <- isPressed $ kb_psiStorm keybindings
    overseerDash <- isPressed $ kb_overseerDash keybindings
    dropFlag <- isPressed $ kb_dropFlag keybindings

    let isMousePressed = fmap (== MouseButtonState'Pressed) . glfwGetMouseButton
    rawMouse1 <-
      let invertButton =
            case invertLeftMouseButton of
              InvertLeftMouseButton -> not
              NoInvertLeftMouseButton -> id
       in invertButton <$> isMousePressed MouseButton'1
    rawMouse2 <- isMousePressed MouseButton'2
    cursorPos <- glfwGetCursorPos

    pure $
      let (mouse1, mouse2) =
            if kb_swapMouseButtons keybindings
              then (rawMouse2, rawMouse1)
              else (rawMouse1, rawMouse2)
          droneCmd =
            case (mouse1, mouse2) of
              (False, False) -> DroneNeutral
              (False, True) -> DroneRepulsion
              (True, False) -> DroneAttraction
              (True, True) -> DroneNeutral

          moveCmd_x = case (left, right) of
            (False, False) -> EQ
            (False, True) -> GT
            (True, False) -> LT
            (True, True) -> EQ
          moveCmd_y = case (down, up) of
            (False, False) -> EQ
            (False, True) -> GT
            (True, False) -> LT
            (True, True) -> EQ
          moveCmd = case (moveCmd_x, moveCmd_y) of
            (LT, LT) -> MoveSW
            (LT, EQ) -> MoveW
            (LT, GT) -> MoveNW
            (EQ, LT) -> MoveS
            (EQ, EQ) -> MoveNeutral
            (EQ, GT) -> MoveN
            (GT, LT) -> MoveSE
            (GT, EQ) -> MoveE
            (GT, GT) -> MoveNE

          psiStormCmd = if psiStorm then PsiStormOn else PsiStormOff
          overseerDashCmd = if overseerDash then OverseerDashOn else OverseerDashOff
          flagCmd = if dropFlag then FlagDrop else FlagNeutral

          buttonStates =
            defaultButtonStates
              & control_move .~ moveCmd
              & control_drone .~ droneCmd
              & control_psiStorm .~ psiStormCmd
              & control_flag .~ flagCmd
              & control_overseerDash .~ overseerDashCmd

          worldPos =
            CS.intoPos (CS.gameView cameraPos) $
              CS.fromPos (cursorCoordSystem windowSize) $
                uncurry Float2 $
                  over both double2Float cursorPos
      in  makeControlsTransport buttonStates worldPos

-- Helper
cursorCoordSystem :: (Int, Int) -> CS.CoordSystem
cursorCoordSystem windowSize =
  CS.makeCoordSystem
    (l / 2)
    (V2 CS.PreserveAxis CS.FlipAxis)
    (Float2 (w / 2) (h / 2))
 where
  (w, h) = over both fromIntegral windowSize
  l = min w h

checkHolds :: GLEnv -> IO (Maybe StatsOverlay, DisplayHelp)
checkHolds glEnv = do
  let isPressed = fmap keyIsPressed . GLFW.getKey (gle_window glEnv)
  ctrl <-
    liftIO $
      (||)
        <$> isPressed GLFW.Key'LeftControl
        <*> isPressed GLFW.Key'RightControl
  ctrlZ <- liftIO $ (&& ctrl) <$> isPressed GLFW.Key'Z
  ctrlX <- liftIO $ (&& ctrl) <$> isPressed GLFW.Key'X
  ctrlH <- liftIO $ (&& ctrl) <$> isPressed GLFW.Key'H
  let statsOverlay
        | ctrlZ = Just RoundStatsOverlay
        | ctrlX = Just CumulativeStatsOverlay
        | otherwise = Nothing
      helpScreen
        | ctrlH = ShowHelp
        | otherwise = HideHelp
  pure (statsOverlay, helpScreen)

checkPlayerQuit :: GLEnv -> IO PlayerContinueStatus
checkPlayerQuit glEnv = do
  q <- isPressed Key'Q
  ctrl <- (||) <$> isPressed Key'LeftControl <*> isPressed Key'RightControl
  windowclose <- GLFW.windowShouldClose $ gle_window glEnv
  pure $
    if ctrl && q || windowclose
      then PlayerQuit
      else PlayerContinue
 where
  isPressed = fmap keyIsPressed . GLFW.getKey (gle_window glEnv)

-- Helper
keyIsPressed :: GLFW.KeyState -> Bool
keyIsPressed = (/=) GLFW.KeyState'Released
