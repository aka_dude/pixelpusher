{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.SinglePlayer (
  runSinglePlayerMenu,
  SinglePlayerResult (..),
  SinglePlayerOptions (..),
  emptySinglePlayerOptions,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.AL.Env (ALEnv)
import Client.Graphics.GL.Env
import Client.Menu
import Client.Scene.Template.Menu

runSinglePlayerMenu ::
  GLEnv ->
  ALEnv ->
  SinglePlayerOptions ->
  [String] ->
  IO (Maybe (SinglePlayerOptions, SinglePlayerResult))
runSinglePlayerMenu glEnv alEnv initialOptions messages =
  menuScene
    glEnv
    alEnv
    "single player"
    messages
    multiplayerMenu
    initialOptions
    (handleInput alEnv)

data SinglePlayerResult
  = SinglePlayer_Start
  | SinglePlayer_Back

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data SinglePlayerOptions = SinglePlayerOptions
  { spo_playerName :: Text
  , spo_botDifficulty :: Int
  , spo_teamSize :: Int
  }
  deriving (Generic)

emptySinglePlayerOptions :: SinglePlayerOptions
emptySinglePlayerOptions =
  SinglePlayerOptions
    { spo_playerName = ""
    , spo_botDifficulty = 1
    , spo_teamSize = 5
    }

instance Toml.FromValue SinglePlayerOptions where
  fromValue =
    Toml.withTable
      "single player options"
      (Toml.genericParseTableDefaults emptySinglePlayerOptions)

instance Toml.ToValue SinglePlayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable SinglePlayerOptions where
  toTable = Toml.genericToTable

type SinglePlayerWidgets :: MenuItemType -> Type
data SinglePlayerWidgets ty where
  Name :: SinglePlayerWidgets 'TextInput
  BotDifficulty :: SinglePlayerWidgets 'IntSlider
  TeamSize :: SinglePlayerWidgets 'IntSlider
  Start :: SinglePlayerWidgets 'Button
  Back :: SinglePlayerWidgets 'Button

multiplayerMenu ::
  NonEmpty (MenuItem SinglePlayerWidgets SinglePlayerOptions)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ TextInputWidget
        Name
        (field @"spo_playerName")
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , IntSliderWidget
        BotDifficulty
        (field @"spo_botDifficulty")
        IntSliderConfig
          { isc_label = "bot level"
          , isc_min = 1
          , isc_max = 5
          , isc_step = 1
          }
    , IntSliderWidget
        TeamSize
        (field @"spo_teamSize")
        IntSliderConfig
          { isc_label = "team size"
          , isc_min = 1
          , isc_max = 5
          , isc_step = 1
          }
    , ButtonWidget
        Start
        ButtonConfig
          { bc_label = "start"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  ALEnv ->
  Maybe (SinglePlayerOptions, MenuUpdateEvent SinglePlayerWidgets) ->
  IO (LoopContinue (SinglePlayerOptions, SinglePlayerResult))
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (options, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (options, SinglePlayer_Start)
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished (options, SinglePlayer_Back)
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged BotDifficulty _newValue -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged TeamSize _newValue -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Start -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (options, SinglePlayer_Start)
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished (options, SinglePlayer_Back)
