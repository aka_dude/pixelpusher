{-# LANGUAGE CPP #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.Title (
  runTitle,
  TitleResult (..),
) where

import Control.Concurrent (forkIO)
import Control.Monad (void)
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import System.Process.Typed

import Client.Audio.AL.Env (ALEnv)
import Client.Graphics.GL.Env
import Client.Menu
import Client.Scene.Template.Menu

runTitle :: GLEnv -> ALEnv -> [String] -> IO (Maybe TitleResult)
runTitle glEnv alEnv messages =
  menuScene glEnv alEnv "pixelpusher" messages titleMenu () (handleInput alEnv)

data TitleResult
  = TitleResult_Multiplayer
  | TitleResult_CustomMultiplayer
  | TitleResult_SinglePlayer
  | TitleResult_Settings
  | TitleResult_Quit

type TitleWidgets :: MenuItemType -> Type
data TitleWidgets ty where
  Multiplayer :: TitleWidgets 'Button
  CustomMultiplayer :: TitleWidgets 'Button
  SinglePlayer :: TitleWidgets 'Button
  Settings :: TitleWidgets 'Button
  Discord :: TitleWidgets 'Button
  Quit :: TitleWidgets 'Button

titleMenu :: NonEmpty (MenuItem TitleWidgets ())
titleMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget Multiplayer (ButtonConfig "multiplayer")
    , ButtonWidget CustomMultiplayer (ButtonConfig "custom multiplayer")
    , ButtonWidget SinglePlayer (ButtonConfig "single player")
    , ButtonWidget Settings (ButtonConfig "settings")
    , ButtonWidget Discord (ButtonConfig "discord")
    , ButtonWidget Quit (ButtonConfig "quit")
    ]

handleInput ::
  ALEnv ->
  Maybe ((), MenuUpdateEvent TitleWidgets) ->
  IO (LoopContinue TitleResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (_model, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape ->
        pure LoopContinue -- Disallow shortcut for exit on title screen
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Multiplayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Multiplayer
      ButtonPressed CustomMultiplayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_CustomMultiplayer
      ButtonPressed SinglePlayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_SinglePlayer
      ButtonPressed Settings -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Settings
      ButtonPressed Discord -> do
        uiSoundHigh alEnv
        openDiscordServerInvite
        pure LoopContinue
      ButtonPressed Quit -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Quit

openDiscordServerInvite :: IO ()
openDiscordServerInvite =
  void . forkIO . void . runProcess $
    proc
#if defined mingw32_HOST_OS
      "explorer"
#elif defined darwin_HOST_OS
      "open"
#else
      "xdg-open" -- Assuming linux
#endif
      ["https://discord.gg/j8hMRN7g3K"]
