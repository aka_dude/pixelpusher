{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scene.CustomMultiplayer (
  runCustomMultiplayerMenu,
  CustomMultiplayerResult (..),
  MultiplayerOptions (..),
  emptyCustomMultiplayerOptions,
) where

import Data.Char (isAsciiLower, isAsciiUpper, isDigit)
import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.AL.Env (ALEnv)
import Client.Graphics.GL.Env
import Client.Menu
import Client.Scene.Template.Menu

runCustomMultiplayerMenu ::
  GLEnv ->
  ALEnv ->
  MultiplayerOptions ->
  [String] ->
  IO (Maybe (MultiplayerOptions, CustomMultiplayerResult))
runCustomMultiplayerMenu glEnv alEnv initialOptions messages =
  menuScene
    glEnv
    alEnv
    "custom multiplayer"
    messages
    multiplayerMenu
    initialOptions
    (handleInput alEnv)

data CustomMultiplayerResult
  = CustomMultiplayer_Connect
  | CustomMultiplayer_Back

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data MultiplayerOptions = MultiplayerOptions
  { mo_serverAddress :: Text
  , mo_playerName :: Text
  }
  deriving (Generic)

emptyCustomMultiplayerOptions :: MultiplayerOptions
emptyCustomMultiplayerOptions =
  MultiplayerOptions
    { mo_serverAddress = ""
    , mo_playerName = ""
    }

instance Toml.FromValue MultiplayerOptions where
  fromValue =
    Toml.withTable
      "multiplayer options"
      (Toml.genericParseTableDefaults emptyCustomMultiplayerOptions)

instance Toml.ToValue MultiplayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable MultiplayerOptions where
  toTable = Toml.genericToTable

type CustomMultiplayerWidgets :: MenuItemType -> Type
data CustomMultiplayerWidgets ty where
  Name :: CustomMultiplayerWidgets 'TextInput
  Connect :: CustomMultiplayerWidgets 'Button
  Back :: CustomMultiplayerWidgets 'Button

multiplayerMenu ::
  NonEmpty (MenuItem CustomMultiplayerWidgets MultiplayerOptions)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ TextInputWidget
        Name
        (field @"mo_serverAddress")
        TextInputConfig
          { tic_label = "server address"
          , tic_maxLength = 255
          , tic_permittedChars = isDomainNameChar
          }
    , TextInputWidget
        Name
        (field @"mo_playerName")
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , ButtonWidget
        Connect
        ButtonConfig
          { bc_label = "connect"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

isDomainNameChar :: Char -> Bool
isDomainNameChar c =
  isDigit c || isAsciiLower c || isAsciiUpper c || c == '.' || c == '-'

handleInput ::
  ALEnv ->
  Maybe (MultiplayerOptions, MenuUpdateEvent CustomMultiplayerWidgets) ->
  IO (LoopContinue (MultiplayerOptions, CustomMultiplayerResult))
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (options, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (options, CustomMultiplayer_Connect)
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished (options, CustomMultiplayer_Back)
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Connect -> do
        uiSoundHigh alEnv
        pure $ LoopFinished (options, CustomMultiplayer_Connect)
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished (options, CustomMultiplayer_Back)
