module Client.Scene.SinglePlayerGame (
  runSinglePlayerGame,
) where

import Control.Monad.ST (stToIO)
import Data.Generics.Product.Fields
import Data.IORef
import Lens.Micro

import Pixelpusher.Game.GameCommand
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerControls (defaultControls)
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))
import Pixelpusher.Game.PlayerName

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.Graphics.GL.Env

import Client.Scene.Game.GameLoop (runGameLoop, singlePlayerGameLoop)
import Client.Scene.Options (PlayerOptions)

--------------------------------------------------------------------------------

runSinglePlayerGame ::
  GLEnv ->
  ALEnv ->
  BaseGameParams ->
  Keybindings ->
  PlayerOptions ->
  PlayerName ->
  Int ->
  Int ->
  IO PlayerOptions
runSinglePlayerGame
  glEnv
  alEnv
  rawGameParams
  keybindings
  playerOptions
  playerName
  botLevel
  teamSize = do
    -- Initialize mutable references
    gameEventsRef <- newIORef []
    playerControlsRef <- newIORef defaultControls

    -- Set game parameters
    let botControlQueueLength = max 0 (7 - botLevel)
        gameParams =
          rawGameParams
            & field @"bgp_botParams" . field @"bgp_botControlQueueLength"
              .~ botControlQueueLength
            & field @"bgp_botParams" . field @"bgp_minBotsPerTeam"
              .~ (0 :: Int)
            & field @"bgp_botParams" . field @"bgp_minTeamSize"
              .~ teamSize

    -- Initialize game state and add a player to the game
    let playerID = PlayerID minBound
        addPlayerCommand = PlayerJoin playerID playerName Nothing
    gameState <- initializeGameState gameParams
    _ <- stToIO $ integrateGameState mempty [addPlayerCommand] gameState

    runGameLoop glEnv alEnv keybindings playerOptions $
      singlePlayerGameLoop playerID gameState playerControlsRef gameEventsRef
