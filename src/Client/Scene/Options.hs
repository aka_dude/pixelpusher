{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Scene.Options (
  runOptionsMenu,
  PlayerOptions (..),
  defaultPlayerOptions,
  OptionsWidgets,
  optionsMenu,
  handleInput,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml

import Client.Audio.AL.Env
import Client.Graphics.GL.Env hiding (FullscreenMode (..))
import Client.Graphics.GL.Env qualified as Graphics
import Client.Menu
import Client.Scene.Template.Menu

-- TODO: Rename stuff from "options" to "audiovisual options"

runOptionsMenu ::
  GLEnv -> ALEnv -> PlayerOptions -> [String] -> IO (Maybe PlayerOptions)
runOptionsMenu glEnv alEnv initialOptions messages =
  menuScene
    glEnv
    alEnv
    "audiovisual"
    messages
    optionsMenu
    initialOptions
    (handleInput glEnv alEnv)

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
--
-- TODO: Separate fields into two groups by whether or not the field is the
-- source of truth? In any case, the synchronization logic for the
-- non-source-of-truth fields should be shared.
data PlayerOptions = PlayerOptions
  { po_masterVolume :: Int
  , po_musicVolume :: Int
  , po_effectsVolume :: Int
  , po_enableFullscreen :: Bool
  , po_enableScreenShake :: Bool
  , po_enableVisualStatic :: Bool
  }
  deriving (Generic)

defaultPlayerOptions :: PlayerOptions
defaultPlayerOptions =
  PlayerOptions
    { po_masterVolume = 70
    , po_musicVolume = 100
    , po_effectsVolume = 100
    , po_enableFullscreen = True
    , po_enableScreenShake = True
    , po_enableVisualStatic = True
    }

instance Toml.FromValue PlayerOptions where
  fromValue =
    Toml.withTable
      "audiovisual options"
      (Toml.genericParseTableDefaults defaultPlayerOptions)

instance Toml.ToValue PlayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable PlayerOptions where
  toTable = Toml.genericToTable

type OptionsWidgets :: MenuItemType -> Type
data OptionsWidgets ty where
  MasterVolume :: OptionsWidgets 'IntSlider
  MusicVolume :: OptionsWidgets 'IntSlider
  EffectsVolume :: OptionsWidgets 'IntSlider
  Fullscreen :: OptionsWidgets 'Toggle
  ScreenShake :: OptionsWidgets 'Toggle
  VisualStatic :: OptionsWidgets 'Toggle
  Back :: OptionsWidgets 'Button

optionsMenu :: NonEmpty (MenuItem OptionsWidgets PlayerOptions)
optionsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ IntSliderWidget
        MasterVolume
        (field @"po_masterVolume")
        (volumeSlider "master volume")
    , IntSliderWidget
        MusicVolume
        (field @"po_musicVolume")
        (volumeSlider "music volume")
    , IntSliderWidget
        EffectsVolume
        (field @"po_effectsVolume")
        (volumeSlider "effects volume")
    , ToggleWidget
        Fullscreen
        (field @"po_enableFullscreen")
        ToggleConfig{tc_label = "fullscreen"}
    , ToggleWidget
        ScreenShake
        (field @"po_enableScreenShake")
        ToggleConfig{tc_label = "screenshake"}
    , ToggleWidget
        VisualStatic
        (field @"po_enableVisualStatic")
        ToggleConfig{tc_label = "visual static"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

volumeSlider :: Text -> IntSliderConfig
volumeSlider label =
  IntSliderConfig
    { isc_label = label
    , isc_min = 0
    , isc_max = 100
    , isc_step = 5
    }

handleInput ::
  GLEnv ->
  ALEnv ->
  Maybe (PlayerOptions, MenuUpdateEvent OptionsWidgets) ->
  IO (LoopContinue PlayerOptions)
handleInput glEnv alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (options, menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished options
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged MasterVolume percent -> do
        setVolumePercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged MusicVolume percent -> do
        setMusicGainPercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged EffectsVolume percent -> do
        setEffectsGainPercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched Fullscreen enableFullscreen -> do
        setFullscreenMode glEnv $
          if enableFullscreen then Graphics.Fullscreen else Graphics.Windowed
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched ScreenShake _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched VisualStatic _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished options
