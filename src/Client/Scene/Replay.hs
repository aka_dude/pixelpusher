{-# LANGUAGE OverloadedStrings #-}

-- | A scrappy replay viewer.
module Client.Scene.Replay (
  playReplay,
) where

import Control.Applicative (empty)
import Control.Exception (bracket_)
import Control.Logging qualified as Logging
import Control.Monad (replicateM, replicateM_, unless, void, when)
import Control.Monad.IO.Class
import Control.Monad.Morph (hoist, lift)
import Control.Monad.ST
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State.Strict
import Data.ByteString.Lazy qualified as BL
import Data.Foldable (foldlM)
import Data.Generics.Product.Fields (field)
import Data.IORef
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.Maybe (fromMaybe, isNothing)
import Data.Sequence (Seq)
import Data.Sequence qualified as Seq
import Data.Serialize qualified as Serialize
import Data.Text qualified as Text
import Data.Traversable (for)
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Clock (runClock)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.SmallList (fromSmallList)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameEvents (GameEvents)
import Pixelpusher.Game.GameProtocol
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.PlayerControls (fromControlsTransport)
import Pixelpusher.Game.PlayerID (getPlayerIDMap32)
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time

import Client.Audio.AL.Env
import Client.Config.Keybindings
import Client.Graphics.CameraInterpolation
import Client.Graphics.GL.Env
import Client.Graphics.Rendering.GameScene
import Client.Graphics.Rendering.SceneDecorations
import Client.Scene.Game.Audio (runGameAudio)

--------------------------------------------------------------------------------
-- Parsing

data Replay = Replay
  { replay_initState :: GameStateSnapshot
  , replay_updates :: [GameStateUpdate]
  }

loadReplay :: FilePath -> IO (Maybe Replay)
loadReplay = fmap readReplay . BL.readFile

readReplay :: BL.ByteString -> Maybe Replay
readReplay bs0 = either (const Nothing) Just $ do
  (initState, bs1) <-
    Serialize.runGetLazyState (Serialize.get @GameStateSnapshot) bs0
  let updates = getUpdates bs1
  pure $ Replay initState updates

getUpdates :: BL.ByteString -> [GameStateUpdate]
getUpdates bs =
  case Serialize.runGetLazyState (Serialize.get @GameStateUpdate) bs of
    Right (update, bs') -> update : getUpdates bs'
    Left _ -> []

--------------------------------------------------------------------------------
-- Playback

data PlaybackState = PlaybackState
  { ps_nextTime :: Int
  , ps_keyFrames :: IntMap GameStateSnapshot
  , ps_paused :: Bool
  , ps_actorID :: Maybe ActorID
  , ps_cameraPos :: Float2
  , ps_cameraVel :: Float2
  , ps_events :: GameEvents
  }
  deriving stock (Generic)

ps_cameraPosVel :: Lens' PlaybackState (Float2, Float2)
ps_cameraPosVel f st =
  f (ps_cameraPos st, ps_cameraVel st) <&> \(pos, vel) ->
    st
      { ps_cameraPos = pos
      , ps_cameraVel = vel
      }

initialPlaybackState :: GameStateSnapshot -> PlaybackState
initialPlaybackState snapshot =
  PlaybackState
    { ps_nextTime = 0
    , ps_keyFrames = IntMap.singleton 0 snapshot
    , ps_paused = False
    , ps_actorID = Nothing
    , ps_cameraPos = Float2 0 0
    , ps_cameraVel = Float2 0 0
    , ps_events = mempty
    }

data Event
  = TogglePause
  | Seek Int
  | PrevFrame
  | NextFrame
  | UseFreeCamera
  | TrackNextPlayer
  | TrackPreviousPlayer
  | ToggleMuteAll
  | ToggleMuteMusic
  | IncreaseVolume
  | DecreaseVolume
  | Quit
  deriving stock (Eq)

handleEvent ::
  ALEnv -> WIM.IntMap ActorID a -> PlaybackState -> Event -> IO PlaybackState
handleEvent alEnv players st event =
  case event of
    TogglePause -> pure $ over (field @"ps_paused") not st
    Seek ticks ->
      pure $ over (field @"ps_nextTime") (+ ticks) st
    PrevFrame ->
      pure $ over (field @"ps_nextTime") (subtract 1) st
    NextFrame ->
      pure $ over (field @"ps_nextTime") (+ 1) st
    UseFreeCamera ->
      pure $ set (field @"ps_actorID") Nothing st
    TrackNextPlayer ->
      pure $
        over
          (field @"ps_actorID")
          ( \case
              Nothing -> fst <$> WIM.lookupMin players
              Just uid -> Just $ maybe uid fst $ WIM.lookupGT uid players
          )
          st
    TrackPreviousPlayer ->
      pure $
        over
          (field @"ps_actorID")
          ( \case
              Nothing -> fst <$> WIM.lookupMax players
              Just uid -> Just $ maybe uid fst $ WIM.lookupLT uid players
          )
          st
    ToggleMuteAll ->
      toggleMute alEnv >> pure st
    ToggleMuteMusic ->
      toggleMusicMute alEnv >> pure st
    IncreaseVolume ->
      increaseVolume alEnv >> pure st
    DecreaseVolume ->
      decreaseVolume alEnv >> pure st
    Quit ->
      pure st

keyCallback :: IORef (Seq Event) -> GLFW.KeyCallback
keyCallback eventsRef _window key _scanCode keySt mods =
  let addEvent :: Maybe Event -> IO ()
      addEvent Nothing = pure ()
      addEvent (Just event) =
        atomicModifyIORef' eventsRef $ \events -> (events Seq.|> event, ())
  in  addEvent $
        if keySt == GLFW.KeyState'Released
          then Nothing
          else case key of
            -- Pausing
            GLFW.Key'Space -> Just TogglePause
            -- Seeking
            GLFW.Key'H ->
              if GLFW.modifierKeysShift mods
                then Just $ Seek $ fromIntegral (-C.tickRate_hz)
                else Just $ Seek $ fromIntegral (-4 * C.tickRate_hz)
            GLFW.Key'L ->
              if GLFW.modifierKeysShift mods
                then Just $ Seek $ fromIntegral C.tickRate_hz
                else Just $ Seek $ fromIntegral (4 * C.tickRate_hz)
            GLFW.Key'Comma -> Just PrevFrame
            GLFW.Key'Period -> Just NextFrame
            -- Player tracking
            GLFW.Key'F -> Just UseFreeCamera
            GLFW.Key'N -> Just TrackNextPlayer
            GLFW.Key'P -> Just TrackPreviousPlayer
            -- Volume
            GLFW.Key'M -> ifCtrl ToggleMuteAll
            GLFW.Key'K -> ifCtrl ToggleMuteMusic
            GLFW.Key'9 -> ifCtrl DecreaseVolume
            GLFW.Key'0 -> ifCtrl IncreaseVolume
            -- Quit
            GLFW.Key'Q -> ifCtrl Quit
            -- Otherwise
            _ -> Nothing
 where
  ifCtrl cmd = if GLFW.modifierKeysControl mods then Just cmd else Nothing

playReplay :: FilePath -> IO ()
playReplay filepath = do
  mReplay <- loadReplay filepath
  case mReplay of
    Nothing ->
      Logging.log $
        "Could not load replay from " <> Text.pack filepath <> "."
    Just replay -> play replay

-- TODO: try to deduplicate this game loop logic

play :: Replay -> IO ()
play replay = do
  gameState <- stToIO $ thaw $ replay_initState replay
  eventsRef <- newIORef Seq.empty
  let audioOptions =
        InitialAudioOptions
          { iao_masterGainPercent = 70
          , iao_musicGainPercent = 100
          , iao_effectsGainPercent = 100
          }
  withGLEnv $ \glEnv ->
    withKeyCallback glEnv (keyCallback eventsRef) $
      withALEnv audioOptions $ \alEnv ->
        withTrackGains alEnv
          $ bracket_
            (GLFW.setCursorInputMode (gle_window glEnv) GLFW.CursorInputMode'Hidden)
            (GLFW.setCursorInputMode (gle_window glEnv) GLFW.CursorInputMode'Normal)
          $ do
            void $
              runMaybeT $
                flip evalStateT initState $
                  runClock C.frameDelay_ns $ do
                    continue <-
                      hoist lift $
                        playLoopBody glEnv alEnv replay lastTime updates eventsRef gameState
                    case continue of
                      Continue -> pure ()
                      DoNotContinue -> empty
 where
  initState = initialPlaybackState (replay_initState replay)
  updates = IntMap.fromAscList $ zip [0 ..] $ replay_updates replay
  lastTime = maybe 0 fst $ IntMap.lookupMax updates

withTrackGains :: ALEnv -> IO a -> IO a
withTrackGains alEnv =
  bracket_
    (setTrackGains alEnv initialGains Nothing)
    (setTrackGains alEnv zeroGains Nothing)
 where
  initialGains =
    TrackGains{tg_slow = 0, tg_fast = 1, tg_slowFast = 1, tg_flag = 0}
  zeroGains =
    TrackGains{tg_slow = 0, tg_fast = 0, tg_slowFast = 0, tg_flag = 0}

data Continue = Continue | DoNotContinue

playLoopBody ::
  GLEnv ->
  ALEnv ->
  Replay ->
  Int ->
  IntMap GameStateUpdate ->
  IORef (Seq Event) ->
  GameState RealWorld ->
  StateT PlaybackState IO Continue
playLoopBody glEnv alEnv replay lastTime updates eventsRef gameState = do
  players <- liftIO $ stToIO $ getGamePlayers gameState
  liftIO GLFW.pollEvents

  -- Handle framebuffer size changes
  liftIO $
    gle_pollFramebufferSizeUpdate glEnv >>= \case
      Nothing -> pure ()
      Just (w, h) -> setSquareViewport (fromIntegral w) (fromIntegral h)

  -- Commands
  events <-
    liftIO $
      atomicModifyIORef' eventsRef (Seq.empty,)

  do
    st <- get
    st' <- liftIO $ foldlM (handleEvent alEnv players) st events
    put st'

  paused <- gets ps_paused

  -- Integrate
  targetTime <-
    field @"ps_nextTime"
      <%= max 0 . min lastTime . if paused then id else (+ 2) -- Oh, so I've hard-coded for 60 Hz here
  currentTime <-
    fmap (fromIntegral . getTime) $ liftIO $ stToIO $ getGameTime gameState

  mActorID <- gets ps_actorID
  gameScene <- liftIO $ stToIO $ getGameScene gameState mActorID

  -- Update camera
  case mActorID of
    Nothing -> do
      let dx = 0.3
      -- Read key states
      whenPressed GLFW.Key'W $ field @"ps_cameraVel" . _2 += dx
      whenPressed GLFW.Key'A $ field @"ps_cameraVel" . _1 -= dx
      whenPressed GLFW.Key'S $ field @"ps_cameraVel" . _2 -= dx
      whenPressed GLFW.Key'D $ field @"ps_cameraVel" . _1 += dx
      field @"ps_cameraVel" <%= Float.map2 (* 0.96)
        >>= (+=) (field @"ps_cameraPos")
    Just uid -> unless paused $ do
      let mOverseerView =
            pv_overseerView . snd
              <$> WIM.lookup uid (scene_players gameScene)
          stopCamera = field @"ps_cameraVel" .= Float2 0 0
          ticksPerFrame = getTicksPerFrame glEnv
      case mOverseerView of
        Nothing -> stopCamera
        Just overseerView ->
          -- The following camera controls should match those in the game loop
          case overseerView of
            OV_Alive OverseerViewAlive{ova_pos, ova_vel} ->
              ps_cameraPosVel
                %= cameraInterpolation
                  ticksPerFrame
                  (Fixed.toFloats ova_pos)
                  (Fixed.toFloats ova_vel)
            OV_Dead OverseerViewDead{ovd_lastPos} ->
              ps_cameraPosVel %= \camera@(_, cameraVel) ->
                cameraInterpolation
                  ticksPerFrame
                  (Fixed.toFloats ovd_lastPos)
                  cameraVel
                  camera

  -- Render
  gets ps_events >>= \gameEvents ->
    gets ps_cameraPos >>= \cameraPos -> liftIO $ do
      renderGameScene glEnv gameScene cameraPos defaultKeybindings $
        SceneDecorations
          { sceneDeco_perspective =
              case mActorID of
                Nothing -> FreePerspective
                Just actorID ->
                  case WIM.lookup actorID (scene_players gameScene) of
                    Nothing -> FreePerspective
                    Just (player, playerView) ->
                      PlayerPerspective $
                        PlayerInfo actorID player playerView
          , sceneDeco_renderStatsOverlay = Nothing
          , sceneDeco_renderHelpOverlay = HideHelp
          , sceneDeco_roomID = Nothing
          , sceneDeco_latency = Nothing
          , sceneDeco_elaspedClientTime = 0
          , sceneDeco_showTutorial = False
          , sceneDeco_showMenu = Nothing -- TODO: enable menu?
          , sceneDeco_showOSCursor = DoNotShowOSCursor
          , sceneDeco_isFramePredicted = False
          , sceneDeco_enableScreenShake = True
          , sceneDeco_enableVisualStatic = True
          }
      GLFW.swapBuffers (gle_window glEnv)
      runGameAudio alEnv gameEvents gameScene cameraPos mActorID

  -- Compute next frame
  nextFrameGameEvents <-
    if targetTime == currentTime
      then pure mempty
      else do
        (keyFrame, snapshot) <-
          gets $
            fromMaybe (0, replay_initState replay)
              . IntMap.lookupLT (targetTime + 1)
              . ps_keyFrames
        let integrateFromKeyFrame = do
              liftIO $ stToIO $ thaw snapshot >>= copy gameState
              replicateM_ (targetTime - keyFrame) $
                void $
                  integrate updates gameState
        if targetTime > currentTime && currentTime >= keyFrame
          then
            fmap mconcat $
              replicateM (targetTime - currentTime) $
                integrate updates gameState
          else mempty <$ integrateFromKeyFrame
  field @"ps_events" .= nextFrameGameEvents

  pure $ if Quit `elem` events then DoNotContinue else Continue
 where
  whenPressed :: GLFW.Key -> StateT s IO () -> StateT s IO ()
  whenPressed key action = do
    st <- liftIO $ GLFW.getKey (gle_window glEnv) key
    when (st == GLFW.KeyState'Pressed) action

-- Helpers

-- | Integrate the game state one step. Store a snapshot of the game if it is a
-- keyframe.
integrate ::
  IntMap GameStateUpdate ->
  GameState RealWorld ->
  StateT PlaybackState IO GameEvents
integrate updates gameState = zoom (field @"ps_keyFrames") $ do
  -- Integrate
  (newTime, gameEvents) <- liftIO $
    stToIO $ do
      oldTime <- fromIntegral . getTime <$> getGameTime gameState
      gameEvents <-
        fmap (fromMaybe mempty) $
          for (IntMap.lookup oldTime updates) $ \update -> do
            let ctrls = WIM.map fromControlsTransport $ getPlayerIDMap32 $ deltaUpdate_playerControls update
                cmds = fromSmallList $ deltaUpdate_gameCommands update
            integrateGameState ctrls cmds gameState -- mutates `gameState`
      newTime <- fromIntegral . getTime <$> getGameTime gameState
      pure (newTime, gameEvents)

  -- Save keyframe
  when (isKeyFrame newTime) $ do
    frameDoesNotExist <- gets $ isNothing . IntMap.lookup newTime
    when frameDoesNotExist $ do
      snapshot <- liftIO $ stToIO $ freeze gameState
      modify' $ IntMap.insert newTime snapshot

  pure gameEvents

isKeyFrame :: Int -> Bool
isKeyFrame n = n `mod` 64 == 0
