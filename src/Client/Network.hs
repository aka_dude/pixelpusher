{-# LANGUAGE OverloadedStrings #-}

module Client.Network (
  Connection,
  connect,
  ClientSendHandles (..),
  runClient,
) where

import Control.Concurrent (forkIO)
import Control.Concurrent.Async (Async, async, cancel, waitAny)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Concurrent.MVar
import Control.Exception (finally)
import Control.Logging qualified as Logging
import Control.Monad (forever)
import Data.Functor (void)
import Data.IORef
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Text qualified as Text
import Data.Text.Encoding (decodeUtf8)
import Network.Socket hiding (connect)
import Network.Socket qualified
import Network.Socket.ByteString qualified as BS

import Pixelpusher.Custom.Clock (getMonotonicTimeNSecInt, threadDelayNSec)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Version qualified as Ver
import Pixelpusher.Network.Constants qualified as NC
import Pixelpusher.Network.Protocol
import Pixelpusher.Network.TCPProtocol

--------------------------------------------------------------------------------

data Connection = Connection
  { conn_tcpConn :: TCPConnection
  , conn_udpSock :: Socket
  }

connect ::
  forall a.
  HostName ->
  Int -> -- Port
  PlayerName ->
  Maybe Int -> -- Room
  (Connection -> IO a) ->
  IO a
connect server port playerName roomNo action = do
  Logging.log $
    "Connecting via TCP to "
      <> Text.pack server
      <> " (port "
      <> Text.pack (show port)
      <> ")."
  runTCPClient server (show port) $ \tcpConn -> do
    -- Send version
    tcpSendData tcpConn $ Ver.toByteString C.version

    -- Return token via UDP
    udpAuthToken <-
      either error id . Serialize.decode @Int <$> tcpRecvData tcpConn
    let payload =
          Serialize.encode @(ClientUDPMsg ()) $
            ClientUDPMsg_AuthToken udpAuthToken
    udpSocket <- connectToServerUDP server port
    BS.sendAll udpSocket payload

    -- Send client parameters
    tcpSendData tcpConn $ Serialize.encode (getPlayerName playerName, roomNo)

    let conn =
          Connection
            { conn_tcpConn = tcpConn
            , conn_udpSock = udpSocket
            }

    action conn

connectToServerUDP :: HostName -> Int -> IO Socket
connectToServerUDP server port = do
  let hints =
        defaultHints
          { addrSocketType = Datagram
          , addrFamily = AF_INET
          }
  addr <- head <$> getAddrInfo (Just hints) (Just server) (Just (show port))
  sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
  Network.Socket.connect sock (addrAddress addr)
  pure sock

data ClientSendHandles send = ClientSendHandles
  { client_sendTCP :: send -> IO ()
  , client_sendUDP :: send -> IO ()
  }

runClient ::
  forall recv send.
  (Serialize recv, Serialize send) =>
  Connection ->
  (recv -> IO ()) ->
  MVar (ClientSendHandles send) ->
  (Int -> IO ()) ->
  IO ()
runClient conn putRecvMsg putSendHandlesVar putLatencyMeasurement = do
  acksRef <- newIORef nullAck
  (tcpSendQueueIn, tcpSendQueueOut) <- UC.newChan @(ClientTCPMsg send)
  (udpSendQueueIn, udpSendQueueOut) <- UC.newChan @send

  let putTCP = UC.writeChan tcpSendQueueIn . ClientTCPMsg_Data
      putUDP = UC.writeChan udpSendQueueIn
  putMVar putSendHandlesVar $
    ClientSendHandles
      { client_sendTCP = putTCP
      , client_sendUDP = putUDP
      }

  asyncs <-
    traverse
      async
      [ runReceiveUDP (conn_udpSock conn) acksRef putRecvMsg
      , runSendUDP (conn_udpSock conn) acksRef udpSendQueueOut
      , runReceiveTCP (conn_tcpConn conn) putRecvMsg putLatencyMeasurement
      , runSendTCP (conn_tcpConn conn) tcpSendQueueOut
      , runStillAlive tcpSendQueueIn
      ]
  void $ waitAnyCancelAsync asyncs

--------------------------------------------------------------------------------

runReceiveUDP ::
  forall recv a.
  (Serialize recv) =>
  Socket ->
  IORef Acks ->
  (recv -> IO ()) ->
  IO a
runReceiveUDP udpSocket acksRef putRecvMsg = forever $ do
  payload <- BS.recv udpSocket 1024
  case Serialize.decode @(ServerUDPMsg recv) payload of
    Left _ -> Logging.warn $ "Received bad UDP msg from server: " <> decodeUtf8 payload
    Right (ServerUDPMsg seqNum recvMsg) -> do
      atomicModifyIORef' acksRef $ \ack -> (insertSeqNum seqNum ack, ())
      putRecvMsg recvMsg

runReceiveTCP ::
  forall recv a.
  (Serialize recv) =>
  TCPConnection ->
  (recv -> IO ()) ->
  (Int -> IO ()) ->
  IO a
runReceiveTCP tcpConn putRecvMsg putLatencyMeasurement = forever $ do
  payload <- tcpRecvData tcpConn
  case Serialize.decode @(ServerTCPMsg recv) payload of
    Left _ -> Logging.warn $ "Recevied bad TCP message from server: " <> decodeUtf8 payload
    Right serverMsg ->
      case serverMsg of
        ServerTCPMsg_Data recvMsg -> putRecvMsg recvMsg
        ServerReturnPing time0 -> do
          time1 <- getMonotonicTimeNSecInt
          let latencyMilliseconds = (time1 - time0) `div` 1000000
          putLatencyMeasurement latencyMilliseconds

runSendUDP ::
  forall a send.
  (Serialize send) =>
  Socket ->
  IORef Acks ->
  UC.OutChan send ->
  IO a
runSendUDP udpSocket acksRef udpSendQueueOut = do
  seqNumRef <- newIORef firstSeqNum
  runSendUDP' seqNumRef udpSocket acksRef udpSendQueueOut

runSendUDP' ::
  (Serialize send) =>
  IORef SeqNum ->
  Socket ->
  IORef Acks ->
  UC.OutChan send ->
  IO a
runSendUDP' seqNumRef udpSocket acksRef udpSendQueueOut = forever $ do
  msg <- UC.readChan udpSendQueueOut

  seqNum <- readIORef seqNumRef
  writeIORef seqNumRef $ incrementSeqNum seqNum

  acks <- readIORef acksRef

  BS.sendAll udpSocket $
    Serialize.encode $
      ClientUDPMsg_Data $
        ClientUDPData
          { clientUDPData_sequenceNumber = seqNum
          , clientUDPData_acks = acks
          , clientUDPData_payload = msg
          }

runSendTCP ::
  (Serialize send) =>
  TCPConnection ->
  UC.OutChan (ClientTCPMsg send) ->
  IO a
runSendTCP tcpConn sendQueueOut =
  forever $ UC.readChan sendQueueOut >>= tcpSendData tcpConn . Serialize.encode

runStillAlive :: forall send a. UC.InChan (ClientTCPMsg send) -> IO a
runStillAlive sendQueue = forever $ do
  time <- getMonotonicTimeNSecInt
  UC.writeChan sendQueue $ ClientStillAlivePing @send time
  threadDelayNSec NC.stillAliveSendInterval_ns

--------------------------------------------------------------------------------

-- | Like 'waitAnyCancel', but cancels the asynchronous operations
-- asynchronously.
--
-- This is to work around the uninterruptible system IO of GHC's Windows
-- runtime. See https://gitlab.haskell.org/ghc/ghc/-/issues/7353
waitAnyCancelAsync :: [Async a] -> IO (Async a, a)
waitAnyCancelAsync asyncs =
  waitAny asyncs `finally` mapM_ (forkIO . cancel) asyncs
