{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Config.CommandLineParameters (
  CommandLineParams (..),
  Command (..),
  parseArgs,
) where

import Data.Text (Text)
import Options.Applicative

import Pixelpusher.Game.Constants qualified as C

import Client.Scene.CustomMultiplayer (MultiplayerOptions (..))

--------------------------------------------------------------------------------

data Command
  = RunClient CommandLineParams
  | Replay FilePath

data CommandLineParams = CommandLineParams
  { clp_port :: Int
  , clp_multiplayerOptions :: Maybe MultiplayerOptions
  }

parseArgs :: IO Command
parseArgs =
  execParser $
    info (helper <*> clientCommand) $
      header ">>= Pixelpusher Client =<<"

clientCommand :: Parser Command
clientCommand =
  fmap Replay replayFile
    <|> fmap RunClient commandLineParams

--------------------------------------------------------------------------------

replayFile :: Parser FilePath
replayFile =
  strOption $
    mconcat
      [ long "replay"
      , short 'R'
      , metavar "FILE"
      , help "Play a replay file"
      ]

--------------------------------------------------------------------------------

commandLineParams :: Parser CommandLineParams
commandLineParams = CommandLineParams <$> port <*> multiplayerOptions

port :: Parser Int
port =
  option auto $
    mconcat
      [ long "port"
      , short 'P'
      , metavar "PORT"
      , value C.defaultPort
      , help "The port on the server to connect to"
      ]

multiplayerOptions :: Parser (Maybe MultiplayerOptions)
multiplayerOptions =
  optional $ do
    mo_serverAddress <- serverAddress
    mo_playerName <- name
    pure MultiplayerOptions{..}

serverAddress :: Parser Text
serverAddress =
  strOption $
    mconcat
      [ long "address"
      , short 'a'
      , metavar "ADDRESS"
      , help "The address of the game server"
      ]

name :: Parser Text
name =
  strOption $
    mconcat
      [ long "name"
      , short 'n'
      , metavar "NAME"
      , help "Join the game under the given name"
      ]

-- room :: Parser Text
-- room =
--   strOption $
--     mconcat
--       [ long "room"
--       , short 'r'
--       , metavar "INT"
--       , value ""
--       , help "Join the game room with the given ID (optional)"
--       ]
