{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Menu (
  MenuItemType (..),
  MenuItem (..),
  ButtonConfig (..),
  ToggleConfig (..),
  IntSliderConfig (..),
  EnumChoiceConfig (..),
  TextInputConfig (..),
  KeybindConfig (..),
  MenuShow (..),
  MenuState,
  makeMenuState,
  getMenuTitle,
  menuModel,
  Selected (..),
  Visibility (..),
  renderMenu,
  MenuUpdateEvent (..),
  handleMenuInput,
) where

import Data.Bifunctor (first)
import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List (intersperse)
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Client.Config.Keybindings (keyToText)
import Client.Menu.Input (MenuInputEvent (..))
import Client.Menu.Zipper (Zipper (..))
import Client.Menu.Zipper qualified as Zipper

--------------------------------------------------------------------------------
-- Configuration

data MenuItemType
  = Button
  | Toggle
  | IntSlider
  | TextInput
  | EnumChoice Type
  | Keybind

type MenuItem :: ids -> Type -> Type
data MenuItem ids s where
  ButtonWidget ::
    ids 'Button -> ButtonConfig -> MenuItem ids s
  ToggleWidget ::
    ids 'Toggle -> Lens' s Bool -> ToggleConfig -> MenuItem ids s
  IntSliderWidget ::
    ids 'IntSlider -> Lens' s Int -> IntSliderConfig -> MenuItem ids s
  -- | Displays all choices on one line of text, so only works if there are a
  -- small number of choices.
  EnumChoiceWidget ::
    (Bounded a, Enum a, Eq a, MenuShow a) =>
    ids ('EnumChoice a) ->
    Lens' s a ->
    EnumChoiceConfig ->
    MenuItem ids s
  TextInputWidget ::
    ids 'TextInput -> Lens' s Text -> TextInputConfig -> MenuItem ids s
  -- | Special-purpose widget for setting custom keybindings
  KeybindWidget ::
    ids 'Keybind ->
    (s -> GLFW.Key) ->
    KeybindConfig ->
    MenuItem ids s

menuItemId :: MenuItem ids s -> SomeWidgetId ids
menuItemId = \case
  ButtonWidget widgetId _ -> SomeWidgetId widgetId
  ToggleWidget widgetId _ _ -> SomeWidgetId widgetId
  IntSliderWidget widgetId _ _ -> SomeWidgetId widgetId
  EnumChoiceWidget widgetId _ _ -> SomeWidgetId widgetId
  TextInputWidget widgetId _ _ -> SomeWidgetId widgetId
  KeybindWidget widgetId _ _ -> SomeWidgetId widgetId

newtype ButtonConfig = ButtonConfig
  { bc_label :: Text
  }

newtype ToggleConfig = ToggleConfig
  { tc_label :: Text
  }

data IntSliderConfig = IntSliderConfig
  { isc_label :: Text
  , isc_min :: Int
  , isc_max :: Int
  , isc_step :: Int
  }

newtype EnumChoiceConfig = EnumChoiceConfig
  { esc_label :: Text
  }

data TextInputConfig = TextInputConfig
  { tic_label :: Text
  , tic_maxLength :: Int
  , tic_permittedChars :: Char -> Bool
  }

-- TODO: Move closer to EnumChoiceConfig
class MenuShow a where
  menuShow :: a -> Text

newtype KeybindConfig = KeybindConfig
  { kc_label :: Text
  }

--------------------------------------------------------------------------------
-- State

data MenuState ids s = MenuState
  { ms_title :: Text
  , ms_zipper :: Zipper (MenuItem ids s)
  , ms_model :: s
  }
  deriving (Generic)

getMenuTitle :: MenuState ids s -> Text
getMenuTitle = ms_title

menuModel :: Lens' (MenuState ids s) s
menuModel = field @"ms_model"

makeMenuState :: Text -> NonEmpty (MenuItem ids s) -> s -> MenuState ids s
makeMenuState title menuItems initialModel =
  MenuState
    { ms_title = title
    , ms_zipper = Zipper.fromNonEmpty menuItems
    , ms_model = initialModel
    }

--------------------------------------------------------------------------------
-- Rendering

data Selected = Selected | NotSelected

data Visibility = Visible | Shadowed | Invisible

renderMenu :: MenuState ids s -> [(Selected, NonEmpty (Visibility, Text))]
renderMenu menuState =
  let Zipper{..} = ms_zipper menuState
      model = ms_model menuState
  in  map ((NotSelected,) . renderMenuItem model NotSelected) (reverse z_above)
        <> [(Selected, renderMenuItem model Selected z_focused)]
        <> map ((NotSelected,) . renderMenuItem model NotSelected) z_below

renderMenuItem :: s -> Selected -> MenuItem ids s -> NonEmpty (Visibility, Text)
renderMenuItem s selected = \case
  ButtonWidget _ buttonConfig ->
    pure (Visible, "[" <> bc_label buttonConfig <> "]")
  ToggleWidget _ lens_ toggleConfig ->
    let label = tc_label toggleConfig
        enabled = view lens_ s
        offVisibility = if enabled then Shadowed else Visible
        onVisibility = if enabled then Visible else Shadowed
    in  (Visible, label)
          :| [ (Shadowed, " :: ")
             , (offVisibility, "off")
             , (Shadowed, " | ")
             , (onVisibility, "on")
             ]
  IntSliderWidget _ lens_ IntSliderConfig{..} ->
    let value = view lens_ s
        leftArrowVisibility
          | value > isc_min = Visible
          | otherwise = Shadowed
        rightArrowVisibility
          | value < isc_max = Visible
          | otherwise = Shadowed
        renderedValue = tshow value
        padding = Text.pack $ replicate (max 0 (maxWidth - valueWidth)) '0'
         where
          valueWidth = Text.length renderedValue
          maxWidth = length $ show isc_max
    in  (Visible, isc_label)
          :| [ (Shadowed, " :: ")
             , (leftArrowVisibility, "< ")
             , (Invisible, padding)
             , (Visible, renderedValue)
             , (rightArrowVisibility, " > ")
             ]
  TextInputWidget _ lens_ TextInputConfig{..} ->
    let suffix =
          case selected of
            NotSelected -> ""
            Selected -> "_"
        text = view lens_ s
    in  (Visible, tic_label)
          :| [ (Shadowed, " :: ")
             , (Visible, text)
             , (Shadowed, suffix)
             ]
  EnumChoiceWidget _ lens_ EnumChoiceConfig{..} ->
    let value = view lens_ s
        values =
          flip map [minBound .. maxBound] $ \val ->
            if val == value
              then (Visible, menuShow val)
              else (Shadowed, menuShow val)
    in  (:|) (Visible, esc_label) $
          (Shadowed, " :: ")
            : intersperse (Shadowed, " | ") values
  KeybindWidget _ getter KeybindConfig{..} ->
    let key = getter s
        keyText = "[" <> keyToText key <> "]"
    in  (:|)
          (Visible, kc_label)
          [ (Shadowed, " :: ")
          , (Visible, keyText)
          ]

--------------------------------------------------------------------------------
-- Update

data MenuUpdateEvent ids where
  Escape ::
    MenuUpdateEvent ids
  Enter ::
    MenuUpdateEvent ids
  MenuScrollUp ::
    SomeWidgetId ids ->
    MenuUpdateEvent ids
  MenuScrollDown ::
    SomeWidgetId ids ->
    MenuUpdateEvent ids
  ButtonPressed ::
    ids 'Button ->
    MenuUpdateEvent ids
  ToggleSwitched ::
    ids 'Toggle ->
    Bool -> -- new value
    MenuUpdateEvent ids
  IntSliderChanged ::
    ids 'IntSlider ->
    Int -> -- new value
    MenuUpdateEvent ids
  EnumChoiceChanged ::
    ids ('EnumChoice a) ->
    a -> -- new value
    MenuUpdateEvent ids
  TextChanged ::
    ids 'TextInput ->
    Text -> -- new value
    MenuUpdateEvent ids
  KeybindTriggered ::
    ids 'Keybind ->
    MenuUpdateEvent ids

type SomeWidgetId :: ids -> Type
data SomeWidgetId ids where
  SomeWidgetId :: ids a -> SomeWidgetId ids

handleMenuInput ::
  MenuInputEvent ->
  MenuState ids s ->
  Maybe (MenuState ids s, MenuUpdateEvent ids)
handleMenuInput inputEvent menuState =
  leftToMaybe $ do
    let MenuState{..} = menuState
        selectedWidget = z_focused ms_zipper
    first (first (flip (set menuModel) menuState)) $
      handleInputWidgets inputEvent ms_model selectedWidget
    handleInputMenu menuState inputEvent
    first (menuState,) $ handleInputGlobal inputEvent

leftToMaybe :: Either a b -> Maybe a
leftToMaybe = \case
  Left a -> Just a
  Right _ -> Nothing

handleInputWidgets ::
  MenuInputEvent ->
  s ->
  MenuItem ids s ->
  Either (s, MenuUpdateEvent ids) ()
handleInputWidgets inputEvent s = \case
  ButtonWidget buttonId _buttonConfig ->
    case inputEvent of
      MenuEnter -> Left (s, ButtonPressed buttonId)
      _ -> Right ()
  ToggleWidget toggleId lens_ _ ->
    case inputEvent of
      MenuEnter ->
        let newVal = not $ view lens_ s
        in  Left (set lens_ newVal s, ToggleSwitched toggleId newVal)
      MenuLeft
        | view lens_ s ->
            let newVal = False
            in  Left (set lens_ newVal s, ToggleSwitched toggleId newVal)
        | otherwise ->
            Right ()
      MenuRight
        | view lens_ s ->
            Right ()
        | otherwise ->
            let newVal = True
            in  Left (set lens_ newVal s, ToggleSwitched toggleId newVal)
      _ ->
        Right ()
  IntSliderWidget intSliderId lens_ IntSliderConfig{..} ->
    case inputEvent of
      MenuLeft
        | val > isc_min ->
            let newVal = max isc_min (val - isc_step)
            in  Left (set lens_ newVal s, IntSliderChanged intSliderId newVal)
        | otherwise ->
            Right ()
      MenuRight
        | val < isc_max ->
            let newVal = min isc_max (val + isc_step)
            in  Left (set lens_ newVal s, IntSliderChanged intSliderId newVal)
        | otherwise ->
            Right ()
      _ -> Right ()
   where
    val = view lens_ s
  EnumChoiceWidget enumChoiceId lens_ EnumChoiceConfig{} ->
    case inputEvent of
      MenuLeft
        | val == minBound ->
            Right ()
        | otherwise ->
            let newVal = pred val
                newState = set lens_ newVal s
                update = EnumChoiceChanged enumChoiceId newVal
            in  Left (newState, update)
      MenuRight
        | val == maxBound ->
            Right ()
        | otherwise ->
            let newVal = succ val
                newState = set lens_ newVal s
                update = EnumChoiceChanged enumChoiceId newVal
            in  Left (newState, update)
      _ -> Right ()
   where
    val = view lens_ s
  TextInputWidget textInputId lens_ TextInputConfig{..} ->
    case inputEvent of
      MenuCharInput c
        | not (tic_permittedChars c) ->
            Right ()
        | Text.length val >= tic_maxLength ->
            Right ()
        | otherwise ->
            let newVal = Text.snoc val c
            in  Left (set lens_ newVal s, TextChanged textInputId newVal)
      MenuCharDelete ->
        case Text.unsnoc val of
          Nothing -> Right ()
          Just (newText, _) ->
            Left (set lens_ newText s, TextChanged textInputId newText)
      MenuPasteText pastedText ->
        let newText = Text.take tic_maxLength $ val <> pastedText
        in  Left (set lens_ newText s, TextChanged textInputId newText)
      _ -> Right ()
   where
    val = view lens_ s
  KeybindWidget keySelectId _lens_ KeybindConfig{} ->
    -- Dummy widget handler for the special-purpose keybind widget; model
    -- updates do not happen here, but in the user event handler.
    case inputEvent of
      MenuEnter ->
        Left (s, KeybindTriggered keySelectId)
      _ -> Right ()

handleInputMenu ::
  MenuState ids s ->
  MenuInputEvent ->
  Either (MenuState ids s, MenuUpdateEvent ids) ()
handleInputMenu menuState = \case
  MenuEnter -> Right ()
  MenuEscape -> Right ()
  MenuUp ->
    Left
      ( over (field @"ms_zipper") Zipper.scrollUp menuState
      , MenuScrollUp (menuItemId menuItem)
      )
  MenuDown ->
    Left
      ( over (field @"ms_zipper") Zipper.scrollDown menuState
      , MenuScrollDown (menuItemId menuItem)
      )
  MenuLeft -> Right ()
  MenuRight -> Right ()
  MenuCharInput{} -> Right ()
  MenuCharDelete -> Right ()
  MenuPasteText{} -> Right ()
 where
  menuItem = z_focused $ ms_zipper menuState

handleInputGlobal :: MenuInputEvent -> Either (MenuUpdateEvent ids) ()
handleInputGlobal = \case
  MenuEnter -> Left Enter
  MenuEscape -> Left Escape
  _ -> Right ()

-- Helper
tshow :: Show a => a -> Text
tshow = Text.pack . show
