{-# LANGUAGE TemplateHaskell #-}

module Client.Audio.Music (
  bassFastOpus,
  bassSlowOpus,
  drumsFastOpus,
  synthSlowFastOpus,
  vocorderFlagOpus,
) where

import Data.ByteString qualified as BS
import Data.FileEmbed (embedFile)

bassFastOpus :: BS.ByteString
bassFastOpus = $(embedFile "music/pixel_pusher_bass_(fast).opus")

bassSlowOpus :: BS.ByteString
bassSlowOpus = $(embedFile "music/pixel_pusher_bass_(slow).opus")

drumsFastOpus :: BS.ByteString
drumsFastOpus = $(embedFile "music/pixel_pusher_drums_(fast).opus")

synthSlowFastOpus :: BS.ByteString
synthSlowFastOpus = $(embedFile "music/pixel_pusher_synth_(slow_fast).opus")

vocorderFlagOpus :: BS.ByteString
vocorderFlagOpus = $(embedFile "music/pixel_pusher_vocorder_(flag).opus")
