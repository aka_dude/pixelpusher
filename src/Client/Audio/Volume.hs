module Client.Audio.Volume (
  Volume,
  muted,
  gainPercent,
  gain,
  initialVolume,
  increaseVolume,
  setGainPercent,
  decreaseVolume,
  toggleMute,
) where

import Foreign.C.Types (CFloat)

data Volume = Volume
  { _volume_muted :: Bool
  , _volume_gain :: Int
  }

muted :: Volume -> Bool
muted = _volume_muted

gainPercent :: Volume -> Int
gainPercent = _volume_gain

gain :: Volume -> CFloat
gain vol = fromIntegral (_volume_gain vol) / 100

initialVolume :: Int -> Volume
initialVolume = Volume False . gainPercentClamp

toggleMute :: Volume -> Volume
toggleMute vol = vol{_volume_muted = not $ _volume_muted vol}

setGainPercent :: Int -> Volume -> Volume
setGainPercent gainPercent_ vol =
  vol{_volume_gain = gainPercentClamp gainPercent_}

increaseVolume :: Volume -> Volume
increaseVolume vol =
  vol{_volume_gain = gainPercentClamp $ _volume_gain vol + gainPercentIncrement}

decreaseVolume :: Volume -> Volume
decreaseVolume vol =
  vol{_volume_gain = gainPercentClamp $ _volume_gain vol - gainPercentIncrement}

gainPercentClamp :: Int -> Int
gainPercentClamp = max 0 . min 100

gainPercentIncrement :: Int
gainPercentIncrement = 10
