{-# LANGUAGE RecordWildCards #-}

module Client.Audio.Game (
  playSfx,
) where

import Control.Arrow ((&&&))
import Control.Monad (unless, when)
import Data.Foldable (foldl')
import Data.List (partition, sortOn)
import Data.Ord (Down (Down))
import Foreign.C.Types (CFloat)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.CastEvents (DashType (..))
import Pixelpusher.Game.CollisionEvents (
  CollisionParticipants (..),
  NeutralCollision (..),
 )
import Pixelpusher.Game.CombatEvents (
  CollisionDamage (..),
  CollisionDamageParticipants (..),
  PsiStormEffect (..),
 )
import Pixelpusher.Game.EntityStore (
  DashCastEvent (..),
  OverseerDeathEvent (..),
  PsiStormCastEvent (..),
 )
import Pixelpusher.Game.FlagEvents (
  FlagCapture (..),
  FlagPickup (..),
  FlagRecovery (..),
  FlagType (BaseFlagType),
 )
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Team

import Client.Audio.AL.Env (
  ALEnv,
  BonkPitch (..),
  FlagNotification (..),
  WhooshPitch (..),
  playBonk,
  playBonkMuffled,
  playDistantCrack,
  playFlagExplosion,
  playFlagNotification,
  playOverseerExplosion,
  playPixelpusherIntro,
  playStaticShock,
  playWhoosh,
 )

playSfx ::
  ALEnv ->
  AudioParams ->
  Float ->
  Float2 ->
  Maybe Team ->
  GameEvents ->
  IO ()
playSfx alEnv params listenRadius listenPos listenTeam gameEvents = do
  let withGainPos ::
        Applicative f =>
        GainPos ->
        (CFloat -> (CFloat, CFloat) -> f ()) ->
        f ()
      withGainPos GainPos{_gain, _relPos = Float2 x y} f =
        when (_gain > 0) $ f (realToFrac _gain) (realToFrac x, realToFrac y)
      averageGainPosition_ :: Int -> [Fixed2] -> GainPos
      averageGainPosition_ limit positions =
        averageGainPosition limit $ map (1,) positions
      averageGainPosition :: Int -> [(Float, Fixed2)] -> GainPos
      averageGainPosition limit gainPositions
        | null gainPositions = zeroGainPos
        | otherwise =
            let gainRelPos =
                  flip map gainPositions $ \(gain, pos) ->
                    let relPos = Fixed.toFloats pos - listenPos
                        distGain =
                          distanceGain listenRadius params (Float.norm2 relPos)
                    in  (gain * distGain, relPos)
                (!sumSqGains, !sumRelPos) =
                  foldl' f (0, 0) $
                    if length gainRelPos <= limit
                      then gainRelPos
                      else take limit $ sortOn (Down . fst) gainRelPos
                 where
                  f (!sumSqGains', !sumRelPos') (gain, relPos) =
                    let gain2 = gain * gain
                    in  ( sumSqGains' + gain2
                        , sumRelPos' + Float.map2 (* gain2) relPos
                        )
            in  if sumSqGains <= 0
                  then zeroGainPos
                  else
                    GainPos
                      (sqrt sumSqGains)
                      (Float.map2 (/ sumSqGains) sumRelPos)

  -- Collision bonks
  let (overseerBonks, droneBonks) =
        partitionCollisions $
          map fromCollisionDamage (ge_collisions gameEvents)
  withGainPos
    (averageGainPosition 4 overseerBonks)
    (playBonk alEnv params BonkLow)
  withGainPos
    (averageGainPosition 4 droneBonks)
    (playBonk alEnv params BonkHigh)

  -- Neutral collision bonks
  let (overseerMuffledBonks, droneMuffledBonks) =
        partitionCollisions $
          map fromNeutralCollision (ge_neutralCollisions gameEvents)
  withGainPos
    (averageGainPosition 4 overseerMuffledBonks)
    (playBonkMuffled alEnv params BonkLow)
  withGainPos
    (averageGainPosition 4 droneMuffledBonks)
    (playBonkMuffled alEnv params BonkHigh)

  -- Overseer deaths
  withGainPos
    (averageGainPosition_ 4 (map ode_pos (ge_overseerDeaths gameEvents)))
    (playOverseerExplosion alEnv params)

  -- Flag captures: explosion
  withGainPos
    (averageGainPosition_ 4 (map flagCapture_position (ge_flagCaptures gameEvents)))
    (playFlagExplosion alEnv params)

  -- Flag captures: chord
  do
    let (alliedFlagCaptures, enemyFlagCaptures) =
          partition (\team -> maybe True (== team) listenTeam) $
            map flagCapture_team $
              ge_flagCaptures gameEvents
    unless (null alliedFlagCaptures) $
      playFlagNotification alEnv params NotifyFlagScorePositive 1 (0, 0)
    unless (null enemyFlagCaptures) $
      playFlagNotification alEnv params NotifyFlagScoreNegative 1 (0, 0)

  -- Flag pickups: arpeggio
  do
    let (alliedFlagPickups, enemyFlagPickups) =
          partition (\team -> maybe True (== team) listenTeam) $
            map flagPickup_carrierTeam $
              filter ((== BaseFlagType) . flagPickup_type) $
                ge_flagPickups gameEvents
    unless (null alliedFlagPickups) $
      playFlagNotification alEnv params NotifyFlagStealPositive 1 (0, 0)
    unless (null enemyFlagPickups) $
      playFlagNotification alEnv params NotifyFlagStealNegative 1 (0, 0)

  -- Flag recoveries: downward arpeggio
  do
    let (alliedFlagRecoveries, enemyFlagRecoveries) =
          partition (\team -> maybe True (== team) listenTeam) $
            map flagRecovery_team $
                ge_flagRecoveries gameEvents
    unless (null alliedFlagRecoveries) $
      playFlagNotification alEnv params NotifyFlagRecoverPositive 1 (0, 0)
    unless (null enemyFlagRecoveries) $
      playFlagNotification alEnv params NotifyFlagRecoverNegative 1 (0, 0)

  -- Dashes
  do
    let (droneDashes, overseerDashes) =
          partition
            ((\case DroneDash -> True; OverseerDash -> False) . dce_dashType)
            (ge_dashes gameEvents)
        dashGain = Fixed.toFloat . dce_effectsMagnitudeFrac
    withGainPos
      ( averageGainPosition
          16
          (map (dashGain &&& dce_pos) droneDashes)
      )
      (playWhoosh alEnv params WhooshHigh)
    withGainPos
      ( averageGainPosition
          3
          (map (dashGain &&& dce_pos) overseerDashes)
      )
      (playWhoosh alEnv params WhooshLow)

  -- Psi-storm casts
  withGainPos
    (averageGainPosition_ 4 (map psce_pos (ge_psiStormCasts gameEvents)))
    (playDistantCrack alEnv params)

  -- Psi-storm damage
  withGainPos
    ( averageGainPosition_
        16
        (map pse_entityPos (ge_psiStormEffects gameEvents))
    )
    (playStaticShock alEnv params)

  -- Match end sfx
  when (PlayMatchEndSfx `elem` ge_gamePhaseTransitions gameEvents) $
    -- Naming: It was originally an intro but we're using it for match end
    playPixelpusherIntro alEnv params

data AudibleCollision = AudibleCollision
  { ac_participants :: CollisionParticipants
  , ac_position :: Fixed2
  , ac_damage :: Fixed
  }

fromCollisionDamage :: CollisionDamage -> AudibleCollision
fromCollisionDamage CollisionDamage{..} =
  AudibleCollision
    { ac_participants = forgetCollisionDamage cd_participants
    , ac_position = cd_position
    , ac_damage = cd_damage
    }

forgetCollisionDamage :: CollisionDamageParticipants -> CollisionParticipants
forgetCollisionDamage = \case
  CollisionDamageOverseers{} -> CollisionOverseers
  CollisionDamageDrones{} -> CollisionDrones
  CollisionDamageOverseerDrone{} -> CollisionOverseerDrone

fromNeutralCollision :: NeutralCollision -> AudibleCollision
fromNeutralCollision NeutralCollision{..} =
  AudibleCollision
    { ac_participants = nce_participants
    , ac_position = nce_position
    , ac_damage = dampedDamage
    }
 where
  dampedDamage = max 0 $ nce_damageEquivalent - minDmg -- Ignore small collisions
  minDmg = 0.1

partitionCollisions ::
  [AudibleCollision] ->
  ([(Float, Fixed2)], [(Float, Fixed2)])
partitionCollisions = foldr f ([], [])
 where
  f audibleCollision (overseers, drones) =
    case ac_participants audibleCollision of
      CollisionOverseers ->
        ( (gain, pos) : overseers
        , drones
        )
      CollisionOverseerDrone ->
        ( (gain * invSqrt2, pos) : overseers
        , (gain * invSqrt2, pos) : drones
        )
      CollisionDrones ->
        ( overseers
        , (gain, pos) : drones
        )
   where
    gain = collisionDamageGain audibleCollision
    pos = ac_position audibleCollision
    invSqrt2 = 1 / sqrt 2

collisionDamageGain :: AudibleCollision -> Float
collisionDamageGain audibleCollision =
  let dmg = Fixed.toFloat $ ac_damage audibleCollision
      dmgMax = 64
  in  sqrt $ dmg / dmgMax

distanceGain :: Float -> AudioParams -> Float -> Float
distanceGain listenRadius params dist =
  1 - (min maxDist (max minDist dist) - minDist) / (maxDist - minDist)
 where
  minDist = listenRadius * Fixed.toFloat (gp_audioMinDistanceFactor params)
  maxDist = listenRadius * Fixed.toFloat (gp_audioMaxDistanceFactor params)

data GainPos = GainPos
  { _gain :: Float
  , _relPos :: Float2
  }

zeroGainPos :: GainPos
zeroGainPos = GainPos{_gain = 0, _relPos = 0}
