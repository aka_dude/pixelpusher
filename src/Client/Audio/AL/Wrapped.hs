{-# LANGUAGE BlockArguments #-}

module Client.Audio.AL.Wrapped (
  makeSource,
  sourceOpusFile,
) where

import Data.ByteString qualified as BS
import Foreign qualified
import Sound.OpenAL.FFI.AL qualified as AL
import Sound.OpusFile qualified as OpusFile

makeSource :: IO AL.Source
makeSource = Foreign.alloca $ \sourcePtr -> do
  AL.alGenSources 1 sourcePtr
  Foreign.peek sourcePtr

-- Copied from the "opusfile" package.
-- Not part of OpenAL, so shouldn't be in this module, but whatever.
sourceOpusFile :: BS.ByteString -> IO AL.Buffer
sourceOpusFile opusFile = do
  Right file <- OpusFile.openMemoryBS opusFile
  pcm <- OpusFile.decodeInt16 file
  OpusFile.free file

  buffer <- Foreign.alloca \bufPtr -> do
    AL.alGenBuffers 1 bufPtr
    Foreign.peek bufPtr

  Foreign.withForeignPtr (OpusFile.pcmData pcm) \pcmData -> do
    let alFormat =
          case OpusFile.pcmChannels pcm of
            Right OpusFile.Stereo ->
              AL.FORMAT_STEREO16
            Right OpusFile.Mono ->
              AL.FORMAT_MONO16
            Left n ->
              error $ "unexpected channels: " <> show n

    AL.alBufferData
      buffer
      alFormat
      (Foreign.castPtr pcmData)
      (fromIntegral $ OpusFile.pcmSize pcm)
      48000

  pure buffer
