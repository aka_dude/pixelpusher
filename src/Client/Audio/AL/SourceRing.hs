{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}

-- | This module is intended to be imported qualified.
module Client.Audio.AL.SourceRing (
  SourceRing,
  init,
  PlayOptions (..),
  play,
) where

import Prelude hiding (init)

import Control.Monad (replicateM)
import Data.IORef (IORef, atomicModifyIORef', newIORef)
import Data.Vector qualified as V
import Foreign qualified
import Foreign.C.Types (CFloat)
import Sound.OpenAL.FFI.AL qualified as AL

import Client.Audio.AL.Wrapped (makeSource)
import Client.Constants qualified as Constants

data SourceRing = SourceRing
  { sr_sources :: V.Vector AL.Source
  , sr_indexRef :: IORef Int
  }

init :: Int -> IO SourceRing
init nSources = do
  sr_sources <- V.fromList <$> replicateM nSources makeSource
  sr_indexRef <- newIORef 0
  pure $ SourceRing{..}

getNextSource :: SourceRing -> IO AL.Source
getNextSource SourceRing{..} =
  fmap (sr_sources V.!) $
    atomicModifyIORef' sr_indexRef $ \i ->
      let j = succ i `mod` length sr_sources in (j, j)

data PlayOptions = PlayOptions
  { play_gain :: CFloat
  , play_pitch :: CFloat
  , play_pos_x :: CFloat
  , play_pos_y :: CFloat
  }

play :: SourceRing -> AL.Buffer -> PlayOptions -> IO ()
play sourceRing (AL.Buffer bufIdx) playOpts = do
  let PlayOptions (clamp -> gain) pitch pos_x pos_y = playOpts
  source <- getNextSource sourceRing
  Foreign.with source $ AL.alSourceStopv 1
  AL.alSourcei source AL.BUFFER (fromIntegral bufIdx)
  Foreign.with gain $ AL.alSourcefv source AL.GAIN
  Foreign.with pitch $ AL.alSourcefv source AL.PITCH
  AL.alSourcei source AL.SOURCE_RELATIVE 1
  Foreign.withArray [pos_x, pos_y, pos_z] $ \ptr ->
    AL.alSourcefv source AL.POSITION $ Foreign.castPtr ptr
  Foreign.with source $ AL.alSourcePlayv 1

clamp :: (Num a, Ord a) => a -> a
clamp = max 0 . min 1

pos_z :: CFloat
pos_z = -realToFrac Constants.viewRadius
