{-# LANGUAGE RecordWildCards #-}

module Client.Audio.AdaptiveMusic (
  -- * Initialization
  MusicHandle,
  withMusic,

  -- * Control
  TrackGains (..),
  setTrackGains,
  getMusicGainPercent,
  setMusicGainPercent,
  toggleMusicMute,
  Muted (..),
  getMusicMute,
) where

import Prelude hiding (init)

import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (withAsync)
import Control.Concurrent.STM (atomically)
import Data.ByteString qualified as BS
import Data.Functor ((<&>))
import Data.Generics.Product.Fields
import Data.Maybe (fromMaybe)
import Foreign qualified
import Foreign.C.Types (CFloat)
import Foreign.Marshal.Array (withArrayLen)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom (over)
import Sound.OpenAL.FFI.AL qualified as AL

import Pixelpusher.Game.Constants qualified as C

import Client.Audio.AL.Wrapped (makeSource, sourceOpusFile)
import Client.Audio.Music qualified as Music (
  bassFastOpus,
  bassSlowOpus,
  drumsFastOpus,
  synthSlowFastOpus,
  vocorderFlagOpus,
 )
import Client.Util.TSignal

--------------------------------------------------------------------------------

newtype MusicHandle = MusicHandle {mh_command :: TSignal MusicStateTarget}

data MusicStateTarget = MusicStateTarget
  { mst_trackGains :: TrackGains
  , mst_muted :: Muted
  , mst_transitionSpeedFactor :: CFloat
  , mst_musicGainPercent :: Int -- gain for all music
  }
  deriving (Eq, Generic)

-- | Sources for the tracks that make up the game music. These tracks must
-- played in sync with each other.
data MusicSources = MusicSources
  { ms_bassFastSource :: AL.Source
  , ms_bassSlowSource :: AL.Source
  , ms_drumsFastSource :: AL.Source
  , ms_synthSlowFastSource :: AL.Source
  , ms_vocorderFlagSource :: AL.Source
  }

data MusicState = MusicState
  { ms_currenTrackGains :: TrackGains
  , ms_target :: MusicStateTarget
  }

data Muted = Muted | NotMuted
  deriving (Eq)

toggleMute :: Muted -> Muted
toggleMute = \case Muted -> NotMuted; NotMuted -> Muted

-- | Gains for each category of track.
data TrackGains = TrackGains
  { tg_slow :: CFloat
  , tg_fast :: CFloat
  , tg_slowFast :: CFloat
  , tg_flag :: CFloat
  }
  deriving (Eq)

initialMusicStateTarget :: Int -> MusicStateTarget
initialMusicStateTarget initialGainPercent =
  MusicStateTarget
    { mst_trackGains = initialTrackGains
    , mst_muted = NotMuted
    , mst_transitionSpeedFactor = 1
    , mst_musicGainPercent = initialGainPercent
    }

-- | Silence music at startup.
initialTrackGains :: TrackGains
initialTrackGains = TrackGains 0 0 0 0

--------------------------------------------------------------------------------
-- Initialization

withMusic :: Int -> (MusicHandle -> IO a) -> IO a
withMusic initialGainPercent action = do
  let initialStateTarget = initialMusicStateTarget initialGainPercent
  musicSources <- initMusicSources initialStateTarget
  targetGainsSignal <- atomically $ newTSignal initialStateTarget
  targetGainsReceiver <-
    atomically $ snd <$> newTSignalReceiver targetGainsSignal
  let initialState = MusicState initialTrackGains initialStateTarget
  withAsync
    (musicThreadActive musicSources targetGainsReceiver initialState)
    (\_ -> action (MusicHandle targetGainsSignal))

initMusicSources :: MusicStateTarget -> IO MusicSources
initMusicSources initialStateTarget = do
  ms_bassFastSource <- makeLoopingSourceFromOpus Music.bassFastOpus
  ms_bassSlowSource <- makeLoopingSourceFromOpus Music.bassSlowOpus
  ms_drumsFastSource <- makeLoopingSourceFromOpus Music.drumsFastOpus
  ms_synthSlowFastSource <- makeLoopingSourceFromOpus Music.synthSlowFastOpus
  ms_vocorderFlagSource <- makeLoopingSourceFromOpus Music.vocorderFlagOpus
  let musicSources = MusicSources{..}
  -- Start silent
  setSourceGains
    musicSources
    (mst_muted initialStateTarget)
    (mst_trackGains initialStateTarget)
    (mst_musicGainPercent initialStateTarget)
  -- Start sources simultaneously
  withArrayLen
    [ ms_bassFastSource
    , ms_bassSlowSource
    , ms_drumsFastSource
    , ms_synthSlowFastSource
    , ms_vocorderFlagSource
    ]
    (AL.alSourcePlayv . fromIntegral)
  pure musicSources

makeLoopingSourceFromOpus :: BS.ByteString -> IO AL.Source
makeLoopingSourceFromOpus opusFile = do
  source <- makeSource
  AL.Buffer bufIdx <- sourceOpusFile opusFile
  AL.alSourcei source AL.BUFFER (fromIntegral bufIdx)
  AL.alSourcei source AL.LOOPING 1
  pure source

--------------------------------------------------------------------------------

-- * Control

-- | Set the gains for each component of the music.
setTrackGains :: MusicHandle -> TrackGains -> Maybe CFloat -> IO ()
setTrackGains musicHandle targetTrackGains speedMaybe =
  atomically $
    modifyTSignal (mh_command musicHandle) $ \stateTarget0 ->
      MusicStateTarget
        { mst_trackGains = clampTrackGains targetTrackGains
        , mst_muted = mst_muted stateTarget0
        , mst_transitionSpeedFactor = fromMaybe 1 speedMaybe
        , mst_musicGainPercent = mst_musicGainPercent stateTarget0
        }

getMusicGainPercent :: MusicHandle -> IO Int
getMusicGainPercent musicHandle =
  atomically $ mst_musicGainPercent <$> readTSignal (mh_command musicHandle)

setMusicGainPercent :: MusicHandle -> Int -> IO ()
setMusicGainPercent musicHandle musicGainPercent =
  atomically $
    modifyTSignal (mh_command musicHandle) $ \stateTarget0 ->
      stateTarget0{mst_musicGainPercent = clampPercent musicGainPercent}

clampTrackGains :: TrackGains -> TrackGains
clampTrackGains trackGains =
  TrackGains
    { tg_slow = clamp $ tg_slow trackGains
    , tg_fast = clamp $ tg_fast trackGains
    , tg_slowFast = clamp $ tg_slowFast trackGains
    , tg_flag = clamp $ tg_flag trackGains
    }

clamp :: (Num a, Ord a) => a -> a
clamp = max 0 . min 1

clampPercent :: Int -> Int
clampPercent = max 0 . min 100

-- | Set the gains for each component of the music.
toggleMusicMute :: MusicHandle -> IO ()
toggleMusicMute musicHandle =
  atomically $
    modifyTSignal (mh_command musicHandle) $
      over (field @"mst_muted") toggleMute

getMusicMute :: MusicHandle -> IO Muted
getMusicMute musicHandle =
  mst_muted <$> atomically (readTSignal (mh_command musicHandle))

--------------------------------------------------------------------------------
-- Thread for cross-fading
--
-- This thread should have exclusive access to the `IORef MusicState`, so
-- we use the non-atomic `IORef` operations.

-- | Wait until a new track gains target arrives, then transition to the
-- 'active' state.
musicThreadWait ::
  MusicSources ->
  TSignalReceiver MusicStateTarget ->
  MusicState ->
  IO a
musicThreadWait sources targetGainsReceiver musicState = do
  musicStateTarget <- atomically $ readTSignalReceiver targetGainsReceiver
  musicThreadActive
    sources
    targetGainsReceiver
    musicState{ms_target = musicStateTarget}

-- | Gradually adjust the track gains towards a target and sleep when the
-- target is reached.
musicThreadActive ::
  MusicSources ->
  TSignalReceiver MusicStateTarget ->
  MusicState ->
  IO a
musicThreadActive sources targetGainsReceiver musicState0 = do
  -- Step the source gains towards the target gains.
  let musicState1 = stepMusicState C.tickDelay_sec musicState0
  setSourceGains
    sources
    (mst_muted (ms_target musicState1))
    (ms_currenTrackGains musicState1)
    (mst_musicGainPercent (ms_target musicState1))

  if ms_currenTrackGains musicState1 == mst_trackGains (ms_target musicState1)
    then do
      -- If the source gains have reached their targets, move to the 'wait'
      -- state.
      musicThreadWait sources targetGainsReceiver musicState1
    else do
      -- Otherwise, continue.
      threadDelay (C.tickDelay_ns `div` 1000)
      musicState2 <-
        atomically (tryReadTSignalReceiver targetGainsReceiver) <&> \case
          Nothing -> musicState1
          Just musicStateTarget ->
            musicState1{ms_target = musicStateTarget}
      musicThreadActive sources targetGainsReceiver musicState2

stepMusicState :: CFloat -> MusicState -> MusicState
stepMusicState dt_sec state0 =
  MusicState
    { ms_currenTrackGains =
        TrackGains
          { tg_slow =
              stepScalar
                gainStep
                (tg_slow targetGains)
                (tg_slow currentGains)
          , tg_fast =
              stepScalar
                gainStep
                (tg_fast targetGains)
                (tg_fast currentGains)
          , tg_slowFast =
              stepScalar
                gainStep
                (tg_slowFast targetGains)
                (tg_slowFast currentGains)
          , tg_flag =
              stepScalar
                gainStep
                (tg_flag targetGains)
                (tg_flag currentGains)
          }
    , ms_target = ms_target state0
    }
 where
  targetGains = mst_trackGains $ ms_target state0
  currentGains = ms_currenTrackGains state0
  gainChangePerSecond = 0.5
  gainStep =
    dt_sec * gainChangePerSecond * mst_transitionSpeedFactor (ms_target state0)

stepScalar :: CFloat -> CFloat -> CFloat -> CFloat
stepScalar stepSize target current
  | target >= current = min target (current + stepSize)
  | otherwise = max target (current - stepSize / decayFactor)
 where
  -- Make values decay more slowly than they increase
  decayFactor = 3

--------------------------------------------------------------------------------
-- Source updates

-- | Set the gains of the music sources.
setSourceGains :: MusicSources -> Muted -> TrackGains -> Int -> IO ()
setSourceGains MusicSources{..} muted trackGains musicGainPercent = do
  let TrackGains{tg_slow, tg_fast, tg_slowFast, tg_flag} = trackGains
      muteFactor = case muted of Muted -> 0; NotMuted -> 1
      musicGain = fromIntegral musicGainPercent / 100
      gain = 0.4 * muteFactor * musicGain

  -- Mixing constants are here
  Foreign.with (0.70 * gain * tg_slow) $ AL.alSourcefv ms_bassSlowSource AL.GAIN
  Foreign.with (0.70 * gain * tg_fast) $ AL.alSourcefv ms_bassFastSource AL.GAIN
  Foreign.with (0.30 * gain * tg_fast) $ AL.alSourcefv ms_drumsFastSource AL.GAIN
  Foreign.with (0.80 * gain * tg_slowFast) $ AL.alSourcefv ms_synthSlowFastSource AL.GAIN
  Foreign.with (0.60 * gain * tg_flag) $ AL.alSourcefv ms_vocorderFlagSource AL.GAIN
