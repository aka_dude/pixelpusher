module Client.Constants where

import Data.Word (Word16)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Constants qualified as C

viewRadius :: Float
viewRadius = Fixed.toFloat C.viewRadius

viewDiameter :: Float
viewDiameter = 2 * viewRadius

pingLifetimeTicks :: Int
pingLifetimeTicks = fromIntegral C.tickRate_hz * 5

maxVisiblePings :: Word16
maxVisiblePings = 2 * fromIntegral C.maxPlayersPerGame

maxVisibleSparks :: Word16
maxVisibleSparks = 1024

deathFadeSeconds :: Float
deathFadeSeconds = 2
