module Client.Util.CancellableTimeout (
  cancellableTimeout,
) where

import Control.Concurrent
import Control.Exception
import Data.IORef
import Data.Unique

import Pixelpusher.Custom.Clock (threadDelayNSec)

--------------------------------------------------------------------------------

newtype Timeout = Timeout Unique

instance Show Timeout where
  show _ = "Timeout"

instance Exception Timeout

data TimeoutStatus
  = TimeoutActive
  | TimeoutCancelled

-- | This function is supposed to be like 'System.Timeout.timeout', but
-- provides the IO computation a way to remove the time limit.
cancellableTimeout :: Int -> (IO () -> IO a) -> IO (Maybe a)
cancellableTimeout t_ns action
  | t_ns < 0 = Just <$> action (pure ())
  | t_ns == 0 = pure Nothing
  | otherwise = do
      timeoutFlag <- newIORef TimeoutActive
      let cancelTimeout = atomicWriteIORef timeoutFlag TimeoutCancelled

      mainTID <- myThreadId
      u <- newUnique
      timeoutTID <- forkIO $ do
        threadDelayNSec t_ns
        readIORef timeoutFlag >>= \case
          TimeoutActive -> throwTo mainTID (Timeout u)
          TimeoutCancelled -> pure ()

      catchJust
        (\(Timeout u') -> if u == u' then Just () else Nothing)
        (fmap Just $ action cancelTimeout `finally` throwTo timeoutTID ThreadKilled)
        (const (pure Nothing))
