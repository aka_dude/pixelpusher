{-# LANGUAGE MultiWayIf #-}

module Client.GlobalKeyCallback (
  globalKeyCallback,
) where

import Graphics.UI.GLFW qualified as GLFW

import Client.Audio.AL.Env
import Client.Graphics.GLFW.KeyCallback

globalKeyCallback :: ALEnv -> KeyCallback
globalKeyCallback _alEnv =
  KeyCallback $ \_window _key _scanCode keySt _mods ->
    let _firstPress = keySt == GLFW.KeyState'Pressed
    in  if
            | otherwise ->
                Nothing
