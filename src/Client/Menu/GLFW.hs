{-# LANGUAGE MultiWayIf #-}

module Client.Menu.GLFW (
  charCallback,
  keyCallback,
) where

import Data.Text qualified as Text
import Graphics.UI.GLFW hiding (KeyCallback)
import Graphics.UI.GLFW qualified as GLFW

import Client.Graphics.GLFW.KeyCallback
import Client.Menu.Input (MenuInputEvent (..), MenuInputQueue)
import Client.Menu.Input qualified as Menu

charCallback :: MenuInputQueue -> GLFW.CharCallback
charCallback inputQueue _window char =
  Menu.pushQueue inputQueue (MenuCharInput char)

keyCallback :: MenuInputQueue -> KeyCallback
keyCallback inputQueue =
  KeyCallback $ \window key _scanCode keySt mods ->
    let shift = GLFW.modifierKeysShift mods
        ctrl = GLFW.modifierKeysControl mods
        firstPress = keySt == GLFW.KeyState'Pressed
    in  if
            | not (keyIsPressed keySt) ->
                Nothing
            -- Enter
            | key == Key'Enter && firstPress ->
                Just $ Menu.pushQueue inputQueue MenuEnter
            | key == Key'Space && firstPress ->
                Just $ Menu.pushQueue inputQueue MenuEnter
            -- Escape
            | key == Key'Escape && firstPress ->
                Just $ Menu.pushQueue inputQueue MenuEscape
            -- Up/Down
            | key == Key'Up ->
                Just $ Menu.pushQueue inputQueue MenuUp
            | key == Key'Down ->
                Just $ Menu.pushQueue inputQueue MenuDown
            | key == Key'Tab ->
                if shift
                  then Just $ Menu.pushQueue inputQueue MenuUp
                  else Just $ Menu.pushQueue inputQueue MenuDown
            -- Left/Right
            | key == Key'Left ->
                Just $ Menu.pushQueue inputQueue MenuLeft
            | key == Key'Right ->
                Just $ Menu.pushQueue inputQueue MenuRight
            -- Text
            | key == Key'Backspace ->
                Just $ Menu.pushQueue inputQueue MenuCharDelete
            | key == Key'V && ctrl ->
                Just $ do
                  GLFW.getClipboardString window >>= \case
                    Nothing -> pure ()
                    Just pastedText ->
                      Menu.pushQueue inputQueue $
                        MenuPasteText (Text.pack pastedText)
            | otherwise ->
                Nothing

-- Helper
keyIsPressed :: GLFW.KeyState -> Bool
keyIsPressed = (/=) GLFW.KeyState'Released -- presssed or repeating
