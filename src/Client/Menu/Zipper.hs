{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}

module Client.Menu.Zipper (
  Zipper (..),
  fromNonEmpty,
  toNonEmpty,
  scrollDown,
  scrollUp,
) where

import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty

data Zipper a = Zipper
  { z_above :: [a]
  , z_focused :: a
  , z_below :: [a]
  }

-- | Focuses the first element
fromNonEmpty :: NonEmpty a -> Zipper a
fromNonEmpty (a :| as) =
  Zipper
    { z_above = []
    , z_focused = a
    , z_below = as
    }

-- | Focuses the last element
fromNonEmptyLast :: NonEmpty a -> Zipper a
fromNonEmptyLast (NonEmpty.reverse -> (a :| as)) =
  Zipper
    { z_above = as
    , z_focused = a
    , z_below = []
    }

toNonEmpty :: Zipper a -> NonEmpty a
toNonEmpty Zipper{..} =
  NonEmpty.prependList (reverse z_above) $
    z_focused :| z_below

-- | Wraps around
scrollDown :: Zipper a -> Zipper a
scrollDown zipper@Zipper{..} =
  case z_below of
    [] -> focusFirst zipper
    (a : as) ->
      Zipper
        { z_above = z_focused : z_above
        , z_focused = a
        , z_below = as
        }

focusFirst :: Zipper a -> Zipper a
focusFirst = fromNonEmpty . toNonEmpty

-- | Wraps around
scrollUp :: Zipper a -> Zipper a
scrollUp zipper@Zipper{..} =
  case z_above of
    [] -> focusLast zipper
    (a : as) ->
      Zipper
        { z_above = as
        , z_focused = a
        , z_below = z_focused : z_below
        }

focusLast :: Zipper a -> Zipper a
focusLast = fromNonEmptyLast . toNonEmpty
