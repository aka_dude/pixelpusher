{-# LANGUAGE PatternSynonyms #-}

module Client.Graphics.CoordinateSystems (
  CoordSystem,
  cs_axisScaling,
  cs_origin,
  AxisOrientation (PreserveAxis, FlipAxis),
  makeCoordSystem,
  intoPos,
  fromPos,
  intoVec,
  fromVec,
  intoScalar,
  fromScalar,
  gameView,
  gameWorld,
  viewport,
  centeredViewport,
) where

import Data.Coerce (coerce)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.Parameters

import Client.Constants qualified as CC

--------------------------------------------------------------------------------

-- | 2D coordinate systems defined by scaling of each axis and an origin. The
-- axis scaling factors must have the same magnitude but may have different
-- signs. This is enforced by the smart constructor `makeCoordSystem`.
data CoordSystem = CoordSystem
  { _cs_scalarScaling :: {-# UNPACK #-} !Float
  , _cs_axisScaling :: {-# UNPACK #-} !Float2 -- magnitudes of components are equal to '_cs_scalarScaling'
  , _cs_origin :: {-# UNPACK #-} !Float2
  }

cs_axisScaling :: CoordSystem -> Float2
cs_axisScaling = _cs_axisScaling

cs_origin :: CoordSystem -> Float2
cs_origin = _cs_origin

newtype AxisOrientation = AxisOrientation Float

pattern PreserveAxis :: AxisOrientation
pattern PreserveAxis = AxisOrientation 1

pattern FlipAxis :: AxisOrientation
pattern FlipAxis = AxisOrientation (-1)

{-# COMPLETE PreserveAxis, FlipAxis :: AxisOrientation #-}

makeCoordSystem :: Float -> V2 AxisOrientation -> Float2 -> CoordSystem
makeCoordSystem scale (V2 orientation_x orientation_y) origin =
  CoordSystem
    { _cs_scalarScaling = scale
    , _cs_axisScaling =
        Float2
          (scale * coerce orientation_x)
          (scale * coerce orientation_y)
    , _cs_origin = origin
    }

intoPos :: CoordSystem -> Float2 -> Float2
intoPos cs v = v * _cs_axisScaling cs + _cs_origin cs

fromPos :: CoordSystem -> Float2 -> Float2
fromPos cs v = (v - _cs_origin cs) / _cs_axisScaling cs

intoVec :: CoordSystem -> Float2 -> Float2
intoVec cs = (* _cs_axisScaling cs)

fromVec :: CoordSystem -> Float2 -> Float2
fromVec cs = (/ _cs_axisScaling cs)

intoScalar :: CoordSystem -> Float -> Float
intoScalar cs = (* _cs_scalarScaling cs)

fromScalar :: CoordSystem -> Float -> Float
fromScalar cs = (/ _cs_scalarScaling cs)

--------------------------------------------------------------------------------

gameView :: Float2 -> CoordSystem
gameView = makeCoordSystem CC.viewRadius (V2 PreserveAxis PreserveAxis)

gameWorld :: GameParams -> CoordSystem
gameWorld params =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 0 0)
 where
  radius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics params

viewport :: Int -> CoordSystem
viewport windowSquareLength =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 radius radius)
 where
  radius = 0.5 * fromIntegral windowSquareLength

centeredViewport :: Int -> CoordSystem
centeredViewport windowSquareLength =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 0 0)
 where
  radius = 0.5 * fromIntegral windowSquareLength
