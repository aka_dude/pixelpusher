{-# LANGUAGE PatternSynonyms #-}

module Client.Graphics.Rendering.TeamView (
  TeamColor (TeamRed, TeamBlue),
  teamDefaultColor,
  viewTeamColor,
  viewPlayerColor,
) where

import Data.Word (Word8)

import Pixelpusher.Custom.Float (Float3 (Float3))
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.Team

--------------------------------------------------------------------------------
-- Team-dependent view of team entities.
--
-- The allied team is blue. The opposing team is red.

newtype TeamColor = TeamColor Word8

{-# COMPLETE TeamRed, TeamBlue :: TeamColor #-}
pattern TeamRed :: TeamColor
pattern TeamRed <- TeamColor 0
pattern TeamBlue :: TeamColor
pattern TeamBlue <- TeamColor 1

teamRed :: TeamColor
teamRed = TeamColor 0

teamBlue :: TeamColor
teamBlue = TeamColor 1

viewTeamColor :: Maybe Team -> Team -> TeamColor
viewTeamColor mViewTeam team =
  case mViewTeam of
    Nothing ->
      case team of
        TeamNW -> teamRed
        TeamSE -> teamBlue
    Just viewTeam -> if team == viewTeam then teamBlue else teamRed

teamDefaultColor :: TeamColor -> Float3
teamDefaultColor = \case
  TeamRed -> red
  TeamBlue -> cyan

--------------------------------------------------------------------------------
-- Player-dependent view of player entities.
--
-- Your entities are teal. Allied entities are blue-ish. Opposing entities are
-- red-ish.

viewPlayerColor :: Maybe ActorID -> Maybe Team -> PlayerColor -> Float3
viewPlayerColor mViewPid = case mViewPid of
  Nothing -> viewPlayerColor'
  Just viewPid -> \mViewTeam color@(PlayerColor _ _ pid) ->
    if viewPid == pid
      then primaryPlayerColour
      else viewPlayerColor' mViewTeam color

-- Helper
viewPlayerColor' :: Maybe Team -> PlayerColor -> Float3
viewPlayerColor' mViewTeam (PlayerColor team color _) =
  case mViewTeam of
    Nothing ->
      case team of
        TeamNW -> redTeamColors color
        TeamSE -> blueTeamColors color
    Just viewTeam ->
      if viewTeam == team then blueTeamColors color else redTeamColors color

primaryPlayerColour :: Float3
primaryPlayerColour = teal

redTeamColors :: ColorVariant -> Float3
redTeamColors = \case
  Color1 -> red
  Color2 -> orange
  Color3 -> pink

blueTeamColors :: ColorVariant -> Float3
blueTeamColors = \case
  Color1 -> cyan
  Color2 -> blue
  Color3 -> indigo

--------------------------------------------------------------------------------
-- Colors from https://material.io/resources/color/

fromHex :: Word8 -> Word8 -> Word8 -> Float3
fromHex r g b = Float3 (f r) (f g) (f b)
 where
  f = (/ 255) . fromIntegral

orange :: Float3
orange = fromHex 0xd3 0x41 0x06 -- Orange 900, average of DeepOrange and Orange

red :: Float3
red = fromHex 0xc6 0x28 0x20 -- Red 800, 0x08 less blue

pink :: Float3
pink = fromHex 0xc2 0x18 0x40 -- Pink 700, 0x1b less blue

indigo :: Float3
indigo = fromHex 0x4d 0x5e 0xba -- Average of Indigo 400 and Indigo 500

blue :: Float3
blue = fromHex 0x0d 0x76 0xc7 -- Average of Light Blue 800 and Blue 700

cyan :: Float3
cyan = fromHex 0x00 0x80 0x9f -- Cyan 800, 0x03 less green, 0x10 more blue

teal :: Float3
teal = fromHex 0x00 0x79 0x6b -- Teal 700
