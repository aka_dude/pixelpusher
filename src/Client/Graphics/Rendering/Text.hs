-- | Utility functions for rendering text.
module Client.Graphics.Rendering.Text (
  stackedLines_bottomJustify,
  stackedLines_topJustify,
  stackedLines_topJustify',
  shadowText,
  shadowTextWith,
  leftPad,
) where

import Data.Foldable (toList)
import Data.List (mapAccumL)
import Data.List.NonEmpty (NonEmpty)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Graphics.Rendering.Entity.Char

--------------------------------------------------------------------------------

-- TODO: clean up these functions

stackedLines_bottomJustify ::
  CharAlignment ->
  Float2 ->
  Float ->
  [(Float4, String)] ->
  [CharLine]
stackedLines_bottomJustify
  horizontalAlignment
  bottomCorner
  lineSpacing
  textLines =
    flip map (zip [0 ..] (reverse textLines)) $ \(lineNo, (color, text)) ->
      let screenPos = bottomCorner & _2 +~ (fromInteger lineNo * lineSpacing)
      in  (horizontalAlignment, screenPos, [(color, text)])

stackedLines_topJustify ::
  Traversable t =>
  CharAlignment ->
  Float2 ->
  Float ->
  t (Float4, String) ->
  (Float2, t CharLine)
stackedLines_topJustify horizontalAlignment topCorner lineSpacing =
  mapAccumL f topCorner
 where
  f :: Float2 -> (Float4, String) -> (Float2, CharLine)
  f topLeft (color, text) =
    ( topLeft & _2 -~ lineSpacing
    , (horizontalAlignment, topLeft, [(color, text)])
    )
{-# INLINEABLE stackedLines_topJustify #-}

-- TODO: rename
stackedLines_topJustify' ::
  Traversable t =>
  CharAlignment ->
  Float2 ->
  Float ->
  t (NonEmpty (a, String)) ->
  (Float2, t (CharLine' a))
stackedLines_topJustify' horizontalAlignment topCorner lineSpacing =
  mapAccumL f topCorner
 where
  f :: Float2 -> NonEmpty (a, String) -> (Float2, CharLine' a)
  f topLeft textFragments =
    ( topLeft & _2 -~ lineSpacing
    , (horizontalAlignment, topLeft, toList textFragments)
    )
{-# INLINEABLE stackedLines_topJustify' #-}

shadowText :: Float -> Float4 -> [CharLine] -> [CharLine]
shadowText fontSize shadowColor textLines = shadowedTextLines ++ textLines
 where
  offset = fontSize / 12
  offsetVec = Float2 offset (-offset)
  shadowedTextLines = flip map textLines $
    \(horizontalAlignment, pos, colorTextPairs) ->
      ( horizontalAlignment
      , pos + offsetVec
      , colorTextPairs & traverse . _1 .~ shadowColor
      )

-- | Note: You have to manually append the returned lines before the text being
-- shadowed so that the original text is drawn over the shadow
shadowTextWith ::
  Float ->
  (a -> Float4) ->
  CharLine' a ->
  CharLine
shadowTextWith fontSize getShadowColor textLine =
  let offset = fontSize / 12
      offsetVec = Float2 offset (-offset)
      (horizontalAlignment, pos, colorTextPairs) = textLine
  in  ( horizontalAlignment
      , pos + offsetVec
      , colorTextPairs & traverse . _1 %~ getShadowColor
      )

leftPad :: Int -> a -> [a] -> [a]
leftPad n x xs
  | n > l = replicate (n - l) x ++ xs
  | otherwise = xs
 where
  l = length xs
