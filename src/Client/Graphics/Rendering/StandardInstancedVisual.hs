{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Common pattern for instanced draw calls.
module Client.Graphics.Rendering.StandardInstancedVisual (
  StandardInstancedVisual,
  StandardUniforms (..),
  Vars_StandardUniforms (..),
  StandardVParams (..),
  loadStandardVisual,
  drawStandardVisual,
) where

import Data.Proxy
import Foreign
import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.Float qualified as Float

import Client.Graphics.CoordinateSystems (CoordSystem, cs_axisScaling, cs_origin)
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL (deriveGLSLVars)
import Client.Graphics.GL.Types (GLRecord)
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped

--------------------------------------------------------------------------------

data StandardInstancedVisual a = StandardInstancedVisual
  { sv_vao :: VertexArray
  , sv_shaderProg :: ShaderProgram
  , sv_nVertices :: GLsizei
  , sv_instanceBuffer :: Buffer
  , sv_instanceBufferCapacityCount :: Word16
  , sv_uniformLocations :: UniformLocations StandardUniforms
  }

data StandardUniforms = StandardUniforms
  { su_viewScale :: Float2
  , su_viewCenter :: Float2
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''StandardUniforms

data StandardVParams a = StandardVParams
  { svp_coordSystem :: CoordSystem
  , svp_instances :: [a]
  }

loadStandardVisual ::
  forall instanceType vertexType.
  (GLAttribute instanceType, GLAttribute vertexType) =>
  [vertexType] ->
  String ->
  String ->
  Word16 ->
  GL (StandardInstancedVisual instanceType)
loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances = do
  (vao, instanceBuffer) <-
    setupVertexArrayObjectInstanced
      maxInstances
      vertices
      (Proxy :: Proxy instanceType)
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniforms <- getUniformLocations shaderProg
  pure $
    StandardInstancedVisual
      { sv_vao = vao
      , sv_shaderProg = shaderProg
      , sv_nVertices = fromIntegral $ length vertices
      , sv_instanceBuffer = instanceBuffer
      , sv_instanceBufferCapacityCount = maxInstances
      , sv_uniformLocations = uniforms
      }

drawStandardVisual ::
  forall instanceType.
  (GLAttribute instanceType) =>
  StandardInstancedVisual instanceType ->
  StandardVParams instanceType ->
  GL ()
drawStandardVisual visual params =
  withVertexArray (sv_vao visual) $
    withShaderProgram (sv_shaderProg visual) $ do
      -- Set uniforms
      writeUniforms (sv_uniformLocations visual) $
        let coordSystem = svp_coordSystem params
        in  StandardUniforms
              { su_viewScale = Float.map2 recip $ cs_axisScaling coordSystem
              , su_viewCenter = cs_origin coordSystem
              }

      -- Set instance data
      let capacityCount = sv_instanceBufferCapacityCount visual
          instances = take (fromIntegral capacityCount) $ svp_instances params
          nInstances = length instances
          bytes =
            fromIntegral $
              nInstances * sum (fieldSizes @instanceType) * sizeOfGLfloat
      instanceData <- toFloatsInstanced instances
      withBuffer GL_ARRAY_BUFFER (sv_instanceBuffer visual) $
        glBufferSubData
          GL_ARRAY_BUFFER
          0
          bytes
          instanceData

      -- Draw
      glDrawArraysInstanced
        GL_TRIANGLE_STRIP
        0
        (sv_nVertices visual)
        (fromIntegral nInstances)
{-# INLINEABLE drawStandardVisual #-}
