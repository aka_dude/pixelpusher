module Client.Graphics.Rendering.SharedVisuals (
  damageFilterWeight,
  invulnerabilityFilter,
  opacityAdjustment,
  flagColors,
) where

import Pixelpusher.Custom.Float (Float3 (Float3))

import Client.Graphics.Rendering.TeamView (TeamColor (..))

--------------------------------------------------------------------------------

damageFilterWeight :: Float -> Float
damageFilterWeight recentDamage =
  sqrt $ min 1 (recentDamage / hardHitDamage)
 where
  hardHitDamage = 8

invulnerabilityFilter :: Float -> Float
invulnerabilityFilter x =
  let weight = 0.4
  in  weight + (1 - weight) * x

-- | Partially compensate for human perception in an unprincipled way.
opacityAdjustment :: Floating a => a -> a
opacityAdjustment x = (2 / 3) * x + (1 / 3) * x * x

flagColors :: TeamColor -> Float3
flagColors = \case
  TeamRed -> Float3 0.7 0.35 0.35
  TeamBlue -> Float3 0.35 0.35 0.7
