module Client.Graphics.Rendering.SceneDecorations (
  SceneDecorations (..),
  ScenePerspective (..),
  PlayerInfo (..),
  getPlayerPerspective,
  StatsOverlay (..),
  DisplayHelp (..),
  ShowOSCursor (..),
) where

import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time

import Client.Menu
import Client.Scene.Options

-- | The part of the rendering specification that captures information from
-- outside of the game state.
data SceneDecorations = SceneDecorations
  { sceneDeco_perspective :: ScenePerspective
  , sceneDeco_renderStatsOverlay :: Maybe StatsOverlay
  , sceneDeco_renderHelpOverlay :: DisplayHelp
  , sceneDeco_roomID :: Maybe Int
  , sceneDeco_latency :: Maybe Int
  , sceneDeco_elaspedClientTime :: Ticks
  , sceneDeco_showTutorial :: Bool
  , sceneDeco_showMenu :: Maybe (MenuState OptionsWidgets PlayerOptions)
  , sceneDeco_showOSCursor :: ShowOSCursor
  , sceneDeco_isFramePredicted :: Bool
  , sceneDeco_enableScreenShake :: Bool
  , sceneDeco_enableVisualStatic :: Bool
  }

data ScenePerspective
  = PlayerPerspective PlayerInfo
  | FreePerspective

data PlayerInfo = PlayerInfo
  { pi_actorID :: ActorID
  , pi_player :: Player
  , pi_playerView :: PlayerView
  }

getPlayerPerspective :: ScenePerspective -> Maybe PlayerInfo
getPlayerPerspective = \case
  PlayerPerspective p -> Just p
  FreePerspective{} -> Nothing

data StatsOverlay = RoundStatsOverlay | CumulativeStatsOverlay

data DisplayHelp = ShowHelp | HideHelp

data ShowOSCursor = ShowOSCursor | DoNotShowOSCursor
