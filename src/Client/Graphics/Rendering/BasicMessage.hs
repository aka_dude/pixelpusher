module Client.Graphics.Rendering.BasicMessage (
  renderBasicMessage,
) where

import Graphics.GL.Core33
import Graphics.UI.GLFW qualified as GLFW

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Graphics.GL.Env
import Client.Graphics.GL.Wrapped (runGL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas (fa_fontSize)
import Client.Graphics.Rendering.Text

-- | Display text on the screen.
renderBasicMessage :: GLEnv -> Float4 -> String -> IO ()
renderBasicMessage glEnv color message = do
  windowLength <-
    fmap (uncurry min) $
      GLFW.getFramebufferSize $
        gle_window glEnv

  glClearColor 0.0 0.0 0.0 1.0
  glClear GL_COLOR_BUFFER_BIT

  let screenWidth = fromIntegral windowLength
      x = screenWidth / 2
      y = screenWidth * 3 / 4
      fontAtlas = gle_fontAtlasLarge glEnv
      spacing = fromIntegral $ fa_fontSize fontAtlas
      msgLines = lines message

  runGL $
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = fontAtlas
        , cvp_screenWidth = fromIntegral windowLength
        , cvp_lines =
            snd $
              stackedLines_topJustify CharAlignCenter (Float2 x y) spacing $
                map (color,) msgLines
        }

  GLFW.swapBuffers $ gle_window glEnv
  GLFW.pollEvents
