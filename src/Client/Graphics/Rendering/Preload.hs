module Client.Graphics.Rendering.Preload (
  preloadVisuals,
) where

import Control.Monad.IO.Class (liftIO)
import Data.Foldable (for_)
import Data.Maybe (fromJust)
import Graphics.UI.GLFW qualified as GLFW

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env (GLEnv (GLEnv))
import Client.Graphics.GL.Wrapped (runGL)
import Client.Graphics.Rendering.Entity.Background
import Client.Graphics.Rendering.Entity.CastRipple
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.Entity.Circle
import Client.Graphics.Rendering.Entity.ControlPoint
import Client.Graphics.Rendering.Entity.CooldownBar
import Client.Graphics.Rendering.Entity.Cross
import Client.Graphics.Rendering.Entity.CursorAttract
import Client.Graphics.Rendering.Entity.CursorRepel
import Client.Graphics.Rendering.Entity.Dash
import Client.Graphics.Rendering.Entity.Drone
import Client.Graphics.Rendering.Entity.DroneControlRadius
import Client.Graphics.Rendering.Entity.Explosion
import Client.Graphics.Rendering.Entity.Flag
import Client.Graphics.Rendering.Entity.Fog
import Client.Graphics.Rendering.Entity.Overseer
import Client.Graphics.Rendering.Entity.OverseerRemains
import Client.Graphics.Rendering.Entity.PingFlash
import Client.Graphics.Rendering.Entity.PsiStorm
import Client.Graphics.Rendering.Entity.Quad
import Client.Graphics.Rendering.Entity.Ring
import Client.Graphics.Rendering.Entity.SoftObstacle
import Client.Graphics.Rendering.Entity.Spark
import Client.Graphics.Rendering.Entity.TeamBase
import Client.Graphics.Rendering.Entity.WorldBounds
import Client.Graphics.Rendering.StandardInstancedVisual

-- | Draw all visuals to force them to be loaded onto the GPU. This prevents
-- lazy loading of visuals from causing in-game interruptions.
preloadVisuals :: GLEnv -> IO ()
preloadVisuals
  -- We pattern match on the `GLEnv` constructor so that we don't forget to
  -- use a visual.
  ( GLEnv
      gle_window
      _gle_refreshRate
      gle_overseer
      gle_drone
      gle_softObstacle
      gle_explosion
      gle_controlPoint
      gle_circle
      gle_quad
      gle_droneControlRadius
      gle_overseerRemains
      gle_char
      gle_fontAtlasSmall
      gle_fontAtlasRegular
      gle_fontAtlasLarge
      _gle_uiPadding
      gle_pingFlash
      gle_ring
      gle_spark
      gle_background
      gle_castRipple
      gle_cross
      gle_dash
      gle_cooldownBar
      gle_worldBounds
      gle_psiStorm
      gle_flag
      gle_teamBase
      gle_fog
      gle_cursorAttract
      gle_cursorRepel
      _gle_framebufferSizeUpdate
      _gle_keyCallback
      _gle_charCallback
    ) = do
    -- Parameters
    let cameraPos = Float2 0 0

    windowSquareLength <-
      uncurry min <$> GLFW.getFramebufferSize gle_window
    let gameParams = makeGameParams 0 defaultBaseGameParams
    let time = addTicks (Ticks 60) initialTime
    let gameViewCS = CS.gameView cameraPos
    let mViewPlayer = Nothing
        mViewTeam = Nothing
    let playerID = fromJust $ fst $ borrowPlayerID $ newPlayerIDPool 2
    let teamColor =
          PlayerColor TeamNW defaultColorVariant (PlayerActor playerID)
    let centeredViewportCS = CS.centeredViewport windowSquareLength

    -- Drawing in the same order as in `GLEnv`

    runGL $ do
      drawOverseer gle_overseer $
        OverseerVParams
          { ovp_coordSystem = gameViewCS
          , ovp_instances =
              pure $
                OverseerInstance
                  { oi_radius = 16
                  , oi_pos = 0
                  , oi_prevPos1 = 0
                  , oi_prevPos2 = 0
                  , oi_prevPos3 = 0
                  , oi_prevPos4 = 0
                  , oi_prevPos5 = 0
                  , oi_prevPos6 = 0
                  , oi_healthFraction = 0.5
                  , oi_color = 1
                  , oi_recentDamage = 0.5
                  , oi_isInvulnerable = False
                  , oi_ridgesScale = 0
                  , oi_angleOffset = 0
                  , oi_psiStormReadySeconds = -1
                  , oi_angleFins = 0
                  , oi_ticksSinceDash = Nothing
                  }
          }

      drawDrone gle_drone $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                fromDroneRendering mViewPlayer mViewTeam time $
                  RenderingComponent
                    { rendering_entityRendering =
                        DroneRendering
                          { dr_color = Right teamColor
                          , dr_healthFraction = 0.5
                          , dr_recentDamage = 0.5
                          , dr_actorID = Nothing
                          , dr_prevPos1 = 0
                          , dr_prevPos2 = 0
                          , dr_prevPos3 = 0
                          , dr_prevPos4 = 0
                          , dr_prevPos5 = 0
                          , dr_prevPos6 = 0
                          , dr_isInvulnerable = False
                          , dr_entityID = 0
                          , dr_team = TeamNW
                          , dr_ticksSinceDash = Nothing
                          }
                    , rendering_pos = 0
                    , rendering_radius = 16
                    }
          }

      drawSoftObstacle gle_softObstacle $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                SoftObstacleInstance
                  { soi_modelScale = 120
                  , soi_modelTrans = 0
                  , soi_color = 1
                  , soi_maxOpacity = 0.12
                  , soi_minOpacity = 0
                  }
          }

      drawExplosion gle_explosion $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                ExplosionInstance
                  { ei_center = 0
                  , ei_offsetVel = 1
                  , ei_color = 1
                  }
          }

      drawControlPoint gle_controlPoint $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                fromControlPointRendering Nothing $
                  RenderingComponent
                    { rendering_entityRendering =
                        ControlPointRendering_Neutral
                    , rendering_pos = 0
                    , rendering_radius = 120
                    }
          }

      drawCircle gle_circle $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CircleInstance
                  { ci_size = 120
                  , ci_center = 0
                  , ci_color = 1
                  }
          }

      -- Not drawing `gle_quad` here, drawing it last instead

      drawDroneControlRadius gle_droneControlRadius $
        DroneControlRadiusVParams
          { dcrvp_modelTrans = 0
          , dcrvp_modelScale = 300
          , dcrvp_viewScale = recip CC.viewRadius
          , dcrvp_viewCenter = cameraPos
          }

      drawOverseerRemains gle_overseerRemains $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                fromOverseerRemainsParticle mViewPlayer mViewTeam time $
                  Particle
                    { particle_endTime = addTicks (Ticks 120) time
                    , particle_startTime = time
                    , particle_type =
                        OverseerRemainsParticle
                          { orp_color = teamColor
                          , orp_pos = 0
                          , orp_scale = 16
                          }
                    }
          }

      let fontAtlases =
            [ gle_fontAtlasLarge
            , gle_fontAtlasRegular
            , gle_fontAtlasSmall
            ]
      for_ fontAtlases $ \fontAtlas ->
        drawChar gle_char $
          CharVParams
            { cvp_fontAtlas = fontAtlas
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                pure $
                  let screenPos =
                        CS.intoPos (CS.viewport windowSquareLength) $
                          CS.fromPos (CS.gameView cameraPos) 0
                  in  (CharAlignCenter, screenPos, [(1, "asdf")])
            }

      drawPingFlash gle_pingFlash $
        StandardVParams
          { svp_coordSystem = centeredViewportCS
          , svp_instances =
              pure $
                PingFlashInstance
                  { pfi_size = 16
                  , pfi_center = 0
                  , pfi_color = 1
                  , pfi_ticksElapsed = 1
                  }
          }

      drawRing gle_ring $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                RingInstance
                  { ri_modelTrans = 0
                  , ri_modelScale = 32
                  , ri_color = 1
                  , ri_thickness = 4
                  , ri_fuzziness = 0.1
                  }
          }

      drawSpark gle_spark $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              fromPsiStormDamageParticles
                mViewPlayer
                mViewTeam
                time
                [ Particle
                    { particle_endTime = addTicks (Ticks 120) time
                    , particle_startTime = time
                    , particle_type =
                        PsiStormDamageParticle
                          { psdp_pos = 0
                          , psdp_vel = 0
                          , psdp_mPlayerColor = Nothing
                          , psdp_damage = 1
                          }
                    }
                ]
          }

      drawBackground gle_background $
        let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
        in  BackgroundVParams
              { bvp_viewScale = recip CC.viewRadius
              , bvp_viewCenter = cameraPos
              , bvp_modelScale = worldRadius * 2
              , bvp_modelTrans = Float2 0 0
              , bvp_worldRadius = worldRadius
              }

      drawWorldBounds gle_worldBounds $
        let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
        in  WorldBoundsVParams
              { wbvp_viewScale = recip CC.viewRadius
              , wbvp_viewCenter = cameraPos
              , wbvp_modelScale = worldRadius * 2
              , wbvp_modelTrans = Float2 0 0
              , wbvp_worldRadius = worldRadius
              }

      drawCastRipple gle_castRipple $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CastRippleInstance
                  { cri_size = 60.0
                  , cri_center = 0
                  , cri_color = 1
                  , cri_ringWidth = 2.0
                  , cri_progress = 0.5
                  }
          }

      drawCross gle_cross $
        StandardVParams
          { svp_coordSystem = centeredViewportCS
          , svp_instances =
              pure $
                CrossInstance
                  { ci_size = 16
                  , ci_center = 0
                  , ci_color = 1
                  }
          }

      drawDash gle_dash $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                DashInstance
                  { di_center = 0
                  , di_initialVelocity = 0
                  , di_scale = 1
                  , di_angle = 0
                  , di_ticksElapsed = 0
                  , di_secondsElapsed = 0
                  , di_opacity = 1
                  }
          }

      drawCooldownBar gle_cooldownBar $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CooldownBarInstance
                  { cbi_center = 0
                  , cbi_widthRadius = 8
                  , cbi_heightRadius = 1
                  , cbi_bloomRadius = 0.5
                  , cbi_color = 1
                  }
          }

      drawPsiStorm gle_psiStorm $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                PsiStormInstance
                  { psi_modelScale = 144
                  , psi_modelTrans = 0
                  , psi_elapsedSeconds = 0
                  , psi_opacityFactor = 1
                  , psi_bloomColor = 1
                  }
          }

      drawFlag gle_flag $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                FlagInstance
                  { fi_size = 8
                  , fi_center = 0
                  , fi_color = 1
                  }
          }

      drawTeamBase gle_teamBase $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                TeamBaseInstance
                  { tbi_size = 8
                  , tbi_center = 0
                  , tbi_color = 1
                  , tbi_fillOpacity = 1
                  }
          }

      drawFog gle_fog $
        FogVParams
          { fvp_viewScale = 1
          , fvp_viewCenter = 0
          , fvp_modelScale = 1
          , fvp_modelTrans = 0
          , fvp_fogRadius = 1
          , fvp_visibleOpacity = 1
          , fvp_ticksMod = 0
          , fvp_lowHealthEffectIntensity = 1
          , fvp_staticIntensity = 1
          }

      drawCursorAttract gle_cursorAttract $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CursorAttractInstance
                  { cai_size = 120
                  , cai_center = 0
                  , cai_color = 1
                  }
          }

      drawCursorRepel gle_cursorRepel $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CursorRepelInstance
                  { cri_size = 120
                  , cri_center = 0
                  , cri_color = 1
                  }
          }

      -- Cover up everything
      drawQuad gle_quad $
        StandardVParams
          { svp_coordSystem = CS.centeredViewport windowSquareLength
          , svp_instances =
              pure $
                QuadInstance
                  { qi_size =
                      let l = fromIntegral windowSquareLength
                      in  Float2 l l
                  , qi_center = Float2 0 0
                  , -- Don't use 100% opacity so that the pixels actually have to
                    -- be computed. Not sure if this is actually necessary.
                    qi_color = Float4 0 0 0 0.99
                  }
          }

    liftIO $ GLFW.swapBuffers gle_window
