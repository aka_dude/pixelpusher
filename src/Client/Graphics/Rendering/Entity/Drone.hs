{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Drone entities
module Client.Graphics.Rendering.Entity.Drone (
  loadDrone,
  drawDrone,
  DroneInstance,
  fromDroneRendering,
  fromDroneDeathParticle,
) where

import Prelude hiding (atan2, length)

import Foreign
import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float3, Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CombatSystem (recentDamageDecayFactorTicks)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.SharedVisuals
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data DroneInstance = DroneInstance
  { di_modelScale :: {-# UNPACK #-} !Float
  , di_modelTrans :: {-# UNPACK #-} !Float2
  , di_coreOpacity :: {-# UNPACK #-} !Float
  , di_color :: {-# UNPACK #-} !Float4
  , di_damageOpacity :: {-# UNPACK #-} !Float
  , di_angleOffset :: {-# UNPACK #-} !Float
  , di_shadowOffsets12 :: {-# UNPACK #-} !Float4
  , di_shadowOffsets34 :: {-# UNPACK #-} !Float4
  , di_shadowOffsets56 :: {-# UNPACK #-} !Float4
  , di_ridgesScale :: {-# UNPACK #-} !Float
  , di_dashChargeFullSeconds :: {-# UNPACK #-} !Float
  -- ^ A value less than 0 means that the dash is on cooldown
  , di_dashEffect :: {-# UNPACK #-} !Float2
  -- ^ Fields: seconds since dash (float), intensity (float)
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''DroneInstance

loadDrone :: GL (StandardInstancedVisual DroneInstance)
loadDrone =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc C.maxNumEntities

drawDrone ::
  StandardInstancedVisual DroneInstance ->
  StandardVParams DroneInstance ->
  GL ()
drawDrone = drawStandardVisual

--------------------------------------------------------------------------------

fromDroneRendering ::
  Maybe (ActorID, Float) ->
  Maybe Team ->
  Time ->
  RenderingComponent DroneRendering ->
  DroneInstance
fromDroneRendering mViewPlayerDashReady mViewTeam time rc =
  DroneInstance
    { di_modelScale = rendering_radius rc
    , di_modelTrans = rendering_pos rc
    , di_coreOpacity = opacityAdjustment (dr_healthFraction dr)
    , di_color =
        let color =
              Float.map3 (invincibilityFilter' . darkeningFilter) $
                either decayColor (viewPlayerColor mViewPlayer mViewTeam) $
                  dr_color dr
        in  Util.fromRGB_A color 1
    , di_damageOpacity = damageFilterWeight (dr_recentDamage dr)
    , di_angleOffset = droneRotation time $ dr_entityID dr
    , di_shadowOffsets12 =
        Util.fromFloat22
          (dr_prevPos1 dr - rendering_pos rc)
          (dr_prevPos2 dr - rendering_pos rc)
    , di_shadowOffsets34 =
        Util.fromFloat22
          (dr_prevPos3 dr - rendering_pos rc)
          (dr_prevPos4 dr - rendering_pos rc)
    , di_shadowOffsets56 =
        Util.fromFloat22
          (dr_prevPos5 dr - rendering_pos rc)
          (dr_prevPos6 dr - rendering_pos rc)
    , di_ridgesScale = droneRidgesScale $ dr_team dr
    , di_dashChargeFullSeconds =
        case mViewPlayerDashReady of
          Nothing -> noDashReadySeconds
          Just (viewActorID, dashReadySeconds) ->
            if Just viewActorID /= dr_actorID dr
              then noDashReadySeconds
              else dashReadySeconds
    , di_dashEffect =
        case dr_ticksSinceDash dr of
          Nothing ->
            noSecondsSinceDash
          Just (ticksSinceDash, intensity) ->
            let secondsSinceDash =
                  fromIntegral ticksSinceDash / fromIntegral C.tickRate_hz
            in  Float2 secondsSinceDash intensity
    }
 where
  mViewPlayer = fmap fst mViewPlayerDashReady

  dr :: DroneRendering
  dr = rendering_entityRendering rc

  decayColor :: Team -> Float3
  decayColor team =
    Float.map3 (\x -> 0.6 * x + 0.15) $
      teamDefaultColor $
        viewTeamColor mViewTeam team

  invincibilityFilter' :: Float -> Float
  invincibilityFilter' =
    if dr_isInvulnerable dr
      then invulnerabilityFilter
      else id

fromDroneDeathParticle :: Time -> Particle DroneDeathParticle -> DroneInstance
fromDroneDeathParticle time particle =
  DroneInstance
    { di_modelScale = Fixed.toFloat ddp_scale
    , di_modelTrans =
        Fixed.toFloats $
          ddp_pos
            + Fixed.map (* (0.5 * fromIntegral ticksSinceDeath)) ddp_vel
    , di_coreOpacity = 0.0
    , di_color = Float4 1 1 1 opacity
    , di_damageOpacity =
        damageFilterWeight $
          Fixed.toFloat ddp_recentDamageAtDeath
            * exp (-fromIntegral ticksSinceDeath * recentDamageDecayFactorTicks)
    , di_angleOffset = droneRotation time ddp_eid
    , di_shadowOffsets12 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets34 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets56 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_ridgesScale = droneRidgesScale ddp_team
    , di_dashChargeFullSeconds = 0
    , di_dashEffect = noSecondsSinceDash
    }
 where
  DroneDeathParticle
    { ddp_eid
    , ddp_pos
    , ddp_vel
    , ddp_scale
    , ddp_team
    , ddp_recentDamageAtDeath
    } =
      particle_type particle
  ticksSinceDeath =
    getTicks $ diffTime time (particle_startTime particle)
  opacity =
    0.7071 ** max 0 (fromIntegral ticksSinceDeath - 1) -- similar to damage flash

-- Slightly darken drone colours to make overseers stand out more.
darkeningFilter :: Floating a => a -> a
darkeningFilter x = let w = 0.1 in (1 - w) * x

-- Have drones rotate slowly over time. Vary drones by their `EntityID`
-- (arbitrary choice).
droneRotation :: Time -> Int -> Float
droneRotation time eid =
  let ticks = getTicks $ time `diffTime` initialTime
      eidPart = (* 0.125) $ fromIntegral $ eid .&. 127
      timePart = if even eid then t else -t
       where
        t = (2 * pi / 512 / 6 *) $ fromIntegral $ ticks .&. 511
  in  eidPart + timePart

droneRidgesScale :: Team -> Float
droneRidgesScale = \case TeamNW -> 1; TeamSE -> 0

noDashReadySeconds :: Float
noDashReadySeconds = -1

noSecondsSinceDash :: Float2
noSecondsSinceDash = Float2 10 0

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ square 1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , invScale :: Expr Float
  , coreOpacity :: Expr Float
  , color :: Expr Float4
  , damageOpacity :: Expr Float
  , angleOffset :: Expr Float
  , shadowOffsets12 :: Expr Float4
  , shadowOffsets34 :: Expr Float4
  , shadowOffsets56 :: Expr Float4
  , ridgesScale :: Expr Float
  , dashReadySeconds :: Expr Float
  , dashEffect :: Expr Float2
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_DroneInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_DroneInstance{..}
  Vars_StandardUniforms{..} =
    let lastShadowOffset = zw_ di_shadowOffsets56 -- needs updating if more shadows are added
        offsetLength = length lastShadowOffset
        lineRadius = 1.2 * di_modelScale
        boxRadiusX = lineRadius + 0.5 * offsetLength
        boxRadiusY = lineRadius
        scaledTranslatedVertex =
          vec2
            ( boxRadiusX * x_ position + 0.5 * offsetLength
            , boxRadiusY * y_ position
            )

        offsetDir =
          ifThenElse'
            (offsetLength `lt` 0.01)
            (vec2 (1, 0))
            (normalize lastShadowOffset)
        rotatedVertex =
          vec2
            ( x_ offsetDir * x_ scaledTranslatedVertex - y_ offsetDir * y_ scaledTranslatedVertex
            , y_ offsetDir * x_ scaledTranslatedVertex + x_ offsetDir * y_ scaledTranslatedVertex
            )

        screenPos = su_viewScale * (rotatedVertex + di_modelTrans - su_viewCenter)
        pipelineVars =
          PipelineVars
            { pos = rotatedVertex ./ di_modelScale
            , invScale = recip di_modelScale
            , coreOpacity = di_coreOpacity
            , color = di_color
            , damageOpacity = di_damageOpacity
            , angleOffset = di_angleOffset
            , shadowOffsets12 = di_shadowOffsets12 ./ di_modelScale
            , shadowOffsets34 = di_shadowOffsets34 ./ di_modelScale
            , shadowOffsets56 = di_shadowOffsets56 ./ di_modelScale
            , ridgesScale = di_ridgesScale
            , dashReadySeconds = di_dashChargeFullSeconds
            , dashEffect = di_dashEffect
            }
    in  pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  -- Body boundaries
  bodyHexRadius <- bind $ hexRadius ridgesScale pos angleOffset
  bodyFuzziness <- bind $ invScale * 0.5
  let bodyMask bound = smoothstepCentered bound bodyFuzziness bodyHexRadius
  bodyOuterMask <- bind $ bodyMask 1.00
  bodyMidMask <- bind $ bodyMask 0.85
  bodyInnerMask <- bind $ bodyMask 0.50

  -- Body regions
  shellMask <- bind $ (1 - bodyOuterMask) * bodyMidMask
  coreMask <- bind $ (1 - bodyMidMask) * bodyInnerMask

  -- Base color
  dashWeight <- do
    secondsSinceDash <- bind $ x_ dashEffect
    dashIntensity <- bind $ y_ dashEffect
    bind $ max' 0 $ min' 1 $ dashIntensity * exp (-secondsSinceDash / 0.1)
  baseColor <-
    bind $
      1
        - ( (1 -. xyz_ color .* (1 + 0.12 * dashWeight))
              .* (1 - 0.2 * dashWeight)
          )

  -- Unpack
  shadowOffset1 <- bind $ xy_ shadowOffsets12
  shadowOffset2 <- bind $ zw_ shadowOffsets12
  shadowOffset3 <- bind $ xy_ shadowOffsets34
  shadowOffset4 <- bind $ zw_ shadowOffsets34
  shadowOffset5 <- bind $ xy_ shadowOffsets56
  shadowOffset6 <- bind $ zw_ shadowOffsets56

  -- Shadows
  let coreMin = 0.5
  shadowBaseOpacity <- bind $ coreMin + (1 - coreMin) * coreOpacity

  shadowFuzziness <- bind $ invScale * 2.0
  let shadowMask posOffset sizeScaling =
        smoothstepCentered sizeScaling shadowFuzziness $
          hexRadius ridgesScale (pos - posOffset) angleOffset
  shadow1Mask <- bind $ shadowMask shadowOffset1 0.98
  shadow2Mask <- bind $ shadowMask shadowOffset2 0.96
  shadow3Mask <- bind $ shadowMask shadowOffset3 0.94
  shadow4Mask <- bind $ shadowMask shadowOffset4 0.92
  shadow5Mask <- bind $ shadowMask shadowOffset5 0.90
  shadow6Mask <- bind $ shadowMask shadowOffset6 0.88

  shadow1Opacity <- bind $ 0.66 * (1 - shadow1Mask) * shadowBaseOpacity
  shadow2Opacity <- bind $ 0.55 * (1 - shadow2Mask) * shadowBaseOpacity
  shadow3Opacity <- bind $ 0.44 * (1 - shadow3Mask) * shadowBaseOpacity
  shadow4Opacity <- bind $ 0.33 * (1 - shadow4Mask) * shadowBaseOpacity
  shadow5Opacity <- bind $ 0.22 * (1 - shadow5Mask) * shadowBaseOpacity
  shadow6Opacity <- bind $ 0.11 * (1 - shadow6Mask) * shadowBaseOpacity
  shadowOpacity <-
    bind $
      max'
        ( max'
            (max' shadow1Opacity shadow2Opacity)
            (max' shadow3Opacity shadow4Opacity)
        )
        (max' shadow5Opacity shadow6Opacity)

  -- Cooldown ready flash
  cooldownWeight <- do
    progress <- bind $ 6 * dashReadySeconds

    let flashWidth = 0.5
    moveRadius <- bind $ 1 + flashWidth
    flashCenter <- bind $ -moveRadius + progress * 2 * moveRadius

    s <- bind $ let sqrt2 = 0.7071067811865475 in sqrt2 * (x_ pos + y_ pos)
    bind $
      step (flashCenter - flashWidth) s
        * (1 - step (flashCenter + flashWidth) s)

  -- Color composition (with manual blending for the core)
  let whiteFilter weight vec = weight +. (1 - weight) *. vec
  shellColor <-
    bind $
      whiteFilter (0.5 * cooldownWeight) $
        whiteFilter damageOpacity baseColor
  coreColor <-
    bind $
      whiteFilter (0.75 * 0.5 * cooldownWeight) $
        whiteFilter (0.75 * damageOpacity) $
          coreOpacity *. baseColor
  colorOut <-
    bind $
      shellMask
        *. shellColor
        + (1 - bodyMidMask)
        *. coreColor
        + bodyOuterMask
        *. baseColor

  -- Opacity composition (with manual blending for the core)
  opacityOut <-
    bind $
      w_ color
        * ( shellMask
              + coreMask * whiteFilter damageOpacity coreOpacity
              + bodyOuterMask * shadowOpacity
          )

  -- Output
  pure $ vec4 (colorOut, opacityOut)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)

hexRadius :: Expr Float -> Expr Float2 -> Expr Float -> Expr Float
hexRadius ridgesScale offset angleOffset =
  let radius = length offset
      angle' = atan2 (y_ offset) (x_ offset)
      finalAngle = angle' + angleOffset
  in  radius * hex ridgesScale finalAngle

-- We set the mean radius is slightly larger than one so that drones that are
-- touching will actually appear to be touching.
hex :: Expr Float -> Expr Float -> Expr Float
hex ridgesScale angle' = 1.01 + ridgesScale * 0.032 * sin (6 * angle')
