{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Soft obstacle entities
module Client.Graphics.Rendering.Entity.SoftObstacle (
  loadSoftObstacle,
  drawSoftObstacle,
  SoftObstacleInstance (..),
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data SoftObstacleInstance = SoftObstacleInstance
  { soi_modelScale :: Float
  , soi_modelTrans :: {-# UNPACK #-} !Float2
  , soi_color :: {-# UNPACK #-} !Float3
  , soi_maxOpacity :: Float
  , soi_minOpacity :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''SoftObstacleInstance

loadSoftObstacle :: GL (StandardInstancedVisual SoftObstacleInstance)
loadSoftObstacle =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc C.maxSoftObstacles

drawSoftObstacle ::
  StandardInstancedVisual SoftObstacleInstance ->
  StandardVParams SoftObstacleInstance ->
  GL ()
drawSoftObstacle = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ disk 32 1.03

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , radius :: Expr Float
  , color :: Expr Float3
  , minOpacity :: Expr Float
  , opacityRange :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_SoftObstacleInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_SoftObstacleInstance{..}
  Vars_StandardUniforms{..} = do
    localPos <- bind $ soi_modelScale *. position
    pure $
      let screenPos =
            su_viewScale * (soi_modelTrans - su_viewCenter + localPos)
          pipelineVars =
            PipelineVars
              { pos = localPos
              , radius = soi_modelScale
              , color = soi_color
              , minOpacity = soi_minOpacity
              , opacityRange = soi_maxOpacity - soi_minOpacity
              }
      in  (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  s <- bind $ length pos
  r <- bind $ max' 0 (s - (radius - 120)) / 120
  r2 <- bind $ r * r
  r4 <- bind $ r2 * r2
  r6 <- bind $ r4 * r2
  r8 <- bind $ r4 * r4
  r16 <- bind $ r8 * r8
  dr <- bind $ (r - 1) * radius
  outerBound <- bind $ 1 - smoothstep (-0.5) 0.5 dr
  opacity <- bind $ outerBound * (opacityRange * 0.5 * (r16 + r6) + minOpacity)
  pure $ vec4 (color, opacity)
