{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Dust cloud effect for dashes
module Client.Graphics.Rendering.Entity.Dash (
  loadDash,
  drawDash,
  DashInstance (..),
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data DashInstance = DashInstance
  { di_center :: {-# UNPACK #-} !Float2
  , di_initialVelocity :: {-# UNPACK #-} !Float2
  , di_scale :: Float
  , di_angle :: Float
  , di_ticksElapsed :: Float
  , di_secondsElapsed :: Float
  , di_opacity :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''DashInstance

loadDash :: GL (StandardInstancedVisual DashInstance)
loadDash =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc C.maxNumEntities

drawDash ::
  StandardInstancedVisual DashInstance ->
  StandardVParams DashInstance ->
  GL ()
drawDash = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon 1.01

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , size :: Expr Float
  , secondsElapsed :: Expr Float
  , fuzziness :: Expr Float
  , baseOpacity :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_DashInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_DashInstance{..}
  Vars_StandardUniforms{..} = do
    fuzziness <- do
      let fuzzSqPerSecond = 24 * 24
      bind $ sqrt $ 1 + fuzzSqPerSecond * di_secondsElapsed

    localPos <- bind $ position .* (di_scale + fuzziness) -- game-world scale
    screenPos <- do
      let tau = 8 -- dust drag time constant
      distanceFactor <- bind $ tau * (1 - exp (-di_ticksElapsed / tau))

      pushVec <- do
        let pushVelocity = 2
        pushDist <- bind $ pushVelocity * distanceFactor
        bind $ vec2 (-pushDist * cos di_angle, -pushDist * sin di_angle)
      driftVec <- bind $ distanceFactor *. di_initialVelocity
      gameWorldPos <- bind $ localPos + pushVec + driftVec
      bind $ su_viewScale * (gameWorldPos + di_center - su_viewCenter)

    let pipelineVars =
          PipelineVars
            { pos = localPos
            , size = di_scale
            , secondsElapsed = di_secondsElapsed
            , fuzziness = fuzziness
            , baseOpacity = di_opacity
            }
    pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  mask <- bind $ 1 - smoothstep (-fuzziness) fuzziness (length pos - size)
  opacity <-
    bind $
      min' (0.5 * exp (-3.5 * secondsElapsed)) (secondsElapsed / 0.05)
  pure $ vec4 (0.5, 0.5, 0.5, opacity * mask * baseOpacity)
