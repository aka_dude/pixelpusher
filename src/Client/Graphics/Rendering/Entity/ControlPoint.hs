{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | A visual for an entity that no longer exists. Repurposed for showing
-- progress towards flag reclamations.
module Client.Graphics.Rendering.Entity.ControlPoint (
  loadControlPoint,
  drawControlPoint,
  ControlPointInstance,
  fromControlPointRendering,
  fromFlagCaptureAreaRendering,
) where

import Data.Word
import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2, Float3 (Float3), Float4 (Float4))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data ControlPointInstance = ControlPointInstance
  { cpi_modelScale :: Float
  , cpi_modelTrans :: Float2
  , cpi_outerColor :: Float4
  , cpi_innerColor :: Float4
  , cpi_innerRadiusSquared :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''ControlPointInstance

loadControlPoint :: GL (StandardInstancedVisual ControlPointInstance)
loadControlPoint =
  loadStandardVisual
    vertices
    vertShaderSrc
    fragShaderSrc
    maxInstances

maxInstances :: Word16
maxInstances =
  2 -- flag recovery areas
    + 8 -- safety

drawControlPoint ::
  StandardInstancedVisual ControlPointInstance ->
  StandardVParams ControlPointInstance ->
  GL ()
drawControlPoint = drawStandardVisual

--------------------------------------------------------------------------------

fromControlPointRendering ::
  Maybe Team ->
  RenderingComponent ControlPointRendering ->
  ControlPointInstance
fromControlPointRendering mViewTeam rc =
  ControlPointInstance
    { cpi_modelScale = rendering_radius rc
    , cpi_modelTrans = rendering_pos rc
    , cpi_outerColor = outerColor
    , cpi_innerColor = innerColor
    , cpi_innerRadiusSquared = innerRadiusSquared
    }
 where
  innerRadiusSquared :: Float
  innerRadiusSquared = 1 - (minFraction + (1 - minFraction) * captureProgress)
   where
    (captureProgress, minFraction) = case rendering_entityRendering rc of
      ControlPointRendering_Neutral -> (0, 0.1)
      ControlPointRendering_Tentative _ progress _ -> (progress, 0.1)
      ControlPointRendering_Captured _ progress _ -> (1 - progress, 0)

  outerColor :: Float4
  outerColor = case rendering_entityRendering rc of
    ControlPointRendering_Neutral -> Util.fromRGB_A purple dull
    ControlPointRendering_Tentative team _ _ ->
      case viewTeamColor mViewTeam team of
        TeamRed -> Util.fromRGB_A red opacity
        TeamBlue -> Util.fromRGB_A blue opacity
     where
      opacity = transition ordinary dull
    ControlPointRendering_Captured team _ _ ->
      case viewTeamColor mViewTeam team of
        TeamRed -> Util.fromRGB_A red opacity
        TeamBlue -> Util.fromRGB_A blue opacity
     where
      opacity = transition (ordinary + flash) ordinary

  innerColor :: Float4
  innerColor = case rendering_entityRendering rc of
    ControlPointRendering_Neutral -> blank
    ControlPointRendering_Tentative{} -> blank
    ControlPointRendering_Captured team _ _ ->
      case viewTeamColor mViewTeam team of
        TeamRed -> Util.fromRGB_A red opacity
        TeamBlue -> Util.fromRGB_A blue opacity
     where
      opacity = transition (bright + flash) bright

  transition :: Float -> Float -> Float
  transition flashyOpacity opacity = case rendering_entityRendering rc of
    ControlPointRendering_Neutral -> opacity
    ControlPointRendering_Tentative _ _ stateDuration ->
      captureFlashFilter flashyOpacity stateDuration opacity
    ControlPointRendering_Captured _ _ stateDuration ->
      captureFlashFilter flashyOpacity stateDuration opacity

  purple = Float3 1 0.5 1
  red = Float3 1 0 0
  blue = Float3 0 0.5 1
  dull = 0.12
  ordinary = 0.18
  bright = 0.24
  flash = 0.06
  blank = Float4 0 0 0 0

captureFlashFilter :: Float -> Ticks -> Float -> Float
captureFlashFilter flashColor (Ticks age) x =
  weight * flashColor + (1.0 - weight) * x
 where
  boundedAge = max 0 $ min durationTicks age
  weight = 1 - fromIntegral boundedAge / fromIntegral durationTicks
  durationTicks = 40

fromFlagCaptureAreaRendering ::
  DynamicsParams ->
  Maybe Team ->
  RenderingComponent FlagCaptureAreaRendering ->
  ControlPointInstance
fromFlagCaptureAreaRendering dynParams mViewTeam rc =
  ControlPointInstance
    { cpi_modelScale = Fixed.toFloat $ gp_flagRecoveryRadius dynParams
    , cpi_modelTrans = rendering_pos rc
    , cpi_outerColor = outerColor
    , cpi_innerColor = blank
    , cpi_innerRadiusSquared = innerRadiusSquared
    }
 where
  innerRadiusSquared :: Float
  innerRadiusSquared = 1 - (minFraction + (1 - minFraction) * progress)
   where
    minFraction = 0.05
    progress = fcar_progress (rendering_entityRendering rc)

  outerColor :: Float4
  outerColor =
    case viewTeamColor mViewTeam (fcar_team (rendering_entityRendering rc)) of
      TeamRed -> Util.fromRGB_A red dull
      TeamBlue -> Util.fromRGB_A blue dull

  red = Float3 1 0 0
  blue = Float3 0 0.5 1
  dull = 0.12
  blank = Float4 0 0 0 0

----------------------------------------------------------------------------------

-- Using `Identity` for its `AttributeData` instance
vertices :: [Position]
vertices = map Position $ hexagon 1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , outerColor :: Expr Float4
  , innerColor :: Expr Float4
  , innerRadiusSquared :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_ControlPointInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_ControlPointInstance{..}
  Vars_StandardUniforms{..} =
    pure $
      let screenPos =
            su_viewScale
              * (cpi_modelTrans - su_viewCenter + cpi_modelScale *. position)
          pipelineVars =
            PipelineVars
              { pos = position
              , outerColor = cpi_outerColor
              , innerColor = cpi_innerColor
              , innerRadiusSquared = cpi_innerRadiusSquared
              }
      in  (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r2 <- bind $ x_ pos * x_ pos + y_ pos * y_ pos
  outer_bound <- bind $ 1 - smoothstep 0.98 1.02 r2
  inner_bound <-
    bind $
      smoothstep (innerRadiusSquared * 0.98) (innerRadiusSquared * 1.02) r2
  color <- bind $ mix innerColor outerColor inner_bound
  pure $ vec4 (xyz_ color, w_ color * outer_bound)
