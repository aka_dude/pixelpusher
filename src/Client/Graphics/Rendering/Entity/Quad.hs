{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic rectangle
module Client.Graphics.Rendering.Entity.Quad (
  loadQuad,
  drawQuad,
  QuadInstance (..),
) where

import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data QuadInstance = QuadInstance
  { qi_size :: Float2
  , qi_center :: Float2
  , qi_color :: Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''QuadInstance

loadQuad :: GL (StandardInstancedVisual QuadInstance)
loadQuad =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawQuad ::
  StandardInstancedVisual QuadInstance ->
  StandardVParams QuadInstance ->
  GL ()
drawQuad = drawStandardVisual

maxInstances :: Word16
maxInstances = 16 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ square 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {color :: Expr Float4}
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_QuadInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_QuadInstance{..}
  Vars_StandardUniforms{..} =
    pure $
      let screenPos =
            su_viewScale * (qi_center - su_viewCenter + qi_size * position)
      in  (screenPos, PipelineVars qi_color)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{color} = pure color
