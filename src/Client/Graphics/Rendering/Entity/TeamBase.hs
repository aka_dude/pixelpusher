{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Team bases for capture the flag
module Client.Graphics.Rendering.Entity.TeamBase (
  loadTeamBase,
  drawTeamBase,
  TeamBaseInstance (..),
) where

import Prelude hiding (length)

import Data.Coerce
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data TeamBaseInstance = TeamBaseInstance
  { tbi_size :: Float
  , tbi_center :: {-# UNPACK #-} !Float2
  , tbi_color :: {-# UNPACK #-} !Float4
  , tbi_fillOpacity :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''TeamBaseInstance

--------------------------------------------------------------------------------

loadTeamBase :: GL (StandardInstancedVisual TeamBaseInstance)
loadTeamBase =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawTeamBase ::
  StandardInstancedVisual TeamBaseInstance ->
  StandardVParams TeamBaseInstance ->
  GL ()
drawTeamBase = drawStandardVisual

maxInstances :: Word16
maxInstances = 4

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = coerce $ hexagon 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , fillOpacity :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_TeamBaseInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_TeamBaseInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ tbi_size *. position
    screenPos <- bind $ su_viewScale * (tbi_center + pos - su_viewCenter)
    let pipelineVars =
          PipelineVars
            { radius = tbi_size
            , pos = pos
            , color = tbi_color
            , fillOpacity = tbi_fillOpacity
            }
    pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ r - radius

  fill <- bind $ fillOpacity * 0.25 * (1 - smoothstep (-0.5) 0.5 (r - radius))

  line <- bind $ 1 - smoothstep 0 1 (abs dr)
  lineBloom <- bind $ 0.6 * exp' (-0.2 * dr * dr)

  opacity <- bind $ max' fill $ max' line lineBloom
  pure $ vec4 (xyz_ color, opacity * w_ color)
