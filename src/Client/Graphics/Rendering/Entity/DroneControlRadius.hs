{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | The large circle around your overseer that shows the range of your
-- commands
module Client.Graphics.Rendering.Entity.DroneControlRadius (
  loadDroneControlRadius,
  drawDroneControlRadius,
  DroneControlRadiusVisual,
  DroneControlRadiusVParams (..),
) where

import Prelude
import Prelude qualified as P

import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types (GLRecord)
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data DroneControlRadiusVisual = DroneControlRadiusVisual
  { dcrv_vao :: VertexArray
  , dcrv_shaderProg :: ShaderProgram
  , dcrv_nVertices :: GLsizei
  , dcrv_uniformLocations :: UniformLocations DroneControlRadiusVParams
  }

data DroneControlRadiusVParams = DroneControlRadiusVParams
  { dcrvp_modelTrans :: Float2
  , dcrvp_modelScale :: Float
  , dcrvp_viewCenter :: Float2
  , dcrvp_viewScale :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''DroneControlRadiusVParams

--------------------------------------------------------------------------------

drawDroneControlRadius ::
  DroneControlRadiusVisual -> DroneControlRadiusVParams -> GL ()
drawDroneControlRadius dcrv params =
  withVertexArray (dcrv_vao dcrv) $
    withShaderProgram (dcrv_shaderProg dcrv) $ do
      writeUniforms (dcrv_uniformLocations dcrv) params
      glDrawArrays GL_LINE_LOOP 0 (dcrv_nVertices dcrv)

loadDroneControlRadius :: GL DroneControlRadiusVisual
loadDroneControlRadius = do
  vao <- setupVertexArrayObject vertices
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniforms <- getUniformLocations shaderProg
  pure $
    DroneControlRadiusVisual
      { dcrv_vao = vao
      , dcrv_shaderProg = shaderProg
      , dcrv_nVertices = fromIntegral $ P.length vertices
      , dcrv_uniformLocations = uniforms
      }

vertices :: [Position]
vertices =
  map Position $
    let n = 128 :: Int
        angles =
          map (\i -> 2 * pi * fromIntegral i / fromIntegral n) [1 .. n]
    in  map (\ang -> Float2 (cos ang) (sin ang)) angles

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

vertShader ::
  Vars_Position ->
  () ->
  Vars_DroneControlRadiusVParams ->
  GLSL (Expr Float2, ())
vertShader Vars_Position{position} () Vars_DroneControlRadiusVParams{..} =
  pure $
    let screenPos =
          dcrvp_viewScale
            *. (dcrvp_modelTrans - dcrvp_viewCenter + dcrvp_modelScale *. position)
    in  (screenPos, ())

fragShader :: Vars_DroneControlRadiusVParams -> () -> GLSL (Expr Float4)
fragShader _ () = pure $ vec4 (1, 1, 1, 0.25)
