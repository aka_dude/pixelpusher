{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | 'X' shape UI element
module Client.Graphics.Rendering.Entity.Cross (
  loadCross,
  drawCross,
  CrossInstance (..),
) where

import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CrossInstance = CrossInstance
  { ci_size :: Float
  , ci_center :: Float2
  , ci_color :: Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CrossInstance

loadCross :: GL (StandardInstancedVisual CrossInstance)
loadCross =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawCross ::
  StandardInstancedVisual CrossInstance ->
  StandardVParams CrossInstance ->
  GL ()
drawCross = drawStandardVisual

maxInstances :: Word16
maxInstances = 16

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , size :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CrossInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CrossInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ ci_size *. position
    rotatedPos <-
      bind $ -- rotate 45 degrees
        0.70710678 * vec2 (x_ pos - y_ pos, x_ pos + y_ pos)
    screenPos <- bind $ su_viewScale * (ci_center - su_viewCenter + rotatedPos)
    let pipelineVars =
          PipelineVars
            { pos = pos
            , size = ci_size
            , color = ci_color
            }
    pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r0 <- bind $ 0.15 * size
  r1 <- bind $ 0.90 * size

  px <- bind $ abs $ x_ pos
  py <- bind $ abs $ y_ pos

  d <- bind $ min' (max' (px - r1) (py - r0)) (max' (px - r0) (py - r1))

  opacity <- bind $ 1.0 - smoothstep (-0.5) 0.5 d
  pure $ vec4 (xyz_ color, w_ color * opacity)
