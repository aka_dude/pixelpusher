{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Cursor for attracting drones
--
-- TODO: Use uniforms rather than instances
module Client.Graphics.Rendering.Entity.CursorAttract (
  loadCursorAttract,
  drawCursorAttract,
  CursorAttractInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CursorAttractInstance = CursorAttractInstance
  { cai_size :: Float
  , cai_center :: {-# UNPACK #-} !Float2
  , cai_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CursorAttractInstance

--------------------------------------------------------------------------------

loadCursorAttract :: GL (StandardInstancedVisual CursorAttractInstance)
loadCursorAttract =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawCursorAttract ::
  StandardInstancedVisual CursorAttractInstance ->
  StandardVParams CursorAttractInstance ->
  GL ()
drawCursorAttract = drawStandardVisual

maxInstances :: Word16
maxInstances = 2 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = coerce $ hexagon 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CursorAttractInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CursorAttractInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ cai_size *. position
    screenPos <- bind $ su_viewScale * (cai_center + pos - su_viewCenter)
    let radius = cai_size
        color = cai_color
    pure (screenPos, PipelineVars{..})

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  let fuzziness = 0.5
      sqrt34 = 0.8660254037844386
  r <- bind $ length pos

  radialFuzziness <- bind $ fuzziness / r
  angle_ <- bind $ atan2 (y_ pos) (x_ pos)
  angleMask <-
    bind $
      1
        - smoothstepCentered
          0.125
          radialFuzziness
          (abs (mod' (angle_ / (pi / 3)) 1 - 0.5))

  hexRadius <-
    bind $
      max' (abs (dot' pos (vec2 (0, 1)))) $
        max'
          (abs (dot' pos (vec2 (-sqrt34, 0.5))))
          (abs (dot' pos (vec2 (sqrt34, 0.5))))
  hexMask <-
    bind $ 1 - smoothstepCentered radius fuzziness hexRadius

  opacity <- bind $ hexMask * angleMask
  pure $ vec4 (xyz_ color, opacity * w_ color)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)
