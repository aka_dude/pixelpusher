{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Effect for the psi-storm ability
module Client.Graphics.Rendering.Entity.PsiStorm (
  loadPsiStorm,
  drawPsiStorm,
  PsiStormInstance (..),
) where

import Prelude hiding (atan2, length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data PsiStormInstance = PsiStormInstance
  { psi_modelScale :: Float
  , psi_modelTrans :: {-# UNPACK #-} !Float2
  , psi_elapsedSeconds :: Float
  , psi_opacityFactor :: Float
  , psi_bloomColor :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''PsiStormInstance

loadPsiStorm :: GL (StandardInstancedVisual PsiStormInstance)
loadPsiStorm =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc C.maxPsiStorms

drawPsiStorm ::
  StandardInstancedVisual PsiStormInstance ->
  StandardVParams PsiStormInstance ->
  GL ()
drawPsiStorm = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ disk 32 1.03

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { localPos :: Expr Float2
  , globalPos :: Expr Float2
  , radius :: Expr Float
  , elapsedSeconds :: Expr Float
  , opacityFactor :: Expr Float
  , bloomColor :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_PsiStormInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_PsiStormInstance{..}
  Vars_StandardUniforms{..} = do
    localPos <- bind $ psi_modelScale *. position
    globalPos <- bind $ localPos + psi_modelTrans
    pure $
      let screenPos = su_viewScale * (globalPos - su_viewCenter)
          pipelineVars =
            PipelineVars
              { localPos = localPos
              , globalPos = globalPos
              , radius = psi_modelScale
              , elapsedSeconds = psi_elapsedSeconds
              , opacityFactor = psi_opacityFactor
              , bloomColor = psi_bloomColor
              }
      in  (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  waves <- do
    t <- bind $ 3 * elapsedSeconds + 0.025 * sin (10 * elapsedSeconds)
    let freq = 0.014
    bind $
      ( 0
          + sin (freq * 1.00 * dot' globalPos (vec2 (1, 0)) + 2.02 * t)
          + sin (freq * 1.02 * dot' globalPos (vec2 (0.65, -0.73)) + 2.47 * t)
          + sin (freq * 1.04 * dot' globalPos (vec2 (-0.56, -0.85)) + 1.54 * t)
          + 0.5 * sin (freq * 2.00 * dot' globalPos (vec2 (0.1, 0.9)) - 2.53 * t)
          + 0.5 * sin (freq * 2.04 * dot' globalPos (vec2 (0.82, 0.62)) - 1.46 * t)
          + 0.5 * sin (freq * 2.08 * dot' globalPos (vec2 (0.78, -0.58)) - 1.98 * t)
          + 0.25 * sin (freq * 4.00 * dot' globalPos (vec2 (0.5, 0.866)) + 2.23 * t)
          + 0.25 * sin (freq * 4.08 * dot' globalPos (vec2 (-0.917, 0.4)) + 1.77 * t)
          + 0.25 * sin (freq * 4.16 * dot' globalPos (vec2 (0.707, -0.707)) + 1.37 * t)
      )
        / 5.25

  r <- bind $ length localPos

  (outerMask, stormBoundary) <- do
    outerMask <- bind $ smoothstep (-0.5) 0.5 (r - radius)

    s <- bind $ max' 0 (r - (radius - 120)) / 120
    s2 <- bind $ s * s
    s4 <- bind $ s2 * s2
    s8 <- bind $ s4 * s4
    s16 <- bind $ s8 * s8
    let boundary = s16

    pure (outerMask, boundary)

  storm <- do
    let fillOpacity = 0.06
        boundaryOpacity = 0.30
    stormOpacity <-
      bind $ boundaryOpacity * stormBoundary + fillOpacity * (1 + waves)
    bind $ vec4 (1, 1, 0.65, stormOpacity)

  bloom <- do
    s <- bind $ 0.33 * (r - radius)
    opacity <- bind $ 0.4 * exp (-s * s)
    bind $ vec4 (xyz_ bloomColor, w_ bloomColor * opacity)

  whole <- bind $ outerMask *. bloom + (1 - outerMask) *. storm
  pure $ vec4 (xyz_ whole, opacityFactor * w_ whole)
