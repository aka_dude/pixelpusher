{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Sparks for indicating damage dealt
module Client.Graphics.Rendering.Entity.Spark (
  loadSpark,
  drawSpark,
  SparkInstance (..),
  fromCollisionParticle,
  fromPsiStormDamageParticles,
) where

import Prelude hiding (length)

import Data.Bool (bool)
import Data.Int (Int32)
import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4,
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.SDF (unevenCapsule)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data SparkInstance = SparkInstance
  { si_center :: {-# UNPACK #-} !Float2
  , si_radius :: Float
  , si_angle :: Float
  , si_tailLength :: Float
  , si_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''SparkInstance

loadSpark :: GL (StandardInstancedVisual SparkInstance)
loadSpark =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc CC.maxVisibleSparks

drawSpark ::
  StandardInstancedVisual SparkInstance ->
  StandardVParams SparkInstance ->
  GL ()
drawSpark = drawStandardVisual

--------------------------------------------------------------------------------

fromPsiStormDamageParticles ::
  Maybe ActorID ->
  Maybe Team ->
  Time ->
  [Particle PsiStormDamageParticle] ->
  [SparkInstance]
fromPsiStormDamageParticles mViewPlayer mViewTeam time =
  concat
    . zipWith (fromPsiStormDamageParticle mViewPlayer mViewTeam time) [0 ..]

fromPsiStormDamageParticle ::
  Maybe ActorID ->
  Maybe Team ->
  Time ->
  Int -> -- for variation of multiple psi-storm hits on the same entity
  Particle PsiStormDamageParticle ->
  [SparkInstance]
fromPsiStormDamageParticle mViewPlayer mViewTeam time randInt particle =
  let psdp = particle_type particle
      damage = Fixed.toFloat $ psdp_damage psdp

      center = Fixed.toFloats $ psdp_pos psdp
      vel = Fixed.toFloats $ psdp_vel psdp
      nSparks = sparkCount (particle_startTime particle) center damage

      randAngle :: Int -> Float
      randAngle i =
        (* (2 * pi)) $
          rand $
            Float.dot2 (Float2 42.0865 63.811) center
              - 28.04 * fromIntegral i
              + 6.301 * fromIntegral randInt
      angles = map randAngle [1 .. nSparks]

      speeds = map (randSpeed damage center) [1 .. nSparks]
      ticksElapsed =
        fromIntegral $ getTicks $ time `diffTime` particle_startTime particle
      color = getDamageColor mViewPlayer mViewTeam $ psdp_mPlayerColor psdp
  in  zipWith3 (makeInstance ticksElapsed center vel) angles speeds (repeat color)

fromCollisionParticle ::
  Maybe ActorID ->
  Maybe Team ->
  Time ->
  Particle CollisionDamageParticle ->
  [SparkInstance]
fromCollisionParticle mViewPlayer mViewTeam time particle =
  let cdp = particle_type particle
      damage = Fixed.toFloat $ cdp_damage cdp

      center = Fixed.toFloats $ cdp_pos (cdp :: CollisionDamageParticle)
      vel = Fixed.toFloats $ cdp_vel cdp
      nSparks = sparkCount (particle_startTime particle) center damage

      baseAng = (+ pi / 2) $ Float.unangle $ Fixed.toFloats $ cdp_direction cdp
      offsetAngle :: Int -> Float
      offsetAngle i =
        let random =
              rand $
                Float.dot2 (Float2 12.9898 78.233) center
                  + 91.25 * fromIntegral i
            randomSym = 2 * random - 1
            sign = signum randomSym
            mag = abs randomSym
            magShaped = mag * (0.05 + mag * (0.05 + mag * mag * 0.9))
            width = pi / 8
        in  sign * width * magShaped
      anglesHalf = map ((+ baseAng) . offsetAngle) [1 .. nSparks]
      angles = anglesHalf >>= \ang -> [ang, ang + pi]

      speeds = map (randSpeed damage center) [1 .. 2 * nSparks]

      ticksElapsed =
        fromIntegral $ getTicks $ time `diffTime` particle_startTime particle

      colors =
        cycle
          [ getDamageColor mViewPlayer mViewTeam (cdp_mPlayerColor1 cdp)
          , getDamageColor mViewPlayer mViewTeam (cdp_mPlayerColor2 cdp)
          , getDamageColor mViewPlayer mViewTeam (cdp_mPlayerColor2 cdp)
          , getDamageColor mViewPlayer mViewTeam (cdp_mPlayerColor1 cdp)
          ]
  in  zipWith3 (makeInstance ticksElapsed center vel) angles speeds colors

-- Helpers

sparkCount :: Time -> Float2 -> Float -> Int
sparkCount startTime pos damage =
  min 32 $
    let (damageFloor, damageFrac) = properFraction damage
        timeFactor = 41.5016 * fromIntegral (getTime startTime `rem` 1009)
        posFactor = Float.dot2 (Float2 (-24.0516) 9.5223) pos
        x = rand $ timeFactor + posFactor
    in  damageFloor + bool 0 1 (damageFrac > x) -- Avoiding rebindable syntax

getDamageColor :: Maybe ActorID -> Maybe Team -> Maybe PlayerColor -> Float3
getDamageColor mViewPlayer mViewTeam =
  maybe (Float3 0 0 0) (viewPlayerColor mViewPlayer mViewTeam)

rand :: Float -> Float
rand x =
  let r = 43758.5453123 * sin x
  in  r - fromIntegral @Int (floor r)

randSpeed :: Float -> Float2 -> Int -> Float
randSpeed damage pos i =
  let rand' =
        rand $ Float.dot2 (Float2 71.233 11.6868) pos + 3.36 * fromIntegral i
      speedSqrtDamageRatio = 1.0
  in  (rand' + 0.5) * sqrt damage * speedSqrtDamageRatio

makeInstance ::
  Int32 ->
  Float2 ->
  Float2 ->
  Float ->
  Float ->
  Float3 ->
  SparkInstance
makeInstance ticksElapsedInt pos vel launchAngle launchSpeed color =
  SparkInstance
    { si_center = pos + Float.map2 (* dist) initVelocityNorm
    , si_radius = 0.7 + 0.4 * opacity
    , si_angle = ang
    , si_tailLength = 6 * speed + 1.5
    , si_color = Util.fromRGB_A (brighten color) opacity
    }
 where
  initVelocity = vel + Float.map2 (* launchSpeed) (Float.angle launchAngle)
  (initSpeed, initVelocityNorm) = Float.normAndNormalized2 initVelocity
  ang = Float.unangle initVelocityNorm

  timeDilation = 2.0
  ticksElapsed -- with 1-tick stop
    | ticksElapsedInt <= 1 = 1
    | otherwise = fromIntegral ticksElapsedInt
  t = ticksElapsed / timeDilation
  c = recip $ initSpeed * timeDilation
  dragCoeff = 0.05
  dist = (log (dragCoeff * t + c) - log c) / dragCoeff
  speed = recip (dragCoeff * t + c) / timeDilation

  opacity = min 1 $ max 0 $ 2 * speed - 0.5
  brighten x = Float3 w w w + Float.map3 (* (1 - w)) x
   where
    w = 0.25 * opacity

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ square 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , kernelRadius :: Expr Float
  , tailLength :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_SparkInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_SparkInstance{..}
  Vars_StandardUniforms{..} = do
    pos_y <- bind $ y_ position * si_radius
    pos_x <-
      bind $
        if x_ position `gt` 0
          then x_ position * si_radius
          else x_ position * si_tailLength

    s <- bind $ sin si_angle
    c <- bind $ cos si_angle
    pos_rotated <- bind $ vec2 (pos_x * c - pos_y * s, pos_x * s + pos_y * c)

    let screenPos = su_viewScale * (pos_rotated + si_center - su_viewCenter)
        pipelineVars =
          PipelineVars
            { pos = vec2 (pos_x, pos_y)
            , kernelRadius = si_radius
            , tailLength = si_tailLength
            , color = si_color
            }
    pure (screenPos, pipelineVars)
   where
    ifThenElse = ifThenElse'

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  sdf <- unevenCapsule tailLength kernelRadius 0 pos
  let fuzziness = 0.25
  opacity <- bind $ 1 - smoothstep (-fuzziness) fuzziness sdf
  pure $ vec4 (xyz_ color, opacity * w_ color)
