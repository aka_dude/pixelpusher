{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic circle
module Client.Graphics.Rendering.Entity.Ring (
  loadRing,
  drawRing,
  RingInstance (..),
) where

import Prelude hiding (length)

import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Constants qualified as CC
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data RingInstance = RingInstance
  { ri_modelScale :: Float
  , ri_modelTrans :: Float2
  , ri_color :: Float4
  , ri_thickness :: Float
  , ri_fuzziness :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''RingInstance

loadRing :: GL (StandardInstancedVisual RingInstance)
loadRing =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawRing ::
  StandardInstancedVisual RingInstance ->
  StandardVParams RingInstance ->
  GL ()
drawRing = drawStandardVisual

-- This visual is currently only used for pings
maxInstances :: Word16
maxInstances = CC.maxVisiblePings

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon 1.03

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  , size :: Expr Float
  , thickness :: Expr Float
  , fuzziness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_RingInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_RingInstance{..}
  Vars_StandardUniforms{..} =
    pure $
      let screenPos =
            su_viewScale
              * (ri_modelTrans - su_viewCenter + ri_modelScale *. position)
          pipelineVars =
            PipelineVars
              { pos = position .* ri_modelScale
              , color = ri_color
              , size = ri_modelScale
              , thickness = ri_thickness
              , fuzziness = ri_fuzziness
              }
      in  (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{pos, color, size, thickness, fuzziness} = do
  r <- bind $ length pos
  let fuzz_width = fuzziness
      outer_bound = size
      inner_bound = size - thickness
  outer_mask <-
    bind $
      1 - smoothstep (outer_bound - fuzz_width) (outer_bound + fuzz_width) r
  inner_mask <-
    bind $
      smoothstep (inner_bound - fuzz_width) (inner_bound + fuzz_width) r
  opacity <- bind $ inner_mask * outer_mask
  pure $ vec4 (xyz_ color, w_ color * opacity)
