{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic disks
module Client.Graphics.Rendering.Entity.Circle (
  loadCircle,
  drawCircle,
  CircleInstance (..),
) where

import Prelude hiding (length)

import Data.Coerce
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CircleInstance = CircleInstance
  { ci_size :: Float
  , ci_center :: {-# UNPACK #-} !Float2
  , ci_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CircleInstance

--------------------------------------------------------------------------------

loadCircle :: GL (StandardInstancedVisual CircleInstance)
loadCircle =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawCircle ::
  StandardInstancedVisual CircleInstance ->
  StandardVParams CircleInstance ->
  GL ()
drawCircle = drawStandardVisual

maxInstances :: Word16
maxInstances =
  fromIntegral C.maxActorsPerGame -- overseers on minimap
    + 2 -- team bases

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = coerce $ hexagon 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CircleInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CircleInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ ci_size *. position
    screenPos <- bind $ su_viewScale * (ci_center + pos - su_viewCenter)
    let radius = ci_size
        color = ci_color
    pure (screenPos, PipelineVars{..})

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ r - radius
  opacity <- bind $ 1 - smoothstep (-0.5) 0.5 dr
  pure $ vec4 (xyz_ color, opacity * w_ color)
