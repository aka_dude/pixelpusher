{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | The cooldown bars beneath your overseer
module Client.Graphics.Rendering.Entity.CooldownBar (
  loadCooldownBar,
  drawCooldownBar,
  CooldownBarInstance (..),
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CooldownBarInstance = CooldownBarInstance
  { cbi_center :: {-# UNPACK #-} !Float2
  , cbi_widthRadius :: Float
  , cbi_heightRadius :: Float
  , cbi_bloomRadius :: Float
  , cbi_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CooldownBarInstance

loadCooldownBar :: GL (StandardInstancedVisual CooldownBarInstance)
loadCooldownBar =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc C.maxNumEntities

drawCooldownBar ::
  StandardInstancedVisual CooldownBarInstance ->
  StandardVParams CooldownBarInstance ->
  GL ()
drawCooldownBar = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ square 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , widthRadius :: Expr Float
  , heightRadius :: Expr Float
  , bloomRadius :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CooldownBarInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CooldownBarInstance{..}
  Vars_StandardUniforms{..} = do
    bloomRadiusExt <- bind $ cbi_bloomRadius * 2
    x_radius <- bind $ cbi_widthRadius + bloomRadiusExt
    let y_radius = bloomRadiusExt
    localPosition <-
      bind $
        vec2 (x_ position * x_radius, y_ position * y_radius)
    screenPos <-
      bind $ su_viewScale * (localPosition + cbi_center - su_viewCenter)

    let pipelineVars =
          PipelineVars
            { pos = localPosition
            , widthRadius = cbi_widthRadius
            , heightRadius = cbi_heightRadius
            , bloomRadius = cbi_bloomRadius
            , color = cbi_color
            }
    pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  dx <- bind $ max' 0 $ abs (x_ pos) - widthRadius + bloomRadius -- to make the zero-width bar look empty
  dy <- bind $ max' 0 $ abs (y_ pos) - heightRadius
  r2 <- bind $ dx * dx + dy * dy
  opacity <- bind $ exp $ negate $ 0.5 * r2 / (bloomRadius * bloomRadius)
  pure $ vec4 (xyz_ color, w_ color * opacity)
