{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Effect for loud visual notification
module Client.Graphics.Rendering.Entity.PingFlash (
  loadPingFlash,
  drawPingFlash,
  PingFlashInstance (..),
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Constants qualified as CC
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data PingFlashInstance = PingFlashInstance
  { pfi_size :: Float
  , pfi_center :: Float2
  , pfi_color :: Float4
  , pfi_ticksElapsed :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''PingFlashInstance

loadPingFlash :: GL (StandardInstancedVisual PingFlashInstance)
loadPingFlash =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc CC.maxVisiblePings

drawPingFlash ::
  StandardInstancedVisual PingFlashInstance ->
  StandardVParams PingFlashInstance ->
  GL ()
drawPingFlash = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

vertShader ::
  Vars_Position ->
  Vars_PingFlashInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_PingFlashInstance{..}
  Vars_StandardUniforms{..} =
    pure $
      let screenPos =
            su_viewScale
              * (pfi_center - su_viewCenter + pfi_size *. position)
          pipelineVars =
            PipelineVars
              { pos = position
              , color = pfi_color
              , progress = pfi_ticksElapsed / fromIntegral C.tickRate_hz
              }
      in  (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  , progress :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  outerCutoff <- bind $ 1 - exp (-2 * progress)
  ring_width <- bind $ 0.2 * (1 - progress)
  innerCutoff <- bind $ outerCutoff - ring_width
  let fuzz_width = 0.01
  innerMask <-
    bind $
      smoothstep (innerCutoff - fuzz_width) (innerCutoff + fuzz_width) r
  outerMask <-
    bind $
      1 - smoothstep (outerCutoff - fuzz_width) (outerCutoff + fuzz_width) r
  opacity <- bind $ innerMask * outerMask
  pure $ vec4 (xyz_ color, w_ color * opacity)
