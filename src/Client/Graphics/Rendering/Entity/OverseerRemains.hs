{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | The fuzzy blob of colour that is an overseer's corpse
module Client.Graphics.Rendering.Entity.OverseerRemains (
  loadOverseerRemains,
  drawOverseerRemains,
  OverseerRemainsInstance,
  fromOverseerRemainsParticle,
) where

import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data OverseerRemainsInstance = OverseerRemainsInstance
  { ori_modelScale :: Float
  , ori_modelTrans :: Float2
  , ori_color :: Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''OverseerRemainsInstance

loadOverseerRemains :: GL (StandardInstancedVisual OverseerRemainsInstance)
loadOverseerRemains =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances
 where
  maxInstances = fromIntegral C.maxActorsPerGame

drawOverseerRemains ::
  StandardInstancedVisual OverseerRemainsInstance ->
  StandardVParams OverseerRemainsInstance ->
  GL ()
drawOverseerRemains = drawStandardVisual

--------------------------------------------------------------------------------

fromOverseerRemainsParticle ::
  Maybe ActorID ->
  Maybe Team ->
  Time ->
  Particle OverseerRemainsParticle ->
  OverseerRemainsInstance
fromOverseerRemainsParticle mViewPlayer mViewTeam time particle =
  let OverseerRemainsParticle{orp_color, orp_pos, orp_scale} =
        particle_type particle
      color = normalize1 $ viewPlayerColor mViewPlayer mViewTeam orp_color
      t = getTicks $ time `diffTime` particle_startTime particle
      t_final =
        getTicks $
          particle_endTime particle `diffTime` particle_startTime particle
      t_inflect = 6 -- Ticks
      opacity =
        (* 0.4) $
          if t < t_inflect
            then fromIntegral t / fromIntegral t_inflect
            else
              let x =
                    fromIntegral (t - t_inflect)
                      / fromIntegral (t_final - t_inflect)
                  x2 = x * x
              in  1 - x2 * x2
  in  OverseerRemainsInstance
        { ori_modelScale = Fixed.toFloat $ 2.5 * orp_scale
        , ori_modelTrans = Fixed.toFloats orp_pos
        , ori_color = Util.fromRGB_A color opacity
        }
 where
  normalize1 :: Float3 -> Float3
  normalize1 v =
    let m = maximum $ Float.toList3 v
    in  Float.map3 (/ m) v

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon outerRadius
 where
  outerRadius = 2.5

-- Visually, the brightness of the normal distribution 2.5 standard
-- deviations from its mean is negligible.

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

vertShader ::
  Vars_Position ->
  Vars_OverseerRemainsInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_OverseerRemainsInstance{..}
  Vars_StandardUniforms{..} =
    let screenPos =
          su_viewScale
            * (ori_modelTrans - su_viewCenter + ori_modelScale *. position)
        pipelineVars =
          PipelineVars
            { pos = position
            , color = ori_color
            }
    in  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r2 <- bind $ let x = x_ pos; y = y_ pos in x * x + y * y
  opacity <- bind $ exp (-r2)
  pure $ vec4 (xyz_ color, w_ color * opacity)
