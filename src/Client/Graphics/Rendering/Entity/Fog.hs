{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Client.Graphics.Rendering.Entity.Fog (
  loadFog,
  drawFog,
  FogVisual,
  FogVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types (GLRecord)
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data FogVisual = FogVisual
  { fv_vao :: VertexArray
  , fv_shaderProg :: ShaderProgram
  , fv_nVertices :: GLsizei
  , fv_uniformLocations :: UniformLocations FogVParams
  }

data FogVParams = FogVParams
  { fvp_viewScale :: Float
  , fvp_viewCenter :: Float2
  , fvp_modelScale :: Float
  , fvp_modelTrans :: Float2
  , fvp_fogRadius :: Float
  , fvp_visibleOpacity :: Float
  , fvp_ticksMod :: Float
  , fvp_lowHealthEffectIntensity :: Float
  , fvp_staticIntensity :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''FogVParams

drawFog :: FogVisual -> FogVParams -> GL ()
drawFog fog params =
  withVertexArray (fv_vao fog) $
    withShaderProgram (fv_shaderProg fog) $ do
      -- Set uniforms
      writeUniforms (fv_uniformLocations fog) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (fv_nVertices fog)

loadFog :: GL FogVisual
loadFog = do
  vao <- setupVertexArrayObject vertices
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniforms <- getUniformLocations shaderProg
  pure $
    FogVisual
      { fv_vao = vao
      , fv_shaderProg = shaderProg
      , fv_nVertices = fromIntegral $ P.length vertices
      , fv_uniformLocations = uniforms
      }

vertices :: [Position]
vertices = map Position $ square 5

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { worldPos :: Expr Float2
  , fogRadius :: Expr Float
  , visibleOpacity :: Expr Float
  , ticksMod :: Expr Float
  , lowHealthEffectIntensity :: Expr Float
  , staticIntensity :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  () ->
  Vars_FogVParams ->
  GLSL (Expr Float2, PipelineVars)
vertShader Vars_Position{position} () Vars_FogVParams{..} = do
  worldPos <- bind $ fvp_modelTrans + fvp_modelScale *. position
  screenPos <- bind $ fvp_viewScale *. (worldPos - fvp_viewCenter)
  pure
    ( screenPos
    , PipelineVars
        { worldPos = worldPos
        , fogRadius = fvp_fogRadius
        , visibleOpacity = fvp_visibleOpacity
        , ticksMod = fvp_ticksMod
        , lowHealthEffectIntensity = fvp_lowHealthEffectIntensity
        , staticIntensity = fvp_staticIntensity
        }
    )

fragShader :: Vars_FogVParams -> PipelineVars -> GLSL (Expr Float4)
fragShader Vars_FogVParams{fvp_modelTrans} PipelineVars{..} = do
  -- Fog
  r <- bind $ length $ worldPos - fvp_modelTrans
  fogOpacity <- bind $ smoothstep (-6) 0 (r - fogRadius)

  -- Static
  staticOpacity <- do
    x <- bind $ x_ worldPos
    y <- bind $ y_ worldPos
    a <-
      bind $
        rand $
          ticksMod
            - 21.0521 * x
            + 10.0672 * y
            + 0.095132 * (x * x `mod'` 1000)
    a3 <- bind $ a * a * a
    b <- bind $ staticIntensity * a3 + (1 - staticIntensity) * 0.125 -- 0.5^3
    truncatedEffectIntensity <-
      bind $ (3 / 2) * max' 0 (lowHealthEffectIntensity - 1 / 3)
    bind $ b * truncatedEffectIntensity

  -- Composition
  opacity <- bind $ max' visibleOpacity $ max' fogOpacity staticOpacity
  pure $ vec4 (0, 0, 0, opacity)

rand :: Expr Float -> Expr Float
rand x =
  let r = 43758.5453123 * sin x
  in  fract' r
