{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | The pattern in the background
module Client.Graphics.Rendering.Entity.Background (
  loadBackground,
  drawBackground,
  BackgroundVisual,
  BackgroundVParams (..),
) where

import Prelude
import Prelude qualified as P

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types (GLRecord)
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data BackgroundVisual = BackgroundVisual
  { bv_vao :: VertexArray
  , bv_shaderProg :: ShaderProgram
  , bv_nVertices :: GLsizei
  , bv_uniformLocations :: UniformLocations BackgroundVParams
  }

data BackgroundVParams = BackgroundVParams
  { bvp_viewScale :: Float
  , bvp_viewCenter :: Float2
  , bvp_modelScale :: Float
  , bvp_modelTrans :: Float2
  , bvp_worldRadius :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''BackgroundVParams

drawBackground :: BackgroundVisual -> BackgroundVParams -> GL ()
drawBackground background params =
  withVertexArray (bv_vao background) $
    withShaderProgram (bv_shaderProg background) $ do
      -- Set uniforms
      writeUniforms (bv_uniformLocations background) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (bv_nVertices background)

loadBackground :: GL BackgroundVisual
loadBackground = do
  vao <- setupVertexArrayObject vertices
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniforms <- getUniformLocations shaderProg
  pure $
    BackgroundVisual
      { bv_vao = vao
      , bv_shaderProg = shaderProg
      , bv_nVertices = fromIntegral $ P.length vertices
      , bv_uniformLocations = uniforms
      }

vertices :: [Position]
vertices = map Position $ square 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {worldPos :: Expr Float2}
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  () ->
  Vars_BackgroundVParams ->
  GLSL (Expr Float2, PipelineVars)
vertShader Vars_Position{position} () Vars_BackgroundVParams{..} = do
  worldPos <- bind $ bvp_modelTrans + bvp_modelScale *. position
  screenPos <- bind $ bvp_viewScale *. (worldPos - bvp_viewCenter)
  pure (screenPos, PipelineVars worldPos)

data LoopState = LoopState
  { offset :: Expr Float2
  , min_dist2_edge_outer :: Expr Float
  , min_dist2_edge_inner :: Expr Float
  }

fragShader :: Vars_BackgroundVParams -> PipelineVars -> GLSL (Expr Float4)
fragShader Vars_BackgroundVParams{..} PipelineVars{worldPos} = do
  let n_circles :: Int
      n_circles = 10

  let n_circles' :: Expr Float
      n_circles' = fromIntegral n_circles
  rot_cos <- bind $ cos (2 * pi / n_circles')
  rot_sin <- bind $ sin (2 * pi / n_circles')

  let stroke_radius = 0.667
      stroke_width = 2 * stroke_radius

  let buffer = 40.0
      overlap = bvp_worldRadius / 30
  offset_magnitude <- bind $ (bvp_worldRadius - buffer) * 0.5 - overlap
  radius_outer <- bind $ (bvp_worldRadius - buffer) * 0.5 + overlap
  radius_inner <- bind $ radius_outer - stroke_width
  radius_outer2 <- bind $ radius_outer * radius_outer
  radius_inner2 <- bind $ radius_inner * radius_inner

  world_diameter2 <- bind (4 * bvp_worldRadius * bvp_worldRadius)
  initLoopState <- do
    offset <- bind (vec2 (offset_magnitude, 0.0))
    pure
      LoopState
        { offset = offset
        , min_dist2_edge_outer = world_diameter2
        , min_dist2_edge_inner = world_diameter2
        }

  LoopState{min_dist2_edge_outer, min_dist2_edge_inner} <-
    flip execStateT initLoopState $
      for_ [1 .. n_circles] $ \_ -> do
        LoopState{offset, min_dist2_edge_outer, min_dist2_edge_inner} <- get

        newLoopState <- lift $ do
          dx <- bind $ x_ worldPos - x_ offset
          dy <- bind $ y_ worldPos - y_ offset
          dist2_center <- bind $ dx * dx + dy * dy
          dist2_edge_outer <- bind $ dist2_center - radius_outer2
          dist2_edge_inner <- bind $ dist2_center - radius_inner2

          -- Comparing squares in a weird way to avoid square roots in the
          -- loop, but at the cost of some distortion (negligible for now)
          new_min_dist2_edge_outer <-
            bind $
              if abs dist2_edge_outer `lt` abs min_dist2_edge_outer
                then dist2_edge_outer
                else min_dist2_edge_outer
          new_min_dist2_edge_inner <-
            bind $
              if abs dist2_edge_inner `lt` abs min_dist2_edge_inner
                then dist2_edge_inner
                else min_dist2_edge_inner
          new_offset <-
            bind $
              vec2
                ( rot_cos * x_ offset - rot_sin * y_ offset
                , rot_sin * x_ offset + rot_cos * y_ offset
                )
          pure
            LoopState
              { offset = new_offset
              , min_dist2_edge_outer = new_min_dist2_edge_outer
              , min_dist2_edge_inner = new_min_dist2_edge_inner
              }

        put newLoopState

  min_dist_edge_outer <-
    bind $ sqrt (max' 0 (min_dist2_edge_outer + radius_outer2)) - radius_outer
  min_dist_edge_inner <-
    bind $ sqrt (max' 0 (min_dist2_edge_inner + radius_inner2)) - radius_inner

  let fuzziness = 0.5
  fill_stroke <-
    bind $
      smoothstep (-fuzziness) fuzziness (min_dist_edge_outer + stroke_radius)
        - smoothstep (-fuzziness) fuzziness (min_dist_edge_outer - stroke_radius)
  mask_stroke <-
    bind $
      1
        - smoothstep (-fuzziness) fuzziness (min_dist_edge_inner + stroke_radius)
        + smoothstep (-fuzziness) fuzziness (min_dist_edge_inner - stroke_radius)
  pattern_mask <- bind $ min' fill_stroke mask_stroke

  v <- bind $ 0.1 * pattern_mask + 0.025
  pure $ vec4 (v, v, v, 1.0)
 where
  ifThenElse = ifThenElse'
