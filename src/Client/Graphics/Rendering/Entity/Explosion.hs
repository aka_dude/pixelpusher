{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Explosion effect for overseer death and flag capture
module Client.Graphics.Rendering.Entity.Explosion (
  loadExplosion,
  drawExplosion,
  ExplosionInstance (..),
  fromOverseerDeathParticle,
  fromFlagExplodeParticle,
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3,
  Float4,
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.SharedVisuals (flagColors)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

-- | Made of a lot of triangles
data ExplosionInstance = ExplosionInstance
  { ei_center :: {-# UNPACK #-} !Float2
  , ei_offsetVel :: {-# UNPACK #-} !Float2
  , ei_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''ExplosionInstance

loadExplosion :: GL (StandardInstancedVisual ExplosionInstance)
loadExplosion =
  loadStandardVisual
    vertices
    vertShaderSrc
    fragShaderSrc
    (fromIntegral maxSparks * (nFlags + fromIntegral C.maxActorsPerGame))
 where
  nFlags = 2

drawExplosion ::
  StandardInstancedVisual ExplosionInstance ->
  StandardVParams ExplosionInstance ->
  GL ()
drawExplosion = drawStandardVisual

--------------------------------------------------------------------------------

maxSparks :: Int
maxSparks = max nOverseerSparks nFlagSparks

nOverseerSparks :: Int
nOverseerSparks = 160

nFlagSparks :: Int
nFlagSparks = 96

data ExplosionPreInstance = ExplosionPreInstance
  { epi_nSparks :: Int
  , epi_bodyRadius :: Float
  , epi_color :: Float3
  , epi_center :: Float2
  , epi_velocity :: Float2
  , epi_ticksElapsed :: Float
  , epi_dragCoeff :: Float
  }

fromOverseerDeathParticle ::
  Maybe ActorID ->
  Maybe Team ->
  Time ->
  Particle OverseerDeathParticle ->
  [ExplosionInstance]
fromOverseerDeathParticle mViewPlayer mViewTeam time particle =
  makeInstance
    ExplosionPreInstance
      { epi_nSparks = nOverseerSparks
      , epi_bodyRadius = Fixed.toFloat (odp_scale odp)
      , epi_color = viewPlayerColor mViewPlayer mViewTeam $ odp_color odp
      , epi_center = Fixed.toFloats $ odp_pos odp
      , epi_velocity = Fixed.toFloats $ odp_vel odp
      , epi_ticksElapsed = fromIntegral $ diffTime time (particle_startTime particle)
      , epi_dragCoeff = 0.025
      }
 where
  odp = particle_type particle

fromFlagExplodeParticle ::
  Maybe Team -> Time -> Particle FlagExplodeParticle -> [ExplosionInstance]
fromFlagExplodeParticle mViewTeam time particle =
  makeInstance
    ExplosionPreInstance
      { epi_nSparks = nFlagSparks
      , epi_bodyRadius = Fixed.toFloat (fep_radius fep)
      , epi_color = flagColors $ viewTeamColor mViewTeam (fep_team fep)
      , epi_center = Fixed.toFloats $ fep_pos fep
      , epi_velocity = Fixed.toFloats $ fep_vel fep
      , epi_ticksElapsed = fromIntegral $ diffTime time (particle_startTime particle)
      , epi_dragCoeff = 0.050
      }
 where
  fep = particle_type particle

makeInstance :: ExplosionPreInstance -> [ExplosionInstance]
makeInstance ExplosionPreInstance{..} =
  let secondsElapsed = epi_ticksElapsed / fromIntegral C.tickRate_hz

      timeDilation = 8
      t =
        epi_ticksElapsed / timeDilation
          + 0.001 -- avoid divison by zero
      a = 3
      s = 2 * t / (1 + a / t)
      ds_dt = let sq x = x * x in 2 * t * (t + 2 * a) / sq (t + a)
  in  flip map [1 .. epi_nSparks] $ \i ->
        let i' = fromIntegral i

            ang = 2 * pi * i' / fromIntegral epi_nSparks
            init_offset = Float.angle ang

            offset_ang =
              2 * pi * rand (Float.map2 (subtract (11.942 * i')) epi_center)
            offset_mag = 0.9 * rand (Float.map2 (+ (20.671 * i')) epi_center)
            launchSpeedFactor = 15
            (init_speed, init_dir) =
              Float.normAndNormalized2 $
                epi_velocity
                  + Float.map2
                    (* launchSpeedFactor)
                    (init_offset + Float.map2 (* offset_mag) (Float.angle offset_ang))

            c = recip $ init_speed * timeDilation
            dragCoeff =
              epi_dragCoeff
                * (0.5 + rand (Float.map2 (subtract (6.015 * i')) epi_center))
            dist = (log (dragCoeff * s + c) - log c) / dragCoeff
            speed = recip (dragCoeff * s + c) * ds_dt / timeDilation

            speedFloor = 0.25
            speedRange = 0.75
            speedOpacity = max 0 $ min 1 $ (speed - speedFloor) / speedRange
            baseColor = Util.fromRGB_A epi_color speedOpacity

            whiteness = exp (-7.2 * secondsElapsed)
            color = Float.map4 (\x -> whiteness + (1 - whiteness) * x) baseColor
            tailFactor = 4.5
        in  ExplosionInstance
              { ei_center =
                  epi_center
                    + Float.map2 (* epi_bodyRadius) init_offset
                    + Float.map2 (* dist) init_dir
              , ei_offsetVel = Float.map2 (* (tailFactor * speed)) init_dir
              , ei_color = color
              }

rand :: Float2 -> Float
rand (Float2 x y) =
  let r = 43758.5453123 * sin (x * 12.9898 + y * 78.233)
  in  r - fromIntegral @Int (floor r)

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position [Float2 1 1, Float2 1 (-1), Float2 0 0]

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars
  { color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_ExplosionInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_ExplosionInstance{..}
  Vars_StandardUniforms{..} = do
    forwardDir <- bind $ normalize ei_offsetVel
    sideDir <- bind $ vec2 (-y_ forwardDir, x_ forwardDir)
    localPos <- bind $ x_ position *. ei_offsetVel + y_ position *. sideDir
    pure $
      let screenPos = su_viewScale * (localPos + ei_center - su_viewCenter)
          pipelineVars = PipelineVars ei_color
      in  (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{color} = pure color
