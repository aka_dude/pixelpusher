{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | The walls of the game world
module Client.Graphics.Rendering.Entity.WorldBounds (
  loadWorldBounds,
  drawWorldBounds,
  WorldBoundsVisual,
  WorldBoundsVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types (GLRecord)
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data WorldBoundsVisual = WorldBoundsVisual
  { wbv_vao :: VertexArray
  , wbv_shaderProg :: ShaderProgram
  , wbv_nVertices :: GLsizei
  , wbv_uniformLocations :: UniformLocations WorldBoundsVParams
  }

data WorldBoundsVParams = WorldBoundsVParams
  { wbvp_viewScale :: Float
  , wbvp_viewCenter :: Float2
  , wbvp_modelScale :: Float
  , wbvp_modelTrans :: Float2
  , wbvp_worldRadius :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''WorldBoundsVParams

--------------------------------------------------------------------------------

drawWorldBounds :: WorldBoundsVisual -> WorldBoundsVParams -> GL ()
drawWorldBounds worldBounds params =
  withVertexArray (wbv_vao worldBounds) $
    withShaderProgram (wbv_shaderProg worldBounds) $ do
      -- Set uniforms
      writeUniforms (wbv_uniformLocations worldBounds) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (wbv_nVertices worldBounds)

loadWorldBounds :: GL WorldBoundsVisual
loadWorldBounds = do
  vao <- setupVertexArrayObject vertices
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniforms <- getUniformLocations shaderProg
  pure $
    WorldBoundsVisual
      { wbv_vao = vao
      , wbv_shaderProg = shaderProg
      , wbv_nVertices = fromIntegral $ P.length vertices
      , wbv_uniformLocations = uniforms
      }

vertices :: [Position]
vertices = map Position $ square 1.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {worldPos :: Expr Float2}
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  () ->
  Vars_WorldBoundsVParams ->
  GLSL (Expr Float2, PipelineVars)
vertShader Vars_Position{position} () Vars_WorldBoundsVParams{..} = do
  worldPos <- bind $ wbvp_modelTrans + wbvp_modelScale *. position
  screenPos <- bind $ wbvp_viewScale *. (worldPos - wbvp_viewCenter)
  pure (screenPos, PipelineVars worldPos)

fragShader :: Vars_WorldBoundsVParams -> PipelineVars -> GLSL (Expr Float4)
fragShader Vars_WorldBoundsVParams{..} PipelineVars{worldPos} = do
  r <- bind $ length worldPos
  dr <- bind $ r - wbvp_worldRadius
  opacity <- bind $ smoothstep (-0.5) 0.5 dr
  pure $ let v = 0.125 in vec4 (v, v, v, opacity)
