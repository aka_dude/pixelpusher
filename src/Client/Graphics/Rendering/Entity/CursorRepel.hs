{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Cursor for repelling drones
--
-- TODO: Use uniforms rather than instances
module Client.Graphics.Rendering.Entity.CursorRepel (
  loadCursorRepel,
  drawCursorRepel,
  CursorRepelInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CursorRepelInstance = CursorRepelInstance
  { cri_size :: Float
  , cri_center :: {-# UNPACK #-} !Float2
  , cri_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CursorRepelInstance

--------------------------------------------------------------------------------

loadCursorRepel :: GL (StandardInstancedVisual CursorRepelInstance)
loadCursorRepel =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawCursorRepel ::
  StandardInstancedVisual CursorRepelInstance ->
  StandardVParams CursorRepelInstance ->
  GL ()
drawCursorRepel = drawStandardVisual

maxInstances :: Word16
maxInstances = 2 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = coerce $ hexagon 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CursorRepelInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CursorRepelInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ cri_size *. position
    screenPos <- bind $ su_viewScale * (cri_center + pos - su_viewCenter)
    let radius = cri_size
        color = cri_color
    pure (screenPos, PipelineVars{..})

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  let fuzziness = 0.5
  r <- bind $ length pos

  radialFuzziness <- bind $ fuzziness / r
  angle_ <- bind $ atan2 (y_ pos) (x_ pos)
  angleMask <-
    bind $
      1
        - smoothstepCentered
          0.3125
          radialFuzziness
          (abs (mod' (angle_ / (pi / 3)) 1 - 0.5))

  outerRing <-
    let outerRadius = radius
        innerRadius = radius - 1
     in bind $
          (1 - smoothstepCentered outerRadius fuzziness r)
            * smoothstepCentered innerRadius fuzziness r
  innerRing <-
    let outerRadius = radius - 2
        innerRadius = radius - 2.5
     in bind $
          (1 - smoothstepCentered outerRadius fuzziness r)
            * smoothstepCentered innerRadius fuzziness r
  opacity <- bind $ max' outerRing innerRing * angleMask
  pure $ vec4 (xyz_ color, opacity * w_ color)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)
