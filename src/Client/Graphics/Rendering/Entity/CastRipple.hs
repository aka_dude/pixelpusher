{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | An expanding circle
module Client.Graphics.Rendering.Entity.CastRipple (
  loadCastRipple,
  drawCastRipple,
  CastRippleInstance (..),
) where

import Prelude hiding (length)

import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Constants qualified as CC
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CastRippleInstance = CastRippleInstance
  { cri_size :: Float
  , cri_center :: Float2
  , cri_color :: Float4
  , cri_ringWidth :: Float
  , cri_progress :: Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CastRippleInstance

loadCastRipple :: GL (StandardInstancedVisual CastRippleInstance)
loadCastRipple =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc CC.maxVisiblePings

drawCastRipple ::
  StandardInstancedVisual CastRippleInstance ->
  StandardVParams CastRippleInstance ->
  GL ()
drawCastRipple = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ hexagon 1.1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

vertShader ::
  Vars_Position ->
  Vars_CastRippleInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CastRippleInstance{..}
  Vars_StandardUniforms{..} =
    let screenPos =
          su_viewScale * (cri_center - su_viewCenter + cri_size *. position)
        pipelineVars =
          PipelineVars
            { size = cri_size
            , pos = position
            , color = cri_color
            , ringWidth = cri_ringWidth
            , progress = cri_progress
            }
    in  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { size :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , ringWidth :: Expr Float
  , progress :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos * size

  inner_bound <- bind $ progress * size + ringWidth
  outer_bound <- bind $ inner_bound + ringWidth

  let fuzziness = 0.25
  innerMask <- bind $ smoothstep (-fuzziness) fuzziness (r - inner_bound)
  outerMask <- bind $ smoothstep (-fuzziness) fuzziness (r - outer_bound)
  mask <- bind $ innerMask - outerMask

  progress2 <- bind $ progress * progress
  opacity_factor <-
    bind $ min' 1 $ (1 - progress2) * inversesqrt (0.01 + progress * 16)

  opacity <- bind $ mask * opacity_factor
  pure $ vec4 (xyz_ color, w_ color * opacity)
