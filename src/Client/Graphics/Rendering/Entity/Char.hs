{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Lines of text
module Client.Graphics.Rendering.Entity.Char (
  loadChar,
  drawChar,
  CharVisual,
  CharVParams (..),
  CharLine,
  CharLine',
  CharAlignment (..),
) where

import Data.Char (ord)
import Data.Generics.Product.Fields (field)
import Data.IntMap.Strict qualified as IM
import Data.Maybe (mapMaybe)
import Data.Proxy
import Data.Traversable (mapAccumL)
import Foreign
import GHC.Generics
import Graphics.GL.Types
import Lens.Micro.Platform.Custom hiding (chars)

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)
import Pixelpusher.Custom.Float qualified as Float

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL hiding (step)
import Client.Graphics.GL.Types
import Client.Graphics.GL.Uniform
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data CharVisual = CharVisual
  { cv_vao :: VertexArray
  , cv_shaderProg :: ShaderProgram
  , cv_nVertices :: GLsizei
  , cv_instanceBuffer :: Buffer
  , cv_instanceBufferCapacityCount :: Word16
  , cv_uniformLocations :: UniformLocations CharUniforms
  }

data CharVParams = CharVParams
  { cvp_fontAtlas :: FontAtlas
  , cvp_screenWidth :: Float
  , cvp_lines :: [CharLine]
  }

type CharLine = CharLine' Float4

type CharLine' c = (CharAlignment, Float2, [(c, String)])
-- ^ [(alignment, screen position, [(colour, text)]]

data CharAlignment = CharAlignLeft | CharAlignCenter | CharAlignRight

data CharUniforms = CharUniforms
  { cu_screenWidth :: Float
  , cu_textureSize :: Float2
  }
  deriving stock (Generic, Show)
  deriving anyclass (GLRecord, GLUniform)

deriveGLSLVars ''CharUniforms

data CharInstance = CharInstance
  { ci_screenPos :: {-# UNPACK #-} !Float2 -- pixels, bottom left
  , ci_texturePos :: {-# UNPACK #-} !Float -- pixels, bottom left
  , ci_charSize :: {-# UNPACK #-} !Float2 -- pixels
  , ci_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic, Show)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''CharInstance

--------------------------------------------------------------------------------

drawChar :: CharVisual -> CharVParams -> GL ()
drawChar visual params = do
  glActiveTexture GL_TEXTURE0
  withVertexArray (cv_vao visual) $
    withShaderProgram (cv_shaderProg visual) $ do
      let atlas = cvp_fontAtlas params
          charMap = fa_charMap atlas

      -- Set uniforms
      let uniforms =
            CharUniforms
              { cu_screenWidth = cvp_screenWidth params
              , cu_textureSize =
                  Float.fromVec2 $
                    fromIntegral <$> fa_textureSize atlas
              }
      writeUniforms (cv_uniformLocations visual) uniforms

      -- Generate instances
      let alignedCharInstances =
            concatMap (uncurry3 (makeInstances charMap)) $ cvp_lines params

      -- Set instance data
      let capacityCount = fromIntegral $ cv_instanceBufferCapacityCount visual
          instances = take capacityCount alignedCharInstances
          nInstances = Prelude.length instances
          bytes =
            fromIntegral $
              nInstances * sum (fieldSizes @CharInstance) * sizeOfGLfloat

      instanceData <- toFloatsInstanced instances
      withBuffer GL_ARRAY_BUFFER (cv_instanceBuffer visual) $
        glBufferSubData
          GL_ARRAY_BUFFER
          0
          bytes
          instanceData

      -- Draw
      withTexture GL_TEXTURE_2D (fa_texture atlas) $
        glDrawArraysInstanced
          GL_TRIANGLE_STRIP
          0
          (cv_nVertices visual)
          (fromIntegral nInstances)
 where
  makeInstances ::
    IM.IntMap AtlasChar ->
    CharAlignment ->
    Float2 ->
    [(Float4, String)] ->
    [CharInstance]
  makeInstances charMap align screenPos coloredText =
    let chars =
          (mapMaybe . traverse) (flip IM.lookup charMap . ord) $
            concatMap (\(c, str) -> zip (repeat c) str) coloredText
        initialOrigin = screenPos
        (finalOrigin, rawCharInstances) = mapAccumL step initialOrigin chars
        advance = finalOrigin ^. _1 - initialOrigin ^. _1
        alignedCharInstances = case align of
          CharAlignLeft -> rawCharInstances
          CharAlignCenter ->
            rawCharInstances
              & traverse . field @"ci_screenPos" . _1 -~ (0.5 * advance)
          CharAlignRight ->
            rawCharInstances
              & traverse . field @"ci_screenPos" . _1 -~ advance
    in  alignedCharInstances

  step :: Float2 -> (Float4, AtlasChar) -> (Float2, CharInstance)
  step origin (color, atlasChar) =
    let charDims = ac_charDims atlasChar
        Float2 w h = cd_size charDims
        charInstance =
          CharInstance
            { ci_screenPos =
                origin -- pixels, bottom left
                  + cd_bearing charDims
                  + Float2 0 (-h)
            , ci_texturePos = ac_atlasPosition atlasChar -- pixels, bottom left
            , ci_charSize = Float2 w h -- pixels
            , ci_color = color
            }
    in  (origin & _1 +~ cd_advance charDims, charInstance)

--------------------------------------------------------------------------------

loadChar :: GL CharVisual
loadChar = do
  let instanceBufferSizeCount = 2048 + 1024
  (vao, instanceBuffer) <-
    setupVertexArrayObjectInstanced
      instanceBufferSizeCount
      vertices
      (Proxy @CharInstance)
  shaderProg <- makeShaderProgram vertShaderSrc fragShaderSrc
  uniformLocs <- getUniformLocations shaderProg
  pure
    CharVisual
      { cv_vao = vao
      , cv_shaderProg = shaderProg
      , cv_nVertices = fromIntegral $ Prelude.length vertices
      , cv_instanceBuffer = instanceBuffer
      , cv_instanceBufferCapacityCount = instanceBufferSizeCount
      , cv_uniformLocations = uniformLocs
      }

vertices :: [Position]
vertices =
  map (Position . uncurry Float2) [(0, 0), (0, 1), (1, 0), (1, 1)]

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { texCoords :: Expr Float2
  , textColor :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_CharInstance ->
  Vars_CharUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_CharInstance{..}
  Vars_CharUniforms{..} = do
    screenRelPos <- bind $ position * ci_charSize
    screenPos <-
      bind $ (ci_screenPos + screenRelPos) .* (2 / cu_screenWidth) .+ (-1)
    let pipelineVars =
          PipelineVars
            { texCoords =
                vec2
                  ( (ci_texturePos + x_ position * x_ ci_charSize) / x_ cu_textureSize
                  , (y_ ci_charSize - y_ position * y_ ci_charSize) / y_ cu_textureSize
                  )
            , textColor = ci_color
            }
    pure (screenPos, pipelineVars)

fragShader :: Vars_CharUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  atlasTexture <- getTexture2D
  sampled <- bind $ r_ $ sampleTexture2D atlasTexture texCoords
  pure $ vec4 (xyz_ textColor, w_ textColor * sampled)

--------------------------------------------------------------------------------
-- Helpers

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c
