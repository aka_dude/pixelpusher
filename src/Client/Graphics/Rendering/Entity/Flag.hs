{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Flag entities (for capture the flag)
module Client.Graphics.Rendering.Entity.Flag (
  loadFlag,
  drawFlag,
  FlagInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

data FlagInstance = FlagInstance
  { fi_size :: Float
  , fi_center :: {-# UNPACK #-} !Float2
  , fi_color :: {-# UNPACK #-} !Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''FlagInstance

loadFlag :: GL (StandardInstancedVisual FlagInstance)
loadFlag =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances

drawFlag ::
  StandardInstancedVisual FlagInstance ->
  StandardVParams FlagInstance ->
  GL ()
drawFlag = drawStandardVisual

maxInstances :: Word16
maxInstances =
  2 -- 2 teams
    * ( 1 -- one flag per team
      + 2 -- flag fade in/out effects
      + 1 -- accidentally rendering two flags during flag transitions?
      + 2 -- safety
      )

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = coerce $ hexagon 2.0

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_FlagInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_FlagInstance{..}
  Vars_StandardUniforms{..} = do
    pos <- bind $ fi_size *. position
    screenPos <- bind $ su_viewScale * (fi_center + pos - su_viewCenter)
    let radius = fi_size
        color = fi_color
    pure (screenPos, PipelineVars{..})

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos

  mask <- bind $ smoothstep (-0.5) 0.5 (r - radius)
  s <- bind $ 0.25 * (r - radius)
  bloom <- bind $ 0.4 * exp (-s * s)

  baseOpacity <- bind $ mask * bloom + (1 - mask)
  baseColor <- bind $ vec4 (xyz_ color, baseOpacity * w_ color)

  t <- bind $ r / radius
  t2 <- bind $ t * t
  t4 <- bind $ t2 * t2
  shineMask <- bind $ 0.3 * (1 - mask) * min' 1 t4
  shine <- bind $ vec4 (shineMask, shineMask, shineMask, 0)

  pure $ baseColor + shine
