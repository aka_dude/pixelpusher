{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Overseer entities
module Client.Graphics.Rendering.Entity.Overseer (
  loadOverseer,
  drawOverseer,
  OverseerInstance (..),
  OverseerGLInstance,
  OverseerVParams (..),
  fromOverseerRendering,
  fromOverseerDeathParticle,
) where

import Prelude hiding (atan2, length, pi)
import Prelude qualified

import Data.Bits ((.&.))
import GHC.Generics

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2, Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Graphics.CoordinateSystems (CoordSystem)
import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types
import Client.Graphics.GL.Util (fromFloat22, fromRGB_A, toRGB_A)
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.SDF (isoscelesTrapezoid)
import Client.Graphics.Rendering.SharedVisuals
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Vertices

--------------------------------------------------------------------------------

-- External instance type
-- This type exists to allow specification of overseer visuals without a value
-- of type `RenderingComponent OverseerRendering` (e.g. to demonstrate how
-- overseer health is indicated in the help screen).
data OverseerInstance = OverseerInstance
  { oi_radius :: Float
  , oi_pos :: Float2
  , oi_prevPos1 :: Float2
  , oi_prevPos2 :: Float2
  , oi_prevPos3 :: Float2
  , oi_prevPos4 :: Float2
  , oi_prevPos5 :: Float2
  , oi_prevPos6 :: Float2
  , oi_healthFraction :: Float
  , oi_color :: Float4
  , oi_recentDamage :: Float
  , oi_isInvulnerable :: Bool
  , oi_ridgesScale :: Float
  , oi_angleOffset :: Float
  , oi_psiStormReadySeconds :: Float
  , oi_angleFins :: Float
  , oi_ticksSinceDash :: Maybe Ticks
  }

-- Internal instance type
data OverseerGLInstance = OverseerGLInstance
  { ogi_modelScale :: {-# UNPACK #-} !Float
  , ogi_modelTrans :: {-# UNPACK #-} !Float2
  , ogi_healthFraction :: {-# UNPACK #-} !Float
  , ogi_color :: {-# UNPACK #-} !Float4
  , ogi_shadowOffsets12 :: {-# UNPACK #-} !Float4
  -- ^ offset1: vec2, offset2: vec2
  , ogi_shadowOffsets34 :: {-# UNPACK #-} !Float4
  -- ^ offset3: vec2, offset4: vec2
  , ogi_shadowOffsets56 :: {-# UNPACK #-} !Float4
  -- ^ offset5: vec2, offset6: vec2
  , ogi_ridgesScale :: {-# UNPACK #-} !Float
  , ogi_angleOffset :: {-# UNPACK #-} !Float
  , ogi_psiStormReadySeconds :: {-# UNPACK #-} !Float
  , ogi_angleFins :: {-# UNPACK #-} !Float
  , ogi_secondsSinceDash :: {-# UNPACK #-} !Float
  }
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''OverseerGLInstance

loadOverseer :: GL (StandardInstancedVisual OverseerGLInstance)
loadOverseer =
  loadStandardVisual vertices vertShaderSrc fragShaderSrc maxInstances
 where
  maxInstances = fromIntegral C.maxActorsPerGame

drawOverseer ::
  StandardInstancedVisual OverseerGLInstance -> OverseerVParams -> GL ()
drawOverseer visual params =
  drawStandardVisual visual $
    StandardVParams
      (ovp_coordSystem params)
      (map makeGLInstance $ ovp_instances params)

data OverseerVParams = OverseerVParams
  { ovp_coordSystem :: CoordSystem
  , ovp_instances :: [OverseerInstance]
  }

--------------------------------------------------------------------------------

fromOverseerRendering ::
  Time ->
  Maybe Team ->
  Maybe ActorID ->
  Float ->
  RenderingComponent OverseerRendering ->
  OverseerInstance
fromOverseerRendering time mViewTeam mViewPlayer mouseAngle rc =
  OverseerInstance
    { oi_radius = rendering_radius rc
    , oi_pos = rendering_pos rc
    , oi_prevPos1 = or_prevPos1 ov
    , oi_prevPos2 = or_prevPos2 ov
    , oi_prevPos3 = or_prevPos3 ov
    , oi_prevPos4 = or_prevPos4 ov
    , oi_prevPos5 = or_prevPos5 ov
    , oi_prevPos6 = or_prevPos6 ov
    , oi_healthFraction = or_healthFraction ov
    , oi_color =
        fromRGB_A (viewPlayerColor mViewPlayer mViewTeam (or_color ov)) 1
    , oi_recentDamage = or_recentDamage ov
    , oi_isInvulnerable = or_isInvulnerable ov
    , oi_ridgesScale = case or_team ov of TeamNW -> 1; TeamSE -> 0
    , oi_angleOffset = overseerRotation time (or_entityID ov)
    , oi_psiStormReadySeconds = case mViewPlayer of
        Nothing -> noPsiStormReadySeconds
        Just actorID ->
          if actorID /= or_actorID ov
            then noPsiStormReadySeconds
            else
              fromIntegral (time `diffTime` or_psiStormCooldownUntil ov)
                / (0.5 * fromIntegral C.tickRate_hz)
    , oi_angleFins = mouseAngle
    , oi_ticksSinceDash = or_ticksSinceDash ov
    }
 where
  ov :: OverseerRendering
  ov = rendering_entityRendering rc

fromOverseerDeathParticle ::
  Time ->
  Float ->
  Particle OverseerDeathParticle ->
  OverseerInstance
fromOverseerDeathParticle time mouseAngle particle =
  OverseerInstance
    { oi_radius = Fixed.toFloat (odp_scale ov)
    , oi_pos = pos
    , oi_prevPos1 = pos
    , oi_prevPos2 = pos
    , oi_prevPos3 = pos
    , oi_prevPos4 = pos
    , oi_prevPos5 = pos
    , oi_prevPos6 = pos
    , oi_healthFraction = 0
    , oi_color = Float4 1 1 1 opacity
    , oi_recentDamage = 0
    , oi_isInvulnerable = False
    , oi_ridgesScale = case odp_killedTeam ov of TeamNW -> 1; TeamSE -> 0
    , oi_angleOffset = overseerRotation time (odp_entityID ov)
    , oi_psiStormReadySeconds = noPsiStormReadySeconds
    , oi_angleFins = mouseAngle
    , oi_ticksSinceDash = Nothing
    }
 where
  ov :: OverseerDeathParticle
  ov = particle_type particle

  pos =
    Fixed.toFloats $
      odp_pos ov
        + Fixed.map (* (0.5 * fromIntegral ticksSinceDeath)) (odp_vel ov)
  ticksSinceDeath =
    getTicks $ diffTime time (particle_startTime particle)
  opacity = 0.5 ** max 0 (fromIntegral ticksSinceDeath - 1)

noPsiStormReadySeconds :: Float
noPsiStormReadySeconds = -1

-- Have overseers rotate slowly over time. Vary drones by their `EntityID`
-- (arbitrary choice).
overseerRotation :: Time -> Int -> Float
overseerRotation time eid =
  let ticks = getTicks $ time `diffTime` initialTime
      eidPart = (* 0.125) $ fromIntegral $ eid .&. 127
      timePart = if even eid then t else -t
       where
        t = (2 * Prelude.pi / 4096 *) $ fromIntegral $ ticks .&. 4095
  in  eidPart + timePart

makeGLInstance :: OverseerInstance -> OverseerGLInstance
makeGLInstance ovInstance =
  OverseerGLInstance
    { ogi_modelScale = oi_radius ovInstance
    , ogi_modelTrans = oi_pos ovInstance
    , ogi_healthFraction = oi_healthFraction ovInstance
    , ogi_color =
        let (color, opacity) = toRGB_A (oi_color ovInstance)
        in  fromRGB_A
              (Float.map3 damageFilter' (Float.map3 brighteningFilter color))
              opacity
    , ogi_shadowOffsets12 =
        fromFloat22
          (oi_prevPos1 ovInstance - oi_pos ovInstance)
          (oi_prevPos2 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets34 =
        fromFloat22
          (oi_prevPos3 ovInstance - oi_pos ovInstance)
          (oi_prevPos4 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets56 =
        fromFloat22
          (oi_prevPos5 ovInstance - oi_pos ovInstance)
          (oi_prevPos6 ovInstance - oi_pos ovInstance)
    , ogi_ridgesScale = oi_ridgesScale ovInstance
    , ogi_angleOffset = oi_angleOffset ovInstance
    , ogi_psiStormReadySeconds = oi_psiStormReadySeconds ovInstance
    , ogi_angleFins = oi_angleFins ovInstance
    , ogi_secondsSinceDash =
        maybe
          noSecondsSinceDash
          ((/ fromIntegral C.tickRate_hz) . fromIntegral)
          (oi_ticksSinceDash ovInstance)
    }
 where
  damageFilter' :: Float -> Float
  damageFilter' =
    if oi_isInvulnerable ovInstance
      then invulnerabilityFilter
      else
        let w = damageFilterWeight (oi_recentDamage ovInstance)
        in  \x -> w + (1 - w) * x

-- Slightly brighten overseers colours to make them stand out more.
brighteningFilter :: Float -> Float
brighteningFilter x = let w = 0.1 in (1 - w) * x + w

noSecondsSinceDash :: Float
noSecondsSinceDash = 10

--------------------------------------------------------------------------------

vertices :: [Position]
vertices = map Position $ square 1

vertShaderSrc, fragShaderSrc :: String
(vertShaderSrc, fragShaderSrc) = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , invScale :: Expr Float
  , healthFraction :: Expr Float
  , color :: Expr Float4
  , shadowOffsets12 :: Expr Float4
  , shadowOffsets34 :: Expr Float4
  , shadowOffsets56 :: Expr Float4
  , ridgesScale :: Expr Float
  , angleOffset :: Expr Float
  , psiStormReadySeconds :: Expr Float
  , angleFins :: Expr Float
  , secondsSinceDash :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (ExprVars)

vertShader ::
  Vars_Position ->
  Vars_OverseerGLInstance ->
  Vars_StandardUniforms ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Vars_Position{position}
  Vars_OverseerGLInstance{..}
  Vars_StandardUniforms{..} =
    let lastShadowOffset = zw_ ogi_shadowOffsets56 -- needs updating if more shadows are added
        offsetLength = length lastShadowOffset
        lineRadius = 1.8 * ogi_modelScale
        boxRadiusX = lineRadius + 0.5 * offsetLength
        boxRadiusY = lineRadius
        scaledTranslatedVertex =
          vec2
            ( boxRadiusX * x_ position + 0.5 * offsetLength
            , boxRadiusY * y_ position
            )

        offsetDir =
          ifThenElse'
            (offsetLength `lt` 0.01)
            (vec2 (1, 0))
            (normalize lastShadowOffset)
        rotatedVertex =
          vec2
            ( x_ offsetDir * x_ scaledTranslatedVertex - y_ offsetDir * y_ scaledTranslatedVertex
            , y_ offsetDir * x_ scaledTranslatedVertex + x_ offsetDir * y_ scaledTranslatedVertex
            )

        screenPos = su_viewScale * (rotatedVertex + ogi_modelTrans - su_viewCenter)
        pipelineVars =
          PipelineVars
            { pos = rotatedVertex ./ ogi_modelScale
            , invScale = recip ogi_modelScale
            , healthFraction = ogi_healthFraction
            , color = ogi_color
            , shadowOffsets12 = ogi_shadowOffsets12 ./ ogi_modelScale
            , shadowOffsets34 = ogi_shadowOffsets34 ./ ogi_modelScale
            , shadowOffsets56 = ogi_shadowOffsets56 ./ ogi_modelScale
            , ridgesScale = ogi_ridgesScale
            , angleOffset = ogi_angleOffset
            , psiStormReadySeconds = ogi_psiStormReadySeconds
            , angleFins = ogi_angleFins
            , secondsSinceDash = ogi_secondsSinceDash
            }
    in  pure (screenPos, pipelineVars)

fragShader :: Vars_StandardUniforms -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  -- Constants
  let pi = 3.1415926535897932384626433832795
      fuzziness = 0.5 * invScale

  -- Radial divisions
  let shell_outer_radius = 1.0
      shell_inner_radius = 0.85
      core_outer_radius = 0.8
      core_inner_radius = 0.5

  radius <- octagon pos angleOffset ridgesScale
  body_outer_mask <-
    bind $ 1 - smoothstepCentered shell_outer_radius fuzziness radius

  -- Body
  body_mask <- do
    core_mask <- do
      core_angular_mask <- do
        angle_ <- bind $ atan2 (y_ pos) (x_ pos)
        angle_frac <- bind $ abs angle_ / pi

        inner_ring_fuzziness <-
          let extra_fuzziness_factor = 4
           in bind $ extra_fuzziness_factor * fuzziness / (pi * radius)
        ring_frac <-
          bind $
            -- So that 100% health is displayed as completely full, and 0% health
            -- as completely empty
            healthFraction * (1 + 1.5 * inner_ring_fuzziness) - 0.75 * inner_ring_fuzziness
        bind $ 1 - smoothstepCentered ring_frac inner_ring_fuzziness angle_frac

      bind $ -- core_mask
        smoothstepCentered core_inner_radius fuzziness radius
          * (1 - smoothstepCentered core_outer_radius fuzziness radius)
          * core_angular_mask

    shell_mask <-
      bind $
        smoothstepCentered
          shell_inner_radius
          fuzziness
          radius
          * body_outer_mask

    bind $ shell_mask + core_mask -- body_mask

  -- Bloom
  bloom_mask <- do
    bloom_radius <-
      bind $
        max' 0 $
          max' (radius - shell_outer_radius) (core_inner_radius - radius)
    bind $ 0.4 * exp' (-40 * bloom_radius * bloom_radius) -- bloom_mask

  -- Unpack
  shadowOffset1 <- bind $ xy_ shadowOffsets12
  shadowOffset2 <- bind $ zw_ shadowOffsets12
  shadowOffset3 <- bind $ xy_ shadowOffsets34
  shadowOffset4 <- bind $ zw_ shadowOffsets34
  shadowOffset5 <- bind $ xy_ shadowOffsets56
  shadowOffset6 <- bind $ zw_ shadowOffsets56

  -- Shadows
  shadow_mask <- do
    offset1 <- bind $ pos - shadowOffset1
    shadow1_mask <- shadowOpacity 0.98 0.66 shell_outer_radius offset1 angleOffset ridgesScale
    offset2 <- bind $ pos - shadowOffset2
    shadow2_mask <- shadowOpacity 0.96 0.55 shell_outer_radius offset2 angleOffset ridgesScale
    offset3 <- bind $ pos - shadowOffset3
    shadow3_mask <- shadowOpacity 0.94 0.44 shell_outer_radius offset3 angleOffset ridgesScale
    offset4 <- bind $ pos - shadowOffset4
    shadow4_mask <- shadowOpacity 0.92 0.33 shell_outer_radius offset4 angleOffset ridgesScale
    offset5 <- bind $ pos - shadowOffset5
    shadow5_mask <- shadowOpacity 0.90 0.22 shell_outer_radius offset5 angleOffset ridgesScale
    offset6 <- bind $ pos - shadowOffset6
    shadow6_mask <- shadowOpacity 0.88 0.11 shell_outer_radius offset6 angleOffset ridgesScale
    bind $
      max'
        ( max'
            (max' shadow1_mask shadow2_mask)
            (max' shadow3_mask shadow4_mask)
        )
        (max' shadow5_mask shadow6_mask)

  -- Fins ("spawners" in diep.io, but in this game drones don't spawn from them)
  fins_outer_mask <- finsOuterMask fuzziness angleFins pos
  fins_mask <- finsOpacity 1.00 1.00 fuzziness angleFins pos
  fins_shadow_mask <- do
    fins1 <- finsOpacity 0.98 0.66 fuzziness angleFins (pos - shadowOffset1)
    fins2 <- finsOpacity 0.96 0.55 fuzziness angleFins (pos - shadowOffset2)
    fins3 <- finsOpacity 0.94 0.44 fuzziness angleFins (pos - shadowOffset3)
    fins4 <- finsOpacity 0.93 0.33 fuzziness angleFins (pos - shadowOffset4)
    fins5 <- finsOpacity 0.90 0.22 fuzziness angleFins (pos - shadowOffset5)
    fins6 <- finsOpacity 0.88 0.11 fuzziness angleFins (pos - shadowOffset6)
    bind $
      max'
        (max' (max' fins1 fins2) (max' fins3 fins4))
        (max' fins5 fins6)
  fins_opacity <-
    bind $
      fins_mask
        + (1 - fins_outer_mask) * fins_shadow_mask

  -- Cooldown ready flash
  flashMask <- do
    progress <- bind $ 3 * psiStormReadySeconds

    let flashWidth = 0.5
    moveRadius <- bind $ 1 + flashWidth
    flashCenter <- bind $ -moveRadius + progress * 2 * moveRadius

    s <- bind $ let sqrt2 = 0.7071067811865475 in sqrt2 * (x_ pos + y_ pos)
    bind $
      step (flashCenter - flashWidth) s
        * (1 - step (flashCenter + flashWidth) s)

  -- Composition
  body_and_bloom_mask <- bind $ max' body_mask bloom_mask
  opacity_out <-
    bind $
      body_and_bloom_mask
        + min' (1 - body_and_bloom_mask) (1 - body_outer_mask)
          * max' shadow_mask fins_opacity

  color_out <- do
    dashWeight <- bind $ max' 0 $ min' 1 $ exp (-secondsSinceDash / 0.1)
    baseColor <-
      bind $
        1
          - ( (1 -. xyz_ color .* (1 + 0.12 * dashWeight))
                .* (1 - 0.2 * dashWeight)
            )
    flashColor <- bind $ flashMask +. (1 - flashMask) *. baseColor
    bind $ body_mask *. flashColor + (1 - body_mask) *. baseColor
  baseOpacity <- bind $ w_ color

  pure $ vec4 (color_out, baseOpacity * opacity_out)

shadowOpacity ::
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float2 ->
  Expr Float ->
  Expr Float ->
  GLSL (Expr Float)
shadowOpacity
  scale
  opacity
  shell_outer_radius
  offset
  angleOffset
  ridgesScale = do
    shadow_radius <- octagon offset angleOffset ridgesScale
    scaled_shadow_radius <- bind $ shadow_radius / scale
    shadow_mask <-
      bind $
        smoothstepCentered
          shell_outer_radius
          shadow_fuzziness
          scaled_shadow_radius
    bind $ opacity * (1 - shadow_mask)
   where
    shadow_fuzziness = 0.1

octagon :: Expr Float2 -> Expr Float -> Expr Float -> GLSL (Expr Float)
octagon offset angleOffset ridgesScale = do
  angle_ <- bind $ angleOffset + atan2 (y_ offset) (x_ offset)
  bind $ length offset * (1 + ridgesScale * 0.021 * sin (angle_ * 8))

finsOpacity ::
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float2 ->
  GLSL (Expr Float)
finsOpacity scale opacity fuzziness angleFins pos = do
  dr <- finsSignedDist scale angleFins pos
  bind $
    0.5 * opacity * (1 - finsSmoothstep fuzziness (abs dr))

finsOuterMask ::
  Expr Float ->
  Expr Float ->
  Expr Float2 ->
  GLSL (Expr Float)
finsOuterMask fuzziness angleFins pos = do
  dr <- finsSignedDist 1 angleFins pos
  bind $ 1 - finsSmoothstep fuzziness dr

finsSmoothstep :: Expr Float -> Expr Float -> Expr Float
finsSmoothstep fuzziness =
  smoothstepCentered
    0.0625 -- line width
    fuzziness

finsSignedDist :: Expr Float -> Expr Float -> Expr Float2 -> GLSL (Expr Float)
finsSignedDist scale angleFins pos = do
  s <- bind $ sin angleFins
  c <- bind $ cos angleFins
  rot <- bind $ vec2 (c * x_ pos - s * y_ pos, s * x_ pos + c * y_ pos)
  rot2 <- bind $ ifThenElse' (x_ rot `lt` 0) (-rot) rot
  rot4 <- bind $ ifThenElse' (y_ rot2 `lt` 0) (vec2 (-y_ rot2, x_ rot2)) rot2
  rot4' <- bind $ vec2 (x_ rot4 - y_ rot4, x_ rot4 + y_ rot4) ./ (sqrt 2 * scale)
  x <- bind $ x_ rot4'
  y_offset <- bind $ 0.03 * x * x
  trapSD <-
    isoscelesTrapezoid
      0.520
      0.675
      0.315
      (rot4' - vec2 (0, 0.9 + y_offset))
  finsSD <- bind $ trapSD + 0.030 -- rounded/sharpened corners
  pure finsSD

-- Helper
smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius = smoothstep (cutoff - radius) (cutoff + radius)
