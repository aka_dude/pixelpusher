{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module is the entry point for rendering the game visuals.
module Client.Graphics.Rendering.GameScene (
  renderGameScene,
) where

import Control.Monad.Trans.Reader (ReaderT (..), runReaderT)
import GHC.Float (double2Float)
import Graphics.GL.Core33
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom (both, over)

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Game.GameScene (GameScene (..))

import Client.Config.Keybindings (Keybindings)
import Client.Graphics.GL.Env (GLEnv (..))
import Client.Graphics.GL.Wrapped (runGL)
import Client.Graphics.Rendering.GameUI.Bottom (drawUIBottom)
import Client.Graphics.Rendering.GameUI.Cursor (drawCursor)
import Client.Graphics.Rendering.GameUI.DarkLayer (drawDarkLayer)
import Client.Graphics.Rendering.GameUI.Overlay (EnableRespawnTips (..), drawOverlay)
import Client.Graphics.Rendering.GameUI.Top (drawUITop)
import Client.Graphics.Rendering.GameUI.TutorialOverlay (
  drawTutorialOverlay,
  shouldShowTutorial,
 )
import Client.Graphics.Rendering.GameWorld (drawGameWorld)
import Client.Graphics.Rendering.SceneDecorations (SceneDecorations (..))
import Client.Scene.Template.Menu (drawMenu)

renderGameScene ::
  GLEnv -> GameScene -> Float2 -> Keybindings -> SceneDecorations -> IO ()
renderGameScene glEnv scene cameraPos keybindings decorations = do
  framebufferSize <- GLFW.getFramebufferSize $ gle_window glEnv
  let windowSquareLength = uncurry min framebufferSize

  -- This is different from the framebuffer size
  windowSize <-
    uncurry Float2 . over both fromIntegral
      <$> GLFW.getWindowSize (gle_window glEnv)

  cursorPos <-
    uncurry Float2 . over both double2Float
      <$> GLFW.getCursorPos (gle_window glEnv)

  glClearColor 0 0 0 1 >> glClear GL_COLOR_BUFFER_BIT

  runGL $ do
    drawGameWorld glEnv scene cameraPos decorations windowSquareLength
    flip runReaderT (glEnv, windowSquareLength) $ do
      let showTutorial =
            shouldShowTutorial
              (scene_gameParams scene)
              (sceneDeco_elaspedClientTime decorations)
              && sceneDeco_showTutorial decorations
          showOSCursor = sceneDeco_showOSCursor decorations
      if
          | showTutorial -> do
              drawTutorialOverlay
                (scene_gameParams scene)
                (sceneDeco_elaspedClientTime decorations)
              drawOverlay DisableRespawnTips scene decorations
              drawCursor showOSCursor windowSize cursorPos scene decorations cameraPos
          | Just menuState <- sceneDeco_showMenu decorations -> do
              drawDarkLayer
              drawMenu [] menuState
          | otherwise -> do
              drawUITop scene decorations
              drawUIBottom (scene_time scene) decorations keybindings
              drawOverlay EnableRespawnTips scene decorations
              drawCursor showOSCursor windowSize cursorPos scene decorations cameraPos
