{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

-- | Common arrangements of vertices for drawing 2D shapes.
module Client.Graphics.Rendering.Vertices (
  Position (..),
  Vars_Position (..),
  square,
  hexagon,
  annulus,
  disk,
) where

import GHC.Generics

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.DSL
import Client.Graphics.GL.Types

--------------------------------------------------------------------------------

-- | A wrapper around a 2D vector with the derived boilerplate needed for our
-- shader DSL.
newtype Position = Position {position :: Float2}
  deriving stock (Generic)
  deriving anyclass (GLRecord, GLAttribute)

deriveGLSLVars ''Position

-- | A square. Intended for use with TRIANGLE_STRIP.
square :: Float -> [Float2]
square radius =
  map
    (Float.map2 (* radius) . uncurry Float2)
    [(-1, -1), (-1, 1), (1, -1), (1, 1)]

-- | A hexagon. Intended for use with TRIANGLE_STRIP.
hexagon :: Float -> [Float2]
hexagon radius = map (Float.map2 (* radius) . hexPt) [0, 1, 5, 2, 4, 3]

hexPt :: Int -> Float2
hexPt i =
  let a = 2 * pi * fromIntegral i / 6
      r = 2 / sqrt 3 -- 1 / cos(pi/6)
  in  Float2 (r * cos a) (r * sin a)

-- | An annulus. Intended for use with TRIANGLE_STRIP.
annulus :: Int -> Float -> Float -> [Float2]
annulus n_int innerRadius outerRadius =
  let n_float = fromIntegral n_int
      toAngle x = 2 * pi * x / n_float
      coveringScale = 1 / cos (pi / n_float)
      outerCircle =
        flip map [0 .. n_int] $
          circle (outerRadius * coveringScale) . toAngle . fromIntegral
      innerCircle =
        flip map [0 .. n_int] $
          circle innerRadius . toAngle . (+ 0.5) . fromIntegral
  in  interleave outerCircle innerCircle
 where
  circle r ang = Float2 (r * cos ang) (r * sin ang)

-- | A disk. Intended for use with TRIANGLE_STRIP.
disk :: Int -> Float -> [Float2]
disk n_int outerRadius =
  let innerRadius = 0 in annulus n_int innerRadius outerRadius

--------------------------------------------------------------------------------
-- Helpers

interleave :: [a] -> [a] -> [a]
interleave (x : xs) (y : ys) = x : y : interleave xs ys
interleave xs [] = xs
interleave [] ys = ys
