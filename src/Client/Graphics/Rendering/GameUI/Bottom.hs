{-# LANGUAGE RecordWildCards #-}

module Client.Graphics.Rendering.GameUI.Bottom (
  drawUIBottom,
) where

import Control.Monad.Trans.Reader
import Data.Bool (bool)
import Data.Maybe (catMaybes)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Config.Keybindings
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.Text (shadowText)

--------------------------------------------------------------------------------

data ClientStatus = ClientStatus
  { mRoomID :: Maybe Int
  , mLatency :: Maybe Int
  , isFramePredicted :: Bool
  }

makeRightLines :: ClientStatus -> [(Float3, String)]
makeRightLines ClientStatus{..} =
  map (grey,) $
    reverse $
      catMaybes
        [ if isFramePredicted then Just "." else Nothing
        , flip fmap mLatency $ \latency -> "Ping :: " ++ show latency ++ " ms"
        -- , flip fmap mRoomID $ \roomID -> "Room :: " ++ show roomID
        ]

data ControlStatus = ControlStatus
  { time :: Time
  , keybindings :: Keybindings
  , mPlayerCooldowns :: Maybe PlayerCooldowns
  , mOverseerHasFlag :: Maybe Bool
  , mTeam :: Maybe Team
  }

makeLeftLines :: ControlStatus -> [(Float3, String)]
makeLeftLines ControlStatus{..} =
  reverse
    [ (castColor, psiStormName ++ " :: " ++ showKey (kb_psiStorm keybindings))
    , (dashColor, "Dash :: " ++ showKey (kb_overseerDash keybindings))
    , (dropFlagColor, "Drop flag :: " ++ showKey (kb_dropFlag keybindings))
    , (grey, "Show controls :: hold Ctrl+H")
    ]
 where
  showKey = drop 4 . show

  cooldownTimeColor = bool darkGrey grey . (time >=)
  castColor =
    maybe
      grey
      (cooldownTimeColor . pc_psiStormCooldownUntil)
      mPlayerCooldowns
  dashColor =
    maybe
      grey
      (cooldownTimeColor . pc_overseerDashCooldownUntil1)
      mPlayerCooldowns
  dropFlagColor = maybe darkGrey (bool darkGrey grey) mOverseerHasFlag

  psiStormName = case mTeam of
    Nothing -> "Psionic storm"
    Just team -> case team of
      TeamNW -> "Ionic storm"
      TeamSE -> "Psionic storm"

-- Shared colors
grey, darkGrey :: Float3
grey = let x = 0.5 in Float3 x x x
darkGrey = let x = 0.25 in Float3 x x x

--------------------------------------------------------------------------------

drawUIBottom ::
  Time -> SceneDecorations -> Keybindings -> ReaderT (GLEnv, Int) GL ()
drawUIBottom time decorations keybindings =
  let mPlayerInfo = getPlayerPerspective (sceneDeco_perspective decorations)
      controlStatus =
        ControlStatus
          { time = time
          , keybindings = keybindings
          , mPlayerCooldowns =
              pv_cooldowns . pi_playerView <$> mPlayerInfo
          , mOverseerHasFlag =
              overseerViewHasFlag . pv_overseerView . pi_playerView
                <$> mPlayerInfo
          , mTeam =
              view player_team . pi_player <$> mPlayerInfo
          }
      clientStatus =
        ClientStatus
          { mRoomID = sceneDeco_roomID decorations
          , mLatency = sceneDeco_latency decorations
          , isFramePredicted = sceneDeco_isFramePredicted decorations
          }
  in  drawStatuses (Just controlStatus) (Just clientStatus)

drawStatuses ::
  Maybe ControlStatus -> Maybe ClientStatus -> ReaderT (GLEnv, Int) GL ()
drawStatuses mControlStatus mClientStatus =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let windowSquareLength' = fromIntegral windowSquareLength

    let fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall

        smallSpacing = fromIntegral $ fontSizeSmall `div` 2
        lineSpacing = fromIntegral $ 5 * fontSizeSmall `div` 4

    let makeDisplayLines ::
          CharAlignment ->
          [(Float3, String)] ->
          [(CharAlignment, Float2, [(Float4, String)])]
        makeDisplayLines align = zipWith (makeDisplayLine align) [0 ..]

        makeDisplayLine ::
          CharAlignment ->
          Int ->
          (Float3, String) ->
          (CharAlignment, Float2, [(Float4, String)])
        makeDisplayLine align lineNo (color, text) =
          ( align
          , Float2 pos_x (smallSpacing + fromIntegral lineNo * lineSpacing)
          , [(Util.fromRGB_A color 1, text)]
          )
         where
          pos_x = case align of
            CharAlignRight -> windowSquareLength' - smallSpacing
            CharAlignLeft -> smallSpacing
            CharAlignCenter -> 0.5 * windowSquareLength'
     in drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlasSmall
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                let leftLines = maybe [] makeLeftLines mControlStatus
                    rightLines = maybe [] makeRightLines mClientStatus
                    displayLines =
                      makeDisplayLines CharAlignLeft leftLines
                        ++ makeDisplayLines CharAlignRight rightLines
                in  shadowText
                      (fromIntegral (fa_fontSize fontAtlasSmall))
                      (Float4 0 0 0 1)
                      displayLines
            }
