module Client.Graphics.Rendering.GameUI.TutorialOverlay (
  drawTutorialOverlay,
  shouldShowTutorial,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (for_)

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Time

import Client.Graphics.GL.Env
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.Text (shadowText)

shouldShowTutorial :: GameParams -> Ticks -> Bool
shouldShowTutorial params elapsedTime =
  elapsedTime <= totalTutorialDuration + gp_startSpawnDelayTicks params

totalTutorialDuration :: Ticks
totalTutorialDuration =
  -- this hard-coded value needs to be updated when the timing of instructions
  -- changes
  4 * spacing + 3 * short + long

spacing :: Ticks
spacing = Ticks C.tickRate_hz

short :: Ticks
short = Ticks $ 3 * C.tickRate_hz

long :: Ticks
long = Ticks $ 6 * C.tickRate_hz

drawTutorialOverlay :: GameParams -> Ticks -> ReaderT (GLEnv, Int) GL ()
drawTutorialOverlay params ticksElapsed =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let fontLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontLarge
        lineSpacing = fromIntegral $ (fontSizeLarge * 3) `div` 2

        fontSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontSmall
        smallSpacing = fromIntegral $ fontSizeSmall `div` 2

        screenWidth = fromIntegral windowSquareLength

        x = 0.5 * screenWidth
        y = 0.5 * screenWidth

        lightGrey = Float4 0.75 0.75 0.75 1.0
        grey = Float4 0.5 0.5 0.5 1.0
        darkGrey = Float4 0.3 0.3 0.3 1.0

    let playTicksElapsed = ticksElapsed - gp_startSpawnDelayTicks params
        mStage
          | playTicksElapsed < spacing =
              Nothing
          | playTicksElapsed < spacing + short =
              Just MoveOverseer
          | playTicksElapsed < 2 * spacing + short =
              Nothing
          | playTicksElapsed < 2 * spacing + short + long =
              Just MoveDrones
          | playTicksElapsed < 2 * spacing + 2 * short + long =
              Just DronesHaveInertia
          | playTicksElapsed < 3 * spacing + 2 * short + long =
              Nothing
          | playTicksElapsed < 3 * spacing + 3 * short + long =
              Just ExtraHelp
          | playTicksElapsed < 4 * spacing + 3 * short + long =
              Nothing
          | otherwise =
              Nothing

    for_ mStage $ \stage -> do
      let Instruction{instr_control, instr_action} =
            case stage of
              MoveOverseer -> moveOverseerInstruction
              MoveDrones -> moveDronesInstruction
              DronesHaveInertia -> dronesInertiaInstruction
              ExtraHelp -> extraHelpInstruction

      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_fontAtlas = fontLarge
          , cvp_screenWidth = fromIntegral windowSquareLength
          , cvp_lines =
              shadowText
                (fromIntegral fontSizeLarge)
                (Float4 0 0 0 1)
                [
                  ( CharAlignCenter
                  , Float2 x (y + 3 * lineSpacing)
                  ,
                    [ (lightGrey, instr_control)
                    , if null instr_control
                        then (grey, "")
                        else (grey, ": ")
                    , (grey, instr_action)
                    ]
                  )
                ]
          }

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = fontSmall
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeSmall)
              (Float4 0 0 0 1)
              [
                ( CharAlignLeft
                , Float2 smallSpacing smallSpacing
                , [(darkGrey, "Escape :: Skip tutorial")]
                )
              ]
        }

data Stage = MoveOverseer | MoveDrones | DronesHaveInertia | ExtraHelp

data Instruction = Instruction
  { instr_control :: String
  , instr_action :: String
  }

moveOverseerInstruction :: Instruction
moveOverseerInstruction =
  Instruction
    { instr_control = "WASD"
    , instr_action = "move overseer"
    }

moveDronesInstruction :: Instruction
moveDronesInstruction =
  Instruction
    { instr_control = "Hold left mouse button"
    , instr_action = "move drones"
    }

dronesInertiaInstruction :: Instruction
dronesInertiaInstruction =
  Instruction
    { instr_control = ""
    , instr_action = "Drones are heavy and accelerate slowly"
    }

extraHelpInstruction :: Instruction
extraHelpInstruction =
  Instruction
    { instr_control = "Hold Ctrl+H"
    , instr_action = "show all controls"
    }
