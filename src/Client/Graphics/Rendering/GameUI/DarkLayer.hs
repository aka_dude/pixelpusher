module Client.Graphics.Rendering.GameUI.DarkLayer (
  drawDarkLayer,
) where

import Control.Monad.Trans.Reader

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float4 (Float4),
 )

import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Quad
import Client.Graphics.Rendering.StandardInstancedVisual

drawDarkLayer :: ReaderT (GLEnv, Int) GL ()
drawDarkLayer = ReaderT $ \(glEnv, windowSquareLength) ->
  let screenWidth = fromIntegral windowSquareLength
  in  drawQuad (gle_quad glEnv) $
        StandardVParams
          { svp_coordSystem = CS.centeredViewport windowSquareLength
          , svp_instances =
              pure $
                QuadInstance
                  { qi_size = Float2 screenWidth screenWidth
                  , qi_center = Float2 0 0
                  , qi_color = Float4 0 0 0 0.67
                  }
          }
