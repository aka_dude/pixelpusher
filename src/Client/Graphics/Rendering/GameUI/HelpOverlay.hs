module Client.Graphics.Rendering.GameUI.HelpOverlay (
  drawHelpOverlay,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Word (Word8)
import Unsafe.Coerce (unsafeCoerce)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float4 (Float4),
 )
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util (fromRGB_A)
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.Entity.Drone
import Client.Graphics.Rendering.Entity.Flag
import Client.Graphics.Rendering.Entity.Overseer
import Client.Graphics.Rendering.Entity.PsiStorm
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.GameUI.DarkLayer (drawDarkLayer)
import Client.Graphics.Rendering.SharedVisuals (flagColors)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Text (shadowText, stackedLines_bottomJustify)

drawHelpOverlay :: GameParams -> ReaderT (GLEnv, Int) GL ()
drawHelpOverlay params = do
  (glEnv, windowSquareLength) <- ask

  let renderHelpText helpLines =
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlasSmall
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                shadowText (fromIntegral fontSizeSmall) (Float4 0 0 0 1) $
                  stackedLines_bottomJustify
                    CharAlignLeft
                    bottomLeft_left
                    lineSpacing
                    helpLines
            }
       where
        fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall
        lineSpacing = fromIntegral $ (fontSizeSmall * 3) `div` 2
        bottomLeft_left = Float2 lineSpacing lineSpacing

  drawDarkLayer
  drawHealthDemo params
  drawPsiStormDemo
  drawFlagDemo params
  lift $ renderHelpText basicHelpLines
 where
  basicHelpLines :: [(Float4, String)]
  basicHelpLines =
    [ (pink, "Game mode: Capture the flag")
    , (red, "     Take the other team's flag and carry it to your base to score")
    , (red, "     To score, your flag must be at your base")
    , (red, "     Friends have blueish colors, foes have reddish colors")
    , (blank, "")
    , (lightBlue, "Game controls")
    , (blue, "    Move overseer :: WASD")
    , (blue, "    Attract drones :: Hold left mouse button")
    , (blue, "    Psionic storm :: LeftShift")
    , (blue, "    Overseer dash :: Space")
    , (blue, "    Drop flag :: F")
    , (blank, "")
    , (lightBlue, "In-game UI controls")
    , (blue, "    Set audiovisual options :: Escape")
    , (blue, "    Show player stats (round) :: hold Ctrl+Z")
    , (blue, "    Show player stats (session) :: hold Ctrl+X")
    , (blue, "    Leave game :: Ctrl+Q")
    , (blank, "")
    , (lightBlue, "Advanced game controls")
    , (blue, "    Repel drones :: Hold right mouse button")
    , (blue, "    Charge drone dash :: Release or hold both mouse buttons")
    ]

  pink = Float4 1.00 0.60 0.60 1
  red = Float4 0.80 0.48 0.48 1
  lightBlue = Float4 0.60 0.80 1.00 1
  blue = Float4 0.48 0.64 0.80 1
  blank = Float4 0.00 0.00 0.00 0

drawHealthDemo :: GameParams -> ReaderT (GLEnv, Int) GL ()
drawHealthDemo params =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let screenWidth = fromIntegral windowSquareLength
    drawChar (gle_char glEnv) $
      let font = gle_fontAtlasSmall glEnv
          fontSize = fa_fontSize font
          lineSpacing = fromIntegral $ (fontSize * 5) `div` 4
          x = 0.75 * screenWidth
          y = 0.275 * screenWidth
      in  CharVParams
            { cvp_fontAtlas = font
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                [
                  ( CharAlignCenter
                  , Float2 x y
                  , pure (Float4 0.55 0.55 0.55 1, "Overseers and drones at")
                  )
                ,
                  ( CharAlignCenter
                  , Float2 x (y - lineSpacing)
                  , pure (Float4 0.55 0.55 0.55 1, "100%, 50%, and 10% health")
                  )
                ]
            }

    do
      let r = CC.viewRadius
          hSpacing = 3 * overseerRadius

          x1 = 0.5 * r - 1 * hSpacing
          x2 = 0.5 * r + 0 * hSpacing
          x3 = 0.5 * r + 1 * hSpacing

          y1 = 64 - 0.5 * r
          y2 = y1 + 2 * (overseerRadius + droneRadius)

      drawOverseer (gle_overseer glEnv) $
        OverseerVParams
          { ovp_coordSystem = CS.gameView $ Float2 0 0
          , ovp_instances =
              [ makeOverseer (Float2 x1 y2) 1.0
              , makeOverseer (Float2 x2 y2) 0.5
              , makeOverseer (Float2 x3 y2) 0.1
              ]
          }
      drawDrone (gle_drone glEnv) $
        StandardVParams
          { svp_coordSystem = CS.gameView $ Float2 0 0
          , svp_instances =
              [ makeDrone (Float2 x1 y1) 1.0
              , makeDrone (Float2 x2 y1) 0.5
              , makeDrone (Float2 x3 y1) 0.1
              ]
          }
 where
  overseerRadius :: Float
  overseerRadius = Fixed.toFloat $ gp_overseerRadius $ gp_dynamics params

  makeOverseer :: Float2 -> Float -> OverseerInstance
  makeOverseer center healthFraction =
    OverseerInstance
      { oi_radius = overseerRadius
      , oi_pos = center
      , oi_prevPos1 = center -- no shadow
      , oi_prevPos2 = center -- no shadow
      , oi_prevPos3 = center -- no shadow
      , oi_prevPos4 = center -- no shadow
      , oi_prevPos5 = center -- no shadow
      , oi_prevPos6 = center -- no shadow
      , oi_healthFraction = healthFraction
      , oi_color =
          fromRGB_A (teamDefaultColor (viewTeamColor (Just TeamNW) TeamSE)) 1
      , oi_recentDamage = 0
      , oi_isInvulnerable = False
      , oi_ridgesScale = 0
      , oi_angleOffset = 0
      , oi_psiStormReadySeconds = -1
      , oi_angleFins = 0
      , oi_ticksSinceDash = Nothing
      }

  droneRadius :: Float
  droneRadius = Fixed.toFloat $ gp_droneRadius $ gp_dynamics params

  makeDrone :: Float2 -> Float -> DroneInstance
  makeDrone center healthFraction =
    fromDroneRendering Nothing Nothing initialTime $
      RenderingComponent
        { rendering_pos = center
        , rendering_radius = droneRadius
        , rendering_entityRendering =
            DroneRendering
              { dr_color = Right $ PlayerColor team Color1 actorID
              , dr_healthFraction = healthFraction
              , dr_recentDamage = 0
              , dr_actorID = Just actorID
              , dr_prevPos1 = center
              , dr_prevPos2 = center
              , dr_prevPos3 = center
              , dr_prevPos4 = center
              , dr_prevPos5 = center
              , dr_prevPos6 = center
              , dr_isInvulnerable = False
              , dr_entityID = 0
              , dr_team = team
              , dr_ticksSinceDash = Nothing
              }
        }
   where
    team = TeamNW -- whatever was needed to make the color match the overseer
    actorID = PlayerActor $ unsafeCoerce (0 :: Word8) -- I just quickly need a 'ActorID'

drawPsiStormDemo :: ReaderT (GLEnv, Int) GL ()
drawPsiStormDemo =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let screenWidth = fromIntegral windowSquareLength
    drawChar (gle_char glEnv) $
      let font = gle_fontAtlasSmall glEnv
          x = 0.75 * screenWidth
          y = 0.5 * screenWidth
      in  CharVParams
            { cvp_fontAtlas = font
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                [
                  ( CharAlignCenter
                  , Float2 x y
                  , pure (Float4 0.55 0.55 0.55 1, "Psionic storms damage ALL overseers and drones")
                  )
                ]
            }

    drawPsiStorm (gle_psiStorm glEnv) $
      let psiStormRadius = 144
          r = CC.viewRadius
          x = 0.5 * r
          y = 0.07 * r + psiStormRadius
      in  StandardVParams
            { svp_coordSystem = CS.gameView $ Float2 0 0
            , svp_instances =
                pure
                  PsiStormInstance
                    { psi_modelScale = psiStormRadius
                    , psi_modelTrans = Float2 x y
                    , psi_elapsedSeconds = 0
                    , psi_opacityFactor = 1
                    , psi_bloomColor = Float4 0 0 0 0
                    }
            }

drawFlagDemo :: GameParams -> ReaderT (GLEnv, Int) GL ()
drawFlagDemo params =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let screenWidth = fromIntegral windowSquareLength
    drawChar (gle_char glEnv) $
      let font = gle_fontAtlasSmall glEnv
          x = 0.75 * screenWidth
          y = 0.1 * screenWidth
      in  CharVParams
            { cvp_fontAtlas = font
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                pure
                  ( CharAlignCenter
                  , Float2 x y
                  , pure (Float4 0.55 0.55 0.55 1, "Flags")
                  )
            }
    drawFlag (gle_flag glEnv) $
      StandardVParams
        { svp_coordSystem = CS.gameView $ Float2 0 0
        , svp_instances =
            let r = CC.viewRadius
                flagRadius = Fixed.toFloat $ gp_flagRadius (gp_dynamics params)
                x1 = 0.5 * r - 3 * flagRadius
                x2 = 0.5 * r + 3 * flagRadius
                y = -0.7 * r
            in  [ makeFlag params (Float2 x1 y) TeamNW
                , makeFlag params (Float2 x2 y) TeamSE
                ]
        }

makeFlag :: GameParams -> Float2 -> Team -> FlagInstance
makeFlag params pos team =
  FlagInstance
    { fi_size = Fixed.toFloat $ gp_flagRadius (gp_dynamics params)
    , fi_center = pos
    , fi_color = flip fromRGB_A 1 $ flagColors $ viewTeamColor Nothing team
    }
