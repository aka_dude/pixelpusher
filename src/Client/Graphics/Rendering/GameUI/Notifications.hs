module Client.Graphics.Rendering.GameUI.Notifications (
  makeKillNotifications,
  makeLogNotifications,
  drawNotificationLines,
) where

import Control.Monad.Trans.Reader
import Data.ByteString.Char8 qualified as BS

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float4 (Float4),
 )
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team

import Client.Graphics.GL.Env
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Text (shadowText)

--------------------------------------------------------------------------------

makeKillNotifications ::
  Maybe Team -> KillNotificationParticle -> [(Float4, String)]
makeKillNotifications mViewTeam kill =
  let killerName =
        maybe "  " getPlayerName' $ knp_mKillerName kill
      killedName = getPlayerName' $ knp_killedName kill
      killerColor =
        teamTextColor $ viewTeamColor mViewTeam $ oppositeTeam $ knp_killedTeam kill
  in  [ (killerColor, killerName ++ " > ")
      , (greyTextColor, killedName)
      ]
 where
  getPlayerName' = BS.unpack . getPlayerName

makeLogNotifications :: Maybe Team -> LogMsgParticle -> [(Float4, String)]
makeLogNotifications mViewTeam = \case
  LogMsg_FlagPickup (FlagPickupParticle carrierTeam _ carrierName) ->
    let carrierColor = viewTeamColor mViewTeam carrierTeam
        msg = case mViewTeam of
          Nothing -> "Flag picked up by "
          Just viewerTeam
            | viewerTeam == carrierTeam -> "Rival flag picked up by "
            | otherwise -> "Team flag picked up by "
    in  [ (greyTextColor, msg)
        , (teamTextColor carrierColor, BS.unpack (getPlayerName carrierName))
        ]
  LogMsg_FlagCapture (FlagCaptureParticle carrierTeam carrierName) ->
    let carrierColor = viewTeamColor mViewTeam carrierTeam
        msg = case mViewTeam of
          Nothing -> "Flag captured by "
          Just viewerTeam
            | viewerTeam == carrierTeam -> "Rival flag captured by "
            | otherwise -> "Team flag captured by "
    in  [ (greyTextColor, msg)
        , (teamTextColor carrierColor, BS.unpack (getPlayerName carrierName))
        ]
  LogMsg_FlagRecover (FlagRecoverParticle carrierTeam mCarrierName _) ->
    let carrierColor = viewTeamColor mViewTeam carrierTeam
        msg1 = case mViewTeam of
          Nothing -> "Flag recovered "
          Just viewerTeam
            | viewerTeam == carrierTeam -> "Team flag recovered "
            | otherwise -> "Rival flag recovered "
        (msg2, msg3) = case mCarrierName of
          Nothing -> ("automatically", "")
          Just carrierName -> ("by ", BS.unpack (getPlayerName carrierName))
    in  [ (greyTextColor, msg1 <> msg2)
        , (teamTextColor carrierColor, msg3)
        ]
  LogMsg_AnyMsg msg -> [(greyTextColor, msg)]

greyTextColor :: Float4
greyTextColor = Float4 0.6 0.6 0.6 1

teamTextColor :: TeamColor -> Float4
teamTextColor team = case team of
  TeamRed -> Float4 0.8 0.2 0.2 1
  TeamBlue -> Float4 0.2 0.5 0.8 1

drawNotificationLines ::
  CharAlignment ->
  Float ->
  [[(Float4, String)]] ->
  ReaderT (GLEnv, Int) GL ()
drawNotificationLines align y_top notifications =
  ReaderT $ \(glEnv, windowSquareLength) ->
    let fontAtlas = gle_fontAtlasSmall glEnv
        fontSize = fa_fontSize fontAtlas

        padding = gle_uiPadding glEnv
        lineSpacing = fromIntegral $ (fontSize * 5) `div` 4
        y_positions =
          iterate (subtract lineSpacing) $ y_top - lineSpacing - padding
        pos_x = case align of
          CharAlignLeft -> padding
          CharAlignRight -> fromIntegral windowSquareLength - padding
          CharAlignCenter -> 0.5 * fromIntegral windowSquareLength
        positions = map (Float2 pos_x) y_positions -- right side
    in  drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlas
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                shadowText (fromIntegral fontSize) (Float4 0 0 0 1) $
                  zipWith
                    (\pos text -> (align, pos, text))
                    positions
                    notifications
            }
