{-# LANGUAGE RecordWildCards #-}

module Client.Graphics.Rendering.GameUI.Score (
  drawScore,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (foldl')
import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamScore

import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Text (shadowText)

--------------------------------------------------------------------------------

drawScore :: GameScene -> SceneDecorations -> ReaderT (GLEnv, Int) GL ()
drawScore GameScene{..} SceneDecorations{sceneDeco_perspective} =
  case scene_gamePhase of
    GamePhase_Game _ teamScore ->
      drawTeamScorePlay
        scene_gameParams
        mViewTeam
        teamScore
        (countOverseers (snd <$> scene_players))
    GamePhase_Intermission _ _ _ finalScore ->
      drawTeamScoreIntermission mViewTeam finalScore
 where
  mViewTeam =
    view player_team . pi_player
      <$> getPlayerPerspective sceneDeco_perspective

countOverseers :: Foldable f => f PlayerView -> Team -> (Int, Int)
countOverseers playerViews =
  let aliveCount :!: deadCount =
        foldl' countOverseer (mempty :!: mempty) playerViews
  in  \case
        TeamNW -> (teamScore_nw aliveCount, teamScore_nw deadCount)
        TeamSE -> (teamScore_se aliveCount, teamScore_se deadCount)
 where
  countOverseer ::
    Pair TeamScore TeamScore -> PlayerView -> Pair TeamScore TeamScore
  countOverseer (aliveCount :!: deadCount) playerView =
    let team = pv_team playerView
    in  case pv_overseerView playerView of
          OV_Alive{} ->
            incrementTeamScore team aliveCount :!: deadCount
          OV_Dead{} ->
            aliveCount :!: incrementTeamScore team deadCount

drawTeamScorePlay ::
  GameParams ->
  Maybe Team ->
  TeamScore ->
  (Team -> (Int, Int)) ->
  ReaderT (GLEnv, Int) GL ()
drawTeamScorePlay params mViewTeam teamScore overseerCounts =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let windowSquareLength' = fromIntegral windowSquareLength
        padding = gle_uiPadding glEnv

        fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall

        colorRed = Float3 0.85 0.00 0.00
        colorBlue = Float3 0.00 0.50 1.00
        colorGrey = Float3 0.60 0.60 0.60

        teamColor :: TeamColor -> Float3
        teamColor TeamRed = colorRed
        teamColor TeamBlue = colorBlue

    let lineSpacing = fromIntegral fontSizeSmall
        top = windowSquareLength' - padding - lineSpacing * 3 / 4 -- adjusted by hand
        flags_x = windowSquareLength' / 4
        posFlags = Float2 flags_x top
        requiredScore = gp_flagsToWin params
        fadeFilter = Float.map3 (* 0.25)

        overseers_x = windowSquareLength' / 2
        posOverseers = Float2 overseers_x top
     in drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlasSmall
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeSmall)
                  (Float4 0 0 0 1)
                  [
                    ( CharAlignRight
                    , posFlags
                    , [(Util.fromRGB_A colorGrey 1, "Flags ")]
                    )
                  ,
                    ( CharAlignLeft
                    , posFlags
                    , let color = teamColor (viewTeamColor mViewTeam TeamNW)
                          score = teamScore_nw teamScore
                      in  [
                            ( Util.fromRGB_A color 1
                            , replicate score '/'
                            )
                          ,
                            ( Util.fromRGB_A (fadeFilter color) 1
                            , replicate (max 0 (requiredScore - score)) '/'
                            )
                          ]
                    )
                  ,
                    ( CharAlignLeft
                    , posFlags - Float2 0 lineSpacing
                    , let color = teamColor (viewTeamColor mViewTeam TeamSE)
                          score = teamScore_se teamScore
                      in  [
                            ( Util.fromRGB_A color 1
                            , replicate score '/'
                            )
                          ,
                            ( Util.fromRGB_A (fadeFilter color) 1
                            , replicate (max 0 (requiredScore - score)) '/'
                            )
                          ]
                    )
                  ,
                    ( CharAlignRight
                    , posOverseers
                    , [(Util.fromRGB_A colorGrey 1, "Overseers ")]
                    )
                  ,
                    ( CharAlignLeft
                    , posOverseers
                    , let color = teamColor $ viewTeamColor mViewTeam TeamNW
                          (nAlive, nDead) = overseerCounts TeamNW
                      in  [
                            ( Util.fromRGB_A color 1
                            , replicate nAlive '/'
                            )
                          ,
                            ( Util.fromRGB_A (fadeFilter color) 1
                            , replicate nDead '/'
                            )
                          ]
                    )
                  ,
                    ( CharAlignLeft
                    , posOverseers - Float2 0 lineSpacing
                    , let color = teamColor $ viewTeamColor mViewTeam TeamSE
                          (nAlive, nDead) = overseerCounts TeamSE
                      in  [
                            ( Util.fromRGB_A color 1
                            , replicate nAlive '/'
                            )
                          ,
                            ( Util.fromRGB_A (fadeFilter color) 1
                            , replicate nDead '/'
                            )
                          ]
                    )
                  ]
            }

drawTeamScoreIntermission ::
  Maybe Team -> TeamScore -> ReaderT (GLEnv, Int) GL ()
drawTeamScoreIntermission mViewTeam finalScore =
  ReaderT $ \(glEnv, windowSquareLength) ->
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        padding = gle_uiPadding glEnv

        windowSquareLength' = fromIntegral windowSquareLength
        center_x = windowSquareLength' / 2
        lineSpacing =
          fromIntegral $
            (`div` 3) $
              (* 2) fontSizeLarge -- adjusted by hand
        top = windowSquareLength' - padding - lineSpacing

        posRight = Float2 center_x top
        posLeft = Float2 center_x top

        colorRed = Float3 0.85 0.00 0.00
        colorBlue = Float3 0.00 0.50 1.00
        opacity = 1

        teamColor :: TeamColor -> Float3
        teamColor TeamRed = colorRed
        teamColor TeamBlue = colorBlue

        scoreTextNW = show $ teamScore_nw finalScore
        scoreTextSE = show $ teamScore_se finalScore
    in  drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = fontAtlasLarge
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeLarge)
                  (Float4 0 0 0 1)
                  [
                    ( CharAlignLeft
                    , posRight
                    ,
                      [
                        ( Util.fromRGB_A
                            (teamColor (viewTeamColor mViewTeam TeamSE))
                            opacity
                        , " " ++ scoreTextSE
                        )
                      ]
                    )
                  ,
                    ( CharAlignRight
                    , posLeft
                    ,
                      [
                        ( Util.fromRGB_A
                            (teamColor (viewTeamColor mViewTeam TeamNW))
                            opacity
                        , scoreTextNW ++ " "
                        )
                      ]
                    )
                  ]
            }
