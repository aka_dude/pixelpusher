module Client.Graphics.Rendering.GameUI.Cursor (
  drawCursor,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (for_)
import Data.Functor ((<&>))
import Lens.Micro.Platform.Custom (view)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2,
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.GameScene (GameScene (..))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus

import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Entity.Circle
import Client.Graphics.Rendering.Entity.CursorAttract
import Client.Graphics.Rendering.Entity.CursorRepel
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.StandardInstancedVisual

--------------------------------------------------------------------------------

drawCursor ::
  ShowOSCursor ->
  Float2 ->
  Float2 ->
  GameScene ->
  SceneDecorations ->
  Float2 ->
  ReaderT (GLEnv, Int) GL ()
drawCursor showOSCursor windowSize cursorPos scene decorations cameraPos = do
  let mOverseerPosPlayer =
        getPlayerPerspective (sceneDeco_perspective decorations)
          <&> \playerInfo ->
            ( Fixed.toFloats $
                overseerViewPosition $
                  pv_overseerView (pi_playerView playerInfo)
            , pi_player playerInfo
            )
  drawCursorHelper
    showOSCursor
    windowSize
    cursorPos
    (Fixed.toFloat (gp_controlRadius (gp_dynamics (scene_gameParams scene))))
    cameraPos
    mOverseerPosPlayer

drawCursorHelper ::
  ShowOSCursor ->
  Float2 ->
  Float2 ->
  Float ->
  Float2 ->
  Maybe (Float2, Player) ->
  ReaderT (GLEnv, Int) GL ()
drawCursorHelper
  showOSCursor
  windowSize
  cursorPos
  controlRadius
  cameraPos
  mOverseerPosPlayer =
    ReaderT $ \(glEnv, _) -> do
      -- Note: This function uses screen coordinates, rather than framebuffer
      -- coordinates

      let windowRadius = 0.5 * minimum (Float.toList2 windowSize)
          windowCenter = Float.map2 (* 0.5) windowSize
          windowCoordSystem =
            CS.makeCoordSystem
              windowRadius
              (V2 CS.PreserveAxis CS.FlipAxis)
              windowCenter

      for_ mOverseerPosPlayer $ \(overseerPos, player) ->
        withBlendFunc GL_SRC_ALPHA GL_ONE $ do
          let droneCommand =
                view control_drone $ control_buttons $ player_controls player
              droneCommandPos =
                Fixed.toFloats $ control_worldPos $ player_controls player
          let gameViewCS = CS.gameView cameraPos
              cursorPosGameWorld =
                CS.intoPos gameViewCS $
                  CS.fromPos windowCoordSystem cursorPos
              truncatedCursorPos =
                truncateDroneCommandPos controlRadius overseerPos cursorPosGameWorld
              truncatedCommandPos =
                truncateDroneCommandPos controlRadius overseerPos droneCommandPos

          case showOSCursor of
            ShowOSCursor -> do
              drawGameWorldCursor glEnv cameraPos cursorPosGameWorld droneCommand 0.5
              drawGameWorldCursor glEnv cameraPos truncatedCursorPos droneCommand 0.5
            DoNotShowOSCursor -> do
              drawGameWorldCursor glEnv cameraPos truncatedCommandPos droneCommand 1

truncateDroneCommandPos :: Float -> Float2 -> Float2 -> Float2
truncateDroneCommandPos controlRadius overseerPos commandPos =
  let (size, direction) = Float.normAndNormalized2 (commandPos - overseerPos)
      r = min controlRadius size
  in  Float.map2 (* r) direction + overseerPos

drawGameWorldCursor ::
  GLEnv ->
  Float2 ->
  Float2 ->
  DroneCommand ->
  Float ->
  GL ()
drawGameWorldCursor glEnv cameraPos pos droneCommand opacityFactor = do
  let gameViewCS = CS.gameView cameraPos
  case droneCommand of
    DroneNeutral ->
      drawCircle (gle_circle glEnv) $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CircleInstance
                  { ci_size = 4
                  , ci_center = pos
                  , ci_color = Float4 1 1 1 (0.70 * opacityFactor)
                  }
          }
    DroneAttraction ->
      drawCursorAttract (gle_cursorAttract glEnv) $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CursorAttractInstance
                  { cai_size = 6
                  , cai_center = pos
                  , cai_color = Float4 1 1 1 opacityFactor
                  }
          }
    DroneRepulsion ->
      drawCursorRepel (gle_cursorRepel glEnv) $
        StandardVParams
          { svp_coordSystem = gameViewCS
          , svp_instances =
              pure $
                CursorRepelInstance
                  { cri_size = 6
                  , cri_center = pos
                  , cri_color = Float4 1 1 1 opacityFactor
                  }
          }
