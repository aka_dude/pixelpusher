module Client.Graphics.Rendering.GameUI.Minimap (
  drawMinimap,
) where

import Control.Monad (when)
import Control.Monad.Trans.Reader
import Data.List (partition)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear (V2 (V2))
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.FlagEvents (FlagType)
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Circle
import Client.Graphics.Rendering.Entity.Cross
import Client.Graphics.Rendering.Entity.PingFlash
import Client.Graphics.Rendering.Entity.Ring
import Client.Graphics.Rendering.SharedVisuals (damageFilterWeight)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView

--------------------------------------------------------------------------------

drawMinimap ::
  GameParams ->
  Time ->
  Maybe Player ->
  [RenderingComponent TeamBaseRendering] ->
  [RenderingComponent OverseerRendering] ->
  [Particle OverseerRemainsParticle] ->
  [Particle FlagPickupParticle] ->
  [(Team, Fixed2, FlagType)] ->
  ReaderT (GLEnv, Int) GL Float
drawMinimap
  params
  time
  mPlayer
  teamBases
  overseers
  overseerRemains
  flagCaptures
  flags =
    ReaderT $ \(glEnv, windowSquareLength) -> do
      let windowSquareLength' = fromIntegral windowSquareLength
          minimapCoordSystem' =
            minimapCoordSystem glEnv windowSquareLength'
          centeredViewportCS = CS.centeredViewport windowSquareLength

      let mViewTeam = view player_team <$> mPlayer
          mActorID = view player_actorID <$> mPlayer

      -- Background
      drawCircle (gle_circle glEnv) $
        StandardVParams
          { svp_coordSystem = CS.centeredViewport windowSquareLength
          , svp_instances =
              pure $
                CircleInstance
                  { ci_size = CS.intoScalar minimapCoordSystem' 1
                  , ci_center = CS.intoPos minimapCoordSystem' 0
                  , ci_color = let a = 0.33 in Float4 a a a 0.33
                  }
          }

      -- Team bases
      let teamBaseCircles = flip map teamBases $ \rc ->
            CircleInstance
              { ci_size =
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) (rendering_radius rc)
              , ci_center =
                  CS.intoPos minimapCoordSystem' $
                    CS.fromPos (CS.gameWorld params) (rendering_pos rc)
              , ci_color =
                  let team =
                        tbr_team $ rendering_entityRendering rc
                  in  flip Util.fromRGB_A 0.33 $
                        case viewTeamColor mViewTeam team of
                          TeamRed -> Float3 1 0.5 0.5
                          TeamBlue -> Float3 0.5 0.5 1
              }

      -- Overseers
      let ovTeamColor TeamRed = Float3 0.85 0.00 0.00
          ovTeamColor TeamBlue = Float3 0.00 0.50 1.00

      let (visibleEnemyOverseers, alliedOverseers) =
            case (gp_requestFogOfWar params, fmap (view player_team) mPlayer) of
              (True, Just team) ->
                filterOverseers
                  (or_team . rendering_entityRendering)
                  rendering_pos
                  team
                  Nothing
                  overseers
              _ -> ([], overseers)
          filteredOverseers =
            let (playerOverseers, otherFilteredOverseers) =
                  partition
                    ( (== mActorID)
                        . Just
                        . or_actorID
                        . rendering_entityRendering
                    )
                    (visibleEnemyOverseers <> alliedOverseers)
            in  -- render player overseer first so that it appears below other
                -- overseers
                playerOverseers ++ otherFilteredOverseers

      let (visibleEnemyOverseerRemains, alliedOverseerRemains) =
            case (gp_requestFogOfWar params, fmap (view player_team) mPlayer) of
              (True, Just team) ->
                filterOverseers
                  ( (\(PlayerColor team' _ _) -> team')
                      . orp_color
                      . particle_type
                  )
                  (Fixed.toFloats . orp_pos . particle_type)
                  team
                  (Just (map rendering_pos alliedOverseers))
                  overseerRemains
              _ -> ([], overseerRemains)
          filteredOverseerRemains = visibleEnemyOverseerRemains <> alliedOverseerRemains

      let alliedOverseerVisionRings = flip map alliedOverseers $ \rc ->
            RingInstance
              { ri_modelScale =
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) CC.viewRadius
              , ri_modelTrans =
                  CS.intoPos minimapCoordSystem' $
                    CS.fromPos (CS.gameWorld params) (rendering_pos rc)
              , ri_color = Float4 1 1 1 0.08
              , ri_thickness =
                  CS.intoScalar minimapCoordSystem' 0.01
              , ri_fuzziness = 0.1
              }

      let overseerRemainsRings = flip map filteredOverseerRemains $ \particle ->
            RingInstance
              { ri_modelScale =
                  (3 *) $
                    CS.intoScalar minimapCoordSystem' $
                      CS.fromScalar (CS.gameWorld params) overseerRadius
              , ri_modelTrans =
                  CS.intoPos minimapCoordSystem' $
                    CS.fromPos (CS.gameWorld params) $
                      Fixed.toFloats $
                        orp_pos $
                          particle_type particle
              , ri_color =
                  let PlayerColor team _ _ = orp_color (particle_type particle)
                  in  Util.fromRGB_A
                        (ovTeamColor (viewTeamColor mViewTeam team))
                        1
              , ri_thickness =
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) (overseerRadius / 3)
              , ri_fuzziness = 1
              }
           where
            overseerRadius =
              Fixed.toFloat $ gp_overseerRadius (gp_dynamics params)

      let overseerCircles = flip map filteredOverseers $ \rc ->
            let OverseerRendering{or_team, or_actorID, or_recentDamage} =
                  rendering_entityRendering rc
            in  CircleInstance
                  { ci_size =
                      (3 *) $
                        CS.intoScalar minimapCoordSystem' $
                          CS.fromScalar (CS.gameWorld params) $
                            scalePlayer or_actorID $
                              rendering_radius rc
                  , ci_center =
                      CS.intoPos minimapCoordSystem' $
                        CS.fromPos (CS.gameWorld params) (rendering_pos rc)
                  , ci_color =
                      let color =
                            Float.map3 (\x -> let w = damageFilterWeight or_recentDamage in w + (1 - w) * x) $
                              highlightPlayer or_actorID $
                                ovTeamColor $
                                  viewTeamColor mViewTeam or_team
                      in  Util.fromRGB_A color 1.0
                  }
           where
            highlightPlayer :: ActorID -> Float3 -> Float3
            highlightPlayer = case mActorID of
              Nothing -> \_pid color -> color
              Just actorID -> \pid color ->
                if actorID /= pid
                  then color
                  else Float.map3 brighteningFilter color

            scalePlayer :: ActorID -> Float -> Float
            scalePlayer = case mActorID of
              Nothing -> const id
              Just actorID -> \pid ->
                if actorID /= pid
                  then id
                  else (* 1.25)

            brighteningFilter :: Floating a => a -> a
            brighteningFilter x = let w = 0.5 in (1 - w) * x + w

      drawRing (gle_ring glEnv) $
        StandardVParams
          { svp_coordSystem = centeredViewportCS
          , svp_instances = alliedOverseerVisionRings ++ overseerRemainsRings
          }

      drawCircle (gle_circle glEnv) $
        StandardVParams
          { svp_coordSystem = centeredViewportCS
          , svp_instances = teamBaseCircles ++ overseerCircles
          }

      -- Flags
      when (gp_showFlagsOnMinimap params) $
        drawCross (gle_cross glEnv) $
          StandardVParams
            { svp_coordSystem = centeredViewportCS
            , svp_instances = flip map flags $ \(team, pos, _) ->
                CrossInstance
                  { ci_size =
                      (3 *) $
                        CS.intoScalar minimapCoordSystem' $
                          CS.fromScalar (CS.gameWorld params) $
                            1.2 * Fixed.toFloat (gp_overseerRadius (gp_dynamics params))
                  , ci_center =
                      CS.intoPos minimapCoordSystem' $
                        CS.fromPos (CS.gameWorld params) $
                          Fixed.toFloats pos
                  , ci_color = case viewTeamColor mViewTeam team of
                      TeamRed -> Float4 1 0.2 0.2 1
                      TeamBlue -> Float4 0.2 0.6 1.0 1
                  }
            }

      -- Flag capture flashes
      drawPingFlash (gle_pingFlash glEnv) $
        StandardVParams
          { svp_coordSystem = centeredViewportCS
          , svp_instances =
              flip map flagCaptures $ \particle ->
                let startTime = particle_startTime particle
                    ticksElapsed = getTicks $ time `diffTime` startTime
                    FlagPickupParticle team pos _ = particle_type particle
                in  PingFlashInstance
                      { pfi_size = CS.intoScalar minimapCoordSystem' 1.5
                      , pfi_center =
                          CS.intoPos minimapCoordSystem' $
                            CS.fromPos (CS.gameWorld params) $
                              Fixed.toFloats pos
                      , pfi_color =
                          let color = case viewTeamColor mViewTeam team of
                                TeamRed -> Float3 1 0.2 0.2
                                TeamBlue -> Float3 0.2 0.6 1.0
                          in  Util.fromRGB_A color 0.7
                      , pfi_ticksElapsed = fromIntegral ticksElapsed
                      }
          }

      let y_bottom = view _2 $ CS.intoPos minimapCoordSystem' $ Float2 0 (-1)
      pure y_bottom

minimapCoordSystem :: GLEnv -> Float -> CS.CoordSystem
minimapCoordSystem glEnv windowSquareLength =
  CS.makeCoordSystem radius (V2 CS.PreserveAxis CS.PreserveAxis) center
 where
  radius = 0.1 * windowSquareLength
  topRight = Float2 x x where x = 0.5 * windowSquareLength
  padding = gle_uiPadding glEnv
  center = Float.map2 (subtract (padding + radius)) topRight

filterOverseers ::
  (a -> Team) -> (a -> Float2) -> Team -> Maybe [Float2] -> [a] -> ([a], [a])
filterOverseers getTeam getPos alliedTeam mVisionPoints overseers =
  let (alliedOverseers, enemyOverseers) =
        partition ((==) alliedTeam . getTeam) overseers
      alliedPositions =
        case mVisionPoints of
          Nothing -> map getPos alliedOverseers
          Just visionPoints -> visionPoints
      inAlliedControlRadius pos =
        any
          ((< CC.viewRadius) . Float.norm2 . subtract pos)
          alliedPositions
      visibleEnemyOverseers =
        filter (inAlliedControlRadius . getPos) enemyOverseers
  in  (visibleEnemyOverseers, alliedOverseers)
