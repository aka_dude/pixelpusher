{-# LANGUAGE RecordWildCards #-}

module Client.Graphics.Rendering.GameUI.Top (
  drawUITop,
) where

import Control.Monad.Trans.Reader
import Data.Maybe (mapMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Particles
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Time

import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.Text (leftPad, shadowText)

import Client.Graphics.Rendering.GameUI.Minimap (drawMinimap)
import Client.Graphics.Rendering.GameUI.Notifications (
  drawNotificationLines,
  makeKillNotifications,
  makeLogNotifications,
 )
import Client.Graphics.Rendering.GameUI.Score (drawScore)

--------------------------------------------------------------------------------

-- | UI elements aligned to the top edge of the screen
drawUITop :: GameScene -> SceneDecorations -> ReaderT (GLEnv, Int) GL ()
drawUITop gameScene@GameScene{..} decorations = do
  let mPlayer =
        pi_player <$> getPlayerPerspective (sceneDeco_perspective decorations)
      mViewTeam = view player_team <$> mPlayer

  -- Minimap
  y_minimapBottom <-
    drawMinimap
      scene_gameParams
      scene_time
      mPlayer
      (rcs_teamBases scene_renderingComps)
      (rcs_overseers scene_renderingComps)
      (particles_overseerRemains scene_particles)
      ((mapMaybe . traverse) getFlagPickupMessage $ particles_logMsg scene_particles)
      scene_flags

  -- Flag capture notifications
  asks snd >>= \windowSquareLength -> do
    let y_minimapBottom' =
          -- ack, the minimap position is in a different coordinate system
          view _2 $
            CS.intoPos (CS.viewport windowSquareLength) $
              CS.fromPos (CS.centeredViewport windowSquareLength) $
                Float2 0 y_minimapBottom
    drawNotificationLines CharAlignRight y_minimapBottom' $
      map (makeLogNotifications mViewTeam . particle_type) $
        particles_logMsg scene_particles

  -- Timer
  y_timerBottom <-
    drawTimer scene_gamePhase scene_time scene_roundStart

  -- Kill/death notifications
  drawNotificationLines CharAlignLeft y_timerBottom $
    map (makeKillNotifications mViewTeam . particle_type) $
      particles_killNotification scene_particles

  -- Score
  drawScore gameScene decorations

-- | Returns a lower-bound y-coordinate for the timer text
drawTimer :: GamePhase -> Time -> Time -> ReaderT (GLEnv, Int) GL Float
drawTimer gamePhase time roundStart =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        padding = gle_uiPadding glEnv

        windowSquareLength' = fromIntegral windowSquareLength
        lineSpacing =
          fromIntegral $ (`div` 3) $ (* 2) fontSizeLarge -- adjusted by hand
        y_top = windowSquareLength' - padding - lineSpacing

        posTopLeft = Float2 padding y_top
        colorGrey = Float3 0.60 0.60 0.60
        opacity = 1

        roundTicks = (`diffTime` roundStart) $ case gamePhase of
          GamePhase_Game _ _ -> time
          GamePhase_Intermission roundEnd _ _ _ -> roundEnd
        secondsElapsed = getTicks roundTicks `div` fromIntegral C.tickRate_hz
        (minutes, seconds) = secondsElapsed `divMod` 60
        timeText = leftPad' (show minutes) ++ ":" ++ leftPad' (show seconds)
         where
          leftPad' = leftPad 2 '0'

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = fontAtlasLarge
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeLarge)
              (Float4 0 0 0 1)
              [
                ( CharAlignLeft
                , posTopLeft
                , [(Util.fromRGB_A colorGrey opacity, timeText)]
                )
              ]
        }
    pure y_top
