{-# LANGUAGE BlockArguments #-}

module Client.Graphics.Rendering.GameUI.Overlay (
  Overlay (..),
  EnableRespawnTips (..),
  drawOverlay,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Bifunctor (bimap)
import Data.ByteString.Char8 qualified as BS
import Data.Char (ord)
import Data.IntMap.Strict qualified as IM
import Data.List (partition)
import Data.Maybe (fromMaybe)
import Data.Vector (Vector)
import Data.Vector qualified as Vector
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Util
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStats
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped (GL)
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.Entity.Quad
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.GameUI.HelpOverlay (drawHelpOverlay)
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView
import Client.Graphics.Rendering.Text (shadowText, stackedLines_bottomJustify)

--------------------------------------------------------------------------------
-- Overlays

data Overlay
  = NoOverlay
  | HelpOverlay
  | StatsOverlay PlayerName (Either PlayerStats AggregatePlayerStats)
  | IntermissionOverlay
      Team
      Time -- ^ Next round time

-- Hack
data EnableRespawnTips = EnableRespawnTips | DisableRespawnTips

drawOverlay ::
  EnableRespawnTips ->
  GameScene ->
  SceneDecorations ->
  ReaderT (GLEnv, Int) GL ()
drawOverlay enableRespawnTips scene decorations =
  case chooseOverlay scene decorations of
    -- None
    NoOverlay ->
      -- Respawn message
      case sceneDeco_perspective decorations of
        FreePerspective -> pure ()
        PlayerPerspective playerInfo ->
          case pv_overseerView (pi_playerView playerInfo) of
            OV_Alive{} -> pure ()
            OV_Dead
              OverseerViewDead{ovd_deathTime, ovd_respawnTime} ->
                drawRespawnMessage
                  enableRespawnTips
                  (scene_time scene)
                  ovd_deathTime
                  ovd_respawnTime
    -- Help
    HelpOverlay ->
      drawHelpOverlay (scene_gameParams scene)
    -- Stats
    StatsOverlay playerName playerStats ->
      drawStatsOverlay
        mViewTeam
        playerName
        playerList
        playerStats
    -- Intermission
    IntermissionOverlay winningTeam nextRoundTime ->
      drawIntermissionOverlay
        mViewTeam
        (nextRoundTime `diffTime` scene_time scene)
        winningTeam
        (WIM.map fst (scene_players scene))
 where
  mViewTeam =
    view player_team . pi_player
      <$> getPlayerPerspective (sceneDeco_perspective decorations)
  playerList = map fst (WIM.elems (scene_players scene))

chooseOverlay :: GameScene -> SceneDecorations -> Overlay
chooseOverlay scene decorations =
  case sceneDeco_renderHelpOverlay decorations of
    ShowHelp ->
      HelpOverlay
    HideHelp ->
      case sceneDeco_perspective decorations of
        FreePerspective -> NoOverlay
        PlayerPerspective PlayerInfo{pi_player = player} -> do
          let playerName = view player_name player
              statsOverlay =
                fromMaybe RoundStatsOverlay $
                  sceneDeco_renderStatsOverlay decorations
              stats = case statsOverlay of
                RoundStatsOverlay ->
                  Left $ player_stats player
                CumulativeStatsOverlay ->
                  Right $
                    getAggregateStats (player_stats player)
                      <> player_aggregateStats player
          case scene_gamePhase scene of
            GamePhase_Game _ _ ->
              case sceneDeco_renderStatsOverlay decorations of
                Nothing -> NoOverlay
                Just _ -> StatsOverlay playerName stats
            GamePhase_Intermission _ nextRoundTime winningTeam _teamScore ->
              IntermissionOverlay winningTeam nextRoundTime

drawRespawnMessage ::
  EnableRespawnTips -> Time -> Time -> Time -> ReaderT (GLEnv, Int) GL ()
drawRespawnMessage enableRespawnTips currentTime deathTime respawnTime =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let ticksUntilRespawn = getTicks $ respawnTime `diffTime` currentTime
        secondsUntilRespawn =
          max 1 $ -- hide the fact that we don't check respawns every tick
            ticksUntilRespawn `ceilDiv` fromIntegral C.tickRate_hz

        fontLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontLarge
        lineSpacing = fromIntegral $ (fontSizeLarge * 3) `div` 2

        fontSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontSmall

        screenWidth = fromIntegral windowSquareLength

        x = 0.5 * screenWidth

        lightGrey = Float4 0.75 0.75 0.75 1.0
        grey = Float4 0.5 0.5 0.5 1.0
        darkGrey = Float4 0.25 0.25 0.25 1.0

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = fontLarge
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeLarge)
              (Float4 0 0 0 1)
              [
                ( CharAlignCenter
                , Float2 x (0.5 * screenWidth + 2 * lineSpacing)
                ,
                  [ (grey, "Spawning in ")
                  , (lightGrey, show secondsUntilRespawn)
                  ]
                )
              ]
        }

    case enableRespawnTips of
      DisableRespawnTips -> pure ()
      EnableRespawnTips -> do
        let tipText =
              Vector.unsafeIndex respawnTips $
                fromIntegral (getTime respawnTime) `mod` numRespawnTips
            secondsSinceDeath =
              fromIntegral (currentTime `diffTime` deathTime)
                / fromIntegral C.tickRate_hz
            frac =
              min 1 $
                max 0 $
                  secondsSinceDeath - 0.25 - CC.deathFadeSeconds
            frac2 = frac * frac
            frac4 = frac2 * frac2
            tipOpacity = frac4
            tipColor = darkGrey & _4 %~ (* tipOpacity)
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_fontAtlas = gle_fontAtlasSmall glEnv
            , cvp_screenWidth = fromIntegral windowSquareLength
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeSmall)
                  (Float4 0 0 0 tipOpacity)
                  [
                    ( CharAlignCenter
                    , Float2 x (0.25 * screenWidth + 0 * lineSpacing)
                    , [(tipColor, tipText)]
                    )
                  ]
            }

respawnTips :: Vector String
respawnTips =
  Vector.fromList
    [ "Tip: Collisions deal equal damage to both entities" -- not true when invulnerable
    , "Tip: Harder collisions deal more damage"
    , "Tip: Collision damage cannot exceed the health of either entity"
    , "Tip: Psionic storm damage stacks (with diminishing returns)"
    , "Tip: Overseer regeneration starts slow but accelerates"
    , "Tip: Collision damage stops overseer health regeneration"
    , "Tip: Drones regenerate faster the more damaged they are"
    , "Tip: Drones respawn after a fixed time"
    , "Tip: Drone dash covers less distance than moving continuously"
    , "Tip: Overseers and their drones are briefly invincible after respawning"
    , "Tip: Obstacles repel harder the closer you get to them"
    ]

numRespawnTips :: Int
numRespawnTips = Vector.length respawnTips

--------------------------------------------------------------------------------

drawStatsOverlay ::
  Maybe Team ->
  PlayerName ->
  [Player] ->
  Either PlayerStats AggregatePlayerStats ->
  ReaderT (GLEnv, Int) GL ()
drawStatsOverlay mViewTeam playerName playerList stats = do
  drawDarkLayer
  drawPlayerStats playerName stats
  drawPlayerList mViewTeam $
    flip map playerList $ \p ->
      ( p ^. player_name
      , p ^. player_team
      )

drawPlayerList ::
  Maybe Team ->
  [(PlayerName, Team)] ->
  ReaderT (GLEnv, Int) GL ()
drawPlayerList mViewTeam playerList =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let screenWidth = fromIntegral windowSquareLength
        fontSizeSmall = fa_fontSize $ gle_fontAtlasSmall glEnv

        playerNameLines column playerInfos =
          let playerListSpacing = fromIntegral $ div (fontSizeSmall * 5) 4
          in  flip map (zip [0 ..] playerInfos) $
                \(i, (tag, team)) ->
                  let color = case viewTeamColor mViewTeam team of
                        TeamRed -> Float4 1 0.5 0.5 1
                        TeamBlue -> Float4 0.5 0.75 1.0 1
                      name = BS.unpack $ getPlayerName tag
                  in  ( CharAlignCenter
                      , column - Float2 0 (i * playerListSpacing)
                      , [(color, name)]
                      )

        (nwNames, seNames) = partition ((== TeamNW) . view _2) playerList
        fontSizeLarge =
          fromIntegral $ -- approx. the size of the header
            fa_fontSize $
              gle_fontAtlasLarge glEnv
        top = screenWidth - fontSizeLarge * 2
        columnLeft = Float2 (screenWidth / 4) top
        columnRight = Float2 (screenWidth * 3 / 4) top

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasSmall glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            playerNameLines columnLeft nwNames
              ++ playerNameLines columnRight seNames
        }

drawPlayerStats ::
  PlayerName ->
  Either PlayerStats AggregatePlayerStats ->
  ReaderT (GLEnv, Int) GL ()
drawPlayerStats playerName stats =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    -- Display stats
    let screenWidth = fromIntegral windowSquareLength

    let fontSize = fa_fontSize $ gle_fontAtlasSmall glEnv
        lineSpacing = fromIntegral $ (fontSize * 3) `div` 2
        vSpace = Float2 0 lineSpacing
        bottomLeft_left = Float2 lineSpacing lineSpacing
        bottomLeft_right = Float2 (screenWidth / 2 - lineSpacing) lineSpacing
        bottomRight_left = Float2 (screenWidth / 2 + lineSpacing) lineSpacing
        bottomRight_right = Float2 (screenWidth - lineSpacing) lineSpacing
        bottomCenter = Float2 (screenWidth / 2) lineSpacing

    let aggStats = either getAggregateStats id stats

    let statLinesLeft :: [(String, String)]
        statLinesLeft =
          [
            ( "Flags captured/recovered:"
            , show (getFlagsCaptured aggStats)
                ++ "/"
                ++ show (getFlagsRecovered aggStats)
            )
          ,
            ( "Kills/Deaths:"
            , let kills = getKills aggStats
                  deaths = getDeaths aggStats
              in  concat
                    [ show kills
                    , "/"
                    , show deaths
                    , " = "
                    , if deaths == 0
                        then "inf"
                        else
                          show @Float $
                            roundPlaces 2 $
                              fromIntegral kills / fromIntegral deaths
                    ]
            )
          ,
            ( "Streak:"
            , let current = either (show . getStreakCurrent) (const "--") stats
                  best = show $ getStreakBest aggStats
              in  current ++ " (best " ++ best ++ ")"
            )
          ]

    let statLinesRight :: [(String, String)]
        statLinesRight =
          [
            ( "Damage dealt/taken:"
            , show (getDamageDealt aggStats)
                ++ "/"
                ++ show (getDamageTaken aggStats)
            )
          ,
            ( "Drone/body/storm atk dmg:"
            , show (getDroneHitDamage aggStats)
                ++ "/"
                ++ show (getBodyHitDamage aggStats)
                ++ "/"
                ++ show (getStormHitDamage aggStats)
            )
          ]

    let statsColor = Float4 0.9 0.9 0.9 1
        nameColor = Float4 1 1 1 1

    let charLines =
          concat
            [ stackedLines_bottomJustify
                CharAlignLeft
                bottomLeft_left
                lineSpacing
                $ map ((,) statsColor . fst) statLinesLeft
            , stackedLines_bottomJustify
                CharAlignRight
                bottomLeft_right
                lineSpacing
                $ map ((,) statsColor . snd) statLinesLeft
            , stackedLines_bottomJustify
                CharAlignLeft
                bottomRight_left
                lineSpacing
                $ map ((,) statsColor . fst) statLinesRight
            , stackedLines_bottomJustify
                CharAlignRight
                bottomRight_right
                lineSpacing
                $ map ((,) statsColor . snd) statLinesRight
            ]

    -- Player name
    let maxLines = fromIntegral $ max (length statLinesLeft) (length statLinesRight)
        nameString = BS.unpack $ getPlayerName playerName
        playerLine =
          ( CharAlignCenter
          , bottomCenter + Float.map2 (* maxLines) vSpace
          , [(nameColor, nameString)]
          )

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasSmall glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            shadowText
              (fromIntegral fontSize)
              (Float4 0 0 0 1)
              (playerLine : charLines)
        }

roundPlaces :: RealFrac a => Int -> a -> a
roundPlaces n =
  let m = 10 ^ n
  in  (/ m) . fromIntegral @Int . round . (* m)

--------------------------------------------------------------------------------

drawIntermissionOverlay ::
  Maybe Team ->
  Ticks ->
  Team ->
  WIM.IntMap ActorID Player ->
  ReaderT (GLEnv, Int) GL ()
drawIntermissionOverlay
  mViewTeam
  ticksUntilNextRound
  winningTeam
  players = do
    drawDarkLayer

    (glEnv, windowSquareLength) <- ask
    let screenWidth = fromIntegral windowSquareLength
        color = case viewTeamColor mViewTeam winningTeam of
          TeamRed -> Float3 1.0 0.0 0.0
          TeamBlue -> Float3 0.0 0.5 1.0
        teamName = case winningTeam of
          TeamNW -> "FORMALISTS"
          TeamSE -> "PURISTS"
        spacing = fromIntegral $ fa_fontSize $ gle_fontAtlasLarge glEnv
        x = screenWidth / 2
        y1 = screenWidth * 7 / 8
        y2 = y1 - spacing
        y3 = y2 - spacing

    do
      let y_leaderboard = y3 - spacing * 2
          leaderboardInfo =
            players
              & WIM.toList
              & filter (isActorPlayer . fst)
              & map \(_, p) ->
                ( p ^. player_name
                , p ^. player_team
                , getAggregateStats (player_stats p)
                )
      drawLeaderboard y_leaderboard mViewTeam leaderboardInfo

    lift $ do
      drawChar (gle_char glEnv) $
        let fontSmall = gle_fontAtlasSmall glEnv
            fontSizeSmall = fromIntegral $ fa_fontSize fontSmall
        in  CharVParams
              { cvp_fontAtlas = fontSmall
              , cvp_screenWidth = fromIntegral windowSquareLength
              , cvp_lines =
                  shadowText fontSizeSmall (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y1
                      , pure (Float4 0.7 0.7 0.7 1.0, "winner")
                      )
              }
      drawChar (gle_char glEnv) $
        let fontLarge = gle_fontAtlasLarge glEnv
            fontSizeLarge = fromIntegral $ fa_fontSize fontLarge
        in  CharVParams
              { cvp_fontAtlas = fontLarge
              , cvp_screenWidth = fromIntegral windowSquareLength
              , cvp_lines =
                  shadowText fontSizeLarge (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y2
                      , pure (Util.fromRGB_A color 1, teamName)
                      )
              }

      let countdownSeconds =
            succ $ (fromIntegral ticksUntilNextRound - 1) `div` C.tickRate_hz
      drawChar (gle_char glEnv) $
        let fontRegular = gle_fontAtlasRegular glEnv
            fontSizeRegular = fromIntegral $ fa_fontSize fontRegular
            leftPad xs =
              replicate (max 0 (2 - length xs)) '0' ++ xs
        in  CharVParams
              { cvp_fontAtlas = fontRegular
              , cvp_screenWidth = fromIntegral windowSquareLength
              , cvp_lines =
                  shadowText fontSizeRegular (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y3
                      , pure $
                          (,) (Float4 0.5 0.5 0.5 1.0) $
                            concat
                              [ "a new round will start in "
                              , leftPad (show countdownSeconds)
                              , " seconds"
                              ]
                      )
              }

drawLeaderboard ::
  Float ->
  Maybe Team ->
  [(PlayerName, Team, AggregatePlayerStats)] ->
  ReaderT (GLEnv, Int) GL ()
drawLeaderboard y_top mViewTeam playerStats =
  ReaderT $ \(glEnv, windowSquareLength) -> do
    let fontSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontSmall
        testChar = '0'
        smallCharWidth =
          cd_advance $
            ac_charDims $
              expectJust ("font atlas missing character: " ++ [testChar]) $
                IM.lookup (ord testChar) $
                  fa_charMap fontSmall

        rowHeight =
          fromIntegral @Int $
            round @Double $
              (\x -> x * 9 / 8) $
                fromIntegral $
                  fa_fontSize $
                    gle_fontAtlasSmall glEnv
        y_row rowNo = y_top - rowHeight * fromIntegral rowNo
        padding = gle_uiPadding glEnv

        totalWidth =
          2 * padding
            + nameWidth
            + fromIntegral (length statsFieldLabels) * colWidth
        nameWidth = smallCharWidth * fromIntegral maxNameLength
        colWidth =
          fromIntegral @Int $
            ceiling $
              fromIntegral statsColumnChars * smallCharWidth

        x_nameStatsDivide =
          nameWidth
            + 0.5 * (fromIntegral windowSquareLength - totalWidth)
        x_statsLeft = x_nameStatsDivide + 0.5 * padding
        x_namesRight = x_nameStatsDivide - 0.5 * padding
        x_statsCol colNo =
          x_statsLeft + 0.5 * colWidth + colWidth * fromIntegral colNo

    let statsCharLines :: Int -> [String] -> [CharLine]
        statsCharLines rowNo fields =
          flip map (zip @Int [0 ..] fields) $ \(colNo, field) ->
            (CharAlignCenter, Float2 (x_statsCol colNo) (y_row rowNo), [(white, field)])

    let rowCharLines ::
          Int ->
          (PlayerName, Team, AggregatePlayerStats) ->
          [CharLine]
        rowCharLines rowNo (name, team, stats) =
          let statsLines = statsCharLines rowNo $ statsFields stats
              nameLine =
                ( CharAlignRight
                , Float2 x_namesRight (y_row rowNo)
                ,
                  [
                    ( case viewTeamColor mViewTeam team of
                        TeamRed -> red
                        TeamBlue -> blue
                    , BS.unpack $ getPlayerName name
                    )
                  ]
                )
          in  nameLine : statsLines

    let (nwStats, seStats) =
          partition ((== TeamNW) . view _2) playerStats
        (labels, sublabels) = unzip statsFieldLabels
        labelCharLines = statsCharLines 0 labels
        sublabelCharLines = statsCharLines 1 sublabels
        nwCharLines = concat $ zipWith rowCharLines [3 ..] nwStats
        seCharLines =
          concat $
            zipWith rowCharLines [3 + 1 + length nwStats ..] seStats
        charLines =
          concat
            [labelCharLines, sublabelCharLines, nwCharLines, seCharLines]

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_fontAtlas = gle_fontAtlasSmall glEnv
        , cvp_screenWidth = fromIntegral windowSquareLength
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeSmall)
              (Float4 0 0 0 1)
              charLines
        }
 where
  white = Float4 0.7 0.7 0.7 1
  blue = Float4 0.5 0.75 1.0 1
  red = Float4 1 0.5 0.5 1

statsColumnChars :: Int
statsColumnChars =
  maximum $ map (uncurry max . bimap length length) statsFieldLabels

statsFieldLabels :: [(String, String)] -- (row 1, row 2)
statsFieldLabels =
  [ ("", "Captures")
  , -- , ("", "Recoveries")
    ("", "Kills")
  , ("", "Deaths")
  , -- , ("", "Streak")
    ("Damage", "dealt")
  , ("Damage", "taken")
  ]

statsFields :: AggregatePlayerStats -> [String]
statsFields stats =
  [ show (getFlagsCaptured stats)
  , -- , show (getFlagsRecovered stats)
    show (getKills stats)
  , show (getDeaths stats)
  , -- , show (getStreakBest stats)
    show (getDamageDealt stats)
  , show (getDamageTaken stats)
  ]

--------------------------------------------------------------------------------
-- Helpers

drawDarkLayer :: ReaderT (GLEnv, Int) GL ()
drawDarkLayer = ReaderT $ \(glEnv, windowSquareLength) ->
  let screenWidth = fromIntegral windowSquareLength
  in  drawQuad (gle_quad glEnv) $
        StandardVParams
          { svp_coordSystem = CS.centeredViewport windowSquareLength
          , svp_instances =
              pure $
                QuadInstance
                  { qi_size = Float2 screenWidth screenWidth
                  , qi_center = Float2 0 0
                  , qi_color = Float4 0 0 0 0.67
                  }
          }
