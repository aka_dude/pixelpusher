{-# LANGUAGE RecordWildCards #-}

module Client.Graphics.Rendering.GameWorld (
  drawGameWorld,
) where

import Control.Arrow ((&&&))
import Control.Monad (guard, when)
import Data.ByteString.Char8 qualified as BS
import Data.Foldable (foldl', for_)
import Data.Maybe (catMaybes, fromMaybe, mapMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.FlagComponent (FlagConditionStatus (..))
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team (Team, oppositeTeam)
import Pixelpusher.Game.TeamBase (teamBaseLocation)
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Graphics.CoordinateSystems qualified as CS
import Client.Graphics.GL.Env
import Client.Graphics.GL.Util qualified as Util
import Client.Graphics.GL.Wrapped
import Client.Graphics.Rendering.Entity.Background
import Client.Graphics.Rendering.Entity.CastRipple
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.Entity.Circle
import Client.Graphics.Rendering.Entity.ControlPoint
import Client.Graphics.Rendering.Entity.CooldownBar
import Client.Graphics.Rendering.Entity.Dash
import Client.Graphics.Rendering.Entity.Drone
import Client.Graphics.Rendering.Entity.DroneControlRadius
import Client.Graphics.Rendering.Entity.Explosion
import Client.Graphics.Rendering.Entity.Explosion qualified as Explosion
import Client.Graphics.Rendering.Entity.Flag
import Client.Graphics.Rendering.Entity.Fog
import Client.Graphics.Rendering.Entity.Overseer
import Client.Graphics.Rendering.Entity.Overseer qualified as Overseer
import Client.Graphics.Rendering.Entity.OverseerRemains
import Client.Graphics.Rendering.Entity.PsiStorm
import Client.Graphics.Rendering.Entity.SoftObstacle
import Client.Graphics.Rendering.Entity.Spark
import Client.Graphics.Rendering.Entity.TeamBase
import Client.Graphics.Rendering.Entity.WorldBounds
import Client.Graphics.Rendering.SceneDecorations
import Client.Graphics.Rendering.SharedVisuals (flagColors)
import Client.Graphics.Rendering.StandardInstancedVisual
import Client.Graphics.Rendering.TeamView

--------------------------------------------------------------------------------

drawGameWorld ::
  GLEnv ->
  GameScene ->
  Float2 ->
  SceneDecorations ->
  Int ->
  GL ()
drawGameWorld glEnv scene rawCameraPos decorations windowSquareLength = do
  -- Unpacking
  let gameParams = scene_gameParams scene
  let particles = scene_particles scene
  let time = scene_time scene

  let mPlayerInfo = getPlayerPerspective $ sceneDeco_perspective decorations
      mActorID = pi_actorID <$> mPlayerInfo
      mPlayer = pi_player <$> mPlayerInfo
      mPlayerView = pi_playerView <$> mPlayerInfo
      mViewTeam = view player_team <$> mPlayer

  -- Screenshake
  let screenshakeOffset =
        case mActorID of
          Nothing -> 0
          Just actorID ->
            let myScreenshakeParticles =
                  filter
                    ((== actorID) . sp_actorID . particle_type)
                    (particles_screenshake particles)
                vectors = flip map myScreenshakeParticles $ \p ->
                  let ScreenshakeParticle{..} = particle_type p
                      t =
                        fromIntegral (time `diffTime` particle_startTime p)
                          / fromIntegral C.tickRate_hz
                      maxDamage = 32 -- roughly overseer headbutt damage
                      hitStrengthFrac =
                        min (Fixed.toFloat sp_damage) maxDamage / maxDamage
                      magnitude0 =
                        let smallMag0 = 4
                            largeMag0 = 8
                        in  smallMag0
                              + hitStrengthFrac * (largeMag0 - smallMag0)
                      decayRate =
                        let decayRateFast = 20
                            decayRateSlow = 5
                        in  decayRateFast
                              + hitStrengthFrac * (decayRateSlow - decayRateFast)
                      freq = 2 * pi * 17.13 -- period of about 4 four frames at 60 FPS, and not close to an integer multiple
                      magnitude =
                        magnitude0 * exp (-decayRate * t) * sin (freq * t)
                  in  Float.map2 (* magnitude) $
                        Float.normalize2 $
                          Fixed.toFloats sp_direction
            in  foldl' (+) 0 vectors
      cameraPos =
        if sceneDeco_enableScreenShake decorations
          then rawCameraPos + screenshakeOffset
          else rawCameraPos

  -- Parameters
  let gameViewCS = CS.gameView cameraPos
  let players = fst <$> scene_players scene
  let _mOverseerView = pv_overseerView <$> mPlayerView
      overseerPos =
        case sceneDeco_perspective decorations of
          FreePerspective -> cameraPos
          PlayerPerspective playerInfo ->
            case pv_overseerView (pi_playerView playerInfo) of
              OV_Alive OverseerViewAlive{ova_pos} ->
                Fixed.toFloats ova_pos
              OV_Dead OverseerViewDead{ovd_lastPos} ->
                Fixed.toFloats ovd_lastPos
      renderingComps = scene_renderingComps scene

  -- Background
  drawBackground (gle_background glEnv) $
    let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
    in  BackgroundVParams
          { bvp_viewScale = recip CC.viewRadius
          , bvp_viewCenter = cameraPos
          , bvp_modelScale = worldRadius * 2
          , bvp_modelTrans = Float2 0 0
          , bvp_worldRadius = worldRadius
          }

  -- Overseer remains
  drawOverseerRemains (gle_overseerRemains glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          map (fromOverseerRemainsParticle mActorID mViewTeam time) $
            particles_overseerRemains particles
      }

  -- Soft obstacles
  drawSoftObstacle (gle_softObstacle glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances = flip map (rcs_softObstacles renderingComps) $ \rc ->
          let SoftObstacleRendering color = rendering_entityRendering rc
          in  SoftObstacleInstance
                { soi_modelScale = rendering_radius rc
                , soi_modelTrans = rendering_pos rc
                , soi_color = color
                , soi_maxOpacity = 0.24
                , soi_minOpacity = 0
                }
      }

  -- Team bases and spawn areas
  drawTeamBase (gle_teamBase glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          let getColor team =
                case viewTeamColor mViewTeam team of
                  TeamRed -> Float3 1 0.5 0.5
                  TeamBlue -> Float3 0.5 0.5 1
              teamBases =
                flip map (rcs_teamBases renderingComps) $ \rc ->
                  TeamBaseInstance
                    { tbi_size = rendering_radius rc
                    , tbi_center = rendering_pos rc
                    , tbi_color =
                        flip Util.fromRGB_A 0.3 $
                          getColor (tbr_team (rendering_entityRendering rc))
                    , tbi_fillOpacity = 1
                    }
              spawnAreas =
                flip map (rcs_spawnAreas renderingComps) $ \rc ->
                  TeamBaseInstance
                    { tbi_size = rendering_radius rc
                    , tbi_center = rendering_pos rc
                    , tbi_color =
                        flip Util.fromRGB_A 0.2 $
                          getColor (tsr_team (rendering_entityRendering rc))
                    , tbi_fillOpacity = 0
                    }
          in  spawnAreas ++ teamBases
      }

  -- Team base flag-capture hint text
  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_fontAtlas = gle_fontAtlasSmall glEnv
      , cvp_screenWidth = fromIntegral windowSquareLength
      , cvp_lines =
          flip mapMaybe (rcs_teamBases renderingComps) $ \rc ->
            let flagConditionStatus =
                  tbr_flagDepositPending (rendering_entityRendering rc)
                opacity = hintOpacityFromFlagCondition time flagConditionStatus
                screenPos =
                  CS.intoPos (CS.viewport windowSquareLength) $
                    CS.fromPos (CS.gameView cameraPos) $
                      rendering_pos rc & _2 +~ 10 - 0.5 * (1 - opacity) -- adjusted by hand
                color = Float4 0.5 0.5 0.5 opacity
                msg = "can't score while flag is away"
            in  Just (CharAlignCenter, screenPos, [(color, msg)])
      }

  -- World bounds
  drawWorldBounds (gle_worldBounds glEnv) $
    let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
    in  WorldBoundsVParams
          { wbvp_viewScale = recip CC.viewRadius
          , wbvp_viewCenter = cameraPos
          , wbvp_modelScale = worldRadius * 2
          , wbvp_modelTrans = Float2 0 0
          , wbvp_worldRadius = worldRadius
          }

  -- Placeholders
  drawCircle (gle_circle glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances = flip map (rcs_placeholders renderingComps) $ \rc ->
          CircleInstance
            { ci_size = rendering_radius rc
            , ci_center = rendering_pos rc
            , ci_color = unPlaceholderRendering $ rendering_entityRendering rc
            }
      }

  -- Flag recovery areas
  drawControlPoint (gle_controlPoint glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          map
            (fromFlagCaptureAreaRendering (gp_dynamics gameParams) mViewTeam)
            (rcs_flagCaptureAreas renderingComps)
      }

  -- Flags
  drawFlag (gle_flag glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          let flagInstance :: Float -> Float2 -> Team -> FlagInstance
              flagInstance radius pos team =
                FlagInstance
                  { fi_size = radius
                  , fi_center = pos
                  , fi_color =
                      flip Util.fromRGB_A 1 $
                        flagColors $
                          viewTeamColor mViewTeam team
                  }
              fromFlag :: RenderingComponent FlagRendering -> (Float, Float2, Team)
              fromFlag rc =
                ( rendering_radius rc
                , rendering_pos rc
                , fr_team $ rendering_entityRendering rc
                )
              fromBaseFlag :: RenderingComponent BaseFlagRendering -> (Float, Float2, Team)
              fromBaseFlag rc =
                ( rendering_radius rc
                , rendering_pos rc
                , unBaseFlagRendering $ rendering_entityRendering rc
                )
              flags =
                map (uncurry3 flagInstance) $
                  map fromBaseFlag (rcs_baseFlags renderingComps)
                    ++ map fromFlag (rcs_flags renderingComps)

              flagParticleInstance :: Particle FlagFadeParticle -> FlagInstance
              flagParticleInstance particle =
                let pos = case particle_type particle of
                      FlagFade_Recover (FlagRecoverParticle _ _ pos_) ->
                        pos_
                      FlagFade_Pickup (FlagPickupParticle _ pos_ _) ->
                        pos_
                      FlagFade_Drop (FlagDropParticle pos_ _) ->
                        pos_
                      FlagFade_Respawn team ->
                        teamBaseLocation gameParams team
                in  FlagInstance
                      { fi_size = Fixed.toFloat $ gp_flagRadius $ gp_dynamics gameParams
                      , fi_center = Fixed.toFloats pos
                      , fi_color =
                          let ticks =
                                time `diffTime` particle_startTime particle
                              seconds =
                                fromIntegral ticks / fromIntegral C.tickRate_hz
                              opacity = exp (-seconds / 0.05)
                          in  Float4 1 1 1 opacity
                      }
              flagParticles =
                map flagParticleInstance $ particles_flagFade particles
          in  flags ++ flagParticles -- particles on top
      }

  -- Flag flag-recovery hint text
  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_fontAtlas = gle_fontAtlasSmall glEnv
      , cvp_screenWidth = fromIntegral windowSquareLength
      , cvp_lines =
          flip mapMaybe (rcs_flagCaptureAreas renderingComps) $ \rc ->
            let flagConditionStatus =
                  fcar_recoveringFlag (rendering_entityRendering rc)
                opacity = hintOpacityFromFlagCondition time flagConditionStatus
                screenPos =
                  CS.intoPos (CS.viewport windowSquareLength) $
                    CS.fromPos (CS.gameView cameraPos) $
                      rendering_pos rc & _2 +~ 18 - 0.5 * (1 - opacity) -- adjusted by hand
                color = Float4 0.5 0.5 0.5 opacity
                msg = "recovering flag ..."
            in  Just (CharAlignCenter, screenPos, [(color, msg)])
      }

  -- Overseers
  drawOverseer (gle_overseer glEnv) $
    OverseerVParams
      { ovp_coordSystem = gameViewCS
      , ovp_instances =
          let overseers =
                flip map (rcs_overseers renderingComps) $ \rc ->
                  let actorID = or_actorID $ rendering_entityRendering rc
                      cursorPos =
                        maybe
                          0
                          (Fixed.toFloats . control_worldPos . player_controls)
                          (WIM.lookup actorID players)
                      Float2 x y = cursorPos - rendering_pos rc
                      finsAngle = -atan2 y x
                  in  fromOverseerRendering
                        time
                        mViewTeam
                        mActorID
                        finsAngle
                        rc
              deadOverseers =
                flip map (particles_overseerDeath particles) $ \particle ->
                  let ov = particle_type particle
                      actorID = odp_actorID ov
                      cursorPos =
                        maybe
                          0
                          (Fixed.toFloats . control_worldPos . player_controls)
                          (WIM.lookup actorID players)
                      Float2 x y = cursorPos - Fixed.toFloats (odp_pos ov)
                      finsAngle = -atan2 y x
                  in  Overseer.fromOverseerDeathParticle
                        time
                        finsAngle
                        particle
          in  deadOverseers ++ overseers
      }

  -- Cooldown bar
  -- Note: We expect at most one instance
  withBlendFunc GL_SRC_ALPHA GL_ONE $ do
    let cooldownBarInstance ::
          Float4 ->
          Ticks ->
          RenderingComponent a ->
          Float ->
          Maybe CooldownBarInstance
        cooldownBarInstance color cooldownTicks rc spacing = do
          guard (cooldownTicks > 0)
          pure $
            CooldownBarInstance
              { cbi_center =
                  rendering_pos rc & _2 -~ (rendering_radius rc + spacing)
              , cbi_widthRadius =
                  (3 / fromIntegral C.tickRate_hz) * fromIntegral cooldownTicks
              , cbi_heightRadius = 0.25
              , cbi_bloomRadius = 0.5
              , cbi_color = color
              }
    for_ mPlayer $ \player ->
      for_ (rcs_overseers renderingComps) $ \rc -> do
        let actorID = view player_actorID player
            overseer = rendering_entityRendering rc
        when (actorID == or_actorID overseer) $ do
          let mPsiStormInstance =
                cooldownBarInstance color cooldownTicks rc spacing
               where
                cooldownTicks =
                  or_psiStormCooldownUntil overseer `diffTime` time
                psiStormControl =
                  view control_psiStorm (control_buttons (player_controls player))
                color = case psiStormControl of
                  PsiStormOff -> Float4 1 1 0 0.5
                  PsiStormOn -> Float4 1 1 0 1
                spacing = 15
          let mDashInstance cooldownTicks =
                cooldownBarInstance color cooldownTicks rc
               where
                color =
                  case view control_overseerDash (control_buttons (player_controls player)) of
                    OverseerDashOff -> Float4 0 1 1 0.5
                    OverseerDashOn -> Float4 0 1 1 1
          drawCooldownBar (gle_cooldownBar glEnv) $
            StandardVParams
              { svp_coordSystem = gameViewCS
              , svp_instances =
                  catMaybes
                    [ mPsiStormInstance
                    , mDashInstance
                        (or_droneDashCooldownUntil1 overseer `diffTime` time)
                        10
                    , mDashInstance
                        (or_droneDashCooldownUntil2 overseer `diffTime` time)
                        12
                    ]
              }

  -- Player tags
  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_fontAtlas = gle_fontAtlasSmall glEnv
      , cvp_screenWidth = fromIntegral windowSquareLength
      , cvp_lines = flip map (rcs_overseers renderingComps) $ \rc ->
          let OverseerRendering{or_team, or_playerName} =
                rendering_entityRendering rc
              screenPos =
                CS.intoPos (CS.viewport windowSquareLength) $
                  CS.fromPos (CS.gameView cameraPos) $
                    rendering_pos rc & _2 +~ rendering_radius rc + 10 -- adjusted by hand
              color = case viewTeamColor mViewTeam or_team of
                TeamRed -> Float4 1 0.5 0.5 1
                TeamBlue -> Float4 0.5 0.75 1.0 1
              nameStr = BS.unpack $ getPlayerName or_playerName
          in  (CharAlignCenter, screenPos, [(color, nameStr)])
      }

  -- Drones
  do
    let getDashFullChargeSeconds :: PlayerCooldowns -> Float
        getDashFullChargeSeconds cooldowns =
          fromMaybe (-1) $ do
            guard $ gp_enableDroneDash $ gp_cast gameParams
            guard $ pc_lastDroneCommand cooldowns == DroneNeutral
            pure $
              let chargingTicks =
                    time `diffTime` pc_lastDroneCommandInitialTime cooldowns
                  fullChargeTicks =
                    chargingTicks
                      - gp_maxDroneDashChargeTicks (gp_cast gameParams)
              in  fromIntegral fullChargeTicks / fromIntegral C.tickRate_hz
    let mViewPlayerDashFullCharge =
          mPlayerInfo <&> \PlayerInfo{pi_actorID, pi_playerView} ->
            (pi_actorID, getDashFullChargeSeconds (pv_cooldowns pi_playerView))
    drawDrone (gle_drone glEnv) $
      StandardVParams
        { svp_coordSystem = gameViewCS
        , svp_instances =
            map (fromDroneRendering mViewPlayerDashFullCharge mViewTeam time) $
              rcs_drones renderingComps
        }

  -- Overseer flag indicators
  drawFlag (gle_flag glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          let makeFlag rc =
                FlagInstance
                  { fi_size = 0.35 * rendering_radius rc
                  , fi_center = rendering_pos rc
                  , fi_color =
                      let ovTeam = or_team (rendering_entityRendering rc)
                          flagTeam = oppositeTeam ovTeam
                      in  flip Util.fromRGB_A 1 $
                            flagColors $
                              viewTeamColor mViewTeam flagTeam
                  }
              -- TODO: Remove this Maybe
              makeFlagFlash ::
                (a -> Maybe (Fixed2, Fixed2)) ->
                Particle a ->
                Maybe FlagInstance
              makeFlagFlash f p = do
                (pos, vel) <- f (particle_type p)
                pure
                  FlagInstance
                    { fi_size =
                        let overseerRadius =
                              Fixed.toFloat $
                                gp_overseerRadius (gp_dynamics gameParams)
                        in  0.35 * overseerRadius
                    , fi_center =
                        Fixed.toFloats $
                          pos + Fixed.map (* fromIntegral elapsedTicks) vel
                    , fi_color =
                        Float4 1 1 1 $
                          (0.9 *) $
                            (0.5 **) $
                              fromIntegral $
                                max 0 (elapsedTicks - 1)
                    }
               where
                elapsedTicks = time `diffTime` particle_startTime p
          in  concat
                [ map makeFlag $
                    filter (or_hasFlag . rendering_entityRendering) $
                      rcs_overseers renderingComps
                , mapMaybe
                    (makeFlagFlash (Just . (fep_pos &&& fep_vel)))
                    (particles_flagExplode particles)
                , mapMaybe
                    ( makeFlagFlash
                        ( \case
                            FlagFade_Drop p -> Just (fdp_pos p, fdp_vel p)
                            _ -> Nothing
                        )
                    )
                    (particles_flagFade particles)
                ]
      }

  -- Dash particles
  drawDash (gle_dash glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          flip map (particles_dash particles) $ \particle ->
            let DashParticle{dp_pos, dp_vel, dp_radius, dp_direction, dp_magnitudeFraction} =
                  particle_type particle
            in  DashInstance
                  { di_center = Fixed.toFloats dp_pos
                  , di_initialVelocity = Fixed.toFloats dp_vel
                  , di_scale = Fixed.toFloat dp_radius
                  , di_angle =
                      let Float2 x y = Fixed.toFloats dp_direction
                      in  atan2 y x
                  , di_ticksElapsed =
                      fromIntegral $
                        time `diffTime` particle_startTime particle
                  , di_secondsElapsed =
                      let ticksElapsed =
                            fromIntegral (time `diffTime` particle_startTime particle)
                      in  ticksElapsed / fromIntegral C.tickRate_hz
                  , di_opacity =
                      min 1 $ max 0 $ Fixed.toFloat dp_magnitudeFraction
                  }
      }

  -- Cast ripple particles
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawCastRipple (gle_castRipple glEnv) $
      StandardVParams
        { svp_coordSystem = gameViewCS
        , svp_instances =
            flip map (particles_castRipple particles) $ \particle ->
              let progress = particleProgress time particle
                  CastRippleParticle color pos = particle_type particle
              in  CastRippleInstance
                    { cri_size = 60.0
                    , cri_center = Fixed.toFloats pos
                    , cri_color = Fixed.toFloats4 color
                    , cri_ringWidth = 2.0
                    , cri_progress = progress
                    }
        }

  -- Overseer death explosions and flag score explosions
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawExplosion (gle_explosion glEnv) $
      StandardVParams
        { svp_coordSystem = gameViewCS
        , svp_instances =
            concatMap
              (Explosion.fromOverseerDeathParticle mActorID mViewTeam time)
              (particles_overseerDeath particles)
              ++ concatMap
                (fromFlagExplodeParticle mViewTeam time)
                (particles_flagExplode particles)
        }

  -- Drone death particles
  drawDrone (gle_drone glEnv) $
    StandardVParams
      { svp_coordSystem = gameViewCS
      , svp_instances =
          map (fromDroneDeathParticle time) $
            particles_droneDeath particles
      }

  -- Collision sparks
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawSpark (gle_spark glEnv) $
      StandardVParams
        { svp_coordSystem = gameViewCS
        , svp_instances =
            let collisionSparks =
                  concatMap (fromCollisionParticle mActorID mViewTeam time) $
                    particles_collisionDamage particles
                psiStormSparks =
                  fromPsiStormDamageParticles mActorID mViewTeam time $
                    particles_psiStormDamage particles
            in  collisionSparks ++ psiStormSparks
        }

  -- Psi-storms
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawPsiStorm (gle_psiStorm glEnv) $
      StandardVParams
        { svp_coordSystem = gameViewCS
        , svp_instances = flip map (rcs_psiStorms renderingComps) $ \rc ->
            let psiStorm = rendering_entityRendering rc
                endTime = psr_endTime psiStorm
            in  PsiStormInstance
                  { psi_modelScale = rendering_radius rc
                  , psi_modelTrans = rendering_pos rc
                  , psi_elapsedSeconds =
                      let lifetime =
                            gp_psiStormLifetimeTicks $ gp_cast gameParams
                          elapsedTicks =
                            time `diffTime` addTicks (-lifetime) endTime
                      in  fromIntegral elapsedTicks / fromIntegral C.tickRate_hz
                  , psi_opacityFactor =
                      let ticksRemaining =
                            max 0 $
                              getTicks (endTime `diffTime` scene_time scene)
                          fadeTicks =
                            getTicks $
                              gp_psiStormFadeTicks (gp_dynamics gameParams)
                      in  min 1 $
                            fromIntegral ticksRemaining / fromIntegral fadeTicks
                  , psi_bloomColor =
                      case psr_color psiStorm of
                        Nothing -> Float4 0 0 0 0
                        Just playerColor ->
                          Util.fromRGB_A
                            (viewPlayerColor mActorID mViewTeam playerColor)
                            1
                  }
        }

  -- Fog
  drawFog (gle_fog glEnv) $
    FogVParams
      { fvp_viewScale = recip CC.viewRadius
      , fvp_viewCenter = cameraPos
      , fvp_modelScale = CC.viewRadius
      , fvp_modelTrans = overseerPos
      , fvp_fogRadius = CC.viewRadius
      , fvp_visibleOpacity =
          case sceneDeco_perspective decorations of
            FreePerspective{} -> 0
            PlayerPerspective playerInfo ->
              case pv_overseerView (pi_playerView playerInfo) of
                OV_Alive{} -> 0
                OV_Dead OverseerViewDead{ovd_deathTime} ->
                  let seconds =
                        fromIntegral (time `diffTime` ovd_deathTime)
                          / fromIntegral C.tickRate_hz
                      frac = min 1 (seconds / CC.deathFadeSeconds)
                      frac2 = frac * frac
                      frac4 = frac2 * frac2
                  in  frac4
      , fvp_ticksMod = fromIntegral $ getTime time `mod` (C.tickRate_hz * 12)
      , fvp_lowHealthEffectIntensity =
          case sceneDeco_perspective decorations of
            FreePerspective{} -> 0
            PlayerPerspective playerInfo ->
              case pv_overseerView (pi_playerView playerInfo) of
                OV_Alive OverseerViewAlive{ova_healthFraction} ->
                  1 - Fixed.toFloat ova_healthFraction
                OV_Dead{} -> 1
      , fvp_staticIntensity =
          if sceneDeco_enableVisualStatic decorations then 1 else 0
      }

  -- Drone control radius
  case sceneDeco_perspective decorations of
    FreePerspective{} -> pure ()
    PlayerPerspective playerInfo ->
      case pv_overseerView (pi_playerView playerInfo) of
        OV_Dead{} -> pure ()
        OV_Alive{} -> do
          withBlendFunc GL_SRC_ALPHA GL_ONE $
            drawDroneControlRadius (gle_droneControlRadius glEnv) $
              DroneControlRadiusVParams
                { dcrvp_modelTrans = overseerPos
                , dcrvp_modelScale =
                    Fixed.toFloat $ gp_controlRadius $ gp_dynamics gameParams
                , dcrvp_viewScale = recip CC.viewRadius
                , dcrvp_viewCenter = cameraPos
                }

hintOpacityFromFlagCondition :: Time -> FlagConditionStatus -> Float
hintOpacityFromFlagCondition time = \case
  FlagConditionSatisfied timeStart ->
    hintOpacity (time `diffTime` timeStart)
  FlagConditionUnsatisfied timeStart timeEnd ->
    let opacityAtEnd = hintOpacity (timeEnd `diffTime` timeStart)
        fade =
          fromIntegral (time `diffTime` timeEnd)
            / fromIntegral hintTransitionTicks
    in  max 0 $ opacityAtEnd - fade

-- Helper
hintOpacity ::
  Ticks -> -- ticks since start of animation
  Float
hintOpacity ticks
  | ticks <= 0 = 0
  | ticks <= endFadeIn =
      fromIntegral ticks / fromIntegral transitionTicks
  | ticks <= startFadeOut = 1
  | ticks <= endFadeOut =
      fromIntegral (transitionTicks - (ticks - startFadeOut))
        / fromIntegral transitionTicks
  | otherwise = 0
 where
  durationTicks = 240
  transitionTicks = hintTransitionTicks
  endFadeIn = transitionTicks
  startFadeOut = endFadeIn + durationTicks
  endFadeOut = startFadeOut + transitionTicks

hintTransitionTicks :: Ticks
hintTransitionTicks = 12

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c
