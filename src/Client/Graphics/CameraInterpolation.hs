module Client.Graphics.CameraInterpolation (
  cameraInterpolation,
) where

import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.Float qualified as Float

-- | Update the camera to track a target. Should be called once per frame.
cameraInterpolation ::
  Float -> Float2 -> Float2 -> (Float2, Float2) -> (Float2, Float2)
cameraInterpolation ticksPerFrame targetPos targetVel (cameraPos, cameraVel)
  -- Immediately jump to target if it is too far away
  | Float.distance2 targetPos cameraPos > cutoff = (targetPos, targetVel)
  | otherwise =
      let vel = w * targetVel + w_compl * cameraVel
          pos =
            w * targetPos
              + w_compl * (cameraPos + Float.map2 (* ticksPerFrame) vel)
      in  (pos, vel)
 where
  w = Float.map2 (* ticksPerFrame) 1.5625e-2 -- a lazy approximation
  w_compl = 1.0 - w
  cutoff = 320.0
