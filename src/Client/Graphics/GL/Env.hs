{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- | Context for all rendering
module Client.Graphics.GL.Env (
  GLEnv (..),
  RefreshRate (..),
  getTicksPerFrame,
  getRefreshRate,
  withGLEnv,
  FullscreenMode (..),
  getFullscreenMode,
  setFullscreenMode,
  toggleFullScreen,
  setSquareViewport,
  withKeyCallback,
  withCharCallback,
) where

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TMVar
import Control.Exception (bracket, bracket_)
import Control.Logging qualified as Logging
import Control.Monad (unless, void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Foldable (traverse_)
import Data.Functor ((<&>))
import Data.IORef
import Data.Int (Int32)
import Data.Text (Text)
import Data.Text qualified as Text
import Graphics.GL.Core33
import Graphics.UI.GLFW (OpenGLProfile (..), WindowHint (..))
import Graphics.UI.GLFW qualified as GLFW

import Client.Graphics.Fonts (signikaRegular)
import Client.Graphics.GL.Wrapped (runGL)
import Client.Graphics.Rendering.Entity.Background
import Client.Graphics.Rendering.Entity.CastRipple
import Client.Graphics.Rendering.Entity.Char
import Client.Graphics.Rendering.Entity.Circle
import Client.Graphics.Rendering.Entity.ControlPoint
import Client.Graphics.Rendering.Entity.CooldownBar
import Client.Graphics.Rendering.Entity.Cross
import Client.Graphics.Rendering.Entity.CursorAttract
import Client.Graphics.Rendering.Entity.CursorRepel
import Client.Graphics.Rendering.Entity.Dash
import Client.Graphics.Rendering.Entity.Drone
import Client.Graphics.Rendering.Entity.DroneControlRadius
import Client.Graphics.Rendering.Entity.Explosion
import Client.Graphics.Rendering.Entity.Flag
import Client.Graphics.Rendering.Entity.Fog
import Client.Graphics.Rendering.Entity.Overseer
import Client.Graphics.Rendering.Entity.OverseerRemains
import Client.Graphics.Rendering.Entity.PingFlash
import Client.Graphics.Rendering.Entity.PsiStorm
import Client.Graphics.Rendering.Entity.Quad
import Client.Graphics.Rendering.Entity.Ring
import Client.Graphics.Rendering.Entity.SoftObstacle
import Client.Graphics.Rendering.Entity.Spark
import Client.Graphics.Rendering.Entity.TeamBase
import Client.Graphics.Rendering.Entity.WorldBounds
import Client.Graphics.Rendering.FontAtlas
import Client.Graphics.Rendering.StandardInstancedVisual

--------------------------------------------------------------------------------

-- | We create one OpenGL context which is passed between OS threads via
-- 'GLFW.makeContextCurrent'. We assume that only one thread makes calls to
-- OpenGL at a time.
--
-- Note that we should only use the main thread to make calls to GLFW. We
-- should also only use bound threads to make calls to OpenGL, after the
-- appropriate calls to 'GLFW.makeContextCurrent'.
data GLEnv = GLEnv
  { gle_window :: GLFW.Window
  , gle_refreshRate :: RefreshRate -- Monitor refresh rate at startup
  , gle_overseer :: StandardInstancedVisual OverseerGLInstance
  , gle_drone :: StandardInstancedVisual DroneInstance
  , gle_softObstacle :: StandardInstancedVisual SoftObstacleInstance
  , gle_explosion :: StandardInstancedVisual ExplosionInstance
  , gle_controlPoint :: StandardInstancedVisual ControlPointInstance
  , gle_circle :: StandardInstancedVisual CircleInstance
  , gle_quad :: StandardInstancedVisual QuadInstance
  , gle_droneControlRadius :: DroneControlRadiusVisual
  , gle_overseerRemains :: StandardInstancedVisual OverseerRemainsInstance
  , gle_char :: CharVisual
  , gle_fontAtlasSmall :: FontAtlas
  , gle_fontAtlasRegular :: FontAtlas
  , gle_fontAtlasLarge :: FontAtlas
  , gle_uiPadding :: Float
  , gle_pingFlash :: StandardInstancedVisual PingFlashInstance
  , gle_ring :: StandardInstancedVisual RingInstance
  , gle_spark :: StandardInstancedVisual SparkInstance
  , gle_background :: BackgroundVisual
  , gle_castRipple :: StandardInstancedVisual CastRippleInstance
  , gle_cross :: StandardInstancedVisual CrossInstance
  , gle_dash :: StandardInstancedVisual DashInstance
  , gle_cooldownBar :: StandardInstancedVisual CooldownBarInstance
  , gle_worldBounds :: WorldBoundsVisual
  , gle_psiStorm :: StandardInstancedVisual PsiStormInstance
  , gle_flag :: StandardInstancedVisual FlagInstance
  , gle_teamBase :: StandardInstancedVisual TeamBaseInstance
  , gle_fog :: FogVisual
  , gle_cursorAttract :: StandardInstancedVisual CursorAttractInstance
  , gle_cursorRepel :: StandardInstancedVisual CursorRepelInstance
  , gle_pollFramebufferSizeUpdate :: IO (Maybe (Int, Int)) -- should not be called by more than one thread at a time
  , -- | For simualting a stack of key callbacks. Not thread-safe.
    gle_keyCallback :: IORef (Maybe GLFW.KeyCallback)
  , -- | For simualting a stack of char callbacks. Not thread-safe.
    gle_charCallback :: IORef (Maybe GLFW.CharCallback)
  }

data RefreshRate = Hz30 | Hz60 | Hz120

getTicksPerFrame :: (Num a) => GLEnv -> a
getTicksPerFrame glEnv =
  case gle_refreshRate glEnv of
    Hz30 -> 4
    Hz60 -> 2
    Hz120 -> 1

getRefreshRate :: (Num a) => GLEnv -> a
getRefreshRate glEnv =
  case gle_refreshRate glEnv of
    Hz30 -> 30
    Hz60 -> 60
    Hz120 -> 120

--------------------------------------------------------------------------------

-- | Must be called from the main thread
withGLEnv :: (GLEnv -> IO a) -> IO ()
withGLEnv action =
  bracket initial final $ \case
    Left errMsg -> do
      GLFW.getError >>= withGlfwErrorText \glfwErr ->
        Logging.log $ errMsg <> "GLFW error: " <> glfwErr
    Right glEnv -> do
      GLFW.getError >>= withGlfwErrorText \glfwErr ->
        Logging.warn $
          "Successfully initialized graphics, but encountered a GLFW error: "
            <> glfwErr
      void $ action glEnv
 where
  initial = initGL
  final _ = do
    GLFW.getError >>= withGlfwErrorText \glfwErr ->
      Logging.warn $ "Found GLFW error: " <> glfwErr
    GLFW.terminate

withGlfwErrorText ::
  Applicative f => (Text -> f ()) -> Maybe (GLFW.Error, String) -> f ()
withGlfwErrorText action =
  traverse_ $ \(err, description) ->
    action $
      Text.pack (show err)
        <> "."
        <> "Description: "
        <> Text.pack description
        <> "."

initGL :: IO (Either Text GLEnv)
initGL = runExceptT $ do
  initSuccess <- liftIO GLFW.init
  unless initSuccess $ throwE "Could not initialize GLFW."

  (window, gle_refreshRate) <- do
    primMonitor <-
      ExceptT $
        annotate "Failed to detect monitor."
          <$> GLFW.getPrimaryMonitor
    videoMode <-
      ExceptT $
        annotate "Failed to get video mode."
          <$> GLFW.getVideoMode primMonitor
    let (windowLength, (windowPos_x, windowPos_y)) =
          defaultWindowLengthAndPos videoMode
        refreshRate = nearestRefreshRate $ GLFW.videoModeRefreshRate videoMode

    liftIO $ do
      GLFW.windowHint $ WindowHint'ContextVersionMajor 3
      GLFW.windowHint $ WindowHint'ContextVersionMinor 3
      GLFW.windowHint $ WindowHint'OpenGLProfile OpenGLProfile'Core
      GLFW.windowHint $ WindowHint'OpenGLForwardCompat True -- required for MacOS
    window <-
      ExceptT $
        annotate "Failed to create GLFW window."
          <$> GLFW.createWindow windowLength windowLength "pixelpusher" Nothing Nothing
    liftIO $ GLFW.setWindowPos window windowPos_x windowPos_y
    pure (window, refreshRate)

  liftIO $ do
    -- Make context (must precede any OpenGL calls)
    GLFW.makeContextCurrent (Just window)

    -- Explictly disable vsync because I can't for the life of me figure out
    -- how to get things to run smoothly with vsync on Windows!!
    GLFW.swapInterval 0

    -- Transparency
    glEnable GL_BLEND
    glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA

    -- Cursor visibility
    GLFW.setCursorInputMode window GLFW.CursorInputMode'Normal

    (width, height) <- GLFW.getFramebufferSize window
    setSquareViewport (fromIntegral width) (fromIntegral height)
    framebufferSizeUpdateVar <- newEmptyTMVarIO
    let gle_pollFramebufferSizeUpdate =
          atomically $ tryTakeTMVar framebufferSizeUpdateVar
    GLFW.setFramebufferSizeCallback window $
      Just (framebufferSizeCallback framebufferSizeUpdateVar)

    let fontSize1080 = 24 :: Double
        fontSize = fontSize1080 * fromIntegral (min width height) / 1080
        sizeSmall = round fontSize
        sizeRegular = round $ 1.5 * fontSize
        sizeLarge = round $ 2 * fontSize

    gle_fontAtlasSmall <- initFontAtlas signikaRegular sizeSmall
    gle_fontAtlasRegular <- initFontAtlas signikaRegular sizeRegular
    gle_fontAtlasLarge <- initFontAtlas signikaRegular sizeLarge
    let gle_uiPadding = fromIntegral $ (`div` 2) sizeLarge

    gle_keyCallback <- newIORef Nothing
    gle_charCallback <- newIORef Nothing

    runGL $ do
      gle_overseer <- loadOverseer
      gle_drone <- loadDrone
      gle_softObstacle <- loadSoftObstacle
      gle_explosion <- loadExplosion
      gle_controlPoint <- loadControlPoint
      gle_circle <- loadCircle
      gle_quad <- loadQuad
      gle_droneControlRadius <- loadDroneControlRadius
      gle_overseerRemains <- loadOverseerRemains
      gle_char <- loadChar
      gle_pingFlash <- loadPingFlash
      gle_ring <- loadRing
      gle_spark <- loadSpark
      gle_background <- loadBackground
      gle_castRipple <- loadCastRipple
      gle_cross <- loadCross
      gle_dash <- loadDash
      gle_cooldownBar <- loadCooldownBar
      gle_worldBounds <- loadWorldBounds
      gle_psiStorm <- loadPsiStorm
      gle_flag <- loadFlag
      gle_teamBase <- loadTeamBase
      gle_fog <- loadFog
      gle_cursorAttract <- loadCursorAttract
      gle_cursorRepel <- loadCursorRepel
      let gle_window = window
      pure GLEnv{..}

framebufferSizeCallback ::
  TMVar (Int, Int) -> GLFW.Window -> Int -> Int -> IO ()
framebufferSizeCallback framebufferSizeVar _window w h = do
  -- Create a notification for setting the viewport on one other thread
  atomically $ do
    _ <- tryTakeTMVar framebufferSizeVar
    putTMVar framebufferSizeVar (w, h)

-- | Helper. Takes the width and height of the framebuffer and sets the OpenGL
-- viewport to the largest square centered in the framebuffer.
setSquareViewport :: Int32 -> Int32 -> IO ()
setSquareViewport w h =
  let l = min w h
  in  glViewport ((w - l) `div` 2) ((h - l) `div` 2) l l

--------------------------------------------------------------------------------
-- Refresh rate

-- Arbitrary projection of a monitor's refresh rate onto the three supported
-- refresh rates supported by the game.
nearestRefreshRate :: Int -> RefreshRate
nearestRefreshRate x
  | x < 45 = Hz30
  | x < 90 = Hz60
  | otherwise = Hz120

--------------------------------------------------------------------------------
-- Fullscreen

data FullscreenMode = Fullscreen | Windowed

-- | Must be called from the main thread
getFullscreenMode :: GLEnv -> IO FullscreenMode
getFullscreenMode glEnv =
  GLFW.getWindowMonitor (gle_window glEnv) <&> \case
    Nothing -> Windowed
    Just _ -> Fullscreen

-- | Must be called from the main thread
setFullscreenMode :: GLEnv -> FullscreenMode -> IO ()
setFullscreenMode glEnv = setFullscreenMode' (gle_window glEnv)

setFullscreenMode' :: GLFW.Window -> FullscreenMode -> IO ()
setFullscreenMode' window requestedMode = do
  GLFW.getWindowMonitor window >>= \case
    Nothing ->
      -- Window is currently windowed
      case requestedMode of
        Windowed ->
          pure ()
        Fullscreen ->
          void $
            runMaybeT $ do
              monitor <- MaybeT GLFW.getPrimaryMonitor
              videoMode <- MaybeT $ GLFW.getVideoMode monitor
              liftIO $ GLFW.setFullscreen window monitor videoMode
    Just monitor ->
      -- Window is currently fullscreened
      case requestedMode of
        Fullscreen ->
          pure ()
        Windowed ->
          void $
            runMaybeT $ do
              (windowLength, (x, y)) <-
                fmap defaultWindowLengthAndPos $ MaybeT $ GLFW.getVideoMode monitor
              liftIO $
                GLFW.setWindowed window windowLength windowLength x y

-- | Must be called from the main thread
toggleFullScreen :: GLFW.Window -> IO ()
toggleFullScreen window = do
  mMonitor <- GLFW.getWindowMonitor window
  case mMonitor of
    Nothing -> void $
      runMaybeT $ do
        monitor <- MaybeT GLFW.getPrimaryMonitor
        videoMode <- MaybeT $ GLFW.getVideoMode monitor
        liftIO $ GLFW.setFullscreen window monitor videoMode
    Just monitor -> void $
      runMaybeT $ do
        (windowLength, (x, y)) <-
          fmap defaultWindowLengthAndPos $ MaybeT $ GLFW.getVideoMode monitor
        liftIO $
          GLFW.setWindowed window windowLength windowLength x y

defaultWindowLengthAndPos :: GLFW.VideoMode -> (Int, (Int, Int))
defaultWindowLengthAndPos videoMode =
  let width = GLFW.videoModeWidth videoMode
      height = GLFW.videoModeHeight videoMode
      minLength = min width height
      windowLength = (7 * minLength) `div` 8
      x = (width - windowLength) `div` 2
      y = (height - windowLength) `div` 2
  in  (windowLength, (x, y))

--------------------------------------------------------------------------------
-- GLFW KeyCallback and CharCallback management

-- | Install a new GLFW key callback before running the given action and
-- restore the current key callback after the action finishes.
withKeyCallback ::
  GLEnv -> GLFW.KeyCallback -> IO a -> IO a
withKeyCallback glEnv keyCallback action = do
  oldKeyCallback <- readIORef (gle_keyCallback glEnv)
  let window = gle_window glEnv
      before = do
        writeIORef (gle_keyCallback glEnv) $ Just keyCallback
        GLFW.setKeyCallback window $ Just keyCallback
      after = do
        writeIORef (gle_keyCallback glEnv) oldKeyCallback
        GLFW.setKeyCallback window oldKeyCallback
  bracket_ before after action

-- | Install a new GLFW char callback before running the given action and
-- restore the current char callback after the action finishes.
withCharCallback ::
  GLEnv -> GLFW.CharCallback -> IO a -> IO a
withCharCallback glEnv charCallback action = do
  oldCharCallback <- readIORef (gle_charCallback glEnv)
  let window = gle_window glEnv
      before = do
        writeIORef (gle_charCallback glEnv) $ Just charCallback
        GLFW.setCharCallback window $ Just charCallback
      after = do
        writeIORef (gle_charCallback glEnv) oldCharCallback
        GLFW.setCharCallback window oldCharCallback
  bracket_ before after action

--------------------------------------------------------------------------------
-- Helpers

annotate :: e -> Maybe a -> Either e a
annotate e m = case m of
  Nothing -> Left e
  Just a -> Right a
