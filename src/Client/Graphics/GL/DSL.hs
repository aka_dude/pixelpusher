{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# LANGUAGE TypeFamilyDependencies #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Ad-hoc embedded DSL for writing GLSL
module Client.Graphics.GL.DSL (
  Expr,
  render,
  (.+),
  (+.),
  (.-),
  (-.),
  (.*),
  (*.),
  (./),
  (/.),
  Dim1 (..),
  Dim2 (..),
  Dim3 (..),
  Dim4 (..),
  vec2,
  vec3,
  vec4,
  lt,
  gt,
  sampleTexture2D,
  ifThenElse',
  max',
  min',
  atan2,
  length,
  step,
  smoothstep,
  dot',
  exp',
  inversesqrt,
  clamp,
  mix,
  normalize,
  mod',
  and',
  floor',
  fract',
  GLSL,
  compileShaders,
  bind,
  getTexture2D,
  ExprVars,
  Variables,
  deriveGLSLVars,
) where

import Prelude hiding (atan2, length)

import Control.Monad.Trans.State.Strict
import Data.DList qualified as DL
import Data.Foldable (toList)
import Data.Kind qualified as K
import Data.List (intercalate)
import Data.Proxy
import Data.Sequence qualified as Seq
import Data.Text qualified as Text
import GHC.Generics hiding (conName)
import GHC.TypeLits
import Language.Haskell.TH
import Language.Haskell.TH qualified as TH

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

import Client.Graphics.GL.Attribute
import Client.Graphics.GL.Types
import Client.Graphics.GL.Uniform

--------------------------------------------------------------------------------

data Expr a where
  Variable :: String -> Expr a
  Literal :: (FloatType a) => a -> Expr a
  InfixOp :: String -> Expr a -> Expr a -> Expr a
  BroadcastInfixOpR :: (FloatType a) => Char -> Expr Float -> Expr a -> Expr a
  BroadcastInfixOpL :: (FloatType a) => Char -> Expr a -> Expr Float -> Expr a
  Func1 :: String -> Expr a -> Expr b
  Func2 :: String -> Expr a -> Expr b -> Expr c
  Func3 :: String -> Expr a -> Expr b -> Expr c -> Expr d
  Func4 :: String -> Expr a -> Expr b -> Expr c -> Expr d -> Expr e
  Negate :: (FloatType a) => Expr a -> Expr a
  Swizzle :: String -> Expr a -> Expr b
  FloatComparisonOp :: String -> Expr Float -> Expr Float -> Expr Bool
  IfThenElse :: Expr Bool -> Expr a -> Expr a -> Expr a

render :: Expr a -> String
render = \case
  Variable name -> name
  Literal x -> renderLit x
  InfixOp symbol expr1 expr2 -> renderInfixOp symbol expr1 expr2
  BroadcastInfixOpR symbol exprFloat exprVec ->
    renderBroadcastR symbol exprFloat exprVec
  BroadcastInfixOpL symbol exprVec exprFloat ->
    renderBroadcastL symbol exprVec exprFloat
  Func1 name expr -> func name [render expr]
  Func2 name expr1 expr2 -> func name [render expr1, render expr2]
  Func3 name expr1 expr2 expr3 ->
    func name [render expr1, render expr2, render expr3]
  Func4 name expr1 expr2 expr3 expr4 ->
    func name [render expr1, render expr2, render expr3, render expr4]
  Negate expr -> "-" ++ parensRender expr
  Swizzle str expr -> parensRender expr ++ "." ++ str
  FloatComparisonOp name expr1 expr2 ->
    parensRender expr1 ++ " " ++ name ++ " " ++ parensRender expr2
  IfThenElse exprBool expr1 expr2 ->
    concat
      [ parensRender exprBool
      , " ? "
      , parensRender expr1
      , " : "
      , parensRender expr2
      ]

parens :: String -> String
parens s = "(" ++ s ++ ")"

func :: String -> [String] -> String
func name args = name ++ parens (intercalate ", " args)

parensRender :: Expr a -> String
parensRender expr = if isAtomic expr then render expr else parens (render expr)

isAtomic :: Expr a -> Bool
isAtomic = \case
  Variable{} -> True
  Literal{} -> True
  InfixOp{} -> False
  BroadcastInfixOpR{} -> False
  BroadcastInfixOpL{} -> False
  Func1{} -> True
  Func2{} -> True
  Func3{} -> True
  Func4{} -> True
  Negate{} -> False
  Swizzle{} -> True
  FloatComparisonOp{} -> False
  IfThenElse{} -> False

renderInfixOp :: String -> Expr a -> Expr a -> String
renderInfixOp symbol expr1 expr2 =
  parensRender expr1 ++ " " ++ symbol ++ " " ++ parensRender expr2

renderBroadcastR :: Char -> Expr Float -> Expr a -> String
renderBroadcastR symbol exprFloat exprVec =
  parensRender exprFloat ++ " " ++ [symbol] ++ " " ++ parensRender exprVec

renderBroadcastL :: Char -> Expr a -> Expr Float -> String
renderBroadcastL symbol exprVec exprFloat =
  parensRender exprVec ++ " " ++ [symbol] ++ " " ++ parensRender exprFloat

--------------------------------------------------------------------------------

class FloatType a where
  renderLit :: a -> String

instance FloatType Float where
  renderLit = show

instance FloatType Float2 where
  renderLit (Float2 x y) = func "vec2" $ map show [x, y]

instance FloatType Float3 where
  renderLit (Float3 x y z) = func "vec3" $ map show [x, y, z]

instance FloatType Float4 where
  renderLit (Float4 x y z w) = func "vec3" $ map show [x, y, z, w]

--------------------------------------------------------------------------------
-- Numeric typeclasses

instance (FloatType a, Num a) => Num (Expr a) where
  (+) = InfixOp "+"
  (-) = InfixOp "-"
  (*) = InfixOp "*"
  negate = Negate
  abs = Func1 "abs"
  signum = Func1 "sign"
  fromInteger = Literal . fromInteger

instance (FloatType a, Fractional a) => Fractional (Expr a) where
  (/) = InfixOp "/"
  recip = InfixOp "/" 1
  fromRational = Literal . fromRational

instance (FloatType a, Floating a) => Floating (Expr a) where
  pi = Literal pi
  exp = Func1 "exp"
  log = Func1 "log"
  sqrt = Func1 "sqrt"
  (**) = Func2 "pow"

  -- logBase :: a -> a -> a
  sin = Func1 "sin"
  cos = Func1 "cos"
  tan = Func1 "tan"
  asin = Func1 "asin"
  acos = Func1 "acos"
  atan = Func1 "atan" -- 1-parameter version of atan
  sinh = Func1 "sinh"
  cosh = Func1 "cosh"
  tanh = Func1 "tanh"
  asinh = Func1 "asinh"
  acosh = Func1 "acosh"
  atanh = Func1 "atanh"

-- GHC.Float.log1p :: a -> a
-- GHC.Float.expm1 :: a -> a
-- GHC.Float.log1pexp :: a -> a
-- GHC.Float.log1mexp :: a -> a

--------------------------------------------------------------------------------
-- Broadcast operations

-- Mnemonic: The side with a dot is the non-scalar

infixl 6 +.
(+.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(+.) = BroadcastInfixOpR '+'

infixl 6 .+
(.+) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.+) = BroadcastInfixOpL '+'

infixl 6 -.
(-.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(-.) = BroadcastInfixOpR '-'

infixl 6 .-
(.-) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.-) = BroadcastInfixOpL '-'

infixl 7 *.
(*.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(*.) = BroadcastInfixOpR '*'

infixl 7 .*
(.*) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.*) = BroadcastInfixOpL '*'

infixl 7 /.
(/.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(/.) = BroadcastInfixOpR '/'

infixl 7 ./
(./) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(./) = BroadcastInfixOpL '/'

--------------------------------------------------------------------------------
-- Swizzling
--
-- Currently, only "in-order" swizzles have been implemented

class Dim1 a where
  x_ :: Expr a -> Expr Float

  r_ :: Expr a -> Expr Float
  r_ = x_

class Dim2 a where
  y_ :: Expr a -> Expr Float
  xy_ :: Expr a -> Expr Float2

class Dim3 a where
  z_ :: Expr a -> Expr Float
  xz_ :: Expr a -> Expr Float2
  yz_ :: Expr a -> Expr Float2
  xyz_ :: Expr a -> Expr Float3

class Dim4 a where
  w_ :: Expr a -> Expr Float
  xw_ :: Expr a -> Expr Float2
  yw_ :: Expr a -> Expr Float2
  zw_ :: Expr a -> Expr Float2
  xyw_ :: Expr a -> Expr Float3
  xzw_ :: Expr a -> Expr Float3
  yzw_ :: Expr a -> Expr Float3
  xyzw_ :: Expr a -> Expr Float4

instance Dim1 Float where
  x_ = Swizzle "x"
instance Dim1 Float2 where
  x_ = Swizzle "x"
instance Dim1 Float3 where
  x_ = Swizzle "x"
instance Dim1 Float4 where
  x_ = Swizzle "x"

instance Dim2 Float2 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"
instance Dim2 Float3 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"
instance Dim2 Float4 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"

instance Dim3 Float3 where
  z_ = Swizzle "z"
  xz_ = Swizzle "xz"
  yz_ = Swizzle "yz"
  xyz_ = Swizzle "xyz"
instance Dim3 Float4 where
  z_ = Swizzle "z"
  xz_ = Swizzle "xz"
  yz_ = Swizzle "yz"
  xyz_ = Swizzle "xyz"

instance Dim4 Float4 where
  w_ = Swizzle "w"
  xw_ = Swizzle "xw"
  yw_ = Swizzle "yw"
  zw_ = Swizzle "zw"
  xyw_ = Swizzle "xyw"
  xzw_ = Swizzle "xzw"
  yzw_ = Swizzle "yzw"
  xyzw_ = Swizzle "xyzw"

--------------------------------------------------------------------------------
-- Vector constructors

-- vec2

vec2 :: (Expr Float, Expr Float) -> Expr Float2
vec2 (x, y) = Func2 "vec2" x y

-- vec3

class Vec3 a where
  vec3 :: a -> Expr Float3

instance
  (a ~ Expr Float, b ~ Expr Float, c ~ Expr Float) =>
  Vec3 (a, b, c)
  where
  vec3 (x, y, z) = Func3 "vec3" x y z

instance (a ~ Expr Float2) => Vec3 (Expr Float, a) where
  vec3 (x, yz) = Func2 "vec3" x yz

instance (a ~ Expr Float) => Vec3 (Expr Float2, a) where
  vec3 (xy, z) = Func2 "vec3" xy z

-- vec4

class Vec4 a where
  vec4 :: a -> Expr Float4

instance
  (a ~ Expr Float, b ~ Expr Float, c ~ Expr Float, d ~ Expr Float) =>
  Vec4 (a, b, c, d)
  where
  vec4 (x, y, z, w) = Func4 "vec4" x y z w

instance (a ~ Expr Float2) => Vec4 (Expr Float, Expr Float, a) where
  vec4 (x, y, wz) = Func3 "vec4" x y wz

instance (a ~ Expr Float) => Vec4 (Expr Float, Expr Float2, a) where
  vec4 (x, yz, w) = Func3 "vec4" x yz w

instance (a ~ Expr Float, b ~ Expr Float) => Vec4 (Expr Float2, a, b) where
  vec4 (xy, z, w) = Func3 "vec4" xy z w

instance (a ~ Expr Float3) => Vec4 (Expr Float, a) where
  vec4 (x, yzw) = Func2 "vec4" x yzw

instance (a ~ Expr Float2) => Vec4 (Expr Float2, a) where
  vec4 (xy, zw) = Func2 "vec4" xy zw

instance (a ~ Expr Float) => Vec4 (Expr Float3, a) where
  vec4 (xyz, w) = Func2 "vec4" xyz w

--------------------------------------------------------------------------------
-- Float comparison operators

lt :: Expr Float -> Expr Float -> Expr Bool
lt = FloatComparisonOp "<"

gt :: Expr Float -> Expr Float -> Expr Bool
gt = FloatComparisonOp ">"

--------------------------------------------------------------------------------
-- Textures

sampleTexture2D :: Expr Texture2D -> Expr Float2 -> Expr Float4
sampleTexture2D = Func2 "texture"

--------------------------------------------------------------------------------
-- Other functions

ifThenElse' :: Expr Bool -> Expr a -> Expr a -> Expr a
ifThenElse' = IfThenElse

max' :: (FloatType a) => Expr a -> Expr a -> Expr a
max' = Func2 "max"

min' :: (FloatType a) => Expr a -> Expr a -> Expr a
min' = Func2 "min"

atan2 :: (FloatType a) => Expr a -> Expr a -> Expr a
atan2 = Func2 "atan" -- 2-parameter version of atan

length :: (FloatType a) => Expr a -> Expr Float
length = Func1 "length"

-- Only one variant is implemented for now
step :: (FloatType a) => Expr Float -> Expr a -> Expr a
step = Func2 "step"

-- Only one variant is implemented for now
smoothstep :: (FloatType a) => Expr Float -> Expr Float -> Expr a -> Expr a
smoothstep = Func3 "smoothstep"

dot' :: (FloatType a) => Expr a -> Expr a -> Expr Float
dot' = Func2 "dot"

exp' :: (FloatType a) => Expr a -> Expr a
exp' = Func1 "exp"

inversesqrt :: (FloatType a) => Expr a -> Expr a
inversesqrt = Func1 "inversesqrt"

-- Only one variant is implemented for now
clamp :: (FloatType a) => Expr a -> Expr Float -> Expr Float -> Expr a
clamp = Func3 "clamp"

-- Only one variant is implemented for now
mix :: (FloatType a) => Expr a -> Expr a -> Expr Float -> Expr a
mix = Func3 "mix"

normalize :: (FloatType a) => Expr a -> Expr a
normalize = Func1 "normalize"

mod' :: Expr Float -> Expr Float -> Expr Float
mod' = Func2 "mod"

and' :: Expr Bool -> Expr Bool -> Expr Bool
and' = InfixOp "&&"

floor' :: (FloatType a) => Expr a -> Expr a
floor' = Func1 "floor"

fract' :: (FloatType a) => Expr a -> Expr a
fract' = Func1 "fract"

--------------------------------------------------------------------------------

data GLSLState = GLSLState
  { nextVar :: Int
  , code :: Seq.Seq String
  , texture2DVars :: Seq.Seq String
  }

initialGLSLState :: GLSLState
initialGLSLState =
  GLSLState
    { nextVar = 0
    , code = mempty
    , texture2DVars = mempty
    }

newtype GLSL a = GLSL {runGLSL :: State GLSLState a}
  deriving newtype (Functor, Applicative, Monad)

newVar :: State GLSLState String
newVar = do
  varInt <- gets nextVar
  modify $ \st -> st{nextVar = varInt + 1}
  let varName = "_" ++ show varInt
  pure varName

bind :: forall a. (GLTypeName a) => Expr a -> GLSL (Expr a)
bind expr = GLSL $ do
  varName <- newVar

  let line =
        concat
          [ Text.unpack $ glslName @a
          , " "
          , varName
          , " = "
          , render expr
          , ";"
          ]
  modify $ \st -> st{code = code st Seq.|> line}

  pure $ Variable varName

data Texture2D

-- | Each use of 'getTexture2D' generates a @uniform sampler2D@ declaration.
-- Use multiple times in order to access multiple textures.
getTexture2D :: GLSL (Expr Texture2D)
getTexture2D = GLSL $ do
  varName <- newVar
  modify $ \st -> st{texture2DVars = texture2DVars st Seq.|> varName}
  pure $ Variable varName

-- | Compile a vertex and fragment shader to a GLSL string. The output of the
-- vertex shader must match the input of the fragment shader. The shaders must
-- also use the same uniforms.
compileShaders ::
  forall vertices instances uniforms vars.
  ( Variables vertices
  , GLAttribute vertices
  , Variables instances
  , GLAttribute instances
  , Variables uniforms
  , GLUniform uniforms
  , ExprVars vars
  ) =>
  ( Vars vertices ->
    Vars instances ->
    Vars uniforms ->
    GLSL (Expr Float2, vars)
  ) ->
  (Vars uniforms -> vars -> GLSL (Expr Float4)) ->
  (String, String)
compileShaders vertexShader fragmentShader =
  (compileVertexShader vertexShader, compileFragmentShader fragmentShader)

-- | Compile a vertex shader to a GLSL string.
compileVertexShader ::
  forall vertices instances uniforms output.
  ( Variables vertices
  , GLAttribute vertices
  , Variables instances
  , GLAttribute instances
  , Variables uniforms
  , GLUniform uniforms
  , ExprVars output
  ) =>
  ( Vars vertices ->
    Vars instances ->
    Vars uniforms ->
    GLSL (Expr Float2, output)
  ) ->
  String
compileVertexShader vertShader =
  let ((glPosition, outputs), glslState) =
        flip runState initialGLSLState $
          runGLSL $
            vertShader (vars @vertices) (vars @instances) (vars @uniforms)
      (nVertexFields, vertexLayout) = layoutDecs @vertices 0
      (_, instanceLayout) = layoutDecs @instances nVertexFields
      body = unlines $ toList $ code glslState
      texture2DUniformDeclarations =
        unlines $
          map texture2DUniformLine $
            toList $
              texture2DVars glslState
  in  unlines
        [ "#version 330 core"
        , ""
        , Text.unpack vertexLayout
        , Text.unpack instanceLayout
        , Text.unpack $ uniformDeclarationsGLSL @uniforms
        , texture2DUniformDeclarations
        , exprOutputDecs @output
        , "void main()"
        , "{"
        , body
        , exprAssign outputs
        , let glPosition4 = vec4 (glPosition, 0, 1)
          in  "gl_Position = " ++ render glPosition4 ++ ";"
        , "}"
        ]

-- | Compile a fragment shader to a GLSL string.
compileFragmentShader ::
  forall input uniforms.
  (Variables uniforms, GLUniform uniforms) =>
  (ExprVars input) =>
  (Vars uniforms -> input -> GLSL (Expr Float4)) ->
  String
compileFragmentShader fragShader =
  let (fragColor, glslState) =
        flip runState initialGLSLState $
          runGLSL $
            fragShader (vars @uniforms) exprVars
      body = unlines $ toList $ code glslState
      texture2DUniformDeclarations =
        unlines $
          map texture2DUniformLine $
            toList $
              texture2DVars glslState
  in  unlines
        [ "#version 330 core"
        , ""
        , Text.unpack $ uniformDeclarationsGLSL @uniforms
        , texture2DUniformDeclarations
        , ""
        , exprInputDecs @input
        , "out vec4 FragColor;"
        , ""
        , "void main()"
        , "{"
        , body
        , "FragColor = " ++ render fragColor ++ ";"
        , "}"
        ]

texture2DUniformLine :: String -> String
texture2DUniformLine name =
  "uniform sampler2D " ++ name ++ ";"

--------------------------------------------------------------------------------
-- Boilerplate:
-- Creating variables for user-defined records of 'Expr's

prefixExprVar :: String -> String
prefixExprVar = (++) "_"

class ExprVars a where
  exprVars :: a
  exprInputDecs :: String
  exprOutputDecs :: String
  exprAssign :: a -> String

  default exprVars :: (Generic a, GExprVars (Rep a)) => a
  exprVars = to $ gExprVars @(Rep a)

  default exprInputDecs :: (GExprVars (Rep a)) => String
  exprInputDecs = exprDecs @a "in"

  default exprOutputDecs :: (GExprVars (Rep a)) => String
  exprOutputDecs = exprDecs @a "out"

  default exprAssign :: (Generic a, GExprVars (Rep a)) => a -> String
  exprAssign x = unlines $ zipWith makeAssign fields values
   where
    fields = toList $ gExprFields @(Rep a)
    values = toList $ gExprValues $ from x
    makeAssign (fieldName, _) value =
      concat [prefixExprVar fieldName, " = ", value, ";"]

instance ExprVars () where
  exprVars = ()
  exprInputDecs = ""
  exprOutputDecs = ""
  exprAssign () = ""

exprDecs :: forall a. (GExprVars (Rep a)) => String -> String
exprDecs str =
  unlines $
    flip map fields $ \(fieldName, typeName) ->
      concat [str, " ", typeName, " ", prefixExprVar fieldName, ";"]
 where
  fields = toList $ gExprFields @(Rep a)

class GExprVars (f :: K.Type -> K.Type) where
  gExprVars :: f x
  gExprFields :: DL.DList (String, String)
  gExprValues :: f x -> DL.DList String

-- Data: pass-through
instance (GExprVars f) => GExprVars (D1 c f) where
  gExprVars = M1 $ gExprVars @f
  {-# INLINE gExprVars #-}
  gExprFields = gExprFields @f
  {-# INLINE gExprFields #-}
  gExprValues (M1 x) = gExprValues x
  {-# INLINE gExprValues #-}

-- Cons: pass-through
instance (GExprVars f) => GExprVars (C1 c f) where
  gExprVars = M1 $ gExprVars @f
  {-# INLINE gExprVars #-}
  gExprFields = gExprFields @f
  {-# INLINE gExprFields #-}
  gExprValues (M1 x) = gExprValues x
  {-# INLINE gExprValues #-}

-- Product: pass-through
instance (GExprVars f, GExprVars g) => GExprVars (f :*: g) where
  gExprVars = gExprVars @f :*: gExprVars @g
  {-# INLINE gExprVars #-}
  gExprFields = gExprFields @f <> gExprFields @g
  {-# INLINE gExprFields #-}
  gExprValues (x :*: y) = gExprValues x <> gExprValues y
  {-# INLINE gExprValues #-}

-- Record selector and its field
instance
  (GLTypeName c, KnownSymbol name) =>
  GExprVars (S1 ('MetaSel ('Just name) su ss ds) (K1 i (Expr c)))
  where
  gExprVars =
    M1 $ K1 $ Variable $ prefixExprVar (symbolVal @name Proxy)
  {-# INLINE gExprVars #-}
  gExprFields =
    DL.singleton (symbolVal @name Proxy, Text.unpack (glslName @c))
  {-# INLINE gExprFields #-}
  gExprValues (M1 (K1 expr)) = DL.singleton $ render expr
  {-# INLINE gExprValues #-}

--------------------------------------------------------------------------------
-- Boilerplate:
-- Mirroring user-defined records as records of variables via Template Haskell

class Variables a where
  type Vars a = (varsTy :: K.Type) | varsTy -> a
  vars :: Vars a

instance Variables () where
  type Vars () = ()
  vars = ()

deriveGLSLVars :: Name -> Q [Dec]
deriveGLSLVars name = do
  (recTyName, recConName, recFields) <- extractSimpleRecord <$> reify name
  pure $ makeVarsDecs recTyName recConName recFields

extractSimpleRecord :: Info -> (Name, Name, [(Name, Type)])
extractSimpleRecord info = case info of
  TyConI (DataD _ typeName _ _ [RecC conName varBangTypes] _) ->
    recordInfo typeName conName varBangTypes
  TyConI (NewtypeD _ typeName _ _ (RecC conName varBangTypes) _) ->
    recordInfo typeName conName varBangTypes
  _ -> error "Not a record with a single constructor"
 where
  recordInfo typeName conName varBangTypes =
    let fields = map (\(name, _bang, typ) -> (name, typ)) varBangTypes
    in  (typeName, conName, fields)

makeVarsDecs :: Name -> Name -> [(Name, Type)] -> [Dec]
makeVarsDecs tyName conName fields = [dataDec, instanceDec]
 where
  dataDec :: Dec
  dataDec =
    DataD [] varName [] Nothing [dataCon] []

  dataCon :: Con
  dataCon = RecC varName $
    flip map fields $ \(name, ty) ->
      ( name
      , Bang TH.NoSourceUnpackedness TH.SourceStrict
      , ConT ''Expr `AppT` ty
      )

  varName :: Name
  varName = mkName $ "Vars_" ++ nameBase conName

  instanceDec :: Dec
  instanceDec =
    InstanceD
      Nothing
      []
      (ConT ''Variables `AppT` ConT tyName)
      [tyInstDec, valDec]

  tyInstDec :: Dec
  tyInstDec =
    TySynInstD $
      TySynEqn Nothing (ConT ''Vars `AppT` ConT tyName) (ConT varName)

  valDec :: Dec
  valDec = ValD (VarP 'vars) (NormalB (RecConE varName valFields)) []

  valFields :: [FieldExp]
  valFields = flip map fields $ \(name, _) ->
    let val = AppE (ConE 'Variable) $ LitE (StringL (nameBase name))
    in  (name, val)
