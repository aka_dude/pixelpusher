module Client.Graphics.GL.Util (
  fromRGB_A,
  toRGB_A,
  fromFloat22,
) where

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

fromRGB_A :: Float3 -> Float -> Float4
fromRGB_A (Float3 a b c) = Float4 a b c

toRGB_A :: Float4 -> (Float3, Float)
toRGB_A (Float4 a b c d) = (Float3 a b c, d)

fromFloat22 :: Float2 -> Float2 -> Float4
fromFloat22 (Float2 a b) (Float2 c d) = Float4 a b c d
