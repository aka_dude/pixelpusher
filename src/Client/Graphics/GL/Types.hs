{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Deriving boilerplate for records of GLSL types
module Client.Graphics.GL.Types (
  GLTypeName (..),
  GLRecord (..),
) where

import Data.DList (DList)
import Data.DList qualified as DList
import Data.Kind
import Data.Proxy
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.Generics
import GHC.TypeLits

import Pixelpusher.Custom.Float (Float2, Float3, Float4)

--------------------------------------------------------------------------------

class GLTypeName a where
  glslName :: Text

instance GLTypeName Float where
  glslName = "float"
  {-# INLINE glslName #-}

instance GLTypeName Float2 where
  glslName = "vec2"
  {-# INLINE glslName #-}

instance GLTypeName Float3 where
  glslName = "vec3"
  {-# INLINE glslName #-}

instance GLTypeName Float4 where
  glslName = "vec4"
  {-# INLINE glslName #-}

--------------------------------------------------------------------------------

class GLRecord a where
  -- | Field names and the names of their GLSL types
  glFields :: [(Text, Text)]
  default glFields :: (GGLRecord (Rep a)) => [(Text, Text)]
  glFields = DList.toList $ gGlFields @(Rep a)

class GGLRecord (f :: Type -> Type) where
  gGlFields :: DList (Text, Text)

-- Data: passthrough
instance (GGLRecord f) => GGLRecord (M1 D c f) where
  gGlFields = gGlFields @f
  {-# INLINE gGlFields #-}

-- Constructors: passthrough
instance (GGLRecord f) => GGLRecord (M1 C c f) where
  gGlFields = gGlFields @f
  {-# INLINE gGlFields #-}

-- Products
instance (GGLRecord f, GGLRecord g) => GGLRecord (f :*: g) where
  gGlFields = gGlFields @f <> gGlFields @g
  {-# INLINE gGlFields #-}

-- Record selectors and their fields
instance
  (GLTypeName c, KnownSymbol name) =>
  GGLRecord (M1 S ('MetaSel ('Just name) su ss ds) (K1 i c))
  where
  gGlFields =
    DList.singleton (Text.pack (symbolVal @name Proxy), glslName @c)
  {-# INLINE gGlFields #-}
