{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Client.Graphics.GL.Wrapped (
  -- * Re-exports
  module M,

  -- * Context
  GL,
  runGL,

  -- * Buffers
  Buffer,
  genBuffer,
  withBuffer,
  glBufferData,
  glBufferDataEmpty,
  glBufferSubData,

  -- * Textures
  Texture,
  getTexture,
  genTexture,
  withTexture,
  glActiveTexture,

  -- * Vertex arrays
  VertexArray,
  genVertexArray,
  withVertexArray,
  glVertexAttribPointer,
  glEnableVertexAttribArray,
  glVertexAttribDivisor,

  -- * Shaders
  ShaderProgram,
  withShaderProgram,
  makeShaderProgram,

  -- * Uniforms
  UniformLoc,
  glUniform1f,
  glUniform2f,
  glUniform3f,
  glUniform4f,
  glGetUniformLocation,

  -- * Buffers
  GLFloatBufferData (..),
  GLAttribute (..),

  -- * Misc
  sizeOfGLfloat,
  withBlendFunc,
  glDrawArrays,
  glDrawArraysInstanced,
) where

import Control.Logging qualified as Logging
import Control.Monad (foldM, when)
import Data.DList (DList)
import Data.DList qualified as DL
import Data.Kind
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector.Storable qualified as VS
import Data.Vector.Storable.Mutable qualified as VSM
import Foreign
import Foreign.C.String (peekCAString, withCAString, withCAStringLen)
import GHC.Generics
import Graphics.GL.Core33 qualified as Internal
import Graphics.GL.Internal.Shared as M (
  pattern GL_ARRAY_BUFFER,
  pattern GL_CLAMP_TO_EDGE,
  pattern GL_DYNAMIC_DRAW,
  pattern GL_FALSE,
  pattern GL_FLOAT,
  pattern GL_FRAGMENT_SHADER,
  pattern GL_LINEAR,
  pattern GL_LINE_LOOP,
  pattern GL_ONE,
  pattern GL_ONE_MINUS_SRC_ALPHA,
  pattern GL_RED,
  pattern GL_SRC_ALPHA,
  pattern GL_STATIC_DRAW,
  pattern GL_TEXTURE0,
  pattern GL_TEXTURE_2D,
  pattern GL_TEXTURE_MAG_FILTER,
  pattern GL_TEXTURE_MIN_FILTER,
  pattern GL_TEXTURE_WRAP_S,
  pattern GL_TEXTURE_WRAP_T,
  pattern GL_TRIANGLE_STRIP,
  pattern GL_UNPACK_ALIGNMENT,
  pattern GL_UNSIGNED_BYTE,
  pattern GL_VERTEX_SHADER,
 )
import Graphics.GL.Types

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

import Client.Graphics.GL.Types

--------------------------------------------------------------------------------
-- Context

-- | A newtype wrapper around IO for GL actions only.
newtype GL a = GL {_runGL :: IO a}
  deriving newtype (Functor, Applicative, Monad)

runGL :: GL a -> IO a
runGL = _runGL

--------------------------------------------------------------------------------
-- Buffers

newtype Buffer = Buffer GLuint

genBuffer :: GL Buffer
genBuffer =
  GL $ fmap Buffer $ alloca $ \ptr -> Internal.glGenBuffers 1 ptr >> peek ptr

-- | Warning: Uses of `withBuffer` should not be nested.
withBuffer :: GLenum -> Buffer -> GL a -> GL a
withBuffer glArray (Buffer buf) action = GL $ do
  Internal.glBindBuffer glArray buf
  x <- runGL action
  Internal.glBindBuffer glArray 0
  pure x

glBufferData ::
  (Storable a) =>
  GLenum ->
  GLsizeiptr ->
  VS.Vector a ->
  GLenum ->
  GL ()
glBufferData a b vec d =
  GL $
    VS.unsafeWith vec $ \ptr ->
      Internal.glBufferData a b (castPtr ptr) d

-- | Like `glBufferData`, but do not transfer any data.
glBufferDataEmpty :: GLenum -> GLsizeiptr -> GLenum -> GL ()
glBufferDataEmpty a b d = GL $ Internal.glBufferData a b nullPtr d

glBufferSubData ::
  (Storable a) =>
  GLenum ->
  GLsizeiptr ->
  GLsizeiptr ->
  VS.Vector a ->
  GL ()
glBufferSubData a b c vec =
  GL $
    VS.unsafeWith vec $ \ptr ->
      Internal.glBufferSubData a b c (castPtr ptr)

--------------------------------------------------------------------------------
-- Textures

newtype Texture = Texture GLuint

getTexture :: Texture -> GLuint
getTexture (Texture u) = u

genTexture :: GL Texture
genTexture =
  GL $
    fmap Texture $
      alloca $
        \ptr -> Internal.glGenTextures 1 ptr >> peek ptr

-- | Warning: Uses of `withTexture` should not be nested.
withTexture :: GLenum -> Texture -> GL a -> GL a
withTexture target (Texture tex) action = GL $ do
  Internal.glBindTexture target tex
  x <- runGL action
  Internal.glBindTexture target 0
  pure x

glActiveTexture :: GLenum -> GL ()
glActiveTexture a = GL $ Internal.glActiveTexture a

--------------------------------------------------------------------------------
-- Vertex arrays

newtype VertexArray = VertexArray GLenum

genVertexArray :: GL VertexArray
genVertexArray =
  GL $
    fmap VertexArray $
      alloca $
        \ptr -> Internal.glGenVertexArrays 1 ptr >> peek ptr

-- | Warning: Uses of `withVertexArray` should not be nested.
withVertexArray :: VertexArray -> GL a -> GL a
withVertexArray (VertexArray va) action = GL $ do
  Internal.glBindVertexArray va
  a <- runGL action
  Internal.glBindVertexArray 0
  pure a

glVertexAttribPointer ::
  GLuint ->
  GLint ->
  GLenum ->
  GLboolean ->
  GLsizei ->
  Int ->
  GL ()
glVertexAttribPointer a b c d e offset =
  GL $ Internal.glVertexAttribPointer a b c d e (offsetPtr offset)

glEnableVertexAttribArray :: GLuint -> GL ()
glEnableVertexAttribArray a = GL $ Internal.glEnableVertexAttribArray a

glVertexAttribDivisor :: GLuint -> GLuint -> GL ()
glVertexAttribDivisor a b = GL $ Internal.glVertexAttribDivisor a b

--------------------------------------------------------------------------------
-- Shaders

newtype ShaderProgram = ShaderProgram GLuint

-- | Warning: Uses of `withShaderProgram` should not be nested.
withShaderProgram :: ShaderProgram -> GL a -> GL a
withShaderProgram (ShaderProgram prog) action = GL $ do
  Internal.glUseProgram prog
  a <- runGL action
  Internal.glUseProgram 0
  pure a

makeShaderProgram :: String -> String -> GL ShaderProgram
makeShaderProgram vertexSrc fragmentSrc = GL $ do
  shaderProg <- Internal.glCreateProgram
  vertShader <- compileShader GL_VERTEX_SHADER vertexSrc
  fragShader <- compileShader GL_FRAGMENT_SHADER fragmentSrc

  Internal.glAttachShader shaderProg vertShader
  Internal.glAttachShader shaderProg fragShader
  Internal.glLinkProgram shaderProg
  checkProgramLinking shaderProg

  Internal.glDeleteShader vertShader
  Internal.glDeleteShader fragShader

  pure $ ShaderProgram shaderProg

compileShader :: GLenum -> String -> IO GLuint
compileShader shaderType shaderSrc = do
  shaderEnum <- Internal.glCreateShader shaderType
  withCAStringLen shaderSrc $ \(cStr, cStrLen) ->
    -- not sure for how long we need to keep these pointers alive
    with cStr $ \cStrP ->
      with (fromIntegral cStrLen) $ \cStrLenP ->
        Internal.glShaderSource shaderEnum 1 cStrP cStrLenP
  Internal.glCompileShader shaderEnum
  checkShaderCompilation shaderEnum
  pure shaderEnum

checkShaderCompilation :: GLuint -> IO ()
checkShaderCompilation shaderEnum = do
  success <- alloca $ \ptr ->
    Internal.glGetShaderiv shaderEnum Internal.GL_COMPILE_STATUS ptr >> peek ptr
  when (success == Internal.GL_FALSE) $ do
    let infoLogLen :: Integral a => a
        infoLogLen = 512
    allocaArray infoLogLen $ \infoLogCStr ->
      alloca $ \infoLogLengthP -> do
        Internal.glGetShaderInfoLog shaderEnum infoLogLen infoLogLengthP infoLogCStr
        errMsgLen <- peek infoLogLengthP
        errMsg <- peekCAString infoLogCStr
        Logging.warn $
          "Shader program compilation failed. Error message length: "
            <> Text.pack (show errMsgLen)
            <> ". Error message: "
            <> Text.pack errMsg
            <> "."

checkProgramLinking :: GLuint -> IO ()
checkProgramLinking program = do
  success <- alloca $ \ptr ->
    Internal.glGetProgramiv program Internal.GL_LINK_STATUS ptr >> peek ptr
  when (success == Internal.GL_FALSE) $ do
    Logging.log "Program linking failed"

    let infoLogLen :: Integral a => a
        infoLogLen = 512
    allocaArray infoLogLen $ \infoLogCStr ->
      alloca $ \infoLogLengthP -> do
        Internal.glGetProgramInfoLog program infoLogLen infoLogLengthP infoLogCStr

        errMsgLen <- peek infoLogLengthP
        errMsg <- peekCAString infoLogCStr
        Logging.log $
          "Shader program linking failed. Error message length: "
            <> Text.pack (show errMsgLen)
            <> ". Error message: "
            <> Text.pack errMsg
            <> "."

------------------------------------------------------------------------------
-- Uniforms

newtype UniformLoc = UniformLoc GLint

glUniform1f :: UniformLoc -> GLfloat -> GL ()
glUniform1f (UniformLoc i) b = GL $ Internal.glUniform1f i b

glUniform2f :: UniformLoc -> GLfloat -> GLfloat -> GL ()
glUniform2f (UniformLoc i) b c = GL $ Internal.glUniform2f i b c

glUniform3f :: UniformLoc -> GLfloat -> GLfloat -> GLfloat -> GL ()
glUniform3f (UniformLoc i) b c d = GL $ Internal.glUniform3f i b c d

glUniform4f :: UniformLoc -> GLfloat -> GLfloat -> GLfloat -> GLfloat -> GL ()
glUniform4f (UniformLoc i) b c d e = GL $ Internal.glUniform4f i b c d e

glGetUniformLocation :: ShaderProgram -> String -> GL UniformLoc
glGetUniformLocation (ShaderProgram shaderProg) name =
  GL $
    fmap UniformLoc $
      withCAString name $
        Internal.glGetUniformLocation shaderProg

------------------------------------------------------------------------------
-- Filling buffers

-- | A class for types consisting entirely of 'Float's.
class GLFloatBufferData a where
  glFloat_nFloats :: Int
  glFloat_toList :: a -> VS.Vector GLfloat
  glFloat_poke :: Ptr GLfloat -> a -> IO (Ptr GLfloat)

instance GLFloatBufferData Float where
  glFloat_nFloats = 1
  {-# INLINE glFloat_nFloats #-}
  glFloat_toList = VS.singleton
  {-# INLINE glFloat_toList #-}
  glFloat_poke p x = do
    poke p x
    pure $ plusPtr p 4
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float2 where
  glFloat_nFloats = 2
  {-# INLINE glFloat_nFloats #-}
  glFloat_toList (Float2 x y) = VS.fromList [x, y]
  {-# INLINE glFloat_toList #-}
  glFloat_poke p (Float2 x y) = do
    poke p x
    pokeElemOff p 1 y
    pure $ plusPtr p 8
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float3 where
  glFloat_nFloats = 3
  {-# INLINE glFloat_nFloats #-}
  glFloat_toList (Float3 x y z) = VS.fromList [x, y, z]
  {-# INLINE glFloat_toList #-}
  glFloat_poke p (Float3 x y z) = do
    poke p x
    pokeElemOff p 1 y
    pokeElemOff p 2 z
    pure $ plusPtr p 12
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float4 where
  glFloat_nFloats = 4
  {-# INLINE glFloat_nFloats #-}
  glFloat_toList (Float4 x y z w) = VS.fromList [x, y, z, w]
  {-# INLINE glFloat_toList #-}
  glFloat_poke p (Float4 x y z w) = do
    poke p x
    pokeElemOff p 1 y
    pokeElemOff p 2 z
    pokeElemOff p 3 w
    pure $ plusPtr p 16
  {-# INLINE glFloat_poke #-}

--------------------------------------------------------------------------------
-- The 'GLAttribute' class

-- | A class for types representing OpenGL vertex attribute data (or OpenGL
-- instance data) that contain float types only.
class GLAttribute a where
  -- | Number of floats in each field
  fieldSizes :: [Int]

  -- | Concatenation of the floats from all fields, in the same order as
  -- `fieldSizes`.
  toFloatsInstanced :: [a] -> GL (VS.Vector GLfloat)

  -- | GLSL layout declarations
  layoutDecs ::
    -- | Existing number of declarations
    Int ->
    -- | (New total number of declarations, new declarations)
    (Int, Text)

  -- Defaults
  default fieldSizes :: (GAttributeData (Rep a)) => [Int]
  fieldSizes = DL.toList $ gFieldSizes @(Rep a)
  {-# INLINE fieldSizes #-}

  default toFloatsInstanced ::
    (Generic a, GAttributeData (Rep a)) => [a] -> GL (VS.Vector GLfloat)
  toFloatsInstanced xs =
    GL $ do
      let len = length xs
          fieldSize = sum (fieldSizes @a)
      floatMVec@(VSM.MVector _ foreignPtr) <- VSM.new $ len * fieldSize
      _ <-
        withForeignPtr foreignPtr $ \ptr ->
          foldM (\p -> gPokeFloats p . from) ptr xs
      VS.unsafeFreeze floatMVec
  {-# INLINE toFloatsInstanced #-}

  default layoutDecs :: (GLRecord a) => Int -> (Int, Text)
  layoutDecs i =
    let fields = glFields @a
        layoutLines = Text.unlines $ zipWith f [i ..] fields
         where
          f loc (fieldName, typeName) =
            Text.concat
              [ "layout (location = "
              , Text.pack (show loc)
              , ") in "
              , typeName
              , " "
              , fieldName
              , ";"
              ]
    in  (i + length fields, layoutLines)

-- | A class for deriving 'GLAtribute' for non-unit records in which all fields
-- are instances of 'GLFloatBufferData'.
class GAttributeData (f :: Type -> Type) where
  gFieldSizes :: DList Int
  gToFloats :: f x -> DList (VS.Vector GLfloat)
  gPokeFloats :: Ptr GLfloat -> f x -> IO (Ptr GLfloat)

-- Metadata (data, constructors, record selectors): passthrough
instance (GAttributeData f) => GAttributeData (M1 i c f) where
  gFieldSizes = gFieldSizes @f
  {-# INLINE gFieldSizes #-}
  gToFloats (M1 x) = gToFloats x
  {-# INLINE gToFloats #-}
  gPokeFloats ptr (M1 x) = gPokeFloats ptr x
  {-# INLINE gPokeFloats #-}

-- Products: passthrough
instance (GAttributeData f, GAttributeData g) => GAttributeData (f :*: g) where
  gFieldSizes = gFieldSizes @f <> gFieldSizes @g
  {-# INLINE gFieldSizes #-}
  gToFloats (x1 :*: x2) = gToFloats x1 <> gToFloats x2
  {-# INLINE gToFloats #-}
  gPokeFloats ptr0 (x1 :*: x2) = do
    ptr1 <- gPokeFloats ptr0 x1
    gPokeFloats ptr1 x2
  {-# INLINE gPokeFloats #-}

-- Fields
instance (GLFloatBufferData c) => GAttributeData (K1 i c) where
  gFieldSizes = DL.singleton $ glFloat_nFloats @c
  {-# INLINE gFieldSizes #-}
  gToFloats (K1 x) = DL.singleton $ glFloat_toList x
  {-# INLINE gToFloats #-}
  gPokeFloats ptr (K1 x) = glFloat_poke ptr x
  {-# INLINE gPokeFloats #-}

--------------------------------------------------------------------------------
-- Basic 'GLAttribute' instances

-- | Empty attributes
instance GLAttribute () where
  fieldSizes = []
  toFloatsInstanced _ = pure VS.empty
  layoutDecs i = (i, mempty)

------------------------------------------------------------------------------
-- Misc

{-# INLINEABLE sizeOfGLfloat #-}
sizeOfGLfloat :: Integral a => a
sizeOfGLfloat = 4

-- | Warning: Uses of `withBlendFunc` should not be nested.
withBlendFunc :: GLenum -> GLenum -> GL a -> GL a
withBlendFunc sfactor dfactor action = GL $ do
  Internal.glBlendFunc sfactor dfactor
  x <- runGL action
  Internal.glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
  pure x

glDrawArrays :: GLenum -> GLint -> GLsizei -> GL ()
glDrawArrays a b c = GL $ Internal.glDrawArrays a b c

glDrawArraysInstanced :: GLenum -> GLint -> GLsizei -> GLsizei -> GL ()
glDrawArraysInstanced a b c d = GL $ Internal.glDrawArraysInstanced a b c d

------------------------------------------------------------------------------
-- Helpers

offsetPtr :: Int -> Ptr a
offsetPtr offset = castPtr $ plusPtr nullPtr offset
