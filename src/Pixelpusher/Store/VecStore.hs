{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generating boilerplate for relating records of scalars to records of
-- arrays
module Pixelpusher.Store.VecStore (
  VecStore (..),
  UnitVecStore (..),
  BoxedVecStore (..),
  StorableVecStore (..),
  makeVecStore,
) where

import Prelude hiding (elem, init, read)

import Control.Monad.ST
import Data.Bool (bool)
import Data.List (foldl')
import Data.Serialize (Serialize)
import Data.Traversable (for)
import Data.Vector qualified as V
import Data.Vector.Generic qualified as VG
import Data.Vector.Generic.Mutable qualified as VGM
import Data.Vector.Mutable qualified as VM

-- "Data.Vector.Serialize" defines orphan instances for serializing vectors.
-- These instances are not required by this module, but by modules that import
-- this one.
import Data.Vector.Serialize ()
import Data.Vector.Storable qualified as VS
import Data.Vector.Storable.Mutable qualified as VSM
import GHC.Generics (Generic)
import Language.Haskell.TH

--------------------------------------------------------------------------------

-- | Versions of record types where fields are replaced with mutable vectors.
class VecStore elem vs | vs -> elem where
  type Snapshot vs

  init :: (forall v a. (VGM.MVector v a) => ST s (v s a)) -> ST s (vs s)
  read ::
    (forall v a. (VGM.MVector v a) => v s a -> ST s a) ->
    vs s ->
    ST s elem
  write ::
    (forall v a. (VGM.MVector v a) => v s a -> a -> ST s ()) ->
    vs s ->
    elem ->
    ST s ()
  freeze ::
    (forall v a. (VG.Vector v a) => VG.Mutable v s a -> ST s (v a)) ->
    vs s ->
    ST s (Snapshot vs)
  thaw ::
    (forall v a. (VG.Vector v a) => v a -> ST s (VG.Mutable v s a)) ->
    Snapshot vs ->
    ST s (vs s)
  copy ::
    (forall v a. (VGM.MVector v a) => v s a -> v s a -> ST s ()) ->
    vs s -> -- target
    vs s -> -- source
    ST s ()

--------------------------------------------------------------------------------
-- Unit vector instance

data UnitVecStore s = UnitVecStore

instance VecStore () UnitVecStore where
  type Snapshot UnitVecStore = ()
  init _ = pure UnitVecStore
  read _ UnitVecStore = pure ()
  {-# INLINE read #-}
  write _ _ () = pure ()
  {-# INLINE write #-}
  freeze _ UnitVecStore = pure ()
  thaw _ () = pure UnitVecStore
  copy _ UnitVecStore UnitVecStore = pure ()

--------------------------------------------------------------------------------
-- Boxed vector instance

newtype BoxedVecStore a s = BoxedVecStore
  {getBoxedMVec :: VM.MVector s a}

instance VecStore a (BoxedVecStore a) where
  type Snapshot (BoxedVecStore a) = V.Vector a
  init f = BoxedVecStore <$> f
  read f = f . getBoxedMVec
  {-# INLINE read #-}
  write f v = f (getBoxedMVec v)
  {-# INLINE write #-}
  freeze f = f . getBoxedMVec
  thaw f = fmap BoxedVecStore . f
  copy f target source = f (getBoxedMVec target) (getBoxedMVec source)

--------------------------------------------------------------------------------
-- Storable vector instance

newtype StorableVecStore a s = StorableVecStore
  {getStorableMVec :: VSM.MVector s a}

instance VS.Storable a => VecStore a (StorableVecStore a) where
  type Snapshot (StorableVecStore a) = VS.Vector a

  init f = StorableVecStore <$> f
  read f = f . getStorableMVec
  {-# INLINE read #-}
  write f v = f (getStorableMVec v)
  {-# INLINE write #-}
  freeze f = f . getStorableMVec
  thaw f = fmap StorableVecStore . f
  copy f target source = f (getStorableMVec target) (getStorableMVec source)

--------------------------------------------------------------------------------
-- Template Haskell

-- | Derive a `VecStore` instance for a record with a single constructor.
--
-- This type generates records like the input record but with the fields
-- wrapped in `Vector`s and `MVector`s.
--
-- MVector type:
-- - Type/Data constructor suffix: "Vec"
-- - field prefix: "vec_"
--
-- Vector type:
-- - Type/Data constructor suffix: "VecSnapshot"
-- - field prefix: "vecSnapshot_"
makeVecStore :: Name -> Q [Dec]
makeVecStore name = do
  recordNamesTypes@(recTyName, recConName, recFields) <-
    extractSimpleRecord <$> reify name
  annotatedFields <- for recFields $ \(fieldName, fieldTy) -> do
    isStorable <-
      bool IsNotStorable IsStorable <$> isInstance ''VS.Storable [fieldTy]
    pure (fieldName, fieldTy, isStorable)
  let (mutVecTypeDecs, mutVecNames) =
        makeMutVecType recTyName recConName annotatedFields
      (vecTypeDecs, vecNames) =
        makeVecType recTyName recConName annotatedFields
      vecStoreInstanceDecs =
        makeVecStoreInstance
          ((fmap . map) fst recordNamesTypes)
          mutVecNames
          vecNames
  pure $ mutVecTypeDecs ++ vecTypeDecs ++ vecStoreInstanceDecs

data IsStorable = IsStorable | IsNotStorable

extractSimpleRecord :: Info -> (Name, Name, [(Name, Type)])
extractSimpleRecord info = case info of
  (TyConI (DataD _ typeName _ _ [RecC conName varBangTypes] _)) ->
    let fields = map (\(name, _bang, typ) -> (name, typ)) varBangTypes
    in  (typeName, conName, fields)
  _ -> error "Not a record with a single constructor"

makeMutVecType ::
  Name -> Name -> [(Name, Type, IsStorable)] -> ([Dec], (Name, Name, [Name]))
makeMutVecType typeName conName fields =
  ([vecDataDec], (vecTypeName, vecConName, vecFieldNames))
 where
  vecDataDec =
    DataD
      []
      vecTypeName
      [PlainTV stateTyVar ()]
      Nothing
      [vecCon]
      []
  vecCon = RecC vecConName vecFields
  vecFields = flip map fields $ \(name, ty, isStorable) ->
    let mutVecTy = case isStorable of
          IsStorable -> ''VSM.MVector
          IsNotStorable -> ''VM.MVector
        vecFieldTy = ConT mutVecTy `AppT` VarT stateTyVar `AppT` ty
        fieldBang = Bang NoSourceUnpackedness SourceStrict
    in  (mkVecFieldName name, fieldBang, vecFieldTy)
  vecFieldNames = map (\(name, _, _) -> name) vecFields

  -- Names
  vecTypeName = mkName $ nameBase typeName ++ "Vec"
  stateTyVar = mkName "s"
  vecConName = mkName $ nameBase conName ++ "Vec"
  mkVecFieldName fieldName =
    mkName $ ("vec_" ++) $ dropWhile (== '_') $ nameBase fieldName

makeVecType ::
  Name -> Name -> [(Name, Type, IsStorable)] -> ([Dec], (Name, Name, [Name]))
makeVecType typeName conName fields =
  (decls, (vecTypeName, vecConName, vecFieldNames))
 where
  decls = [vecDataDec, vecDeriveDec]

  vecDataDec = DataD [] vecTypeName [] Nothing [vecCon] derive
  vecCon = RecC vecConName vecFields
  vecFields = flip map fields $ \(name, ty, isStorable) ->
    let vecTy = case isStorable of
          IsStorable -> ''VS.Vector
          IsNotStorable -> ''V.Vector
        vecFieldTy = ConT vecTy `AppT` ty
        fieldBang = Bang NoSourceUnpackedness SourceStrict
    in  (mkVecFieldName name, fieldBang, vecFieldTy)
  derive = [DerivClause Nothing [ConT ''Generic]]
  vecFieldNames = map (\(name, _, _) -> name) vecFields

  vecDeriveDec =
    InstanceD Nothing [] (ConT ''Serialize `AppT` ConT vecTypeName) []

  -- Names
  vecTypeName = mkName $ nameBase typeName ++ "VecSnapshot"
  vecConName = mkName $ nameBase conName ++ "VecSnapshot"
  mkVecFieldName fieldName =
    mkName $ ("vecSnapshot_" ++) $ dropWhile (== '_') $ nameBase fieldName

makeVecStoreInstance ::
  (Name, Name, [Name]) -> -- element names
  (Name, Name, [Name]) -> -- mutable vector names
  (Name, Name, [Name]) -> -- vector names
  [Dec]
makeVecStoreInstance elem mutVec vec =
  (: []) $
    InstanceD
      Nothing
      []
      (ConT ''VecStore `AppT` ConT elemTyName `AppT` ConT mutVecTyName)
      -- `Snapshot` type family instance
      [ TySynInstD $
          TySynEqn
            Nothing
            (ConT ''Snapshot `AppT` ConT mutVecTyName)
            (ConT vecTyName)
      , -- `init` implementation
        FunD
          'init
          [ let initFunc = mkName "f"
            in  Clause
                  [VarP initFunc]
                  ( NormalB $
                      mkAppChain $
                        (ConE mutVecConName :) $
                          replicate (length mutVecFields) (VarE initFunc)
                  )
                  []
          ]
      , -- `read` implementation
        FunD
          'read
          [ let readFunc = mkName "f"
                mutVec' = mkName "v"
            in  Clause
                  [VarP readFunc, VarP mutVec']
                  ( NormalB $
                      mkAppChain $
                        (ConE elemConName :) $
                          flip map mutVecFields $ \mutVecField ->
                            VarE readFunc
                              `AppE` AppE (VarE mutVecField) (VarE mutVec')
                  )
                  []
          ]
      , PragmaD $
          InlineP 'read Inline FunLike AllPhases
      , -- `write` implementation
        FunD
          'write
          [ let writeFunc = mkName "f"
                mutVec' = mkName "v"
                elem' = mkName "x"
            in  Clause
                  [VarP writeFunc, VarP mutVec', VarP elem']
                  ( NormalB $
                      DoE Nothing $
                        zipWith
                          ( \mutVecField elemField ->
                              NoBindS $
                                VarE writeFunc
                                  `AppE` AppE (VarE mutVecField) (VarE mutVec')
                                  `AppE` AppE (VarE elemField) (VarE elem')
                          )
                          mutVecFields
                          elemFields
                  )
                  []
          ]
      , PragmaD $
          InlineP 'write Inline FunLike AllPhases
      , -- `freeze` implementation
        FunD
          'freeze
          [ let freezeFunc = mkName "f"
                mutVec' = mkName "v"
            in  Clause
                  [VarP freezeFunc, VarP mutVec']
                  ( NormalB $
                      mkAppChain $
                        (ConE vecConName :) $
                          flip map mutVecFields $ \mutVecField ->
                            VarE freezeFunc
                              `AppE` AppE (VarE mutVecField) (VarE mutVec')
                  )
                  []
          ]
      , -- `thaw` implementation
        FunD
          'thaw
          [ let thawFunc = mkName "f"
                vec' = mkName "v"
            in  Clause
                  [VarP thawFunc, VarP vec']
                  ( NormalB $
                      mkAppChain $
                        (ConE mutVecConName :) $
                          flip map vecFields $ \vecField ->
                            VarE thawFunc
                              `AppE` AppE (VarE vecField) (VarE vec')
                  )
                  []
          ]
      , -- `copy` implementation
        FunD
          'copy
          [ let copyFunc = mkName "f"
                target = mkName "target"
                source = mkName "source"
            in  Clause
                  [VarP copyFunc, VarP target, VarP source]
                  ( NormalB $
                      DoE Nothing $
                        flip map mutVecFields $ \mutVecField ->
                          NoBindS $
                            VarE copyFunc
                              `AppE` AppE (VarE mutVecField) (VarE target)
                              `AppE` AppE (VarE mutVecField) (VarE source)
                  )
                  []
          ]
      ]
 where
  (elemTyName, elemConName, elemFields) = elem
  (mutVecTyName, mutVecConName, mutVecFields) = mutVec
  (vecTyName, vecConName, vecFields) = vec

mkAppChain :: [Exp] -> Exp
mkAppChain (exp1 : exp2 : exps) =
  let base = InfixE (Just exp1) (VarE '(<$>)) (Just exp2)
  in  foldl' (\e1 e2 -> InfixE (Just e1) (VarE '(<*>)) (Just e2)) base exps
mkAppChain _ = error "mkAppChain: At least two expressions required"
