-- | Protocol for sending messages over TCP
module Pixelpusher.Network.TCPProtocol (
  TCPConnection,
  ReconnectFlag (..),
  TCPConnectionClosedException (..),
  tcpSendData,
  tcpSendClose,
  tcpRecvData,
  runTCPClient,
  runTCPServer,
) where

import Control.Concurrent (forkFinally, forkIO)
import Control.Concurrent.Async (async, wait)
import Control.Exception
import Control.Monad (forever, when)
import Data.Bits (finiteBitSize)
import Data.ByteString qualified as BS
import Data.Functor (void)
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Serialize.Text ()
import Data.Text (Text)
import Data.Word (Word32)
import GHC.Generics
import Network.Socket
import Network.Socket.ByteString

--------------------------------------------------------------------------------

-- We use a trivial protocol for sending send messages over TCP: we prefix each
-- message by its MsgLenRep. We also try to work around the use of blocking
-- network IO by the Windows runtime.

newtype TCPConnection = TCPConnection {getSocket :: Socket}

type MsgLenRep = Word32

lengthBytes :: Int
lengthBytes = fromIntegral $ finiteBitSize @MsgLenRep 0 `div` 8

-- | Message types
data Message
  = DataMsg !BS.ByteString
  | CloseMsg ReconnectFlag !Text
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ReconnectFlag = Reconnect | DoNotReconnect
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

-- | A exception signalling the closure of the connection. May provide a reason
-- for the closure, and whether your peer intends for you to recononect.
data TCPConnectionClosedException
  = ClosedBecause ReconnectFlag !Text
  | UnexpectedClose
  deriving stock (Show)

instance Exception TCPConnectionClosedException

tcpSendData :: TCPConnection -> BS.ByteString -> IO ()
tcpSendData tcpConn = tcpSendMessage tcpConn . DataMsg

tcpSendClose :: TCPConnection -> ReconnectFlag -> Text -> IO ()
tcpSendClose tcpConn reconnect reason =
  tcpSendMessage tcpConn $ CloseMsg reconnect reason

tcpSendMessage :: TCPConnection -> Message -> IO ()
tcpSendMessage tcpConn msg = do
  let payload = Serialize.encode msg
  sendAll (getSocket tcpConn) $
    Serialize.encode @MsgLenRep (fromIntegral $ BS.length payload) <> payload

-- Throws a `TCPConnectionClosedException` when either a close message is
-- received or when TCP the connection is closed.
tcpRecvData :: TCPConnection -> IO BS.ByteString
tcpRecvData tcpConn = do
  lenPrefixBytes <- recvAll tcpConn lengthBytes
  let lenPrefix =
        fromIntegral $
          either error id $
            Serialize.decode @MsgLenRep lenPrefixBytes

  msgBytes <- recvAll tcpConn lenPrefix

  case either error id $ Serialize.decode @Message msgBytes of
    CloseMsg reconnect reason -> throwIO $ ClosedBecause reconnect reason
    DataMsg bs -> pure bs

-- Throws a `TCPConnectionClosedException` when either a close message is
-- received or when TCP the connection is closed.
recvAll :: TCPConnection -> Int -> IO BS.ByteString
recvAll tcpConn size
  | size <= 0 = pure BS.empty
  | otherwise = do
      bytes <- recv (getSocket tcpConn) size
      when (BS.null bytes) $ throwIO UnexpectedClose
      if BS.length bytes < size
        then do
          let missing = size - BS.length bytes
          moreBytes <- recvAll tcpConn missing
          pure $ bytes <> moreBytes
        else pure bytes

--------------------------------------------------------------------------------
-- Client and server

-- Based on the examples from the `network` package

-- This function connects and closes a TCP asynchronously to work around the
-- uninterruptible system IO of GHC's Windows runtime.
-- See https://gitlab.haskell.org/ghc/ghc/-/issues/7353
runTCPClient :: HostName -> ServiceName -> (TCPConnection -> IO a) -> IO a
runTCPClient hostName serviceName action = mask $ \restore -> do
  tcpSockAsync <- async connectClientTCPSocket
  tcpSock <- restore $ wait tcpSockAsync
  restore (action tcpSock)
    `finally` forkIO (handle printException $ gracefulClose (getSocket tcpSock) 5000)
 where
  connectClientTCPSocket :: IO TCPConnection
  connectClientTCPSocket = do
    let hints = defaultHints{addrSocketType = Stream}
    addr <-
      head <$> getAddrInfo (Just hints) (Just hostName) (Just serviceName)
    sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
    setSocketOption sock NoDelay 1
    connect sock $ addrAddress addr
    pure $ TCPConnection sock

runTCPServer ::
  Maybe HostName -> ServiceName -> (TCPConnection -> IO a) -> IO S
runTCPServer mHostName serviceName server = do
  let hints =
        defaultHints
          { addrFlags = [AI_PASSIVE]
          , addrSocketType = Stream
          }
  addr <- head <$> getAddrInfo (Just hints) mHostName (Just serviceName)
  bracket (open addr) close loop
 where
  open :: AddrInfo -> IO Socket
  open addr = do
    sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
    setSocketOption sock ReuseAddr 1
    setSocketOption sock NoDelay 1
    withFdSocket sock setCloseOnExecIfNeeded
    bind sock $ addrAddress addr
    listen sock 1024
    pure sock

  loop :: Socket -> IO a
  loop sock = forever $ do
    (conn, _peer) <- accept sock
    void $
      forkFinally
        (server (TCPConnection conn))
        (const $ handle printException $ gracefulClose conn 5000)

-- | A catch-all handler
printException :: SomeException -> IO ()
printException = print
