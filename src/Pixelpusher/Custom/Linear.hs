{-
The contents of this module is taken from the linear-1.21.9 package.

Copyright 2011-2015 Edward Kmett

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. Neither the name of the author nor the names of his contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
-}

module Pixelpusher.Custom.Linear (
  V2 (V2),
  V3 (V3),
  V4 (V4),
  Epsilon (nearZero),
) where

import Data.Functor ((<&>))
import Data.Serialize (Serialize)
import Foreign.C.Types (CDouble, CFloat)
import GHC.Generics
import Lens.Micro.Internal (Field1 (..), Field2 (..))

--------------------------------------------------------------------------------
-- Strict versions of vectors

data V2 a = V2 !a !a
  deriving stock (Eq, Functor, Generic, Show)
  deriving anyclass (Serialize)

data V3 a = V3 !a !a !a
  deriving stock (Eq, Functor, Generic, Show)
  deriving anyclass (Serialize)

data V4 a = V4 !a !a !a !a
  deriving stock (Eq, Functor, Generic, Show)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------
-- Field access lenses

instance Field1 (V2 a) (V2 a) a a where
  _1 f (V2 x y) = f x <&> \x' -> V2 x' y
  {-# INLINE _1 #-}

instance Field2 (V2 a) (V2 a) a a where
  _2 f (V2 x y) = f y <&> \y' -> V2 x y'
  {-# INLINE _2 #-}

--------------------------------------------------------------------------------
-- 'Epsilon' typeclass

-- | Provides a fairly subjective test to see if a quantity is near zero.
--
-- >>> nearZero (1e-11 :: Double)
-- False
--
-- >>> nearZero (1e-17 :: Double)
-- True
--
-- >>> nearZero (1e-5 :: Float)
-- False
--
-- >>> nearZero (1e-7 :: Float)
-- True
class Num a => Epsilon a where
  -- | Determine if a quantity is near zero.
  nearZero :: a -> Bool

-- | @'abs' a '<=' 1e-6@
instance Epsilon Float where
  nearZero a = abs a <= 1e-6

-- | @'abs' a '<=' 1e-12@
instance Epsilon Double where
  nearZero a = abs a <= 1e-12

-- | @'abs' a '<=' 1e-6@
instance Epsilon CFloat where
  nearZero a = abs a <= 1e-6

-- | @'abs' a '<=' 1e-12@
instance Epsilon CDouble where
  nearZero a = abs a <= 1e-12
