{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Custom.SmallList (
  SmallList,
  makeSmallList,
  fromSmallList,
) where

import Control.Monad (replicateM)
import Data.Foldable (traverse_)
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Word (Word8)

-- | Wrapper lists of length at most 255 for a smaller binary serialization
-- size.
--
-- Invariant: `sl_size` is the length of `sl_list`
data SmallList a = SmallList
  { sl_size :: Word8
  , sl_list :: [a]
  }

makeSmallList :: [a] -> Maybe (SmallList a)
makeSmallList xs
  | len <= 255 = Just SmallList{sl_size = fromIntegral len, sl_list = xs}
  | otherwise = Nothing
 where
  len = length xs

fromSmallList :: SmallList a -> [a]
fromSmallList = sl_list

instance (Serialize a) => Serialize (SmallList a) where
  put :: SmallList a -> Serialize.Put
  put SmallList{..} = do
    Serialize.put sl_size
    traverse_ Serialize.put sl_list

  get :: Serialize.Get (SmallList a)
  get = do
    sl_size <- Serialize.get @Word8
    sl_list <- replicateM (fromIntegral sl_size) Serialize.get
    pure SmallList{..}
