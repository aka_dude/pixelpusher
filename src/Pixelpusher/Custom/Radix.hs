{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

{-
The following is the license of "vector-algorithms" package on which
this module is based.

Copyright (c) 2015 Dan Doel
Copyright (c) 2015 Tim Baumann

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. Neither the name of the author nor the names of his contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
-}

-- | This module is a specialization of "Data.Vector.Algorithms.Radix"
-- from the "vector-algorithms" package, for the purpose of avoiding
-- performance issues caused by a lack of inlining and specialization.
module Pixelpusher.Custom.Radix (sort, sortBy, Radix (..)) where

import Prelude hiding (length, read)

import Control.Monad
import Control.Monad.ST

import Data.Bits (shiftR, (.&.))
import Data.Vector.Generic.Mutable
import Data.Vector.Primitive.Mutable qualified as PV

import Data.Word

class Radix e where
  -- | The number of passes necessary to sort an array of es
  passes :: e -> Int

  -- | The size of an auxiliary array
  size :: e -> Int

  -- | The radix function parameterized by the current pass
  radix :: Int -> e -> Int

instance Radix Word8 where
  passes _ = 1
  {-# INLINE passes #-}
  size _ = 256
  {-# INLINE size #-}
  radix _ = fromIntegral
  {-# INLINE radix #-}

instance Radix Word16 where
  passes _ = 2
  {-# INLINE passes #-}
  size _ = 256
  {-# INLINE size #-}
  radix 0 e = fromIntegral (e .&. 255)
  radix 1 e = fromIntegral ((e `shiftR` 8) .&. 255)
  {-# INLINE radix #-}

-- | Sorts an array based on the Radix instance.
sort ::
  forall e v s.
  (MVector v e, Radix e) =>
  v s e ->
  ST s ()
sort = sortBy (passes e) (size e) radix
 where
  e :: e
  e = undefined
{-# INLINEABLE sort #-}

-- | Radix sorts an array using custom radix information requires the
-- number of passes to fully sort the array, the size of of auxiliary
-- arrays necessary (should be one greater than the maximum value
-- returned by the radix function), and a radix function, which takes
-- the pass and an element, and returns the relevant radix.
sortBy ::
  (MVector v e) =>
  -- | the number of passes
  Int ->
  -- | the size of auxiliary arrays
  Int ->
  -- | the radix function
  (Int -> e -> Int) ->
  -- | the array to be sorted
  v s e ->
  ST s ()
sortBy passes' size' rdx arr = do
  tmp <- new (length arr)
  count <- new size'
  radixLoop passes' rdx arr tmp count
{-# INLINE sortBy #-}

radixLoop ::
  (MVector v e) =>
  Int -> -- passes
  (Int -> e -> Int) -> -- radix function
  v s e -> -- array to sort
  v s e -> -- temporary array
  PV.MVector s Int -> -- radix count array
  ST s ()
radixLoop passes' rdx src dst count = go False 0
 where
  go swap' k
    | k < passes' =
        if swap'
          then body rdx dst src count k >> go (not swap') (k + 1)
          else body rdx src dst count k >> go (not swap') (k + 1)
    | otherwise = when swap' (unsafeCopy src dst)
{-# INLINE radixLoop #-}

body ::
  (MVector v e) =>
  (Int -> e -> Int) -> -- radix function
  v s e -> -- source array
  v s e -> -- destination array
  PV.MVector s Int -> -- radix count
  Int -> -- current pass
  ST s ()
body rdx src dst count k = do
  countLoop (rdx k) src count
  accumulate count
  moveLoop k rdx src dst count
{-# INLINE body #-}

accumulate :: PV.MVector s Int -> ST s ()
accumulate count = go 0 0
 where
  len = length count
  go i acc
    | i < len = do
        ci <- unsafeRead count i
        unsafeWrite count i acc
        go (i + 1) (acc + ci)
    | otherwise = return ()
{-# INLINE accumulate #-}

moveLoop ::
  (MVector v e) =>
  Int ->
  (Int -> e -> Int) ->
  v s e ->
  v s e ->
  PV.MVector s Int ->
  ST s ()
moveLoop k rdx src dst prefix = go 0
 where
  len = length src
  go i
    | i < len = do
        srci <- unsafeRead src i
        pf <- inc prefix (rdx k srci)
        unsafeWrite dst pf srci
        go (i + 1)
    | otherwise = return ()
{-# INLINE moveLoop #-}

countLoop ::
  (MVector v e) =>
  (e -> Int) ->
  v s e ->
  PV.MVector s Int ->
  ST s ()
countLoop rdx src count = set count 0 >> go 0
 where
  len = length src
  go i
    | i < len = unsafeRead src i >>= inc count . rdx >> go (i + 1)
    | otherwise = return ()
{-# INLINE countLoop #-}

inc :: (MVector v Int) => v s Int -> Int -> ST s Int
inc arr i = unsafeRead arr i >>= \e -> unsafeWrite arr i (e + 1) >> return e
{-# INLINE inc #-}
