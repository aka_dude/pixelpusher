-- | Assorted utility functions
module Pixelpusher.Custom.Util (
  listSetBits,
  whenJust,
  caseJust,
  mapMaybeM,
  expectJust,
  ceilDiv,
  uniformUnitDisc,
  modifyReturnNewSTRef',
  modifyReturnOldSTRef',
  modifyResultSTRef',
  eitherToMaybe,
) where

import Control.Monad.ST
import Data.Bits
import Data.STRef (STRef, readSTRef, writeSTRef)
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed

-- | Returns the indices of set bits in increasing order
listSetBits :: FiniteBits b => b -> [Int]
listSetBits bits = go bits 0
 where
  go !b !shiftAcc =
    if b == zeroBits
      then []
      else
        let trailingZeros = countTrailingZeros b
            !setBitNum = shiftAcc + trailingZeros
        in  setBitNum : go (shiftR b (trailingZeros + 1)) (setBitNum + 1)

whenJust :: Applicative m => Maybe a -> (a -> m ()) -> m ()
whenJust m f = maybe (pure ()) f m

caseJust :: Applicative m => (a -> m ()) -> Maybe a -> m ()
caseJust = flip whenJust

mapMaybeM :: Monad m => (a -> m (Maybe b)) -> [a] -> m [b]
mapMaybeM f (x : xs) =
  f x >>= \case
    Nothing -> mapMaybeM f xs
    Just y -> (:) y <$> mapMaybeM f xs
mapMaybeM _ [] = pure []
{-# INLINE mapMaybeM #-}

expectJust :: String -> Maybe a -> a
expectJust _ (Just a) = a
expectJust errMsg Nothing = error errMsg

ceilDiv :: Integral a => a -> a -> a
ceilDiv n d = (n - 1) `div` d + 1

uniformUnitDisc :: MWC.Gen s -> ST s Fixed2
uniformUnitDisc gen = do
  posAngle <- MWC.uniformR (0, 2 * pi) gen
  posRadius <- sqrt <$> MWC.uniformR (0, 1) gen
  pure $ Fixed.map (* posRadius) $ Fixed.angle posAngle

modifyReturnNewSTRef' :: STRef s a -> (a -> a) -> ST s a
modifyReturnNewSTRef' ref f = do
  x <- f <$> readSTRef ref
  writeSTRef ref x
  seq x $ pure x

modifyReturnOldSTRef' :: STRef s a -> (a -> a) -> ST s a
modifyReturnOldSTRef' ref f = do
  x <- readSTRef ref
  let y = f x
  writeSTRef ref y
  seq y $ pure x

modifyResultSTRef' :: STRef s a -> (a -> (b, a)) -> ST s b
modifyResultSTRef' ref f = do
  x <- readSTRef ref
  let (r, y) = f x
  writeSTRef ref y
  seq y $ pure r

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe (Right b) = Just b
eitherToMaybe (Left _) = Nothing
