module Pixelpusher.Custom.Clock (
  runClock,
  runAdjustableClock,
  runApproximateClock,
  getMonotonicTimeNSecInt,
  threadDelayNSec,
) where

import Control.Concurrent (threadDelay)
import Control.Monad (forever, when)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.IORef
import GHC.Clock

--------------------------------------------------------------------------------

-- Reminder: `threadDelay` only guarantees that a thread will sleep for _at
-- least_ the specified number of microseconds.

-- | Periodically call the given IO action. The period is specified in
-- nanoseconds.
runClock :: forall m a. MonadIO m => Int -> m () -> m a
runClock period_ns action = do
  startTime_ns <- liftIO getMonotonicTimeNSecInt
  tick (startTime_ns + period_ns)
 where
  tick :: Int -> m a
  tick targetTime_ns = do
    liftIO $ sleepUntil targetTime_ns
    action
    tick (targetTime_ns + period_ns)
{-# INLINEABLE runClock #-}

-- | Periodically call the given IO action. The period is specified in
-- nanoseconds. The action is given an IO action to shift the timing of the
-- clock.
runAdjustableClock ::
  forall m a. MonadIO m => Int -> ((Int -> IO ()) -> m ()) -> m a
runAdjustableClock period_ns action = do
  delayRef <- liftIO $ newIORef 0
  startTime_ns <- liftIO getMonotonicTimeNSecInt
  tick delayRef (startTime_ns + period_ns)
 where
  tick :: IORef Int -> Int -> m a
  tick delayRef targetTime_ns = do
    liftIO $ sleepUntil targetTime_ns
    action (atomicWriteIORef delayRef)
    delay <- liftIO $ atomicModifyIORef' delayRef (0,)
    tick delayRef (targetTime_ns + period_ns + delay)
{-# INLINEABLE runAdjustableClock #-}

-- | Periodically call the given IO action. The period is specified in
-- nanoseconds. This clock is prone to drift due to the imprecision of
-- `threadDelay`.
runApproximateClock :: forall m a. MonadIO m => Int -> m () -> m a
runApproximateClock period_ns action =
  forever $ action >> liftIO (threadDelayNSec period_ns)
{-# INLINEABLE runApproximateClock #-}

getMonotonicTimeNSecInt :: IO Int
getMonotonicTimeNSecInt = fromIntegral <$> getMonotonicTimeNSec

sleepUntil :: Int -> IO ()
sleepUntil targetTime_ns = do
  currentTime_ns <- getMonotonicTimeNSecInt
  let sleepDuration_ns = targetTime_ns - currentTime_ns
  when (sleepDuration_ns > 0) $ threadDelayNSec sleepDuration_ns

threadDelayNSec :: Int -> IO ()
threadDelayNSec ns = let us = ns `div` 1000 in threadDelay us
{-# INLINE threadDelayNSec #-}
