{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE TypeFamilies #-}

-- | A wrapper around "Data.IntMap.Strict" for newtypes of `Int`.
--
-- This module is intended to be imported qualified.
module Pixelpusher.Custom.WrappedIntMap (
  IsInt (..),
  Identity,
  makeIdentity,
  IntMap,
  -- Construction
  empty,
  singleton,
  fromList,
  fromAscList,
  fromListWith,
  -- Insertion
  insert,
  -- Deletion/Update
  delete,
  -- Query
  lookup,
  member,
  lookupLT,
  lookupGT,
  -- Combine
  intersectionWith,
  -- Traversal
  map,
  mapWithKey,
  traverseWithKey,
  mapKeys,
  -- Conversion
  elems,
  keys,
  toList,
  toAscList,
  -- Filter
  filter,
  filterWithKey,
  split,
  -- Min/Max
  lookupMin,
  lookupMax,
  -- Merge
  merge,
) where

import Prelude hiding (filter, lookup, map)
import Prelude qualified as P

import Data.Bifunctor (first)
import Data.IntMap.Merge.Strict qualified as IM
import Data.IntMap.Strict qualified as IM
import Data.Serialize (Serialize)
import Data.Word (Word16)
import Lens.Micro.Internal (At (..), Index, IxValue, Ixed (..))
import Lens.Micro.Platform.Custom

--------------------------------------------------------------------------------

-- | Show how `a` injects into `Int`, but show it only for this module.
class IsInt a where
  toInt :: a -> Identity Int
  fromInt :: Int -> Identity a

newtype Identity a = Identity {runIdentity :: a}

makeIdentity :: a -> Identity a
makeIdentity = Identity

toInt' :: IsInt a => a -> Int
toInt' = runIdentity . toInt

fromInt' :: IsInt a => Int -> a
fromInt' = runIdentity . fromInt

instance IsInt Int where
  toInt = makeIdentity
  {-# INLINE toInt #-}
  fromInt = makeIdentity
  {-# INLINE fromInt #-}

instance IsInt Word16 where
  toInt = makeIdentity . fromIntegral
  {-# INLINE toInt #-}
  fromInt = makeIdentity . fromIntegral
  {-# INLINE fromInt #-}

--------------------------------------------------------------------------------

newtype IntMap k a = IntMap (IM.IntMap a)
  deriving newtype (Eq, Foldable, Functor, Monoid, Semigroup, Serialize, Show)

instance Traversable (IntMap k) where
  traverse f (IntMap m) = IntMap <$> traverse f m
  {-# INLINE traverse #-}

intMap :: Lens' (IntMap k a) (IM.IntMap a)
intMap f (IntMap im) = IntMap <$> f im

type instance Index (IntMap k a) = k

type instance IxValue (IntMap k a) = a

instance IsInt k => Ixed (IntMap k a) where
  ix k = intMap . ix (toInt' k)

instance IsInt k => At (IntMap k a) where
  at k = intMap . at (toInt' k)

-- Construction

empty :: IntMap k a
empty = IntMap IM.empty
{-# INLINE empty #-}

singleton :: IsInt k => k -> a -> IntMap k a
singleton k a = IntMap $ IM.singleton (toInt' k) a
{-# INLINE singleton #-}

fromList :: IsInt k => [(k, a)] -> IntMap k a
fromList xs = IntMap $ IM.fromList $ (P.map . first) toInt' xs
{-# INLINE fromList #-}

fromAscList :: IsInt k => [(k, a)] -> IntMap k a
fromAscList xs = IntMap $ IM.fromAscList $ (P.map . first) toInt' xs
{-# INLINE fromAscList #-}

fromListWith :: IsInt k => (a -> a -> a) -> [(k, a)] -> IntMap k a
fromListWith f xs = IntMap $ IM.fromListWith f $ (P.map . first) toInt' xs
{-# INLINE fromListWith #-}

-- Insertion

insert :: IsInt k => k -> a -> IntMap k a -> IntMap k a
insert k x (IntMap m) = IntMap $ IM.insert (toInt' k) x m
{-# INLINE insert #-}

-- Deletion/Update

delete :: IsInt k => k -> IntMap k a -> IntMap k a
delete k (IntMap m) = IntMap $ IM.delete (toInt' k) m
{-# INLINE delete #-}

-- Query

lookup :: IsInt k => k -> IntMap k a -> Maybe a
lookup k (IntMap m) = IM.lookup (toInt' k) m
{-# INLINE lookup #-}

member :: IsInt k => k -> IntMap k a -> Bool
member k (IntMap m) = IM.member (toInt' k) m
{-# INLINE member #-}

-- | Note: Orders the keys by their `Int` representations.
lookupLT :: IsInt k => k -> IntMap k a -> Maybe (k, a)
lookupLT k (IntMap m) = first fromInt' <$> IM.lookupLT (toInt' k) m
{-# INLINE lookupLT #-}

-- | Note: Orders the keys by their `Int` representations.
lookupGT :: IsInt k => k -> IntMap k a -> Maybe (k, a)
lookupGT k (IntMap m) = first fromInt' <$> IM.lookupGT (toInt' k) m
{-# INLINE lookupGT #-}

-- Combine

intersectionWith :: (a -> b -> c) -> IntMap k a -> IntMap k b -> IntMap k c
intersectionWith f (IntMap ma) (IntMap mb) =
  IntMap $ IM.intersectionWith f ma mb
{-# INLINE intersectionWith #-}

-- Traversal

map :: (a -> b) -> IntMap k a -> IntMap k b
map f (IntMap m) = IntMap $ IM.map f m
{-# INLINE map #-}

mapWithKey :: IsInt k => (k -> a -> b) -> IntMap k a -> IntMap k b
mapWithKey f (IntMap m) = IntMap $ IM.mapWithKey (f . fromInt') m
{-# INLINE mapWithKey #-}

traverseWithKey ::
  (IsInt k, Applicative f) => (k -> a -> f b) -> IntMap k a -> f (IntMap k b)
traverseWithKey f (IntMap m) =
  IntMap <$> IM.traverseWithKey (f . fromInt') m
{-# INLINE traverseWithKey #-}

mapKeys :: (IsInt k1, IsInt k2) => (k1 -> k2) -> IntMap k1 a -> IntMap k2 a
mapKeys f (IntMap m) = IntMap $ IM.mapKeys (toInt' . f . fromInt') m
{-# INLINE mapKeys #-}

-- Conversion

elems :: IntMap k a -> [a]
elems (IntMap m) = IM.elems m
{-# INLINE elems #-}

keys :: IsInt k => IntMap k a -> [k]
keys (IntMap m) = P.map fromInt' $ IM.keys m
{-# INLINE keys #-}

toList :: IsInt k => IntMap k a -> [(k, a)]
toList (IntMap m) = (P.map . first) fromInt' $ IM.toList m
{-# INLINE toList #-}

toAscList :: IsInt k => IntMap k a -> [(k, a)]
toAscList (IntMap m) = (P.map . first) fromInt' $ IM.toAscList m
{-# INLINE toAscList #-}

-- Filter

filter :: (a -> Bool) -> IntMap k a -> IntMap k a
filter f (IntMap m) = IntMap $ IM.filter f m
{-# INLINE filter #-}

filterWithKey :: IsInt k => (k -> a -> Bool) -> IntMap k a -> IntMap k a
filterWithKey f (IntMap m) = IntMap $ IM.filterWithKey (f . fromInt') m
{-# INLINE filterWithKey #-}

split :: IsInt k => k -> IntMap k a -> (IntMap k a, IntMap k a)
split k (IntMap m) = case IM.split (toInt' k) m of
  (m1, m2) -> (IntMap m1, IntMap m2)
{-# INLINE split #-}

-- Min/Max

-- | Note: Orders the keys by their `Int` representations.
lookupMin :: IsInt k => IntMap k a -> Maybe (k, a)
lookupMin (IntMap m) = first fromInt' <$> IM.lookupMin m
{-# INLINE lookupMin #-}

-- | Note: Orders the keys by their `Int` representations.
lookupMax :: IsInt k => IntMap k a -> Maybe (k, a)
lookupMax (IntMap m) = first fromInt' <$> IM.lookupMax m
{-# INLINE lookupMax #-}

-- Merge

merge ::
  IM.SimpleWhenMissing a c ->
  IM.SimpleWhenMissing b c ->
  IM.SimpleWhenMatched a b c ->
  IntMap k a ->
  IntMap k b ->
  IntMap k c
merge missingLeft missingRight matching (IntMap ma) (IntMap mb) =
  IntMap $ IM.merge missingLeft missingRight matching ma mb
