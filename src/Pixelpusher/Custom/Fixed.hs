-- | Fixed-point numbers
module Pixelpusher.Custom.Fixed (
  Fixed,
  toFloat,
  fromFloat,
  toDouble,
  fromDouble,
  toCFloat,
  approxExp,
  Fixed2 (..),
  toFloats,
  fromFloats,
  toV2Floats,
  fromV2Floats,
  fromVec,
  toDoubles,
  fromDoubles,
  dot,
  map,
  norm,
  normLeq,
  normGt,
  normalize,
  normAndNormalize,
  normSq,
  normSqApprox,
  angle,
  Fixed4 (..),
  toFloats4,
  fromFloats4,
) where

import Prelude hiding (map)

import Data.Coerce (coerce)
import Data.Functor ((<&>))
import Data.Serialize (Serialize)
import Foreign
import Foreign.C.Types (CFloat, CInt (CInt))
import GHC.Generics (Generic)
import Lens.Micro.Internal (Field1 (..), Field2 (..))
import Numeric.Fixed qualified as F
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Custom.Linear (V2 (V2), nearZero)

--------------------------------------------------------------------------------

-- | Fixed-point numbers via `Numeric.Fixed.Fixed`
newtype Fixed = Fixed F.Fixed
  deriving newtype
    (Show, Eq, Ord, Bounded, Num, Fractional, Real, RealFrac, Floating, Storable)
  deriving (Serialize) via Int32

getFixed :: Fixed -> Int32
getFixed = coerce

makeFixed :: Int32 -> Fixed
makeFixed = coerce

instance MWC.Variate Fixed where
  uniform gen = coerce <$> MWC.uniform @Int32 gen
  {-# INLINE uniform #-}
  uniformR (a, b) gen =
    coerce <$> MWC.uniformR @Int32 (coerce a, coerce b) gen
  {-# INLINE uniformR #-}

-- Conversion to/from floating point numbers.
-- The following functions were copied from `Numeric.Fixed`.

toFloat :: Fixed -> Float
toFloat x = fromIntegral (getFixed x) / 65536
{-# INLINE toFloat #-}

fromFloat :: Float -> Fixed
fromFloat x = makeFixed $ floor (x * 65536 + 0.5)
{-# INLINE fromFloat #-}

toDouble :: Fixed -> Double
toDouble x = fromIntegral (getFixed x) / 65536
{-# INLINE toDouble #-}

fromDouble :: Double -> Fixed
fromDouble x = makeFixed $ floor (x * 65536 + 0.5)
{-# INLINE fromDouble #-}

toCFloat :: Fixed -> CFloat
toCFloat x = fromIntegral (getFixed x) / 65536
{-# INLINE toCFloat #-}

approxExp :: Fixed -> Fixed
approxExp = fromDouble . approxExp' . toDouble
{-# INLINE approxExp #-}

-- | Fast approximate exponentiation for values near 0
approxExp' :: Double -> Double
approxExp' x =
  let y1 = 1 + x * 3.90625e-3 -- 1/256
      y2 = y1 * y1
      y4 = y2 * y2
      y8 = y4 * y4
      y16 = y8 * y8
      y32 = y16 * y16
      y64 = y32 * y32
      y128 = y64 * y64
      y256 = y128 * y128
  in  y256

--------------------------------------------------------------------------------

-- | A monomorphic, strict pair of fixed-point numbers. For performance.
data Fixed2 = Fixed2 !Fixed !Fixed
  deriving stock (Generic, Eq, Show)
  deriving anyclass (Serialize)

-- Field access lenses
instance Field1 Fixed2 Fixed2 Fixed Fixed where
  _1 f (Fixed2 x y) = f x <&> \x' -> Fixed2 x' y
  {-# INLINE _1 #-}

instance Field2 Fixed2 Fixed2 Fixed Fixed where
  _2 f (Fixed2 x y) = f y <&> \y' -> Fixed2 x y'
  {-# INLINE _2 #-}

instance Storable Fixed2 where
  sizeOf _ = 8
  {-# INLINE sizeOf #-}
  alignment _ = 8
  {-# INLINE alignment #-}
  peek p = do
    let q = castPtr p
    x <- peek q
    y <- peekElemOff q 1
    pure $ Fixed2 x y
  {-# INLINE peek #-}
  poke p (Fixed2 x y) = do
    let q = castPtr p
    poke q x
    pokeElemOff q 1 y
  {-# INLINE poke #-}

instance Num Fixed2 where
  (+) (Fixed2 x y) (Fixed2 x' y') = Fixed2 (x + x') (y + y')
  {-# INLINE (+) #-}
  (-) (Fixed2 x y) (Fixed2 x' y') = Fixed2 (x - x') (y - y')
  {-# INLINE (-) #-}
  (*) (Fixed2 x y) (Fixed2 x' y') = Fixed2 (x * x') (y * y')
  {-# INLINE (*) #-}
  negate (Fixed2 x y) = Fixed2 (negate x) (negate y)
  {-# INLINE negate #-}
  abs (Fixed2 x y) = Fixed2 (abs x) (abs y)
  {-# INLINE abs #-}
  signum (Fixed2 x y) = Fixed2 (signum x) (signum y)
  {-# INLINE signum #-}
  fromInteger i = let f = fromIntegral i in Fixed2 f f
  {-# INLINE fromInteger #-}

toFloats :: Fixed2 -> Float2
toFloats (Fixed2 x y) = Float2 (toFloat x) (toFloat y)
{-# INLINE toFloats #-}

fromFloats :: Float2 -> Fixed2
fromFloats (Float2 x y) = Fixed2 (fromFloat x) (fromFloat y)
{-# INLINE fromFloats #-}

toV2Floats :: Fixed2 -> V2 Float
toV2Floats (Fixed2 x y) = V2 (toFloat x) (toFloat y)
{-# INLINE toV2Floats #-}

fromV2Floats :: V2 Float -> Fixed2
fromV2Floats (V2 x y) = Fixed2 (fromFloat x) (fromFloat y)
{-# INLINE fromV2Floats #-}

fromVec :: V2 Fixed -> Fixed2
fromVec (V2 x y) = Fixed2 x y

toDoubles :: Fixed2 -> V2 Double
toDoubles (Fixed2 x y) = V2 (toDouble x) (toDouble y)
{-# INLINE toDoubles #-}

fromDoubles :: V2 Double -> Fixed2
fromDoubles (V2 x y) = Fixed2 (fromDouble x) (fromDouble y)
{-# INLINE fromDoubles #-}

dot :: Fixed2 -> Fixed2 -> Fixed
dot (Fixed2 x y) (Fixed2 x' y') = x * x' + y * y'
{-# INLINE dot #-}

map :: (Fixed -> Fixed) -> Fixed2 -> Fixed2
map f (Fixed2 x y) = Fixed2 (f x) (f y)
{-# INLINE map #-}

-- Converting to `Double` to avoid potential fixed-point overflow.
norm :: Fixed2 -> Fixed
norm (Fixed2 x y) =
  let x' = toDouble x
      y' = toDouble y
  in  fromDouble $ sqrt (x' * x' + y' * y')
{-# INLINE norm #-}

-- Working in `Int64` to avoid potential fixed-point overflow.
normLeq :: Fixed2 -> Fixed -> Bool
normLeq (Fixed2 x y) r =
  let x' = fromIntegral $ getFixed x :: Int64
      y' = fromIntegral $ getFixed y
      r' = fromIntegral $ getFixed r
  in  x' * x' + y' * y' <= r' * r'
{-# INLINE normLeq #-}

normGt :: Fixed2 -> Fixed -> Bool
normGt = fmap (fmap not) normLeq
{-# INLINE normGt #-}

-- Based on Linear.Metric.normalize
normalize :: Fixed2 -> Fixed2
normalize v@(Fixed2 x y) =
  if nearZero d2 || nearZero (1.0 - d2)
    then v
    else Fixed2 (fromDouble $ x' / d) (fromDouble $ y' / d)
 where
  x' = toDouble x
  y' = toDouble y
  d2 = x' * x' + y' * y'
  d = sqrt d2
{-# INLINE normalize #-}

-- Based on Linear.Metric.normalize
normAndNormalize :: Fixed2 -> (Fixed, Fixed2)
normAndNormalize v@(Fixed2 x y) =
  let normalized =
        if nearZero d2 || nearZero (1.0 - d2)
          then v
          else Fixed2 (fromDouble $ x' / d) (fromDouble $ y' / d)
  in  (fromDouble d, normalized)
 where
  x' = toDouble x
  y' = toDouble y
  d2 = x' * x' + y' * y'
  d = sqrt d2
{-# INLINE normAndNormalize #-}

-- Note: Take care to avoid fixed-point overflow when using this function.
normSq :: Fixed2 -> Fixed
normSq (Fixed2 x y) = x * x + y * y
{-# INLINE normSq #-}

-- Low-resolution version of 'normSq' that is much harder to overflow
normSqApprox :: Fixed2 -> Int32
normSqApprox (Fixed2 x y) =
  let x' = fromIntegral @_ @Int64 $ coerce @_ @Int32 x
      y' = fromIntegral @_ @Int64 $ coerce @_ @Int32 y
  in  fromIntegral $ unsafeShiftR (x' * x' + y' * y') 32

angle :: Fixed -> Fixed2
angle ang = Fixed2 (cos ang) (sin ang)
{-# INLINE angle #-}

--------------------------------------------------------------------------------

-- | A monomorphic, strict 4-vector of fixed-point numbers. For performance.
data Fixed4 = Fixed4 !Fixed !Fixed !Fixed !Fixed
  deriving stock (Generic, Eq)
  deriving anyclass (Serialize)

instance Storable Fixed4 where
  sizeOf _ = 4 * sizeOf (undefined :: Fixed)
  {-# INLINE sizeOf #-}
  alignment _ = alignment (undefined :: Fixed)
  {-# INLINE alignment #-}
  poke ptr (Fixed4 x y z w) = do
    poke ptr' x
    pokeElemOff ptr' 1 y
    pokeElemOff ptr' 2 z
    pokeElemOff ptr' 3 w
   where
    ptr' = castPtr ptr
  {-# INLINE poke #-}
  peek ptr =
    Fixed4
      <$> peek ptr'
      <*> peekElemOff ptr' 1
      <*> peekElemOff ptr' 2
      <*> peekElemOff ptr' 3
   where
    ptr' = castPtr ptr
  {-# INLINE peek #-}

instance Num Fixed4 where
  (+) (Fixed4 x y z w) (Fixed4 x' y' z' w') =
    Fixed4 (x + x') (y + y') (z + z') (w + w')
  {-# INLINE (+) #-}
  (-) (Fixed4 x y z w) (Fixed4 x' y' z' w') =
    Fixed4 (x - x') (y - y') (z - z') (w - w')
  {-# INLINE (-) #-}
  (*) (Fixed4 x y z w) (Fixed4 x' y' z' w') =
    Fixed4 (x * x') (y * y') (z * z') (w * w')
  {-# INLINE (*) #-}
  negate (Fixed4 x y z w) = Fixed4 (negate x) (negate y) (negate z) (negate w)
  {-# INLINE negate #-}
  abs (Fixed4 x y z w) = Fixed4 (abs x) (abs y) (abs z) (abs w)
  {-# INLINE abs #-}
  signum (Fixed4 x y z w) = Fixed4 (signum x) (signum y) (signum z) (signum w)
  {-# INLINE signum #-}
  fromInteger i = let f = fromIntegral i in Fixed4 f f f f
  {-# INLINE fromInteger #-}

toFloats4 :: Fixed4 -> Float4
toFloats4 (Fixed4 x y z w) =
  Float4 (toFloat x) (toFloat y) (toFloat z) (toFloat w)
{-# INLINE toFloats4 #-}

fromFloats4 :: Float4 -> Fixed4
fromFloats4 (Float4 x y z w) =
  Fixed4 (fromFloat x) (fromFloat y) (fromFloat z) (fromFloat w)
{-# INLINE fromFloats4 #-}
