-- | This module is intended to be imported qualified.
module Pixelpusher.Custom.SmallBitSet (
  SmallBitSet,
  new,
  takeSmallestBit,
  returnBit,
) where

import Data.Bits
import Data.Serialize (Serialize)
import Data.Word (Word64, Word8)

newtype SmallBitSet = SmallBitSet Word64
  deriving newtype (Serialize)

new :: Word8 -> SmallBitSet
new size =
  SmallBitSet $ shiftL 1 (fromIntegral size) - 1

takeSmallestBit :: SmallBitSet -> (Maybe Word8, SmallBitSet)
takeSmallestBit (SmallBitSet w)
  | w == 0 = (Nothing, SmallBitSet w)
  | otherwise =
      let w' = w .&. (w - 1) -- w with lowest bit removed
      in  ( let singletonBit = w `xor` w'
                bitNo = countTrailingZeros singletonBit
            in  Just $ fromIntegral bitNo
          , SmallBitSet w'
          )

returnBit :: Word8 -> SmallBitSet -> SmallBitSet
returnBit bitNo (SmallBitSet w) =
  SmallBitSet $ w .|. shiftL 1 (fromIntegral bitNo)
