{-# OPTIONS_GHC -Wno-orphans #-}

module Pixelpusher.Custom.SockAddr where

import Data.Hashable
import Data.Word (Word16)
import Network.Socket

-- Orphan instance
instance Hashable SockAddr where
  hashWithSalt s (SockAddrInet p a) =
    let p' = fromIntegral p :: Word16
    in  s `hashWithSalt` p' `hashWithSalt` a
  hashWithSalt s (SockAddrInet6 p _ a _) =
    let p' = fromIntegral p :: Word16
    in  s `hashWithSalt` p' `hashWithSalt` a
  hashWithSalt s (SockAddrUnix a) = hashWithSalt s a
