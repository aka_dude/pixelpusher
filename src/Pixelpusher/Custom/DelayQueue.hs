-- | For adding reaction time to bots
module Pixelpusher.Custom.DelayQueue (
  DelayQueue,
  emptyDelayQueue,
  pushDelayQueue,
) where

import Data.Sequence (Seq, ViewL (..), (|>))
import Data.Sequence qualified as Seq
import Data.Serialize (Serialize)

-- | A bounded FIFO queue, where pushing an element to back of the queue pops an
-- element from the front. Intended for adding reaction time to bots.
--
-- When the queue is too large, added elements are dropped but the queue is
-- still popped; when the queue is too small, nothing is popped from the queue
-- but new elements are still pushed.

-- Implementation note: Push on right, pop from left
newtype DelayQueue a = DelayQueue (Seq a)
  deriving newtype (Serialize, Foldable)

emptyDelayQueue :: DelayQueue a
emptyDelayQueue = DelayQueue Seq.empty

-- Takes the size limit as a paramter, rather than having it be part of the
-- queue itself, in order to support dynamic adjustment of bot input delays
-- during testing.
pushDelayQueue :: Int -> a -> DelayQueue a -> (Maybe a, DelayQueue a)
pushDelayQueue sizeLimit a (DelayQueue as)
  | queueLength == sizeLimit =
      -- This will be the most common case, so we check for it first.
      -- Insert new element and pop
      case Seq.viewl (as |> a) of
        EmptyL -> (Nothing, DelayQueue Seq.empty)
        a' :< as' -> (Just a', DelayQueue as')
  | queueLength < sizeLimit =
      -- Push new element and do not pop
      (Nothing, DelayQueue (as |> a))
  | otherwise -- queue length > size limit
  -- Discard new element and pop
    =
      case Seq.viewl as of
        EmptyL -> (Nothing, DelayQueue Seq.empty)
        a' :< as' -> (Just a', DelayQueue as')
 where
  queueLength = Seq.length as
