module Pixelpusher.Game.PlayerID (
  PlayerID,
  PlayerIDMap32,
  getPlayerIDMap32,
  makePlayerIDMap32,
  PlayerIDPool,
  newPlayerIDPool,
  borrowPlayerID,
  returnPlayerID,
) where

import Control.Monad (replicateM)
import Data.Bits (popCount, setBit, zeroBits)
import Data.List (foldl')
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Word (Word32, Word8)

import Pixelpusher.Custom.SmallBitSet (SmallBitSet)
import Pixelpusher.Custom.SmallBitSet qualified as SmallBitSet
import Pixelpusher.Custom.Util (listSetBits)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))

--------------------------------------------------------------------------------

-- | A newtype for defining a Binary instance on maps of PlayerIDs that takes
-- advantage of the small number of players to be more compact. Only allows
-- `PlayerID`s from 0 to 31 as keys.
newtype PlayerIDMap32 a = PlayerIDMap32
  {getPlayerIDMap32 :: WIM.IntMap PlayerID a}
  deriving stock (Eq, Show)

-- | Filters out `PlayerID` keys larger than `PlayerID 31`
makePlayerIDMap32 :: WIM.IntMap PlayerID a -> PlayerIDMap32 a
makePlayerIDMap32 =
  PlayerIDMap32 . WIM.filterWithKey (\k _ -> k <= PlayerID 31)

-- | Uses a bitfield to represent the `PlayerID` keys
instance (Serialize a) => Serialize (PlayerIDMap32 a) where
  put :: PlayerIDMap32 a -> Serialize.Put
  put (PlayerIDMap32 m) = do
    let (uids, as) = unzip $ WIM.toAscList m
        bitfield =
          foldl' setBit zeroBits $
            map (fromIntegral . getPlayerID) uids
    Serialize.put @Word32 bitfield
    mapM_ Serialize.put as

  get :: Serialize.Get (PlayerIDMap32 a)
  get = do
    bitfield <- Serialize.get @Word32
    let size = popCount bitfield
        uids = map (PlayerID . fromIntegral) $ listSetBits bitfield
    as <- replicateM size Serialize.get
    pure $ PlayerIDMap32 $ WIM.fromAscList $ zip uids as

--------------------------------------------------------------------------------

-- | A pool of a finite number of 'PlayerID's.
newtype PlayerIDPool = PlayerIDPool SmallBitSet
  deriving newtype (Serialize)

newPlayerIDPool :: Word8 -> PlayerIDPool
newPlayerIDPool maxUsers = PlayerIDPool $ SmallBitSet.new maxUsers

borrowPlayerID :: PlayerIDPool -> (Maybe PlayerID, PlayerIDPool)
borrowPlayerID (PlayerIDPool bitSet) =
  let (mWord8, bitSet') = SmallBitSet.takeSmallestBit bitSet
  in  (PlayerID <$> mWord8, PlayerIDPool bitSet')

returnPlayerID :: PlayerID -> PlayerIDPool -> PlayerIDPool
returnPlayerID (PlayerID word8) (PlayerIDPool bitSet) =
  PlayerIDPool $ SmallBitSet.returnBit word8 bitSet
