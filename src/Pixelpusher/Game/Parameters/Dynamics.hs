{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Dynamics (
  DynamicsParams (..),
  makeDynamicsParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Damage
import Pixelpusher.Game.Parameters.Base.Dynamics
import Pixelpusher.Game.Parameters.Base.Flag
import Pixelpusher.Game.Parameters.Base.GameScaling
import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.SoftObstacle

data DynamicsParams = DynamicsParams
  -- World
  { gp_worldRadius :: Fixed
  , -- Drag and acceleration
    gp_overseerAcceleration :: Fixed
  , gp_overseerDrag :: Fixed
  , gp_droneAcceleration :: Fixed
  , gp_droneDrag :: Fixed
  , gp_flagDrag :: Fixed
  , -- Radii
    gp_overseerRadius :: Fixed
  , gp_droneRadius :: Fixed
  , gp_flagRadius :: Fixed
  , gp_flagRecoveryRadius :: Fixed
  , -- Density
    gp_softObstacleDensityFactor :: Fixed
  , -- Collisions
    gp_droneDroneCollisionDamageFactor :: Fixed
  , gp_overseerDroneCollisionDamageFactor :: Fixed
  , gp_overseerOverseerCollisionDamageFactor :: Fixed
  , gp_combatDeltaV :: Fixed
  , gp_enableFriendlyDroneCollison :: Bool
  , -- Soft obstacles
    gp_softObstacleRadius :: Fixed
  , gp_softObstacleCurvature :: Fixed
  , gp_softObstacleReplusion :: Fixed
  , gp_softObstacleAverageSpeed :: Fixed
  , gp_softObstacleWorldGradientFactor :: Fixed
  , -- Psi-storms
    gp_psiStormRadius :: Fixed
  , gp_psiStormHitPeriodTicks :: Ticks
  , gp_psiStormInitialGrowthFraction :: Fixed
  , gp_psiStormGrowthConstant :: Fixed
  , gp_psiStormFadeTicks :: Ticks
  , -- Control radius
    gp_controlRadius :: Fixed
  , -- Damage rate
    gp_damageEnergyRatio :: Fixed
  , gp_energyDamageRatio :: Fixed
  , -- Explosions
    gp_overseerExplosionRadius :: Fixed
  , gp_overseerExplosionForceConstant :: Fixed
  , gp_overseerExplosionDuration :: Ticks
  , -- Misc
    gp_teamBaseRadius :: Fixed
  , gp_spawnAreaRadius :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeDynamicsParams ::
  Int ->
  BaseGameScalingParams Fixed ->
  BaseDynamicsParams Fixed ->
  BaseDamageParams Fixed ->
  BaseFlagParams Fixed ->
  BaseSoftObstacleParams Fixed ->
  BasePsiStormParams Fixed ->
  DynamicsParams
makeDynamicsParams
  gameSize
  BaseGameScalingParams{..}
  BaseDynamicsParams{..}
  BaseDamageParams{..}
  BaseFlagParams{..}
  BaseSoftObstacleParams{..}
  BasePsiStormParams{..} =
    DynamicsParams
      { -- World
        gp_worldRadius =
          0.5 -- (0.5*) converts the width to a radius
            * sqrt (fromIntegral gameSize)
            * bgp_worldLengthPerSqrtSize
      , -- Drag and acceleration
        gp_overseerAcceleration = bgp_overseerAcceleration
      , gp_overseerDrag = bgp_overseerDrag
      , gp_droneAcceleration = bgp_droneAcceleration
      , gp_droneDrag = bgp_droneDrag
      , gp_flagDrag = bgp_flagDrag
      , -- Radii
        gp_overseerRadius = bgp_overseerRadius
      , gp_droneRadius = bgp_droneRadius
      , gp_flagRadius = bgp_flagRadius
      , gp_flagRecoveryRadius = bgp_flagRecoveryRadius
      , -- Density
        gp_softObstacleDensityFactor = bgp_softObstacleDensityFactor
      , -- Collisions
        gp_droneDroneCollisionDamageFactor = bgp_droneDroneCollisionDamageFactor
      , gp_overseerDroneCollisionDamageFactor = bgp_overseerDroneCollisionDamageFactor
      , gp_overseerOverseerCollisionDamageFactor = bgp_overseerOverseerCollisionDamageFactor
      , gp_combatDeltaV = bgp_combatDeltaV
      , gp_enableFriendlyDroneCollison = bgp_enableFriendlyDroneCollison
      , -- Soft obstacles
        gp_softObstacleRadius = fromIntegral bgp_softObstacleRadius
      , gp_softObstacleCurvature = bgp_softObstacleCurvature
      , gp_softObstacleReplusion = bgp_softObstacleReplusion
      , gp_softObstacleAverageSpeed = bgp_softObstacleAverageSpeed
      , gp_softObstacleWorldGradientFactor = bgp_softObstacleWorldGradientFactor
      , -- Psi-storms
        gp_psiStormRadius = fromIntegral bgp_psiStormRadius
      , gp_psiStormHitPeriodTicks = fromIntegral bgp_psiStormHitPeriodTicks
      , gp_psiStormInitialGrowthFraction = bgp_psiStormInitialGrowthFraction
      , gp_psiStormGrowthConstant = bgp_psiStormGrowthConstant
      , gp_psiStormFadeTicks = fromIntegral bgp_psiStormFadeTicks
      , -- Control radius
        gp_controlRadius = bgp_controlRadius
      , -- Damage rate
        gp_damageEnergyRatio = bgp_damageEnergyRatio / C.density
      , gp_energyDamageRatio = C.density / bgp_damageEnergyRatio
      , -- Explosions
        gp_overseerExplosionRadius = bgp_overseerExplosionRadius
      , gp_overseerExplosionForceConstant = bgp_overseerExplosionForceConstant
      , gp_overseerExplosionDuration = fromIntegral bgp_overseerExplosionDurationTicks
      , -- Misc
        gp_teamBaseRadius = bgp_teamBaseRadius
      , gp_spawnAreaRadius = bgp_spawnAreaRadius
      }
