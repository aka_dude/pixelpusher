{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Combat (
  CombatParams (..),
  makeCombatParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.Regen

data CombatParams = CombatParams
  { gp_healthRegenPeriodTicks :: Ticks
  , gp_quadraticHealthFullRegenTicks :: Ticks
  , gp_quadraticHealthRegenRateDivisor :: Fixed
  , gp_droneMaxHealth :: Fixed
  , gp_droneHealthRegenPeriodFactor :: Fixed
  , gp_droneHealthRegenTimeConstantTicks :: Fixed
  , gp_overseerMaxHealth :: Fixed
  , gp_droneRespawnCheckInterval :: Ticks
  , gp_droneRespawnDelay :: Ticks
  , gp_droneRespawnHealth :: Fixed
  , gp_overseerRespawnDelay :: Ticks
  , gp_psiStormDamagePerHit :: Fixed
  , gp_psiStormInterruptsRegen :: Bool
  , gp_playerRespawnOverseerInvulnerabilityTicks :: Ticks
  , gp_playerRespawnDroneInvulnerabilityTicks :: Ticks
  , gp_playerRespawnOverseerBonusHealthFraction :: Fixed
  , gp_psiStormOverseerDamageFactor :: Fixed
  , gp_unlimitedSpawnInvulnerability :: Bool
  , gp_drainFlagCarrierHealth :: Bool
  , gp_drainFlagCarrierHealthRate :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeCombatParams ::
  BaseRegenParams Fixed ->
  BasePsiStormParams Fixed ->
  CombatParams
makeCombatParams
  BaseRegenParams{..}
  BasePsiStormParams{..} =
    CombatParams
      { gp_healthRegenPeriodTicks = fromIntegral bgp_healthRegenPeriodTicks
      , gp_quadraticHealthFullRegenTicks =
          fromIntegral bgp_quadraticHealthFullRegenTicks
      , gp_quadraticHealthRegenRateDivisor =
          let regenPeriods =
                fromIntegral bgp_quadraticHealthFullRegenTicks
                  / fromIntegral bgp_healthRegenPeriodTicks
              numHealUnits = 0.5 * regenPeriods * (regenPeriods + 1)
          in  numHealUnits
      , gp_droneMaxHealth = droneMaxHealth
      , gp_droneHealthRegenPeriodFactor =
          let halfLife =
                fromIntegral bgp_droneHealthRegenerationHalfLifeTicks
              t = fromIntegral bgp_healthRegenPeriodTicks
          in  exp (log 0.5 * (t / halfLife))
      , gp_droneHealthRegenTimeConstantTicks =
          let halfLife = fromIntegral bgp_droneHealthRegenerationHalfLifeTicks
          in  halfLife / log 0.5
      , gp_overseerMaxHealth = bgp_overseerMaxHealth
      , gp_droneRespawnCheckInterval =
          fromIntegral bgp_droneRespawnCheckIntervalTicks
      , gp_droneRespawnDelay = fromIntegral bgp_droneRespawnDelayTicks
      , gp_droneRespawnHealth =
          let halfLife =
                fromIntegral bgp_droneHealthRegenerationHalfLifeTicks
              t = fromIntegral bgp_droneRespawnDelayTicks
          in  (1.0 - exp (log 0.5 * (t / halfLife))) * bgp_droneMaxHealth
      , gp_overseerRespawnDelay = fromIntegral bgp_respawnDelayTicks
      , gp_psiStormDamagePerHit = bgp_psiStormDamagePerHit
      , gp_psiStormInterruptsRegen = bgp_psiStormInterruptsRegen
      , gp_playerRespawnOverseerInvulnerabilityTicks =
          fromIntegral bgp_playerRespawnOverseerInvulnerabilityTicks
      , gp_playerRespawnDroneInvulnerabilityTicks =
          fromIntegral bgp_playerRespawnDroneInvulnerabilityTicks
      , gp_playerRespawnOverseerBonusHealthFraction =
          bgp_playerRespawnOverseerBonusHealthFraction
      , gp_psiStormOverseerDamageFactor = bgp_psiStormOverseerDamageFactor
      , gp_unlimitedSpawnInvulnerability = bgp_unlimitedSpawnInvulnerability
      , gp_drainFlagCarrierHealth = bgp_drainFlagCarrierHealth
      , gp_drainFlagCarrierHealthRate = bgp_drainFlagCarrierHealthRate
      }
   where
    droneMaxHealth = bgp_droneMaxHealth
