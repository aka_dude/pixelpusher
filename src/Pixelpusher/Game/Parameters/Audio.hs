{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Audio (
  AudioParams (..),
  makeAudioParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

import Pixelpusher.Game.Parameters.Base.Audio

data AudioParams = AudioParams
  { gp_audioCollisionGain :: Fixed
  , gp_audioMuffledCollisionGain :: Fixed
  , gp_audioDroneDashGain :: Fixed
  , gp_audioPsiStormShockGain :: Fixed
  , gp_audioPsiStormCastGain :: Fixed
  , gp_audioOverseerExplosionGain :: Fixed
  , gp_audioFlagExplosionGain :: Fixed
  , gp_audioMinDistanceFactor :: Fixed
  , gp_audioMaxDistanceFactor :: Fixed
  , gp_audioFlagCaptureGain :: Fixed
  , gp_audioMatchEndGain :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeAudioParams :: BaseAudioParams Fixed -> AudioParams
makeAudioParams BaseAudioParams{..} =
  AudioParams
    { gp_audioCollisionGain = bgp_audioCollisionGain
    , gp_audioMuffledCollisionGain = bgp_audioMuffledCollisionGain
    , gp_audioDroneDashGain = bgp_audioDroneDashGain
    , gp_audioPsiStormShockGain = bgp_audioPsiStormShockGain
    , gp_audioPsiStormCastGain = bgp_audioPsiStormCastGain
    , gp_audioOverseerExplosionGain = bgp_audioOverseerExplosionGain
    , gp_audioFlagExplosionGain = bgp_audioFlagExplosionGain
    , gp_audioMinDistanceFactor = bgp_audioMinDistanceFactor
    , gp_audioMaxDistanceFactor = bgp_audioMaxDistanceFactor
    , gp_audioFlagCaptureGain = bgp_audioFlagCaptureGain
    , gp_audioMatchEndGain = bgp_audioMatchEndGain
    }
