{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Flag (
  FlagParams (..),
  makeFlagParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Flag

data FlagParams = FlagParams
  { gp_enableFlagDrops :: Bool
  , gp_flagPickupDisableTicks :: Ticks
  , gp_flagRecoveryTicks :: Ticks
  , gp_flagAutoRecoveryFactor :: Int
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeFlagParams :: BaseFlagParams Fixed -> FlagParams
makeFlagParams BaseFlagParams{..} =
  FlagParams
    { gp_enableFlagDrops = bgp_enableFlagDrops
    , gp_flagPickupDisableTicks = fromIntegral bgp_flagPickupDisableTicks
    , gp_flagRecoveryTicks = fromIntegral bgp_flagRecoveryTicks
    , gp_flagAutoRecoveryFactor = bgp_flagAutoRecoveryFactor
    }
