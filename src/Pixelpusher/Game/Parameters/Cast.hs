{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Cast (
  CastParams (..),
  makeCastParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Dash
import Pixelpusher.Game.Parameters.Base.Dynamics
import Pixelpusher.Game.Parameters.Base.PsiStorm

data CastParams = CastParams
  { gp_psiStormLifetimeTicks :: Ticks
  , gp_psiStormCastCooldownTicks :: Ticks
  , gp_castRange :: Fixed
  , gp_overseerDashDeltaV :: Fixed
  , gp_overseerDashCooldownTicks :: Ticks
  , gp_enableOverseerDash :: Bool
  , gp_enableDroneDash :: Bool
  , gp_droneDashDeltaVFactor :: Fixed
  , gp_maxDroneDashChargeTicks :: Ticks
  , gp_enableOverseerDoubleDash :: Bool
  , gp_overseerDoubleDashDelayTicks :: Ticks
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeCastParams ::
  BaseDynamicsParams Fixed ->
  BasePsiStormParams Fixed ->
  BaseDashParams Fixed ->
  CastParams
makeCastParams
  BaseDynamicsParams{..}
  BasePsiStormParams{..}
  BaseDashParams{..} =
    CastParams
      { gp_psiStormLifetimeTicks = fromIntegral bgp_psiStormLifetimeTicks
      , gp_psiStormCastCooldownTicks = fromIntegral bgp_psiStormCastCooldownTicks
      , gp_castRange = bgp_controlRadius
      , gp_overseerDashDeltaV = bgp_overseerDashDeltaVFactor * bgp_overseerAcceleration
      , gp_overseerDashCooldownTicks = fromIntegral bgp_overseerDashCooldownTicks
      , gp_enableOverseerDash = bgp_enableOverseerDash
      , gp_enableDroneDash = bgp_enableDroneDash
      , gp_droneDashDeltaVFactor = bgp_droneDashEfficiencyFactor * bgp_droneAcceleration
      , gp_maxDroneDashChargeTicks = fromIntegral bgp_maxDroneDashChargeTicks
      , gp_enableOverseerDoubleDash = bgp_enableOverseerDoubleDash
      , gp_overseerDoubleDashDelayTicks = fromIntegral bgp_overseerDoubleDashDelayTicks
      }
