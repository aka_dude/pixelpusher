{-# LANGUAGE RecordWildCards #-}

-- | Parameters for quickly iterating on constants during development
module Pixelpusher.Game.Parameters.Test (
  TestParams (..),
  makeTestParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

import Pixelpusher.Game.Parameters.Base.Test

data TestParams = TestParams
  { gp_testFloat1 :: Fixed
  , gp_testFloat2 :: Fixed
  , gp_testFloat3 :: Fixed
  , gp_testFloat4 :: Fixed
  , gp_testFloat5 :: Fixed
  , gp_testFloat6 :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeTestParams :: BaseTestParams Fixed -> TestParams
makeTestParams BaseTestParams{..} =
  TestParams
    { gp_testFloat1 = bgp_testFloat1
    , gp_testFloat2 = bgp_testFloat2
    , gp_testFloat3 = bgp_testFloat3
    , gp_testFloat4 = bgp_testFloat4
    , gp_testFloat5 = bgp_testFloat5
    , gp_testFloat6 = bgp_testFloat6
    }
