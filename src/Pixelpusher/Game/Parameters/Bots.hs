{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Bots (
  BotParams (..),
  makeBotParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

import Pixelpusher.Game.Parameters.Base.Bots
import Pixelpusher.Game.Time (Ticks)

data BotParams = BotParams
  { gp_minBotsPerTeam :: Int
  , gp_minTeamSize :: Int
  , gp_botControlFrequencyTicks :: Ticks
  , gp_botControlQueueLength :: Int
  , gp_botMaxPredictionTicks :: Int
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeBotParams :: BaseBotParams Fixed -> BotParams
makeBotParams BaseBotParams{..} =
  BotParams
    { gp_minBotsPerTeam = bgp_minBotsPerTeam
    , gp_minTeamSize = bgp_minTeamSize
    , gp_botControlFrequencyTicks = fromIntegral bgp_botControlFrequencyTicks
    , gp_botControlQueueLength = bgp_botControlQueueLength
    , gp_botMaxPredictionTicks =
        min
          (fromIntegral bgp_botMaxPredictionTicks)
          (bgp_botControlQueueLength * fromIntegral bgp_botControlFrequencyTicks)
    }
