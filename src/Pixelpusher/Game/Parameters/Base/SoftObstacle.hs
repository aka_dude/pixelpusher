module Pixelpusher.Game.Parameters.Base.SoftObstacle (
  BaseSoftObstacleParams (..),
  defaultSoftObstacleParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseSoftObstacleParams a = BaseSoftObstacleParams
  { bgp_softObstacleRadius :: Int
  , bgp_softObstacleCurvature :: a
  , bgp_softObstacleReplusion :: a
  , bgp_softObstacleAverageSpeed :: a
  , bgp_softObstacleWorldGradientFactor :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseSoftObstacleParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseSoftObstacleParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseSoftObstacleParams Double) where
  toTable = Toml.genericToTable

defaultSoftObstacleParams :: BaseSoftObstacleParams Double
defaultSoftObstacleParams =
  BaseSoftObstacleParams
    { bgp_softObstacleRadius = 120
    , bgp_softObstacleCurvature = 3.0e-3
    , bgp_softObstacleReplusion = 1e-4
    , bgp_softObstacleAverageSpeed = 4.0e-2
    , bgp_softObstacleWorldGradientFactor = 1
    }
