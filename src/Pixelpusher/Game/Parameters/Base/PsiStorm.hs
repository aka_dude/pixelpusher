module Pixelpusher.Game.Parameters.Base.PsiStorm (
  BasePsiStormParams (..),
  defaultPsiStormParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BasePsiStormParams a = BasePsiStormParams
  { bgp_psiStormLifetimeTicks :: Int
  , bgp_psiStormRadius :: Int
  , bgp_psiStormCastCooldownTicks :: Int
  , bgp_psiStormDamagePerHit :: a
  , bgp_psiStormHitPeriodTicks :: Int
  , bgp_psiStormInitialGrowthFraction :: a
  , bgp_psiStormGrowthConstant :: a
  , bgp_psiStormFadeTicks :: Int
  , bgp_psiStormInterruptsRegen :: Bool
  , bgp_psiStormOverseerDamageFactor :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BasePsiStormParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BasePsiStormParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BasePsiStormParams Double) where
  toTable = Toml.genericToTable

defaultPsiStormParams :: BasePsiStormParams Double
defaultPsiStormParams =
  BasePsiStormParams
    { bgp_psiStormLifetimeTicks = hitPeriodTicks * (hits + 2)
    , bgp_psiStormRadius = 144
    , bgp_psiStormCastCooldownTicks = fromIntegral C.tickRate_hz * 8
    , bgp_psiStormDamagePerHit = 3.0
    , bgp_psiStormHitPeriodTicks = hitPeriodTicks
    , bgp_psiStormInitialGrowthFraction = 0.03
    , bgp_psiStormGrowthConstant = 0.041667
    , bgp_psiStormFadeTicks = fromIntegral C.tickRate_hz `div` 3
    , bgp_psiStormInterruptsRegen = False
    , bgp_psiStormOverseerDamageFactor = 0.875
    }
 where
  -- Just like the psi-storm in StarCraft: Brood War!
  hitPeriodTicks = fromIntegral C.tickRate_hz `div` 3
  hits = 8
