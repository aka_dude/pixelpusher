-- | Parameters for quickly iterating on constants during development
module Pixelpusher.Game.Parameters.Base.Test (
  BaseTestParams (..),
  defaultTestParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseTestParams a = BaseTestParams
  { bgp_testFloat1 :: a
  , bgp_testFloat2 :: a
  , bgp_testFloat3 :: a
  , bgp_testFloat4 :: a
  , bgp_testFloat5 :: a
  , bgp_testFloat6 :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseTestParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseTestParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseTestParams Double) where
  toTable = Toml.genericToTable

defaultTestParams :: BaseTestParams Double
defaultTestParams =
  BaseTestParams
    { bgp_testFloat1 = 0
    , bgp_testFloat2 = 0
    , bgp_testFloat3 = 0
    , bgp_testFloat4 = 0
    , bgp_testFloat5 = 0
    , bgp_testFloat6 = 0
    }
