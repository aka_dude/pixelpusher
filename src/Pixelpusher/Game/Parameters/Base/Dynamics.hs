module Pixelpusher.Game.Parameters.Base.Dynamics (
  BaseDynamicsParams (..),
  defaultDynamicsParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseDynamicsParams a = BaseDynamicsParams
  { bgp_overseerAcceleration :: a
  , bgp_overseerDrag :: a
  , bgp_droneAcceleration :: a
  , bgp_droneDrag :: a
  , bgp_flagDrag :: a
  , bgp_controlRadius :: a
  , bgp_overseerRadius :: a
  , bgp_droneRadius :: a
  , bgp_softObstacleDensityFactor :: a
  , bgp_overseerExplosionRadius :: a
  , bgp_overseerExplosionForceConstant :: a
  , bgp_overseerExplosionDurationTicks :: Int
  , bgp_teamBaseRadius :: a
  , bgp_spawnAreaRadius :: a
  , bgp_enableFriendlyDroneCollison :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDynamicsParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDynamicsParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDynamicsParams Double) where
  toTable = Toml.genericToTable

defaultDynamicsParams :: BaseDynamicsParams Double
defaultDynamicsParams =
  BaseDynamicsParams
    { bgp_overseerAcceleration = 0.0448
    , bgp_overseerDrag = 0.02625
    , bgp_droneAcceleration = 0.0384
    , bgp_droneDrag = 0.0125
    , bgp_flagDrag = 0.01
    , bgp_controlRadius = 375
    , bgp_overseerRadius = 14
    , bgp_droneRadius = 10
    , bgp_softObstacleDensityFactor = 4
    , bgp_overseerExplosionRadius = 128
    , bgp_overseerExplosionForceConstant = 0.1
    , bgp_overseerExplosionDurationTicks = 16
    , bgp_teamBaseRadius = 100
    , bgp_spawnAreaRadius = 100
    , bgp_enableFriendlyDroneCollison = False
    }
