module Pixelpusher.Game.Parameters.Base.Regen (
  BaseRegenParams (..),
  defaultRegenParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseRegenParams a = BaseRegenParams
  { bgp_healthRegenPeriodTicks :: Int
  , bgp_quadraticHealthFullRegenTicks :: Int
  , bgp_droneMaxHealth :: a
  , bgp_droneHealthRegenerationHalfLifeTicks :: Int
  , bgp_overseerMaxHealth :: a
  , bgp_droneRespawnDelayTicks :: Int
  , bgp_droneRespawnCheckIntervalTicks :: Int
  , bgp_playerRespawnOverseerInvulnerabilityTicks :: Int
  , bgp_playerRespawnDroneInvulnerabilityTicks :: Int
  , bgp_playerRespawnOverseerBonusHealthFraction :: a
  , bgp_respawnDelayTicks :: Int
  , bgp_unlimitedSpawnInvulnerability :: Bool
  , bgp_drainFlagCarrierHealth :: Bool
  , bgp_drainFlagCarrierHealthRate :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseRegenParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseRegenParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseRegenParams Double) where
  toTable = Toml.genericToTable

defaultRegenParams :: BaseRegenParams Double
defaultRegenParams =
  BaseRegenParams
    { bgp_healthRegenPeriodTicks = fromIntegral C.tickRate_hz `div` 12
    , bgp_quadraticHealthFullRegenTicks = 12 * fromIntegral C.tickRate_hz
    , bgp_droneMaxHealth = 20.25
    , bgp_droneHealthRegenerationHalfLifeTicks =
        6 * fromIntegral C.tickRate_hz
    , bgp_overseerMaxHealth = 56.0
    , bgp_droneRespawnDelayTicks = 9 * fromIntegral C.tickRate_hz `div` 2
    , bgp_droneRespawnCheckIntervalTicks = fromIntegral C.tickRate_hz `div` 5
    , bgp_playerRespawnOverseerInvulnerabilityTicks = 2 * fromIntegral C.tickRate_hz
    , bgp_playerRespawnDroneInvulnerabilityTicks = fromIntegral C.tickRate_hz
    , bgp_playerRespawnOverseerBonusHealthFraction = 0.25
    , bgp_respawnDelayTicks = 8 * fromIntegral C.tickRate_hz
    , bgp_unlimitedSpawnInvulnerability = False
    , bgp_drainFlagCarrierHealth = False
    , bgp_drainFlagCarrierHealthRate = 1.0
    }
