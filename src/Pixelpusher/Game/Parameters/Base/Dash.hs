module Pixelpusher.Game.Parameters.Base.Dash (
  BaseDashParams (..),
  defaultDashParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseDashParams a = BaseDashParams
  { bgp_overseerDashDeltaVFactor :: a
  , bgp_overseerDashCooldownTicks :: Int
  , bgp_enableOverseerDash :: Bool
  , bgp_enableDroneDash :: Bool
  , bgp_droneDashEfficiencyFactor :: a
  , bgp_maxDroneDashChargeTicks :: Int
  , bgp_enableOverseerDoubleDash :: Bool
  , bgp_overseerDoubleDashDelayTicks :: Int
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDashParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDashParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDashParams Double) where
  toTable = Toml.genericToTable

defaultDashParams :: BaseDashParams Double
defaultDashParams =
  BaseDashParams
    { bgp_overseerDashDeltaVFactor = 25
    , bgp_overseerDashCooldownTicks = 3 * fromIntegral C.tickRate_hz
    , bgp_enableOverseerDash = True
    , bgp_enableDroneDash = True
    , bgp_droneDashEfficiencyFactor = 0.75
    , bgp_maxDroneDashChargeTicks = 60
    , bgp_enableOverseerDoubleDash = False
    , bgp_overseerDoubleDashDelayTicks = 30
    }
