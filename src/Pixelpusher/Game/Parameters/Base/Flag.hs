module Pixelpusher.Game.Parameters.Base.Flag (
  BaseFlagParams (..),
  defaultFlagParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseFlagParams a = BaseFlagParams
  { bgp_enableFlagDrops :: Bool
  , bgp_flagPickupDisableTicks :: Int
  , bgp_flagRadius :: a
  , bgp_flagRecoveryRadius :: a
  , bgp_flagRecoveryTicks :: Int
  , bgp_flagAutoRecoveryFactor :: Int
  }
  deriving stock (Functor, Generic)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseFlagParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseFlagParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseFlagParams Double) where
  toTable = Toml.genericToTable

defaultFlagParams :: BaseFlagParams Double
defaultFlagParams =
  BaseFlagParams
    { bgp_enableFlagDrops = True
    , bgp_flagPickupDisableTicks = fromIntegral C.tickRate_hz
    , bgp_flagRadius = 8
    , bgp_flagRecoveryRadius = 90
    , bgp_flagRecoveryTicks = 5 * fromIntegral C.tickRate_hz
    , bgp_flagAutoRecoveryFactor = 3
    }
