module Pixelpusher.Game.Parameters.Base.Misc (
  BaseMiscParams (..),
  defaultMiscParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseMiscParams a = BaseMiscParams
  { bgp_disassociatedDroneLifetimeTicks :: Int
  , bgp_minIntermissionTicks :: Int
  , bgp_maxIntermissionTicks :: Int
  , bgp_requestFogOfWar :: Bool
  , bgp_startSpawnDelayTicks :: Int
  , bgp_showFlagsOnMinimap :: Bool
  , bgp_overseerDroneLimit :: Int
  , bgp_flagsToWin :: Int
  , bgp_spawnLocationAngleDegrees :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseMiscParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseMiscParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseMiscParams Double) where
  toTable = Toml.genericToTable

defaultMiscParams :: BaseMiscParams Double
defaultMiscParams =
  BaseMiscParams
    { bgp_minIntermissionTicks = 15 * fromIntegral C.tickRate_hz
    , bgp_maxIntermissionTicks = 45 * fromIntegral C.tickRate_hz
    , bgp_requestFogOfWar = True
    , bgp_disassociatedDroneLifetimeTicks = 6 * fromIntegral C.tickRate_hz
    , bgp_startSpawnDelayTicks = fromIntegral $ C.tickRate_hz * 3
    , bgp_showFlagsOnMinimap = True
    , bgp_overseerDroneLimit = 6
    , bgp_flagsToWin = 3
    , bgp_spawnLocationAngleDegrees = 45
    }
