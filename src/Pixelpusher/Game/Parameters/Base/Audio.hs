module Pixelpusher.Game.Parameters.Base.Audio (
  BaseAudioParams (..),
  defaultAudioParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseAudioParams a = BaseAudioParams
  { bgp_audioCollisionGain :: a
  , bgp_audioMuffledCollisionGain :: a
  , bgp_audioDroneDashGain :: a
  , bgp_audioPsiStormShockGain :: a
  , bgp_audioPsiStormCastGain :: a
  , bgp_audioOverseerExplosionGain :: a
  , bgp_audioFlagExplosionGain :: a
  , bgp_audioMinDistanceFactor :: a
  , bgp_audioMaxDistanceFactor :: a
  , bgp_audioFlagCaptureGain :: a
  , bgp_audioMatchEndGain :: a
  }
  deriving stock (Functor, Generic)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseAudioParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseAudioParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseAudioParams Double) where
  toTable = Toml.genericToTable

defaultAudioParams :: BaseAudioParams Double
defaultAudioParams =
  BaseAudioParams
    { bgp_audioCollisionGain = 0.85
    , bgp_audioMuffledCollisionGain = 1.0
    , bgp_audioDroneDashGain = 1.3
    , bgp_audioPsiStormShockGain = 0.35
    , bgp_audioPsiStormCastGain = 1.0
    , bgp_audioOverseerExplosionGain = 0.84
    , bgp_audioFlagExplosionGain = 0.6
    , bgp_audioMinDistanceFactor = 0.75
    , bgp_audioMaxDistanceFactor = 1.5
    , bgp_audioFlagCaptureGain = 0.30
    , bgp_audioMatchEndGain = 0.30
    }
