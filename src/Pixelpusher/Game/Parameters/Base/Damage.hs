module Pixelpusher.Game.Parameters.Base.Damage (
  BaseDamageParams (..),
  defaultDamageParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseDamageParams a = BaseDamageParams
  { bgp_droneDroneCollisionDamageFactor :: a
  , bgp_overseerDroneCollisionDamageFactor :: a
  , bgp_overseerOverseerCollisionDamageFactor :: a
  , bgp_damageEnergyRatio :: a
  , bgp_combatDeltaV :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDamageParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDamageParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDamageParams Double) where
  toTable = Toml.genericToTable

defaultDamageParams :: BaseDamageParams Double
defaultDamageParams =
  BaseDamageParams
    { bgp_droneDroneCollisionDamageFactor = 1.2
    , bgp_overseerDroneCollisionDamageFactor = 1.0
    , bgp_overseerOverseerCollisionDamageFactor = 1.0
    , bgp_damageEnergyRatio = 5.859375e-2
    , bgp_combatDeltaV = 0.8
    }
