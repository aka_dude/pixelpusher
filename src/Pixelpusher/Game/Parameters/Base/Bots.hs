module Pixelpusher.Game.Parameters.Base.Bots (
  BaseBotParams (..),
  defaultBotParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseBotParams a = BaseBotParams
  { bgp_minBotsPerTeam :: Int
  , bgp_minTeamSize :: Int
  , bgp_botControlFrequencyTicks :: Int
  , bgp_botControlQueueLength :: Int
  , bgp_botMaxPredictionTicks :: Int
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseBotParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseBotParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseBotParams Double) where
  toTable = Toml.genericToTable

defaultBotParams :: BaseBotParams Double
defaultBotParams =
  BaseBotParams
    { bgp_minBotsPerTeam = 1
    , bgp_minTeamSize = 4
    , bgp_botControlFrequencyTicks = 9
    , bgp_botControlQueueLength = 4
    , bgp_botMaxPredictionTicks = 24
    }
