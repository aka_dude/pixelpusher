module Pixelpusher.Game.Parameters.Base.GameScaling (
  BaseGameScalingParams (..),
  defaultGameScalingParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

--------------------------------------------------------------------------------

data BaseGameScalingParams a = BaseGameScalingParams
  { bgp_worldLengthPerSqrtSize :: a
  , bgp_teamBaseSpacingPerSqrtSize :: a
  , bgp_softObstaclesPerSize :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseGameScalingParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseGameScalingParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseGameScalingParams Double) where
  toTable = Toml.genericToTable

defaultGameScalingParams :: BaseGameScalingParams Double
defaultGameScalingParams =
  BaseGameScalingParams
    { bgp_worldLengthPerSqrtSize = 885
    , bgp_teamBaseSpacingPerSqrtSize = 165
    , bgp_softObstaclesPerSize = 1.8
    }
