{-# LANGUAGE GADTs #-}

module Pixelpusher.Game.Stats (
  recordStats,
) where

import Control.Monad.State.Class
import Control.Monad.Trans.State.Strict (StateT)
import Data.Foldable (foldl')
import Data.Generics.Product.Fields
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.EntityStore qualified as ES
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStats

recordStats ::
  (Monad m) =>
  [ES.OverseerDeathEvent] ->
  [ES.OverseerHitEvent] ->
  FlagEvents ->
  StateT (WIM.IntMap ActorID Player) m ()
recordStats overseerDeaths overseerHits flagEvents = do
  -- Overseer deaths, hits
  modify' $
    (\players -> foldl' (flip recordKill) players overseerDeaths)
      . (\players -> foldl' (flip recordOverseerHit) players overseerHits)

  -- Capture-the-flag contributions
  modify' $ recordFlagStats flagEvents

-- | Update player kills and deaths resulting from an overseer death.
recordKill ::
  ES.OverseerDeathEvent ->
  WIM.IntMap ActorID Player ->
  WIM.IntMap ActorID Player
recordKill death players =
  let mKillerTeam =
        fmap (view player_team) $
          flip WIM.lookup players =<< ES.ode_killer death
  in  case mKillerTeam of
        Nothing -> addDeath players
        Just killerTeam ->
          if ES.ode_owner_team death == killerTeam
            then addDeath players
            else addDeath $ addKill players
 where
  addDeath :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
  addDeath = ix (ES.ode_owner death) . field @"player_stats" %~ incrementDeaths

  addKill :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
  addKill =
    maybe
      id
      (\killer -> ix killer . field @"player_stats" %~ incrementKills)
      (ES.ode_killer death)

recordOverseerHit ::
  ES.OverseerHitEvent ->
  WIM.IntMap ActorID Player ->
  WIM.IntMap ActorID Player
recordOverseerHit
  ES.OverseerHitEvent
    { ES.ohe_attacker = mAttacker
    , ES.ohe_defender = defender
    , ES.ohe_hitType = hitType
    , ES.ohe_damage = damage
    } =
    if mAttacker == Just defender
      then addDmgTaken
      else addDmgTaken . addDmgDealt
   where
    addDmgTaken :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
    addDmgTaken = ix defender . field @"player_stats" %~ addDamageTaken damage

    addDmgDealt :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
    addDmgDealt = case mAttacker of
      Nothing -> id
      Just attacker ->
        case hitType of
          ES.DroneHit ->
            ix attacker . field @"player_stats"
              %~ addDamageDealt damage
                . addDroneHitDamage damage
          ES.BodyHit ->
            ix attacker . field @"player_stats"
              %~ addDamageDealt damage
                . addBodyHitDamage damage
          ES.PsiStormHit ->
            ix attacker . field @"player_stats"
              %~ addDamageDealt damage
                . addStormHitDamage damage

recordFlagStats ::
  FlagEvents -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagStats flagEvents =
  recordFlagRecoveries (flagEvents_recoveries flagEvents)
    . recordFlagCaptures (flagEvents_captures flagEvents)

recordFlagCaptures ::
  [FlagCapture] -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagCaptures flagCaptures players =
  foldl' (flip recordFlagCapture) players flagCaptures
 where
  recordFlagCapture (FlagCapture{flagCapture_actorID}) =
    ix flagCapture_actorID . field @"player_stats" %~ incrementFlagsCaptured

recordFlagRecoveries ::
  [FlagRecovery] -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagRecoveries flagRecoveries players =
  foldl' (flip recordFlagRecovery) players flagRecoveries
 where
  recordFlagRecovery (FlagRecovery _ mPid _) =
    maybe
      id
      (\pid -> ix pid . field @"player_stats" %~ incrementFlagsRecovered)
      mPid
