{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.MasterMinionSystem (
  MasterComponent,
  MinionComponent (..),
  MasterMinionStore,
  newMasterMinionStore,
  clearMasterMinionStore,
  MasterMinionStoreSnapshot,
  registerMaster,
  registerMinion,
  unregisterMinion,
  unregisterMaster,
  recruitingMasters,
  getMinions,
  lookupMaster,
) where

import Control.Monad.ST
import Data.Generics.Product.Fields
import Data.Sequence qualified as S
import Data.Serialize (Serialize)
import Data.Void (Void)
import Foreign.Storable (Storable)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Mutable as Mutable
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (BoxedVecStore, StorableVecStore)

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core (
  CoreM,
  coreM,
 )
import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core qualified as Store (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------
-- Components

-- | Entities with references to other entities
data MasterComponent = MasterComponent
  { master_minionIDs :: WIS.IntSet (EntityID 'Drone)
  , master_minionDeathTimes :: S.Seq Time
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

newMasterComponent :: Int -> MasterComponent
newMasterComponent minionLimit =
  MasterComponent
    { master_minionIDs = WIS.empty
    , master_minionDeathTimes = S.replicate minionLimit initialTime
    }

addMinion :: EntityID 'Drone -> MasterComponent -> MasterComponent
addMinion minionID =
  over (field @"master_minionIDs") (WIS.insert minionID)
    . over (field @"master_minionDeathTimes") dropLeft
 where
  dropLeft :: S.Seq a -> S.Seq a
  dropLeft s = case S.viewl s of
    S.EmptyL -> s
    _ S.:< s' -> s'

removeMinion ::
  EntityID 'Drone -> Time -> MasterComponent -> MasterComponent
removeMinion minionID time =
  over (field @"master_minionIDs") (WIS.delete minionID)
    . over (field @"master_minionDeathTimes") (S.|> time)

hasReadyMinionRespawn :: CombatParams -> Time -> MasterComponent -> Bool
hasReadyMinionRespawn params time masterComp =
  case master_minionDeathTimes masterComp of
    deathTime S.:<| _ ->
      diffTime time deathTime >= gp_droneRespawnDelay params
    _ -> False

-- | Entities that serve other entities
newtype MinionComponent = MinionComponent
  { minion_masterEID :: EntityID 'Overseer
  }
  deriving newtype (Serialize, Storable)

--------------------------------------------------------------------------------
-- Store

-- Invariant: The master of each minion exists in the masters store.

data MasterMinionStore s = MasterMinionStore
  { mm_masters ::
      DenseStore
        s
        (EntityID 'Overseer)
        (EntityID 'Overseer)
        (BoxedVecStore MasterComponent)
  , mm_minions ::
      DenseStore
        s
        (EntityID 'Drone)
        Void
        (StorableVecStore MinionComponent)
  }

newMasterMinionStore :: ST s (MasterMinionStore s)
newMasterMinionStore =
  MasterMinionStore
    <$> DS.newDenseStore (fromIntegral C.maxActorsPerGame)
    <*> DS.newDenseStore C.maxNumEntities

clearMasterMinionStore :: MasterMinionStore s -> CoreM s ()
clearMasterMinionStore store = do
  Store.clear $ mm_masters store
  Store.clear $ mm_minions store

data MasterMinionStoreSnapshot = MasterMinionStoreSnapshot
  { _snapsGot_masters ::
      DenseStoreSnapshot (EntityID 'Overseer) (BoxedVecStore MasterComponent)
  , _snapshot_minions ::
      DenseStoreSnapshot (EntityID 'Drone) (StorableVecStore MinionComponent)
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s MasterMinionStoreSnapshot (MasterMinionStore s) where
  freeze :: MasterMinionStore s -> ST s MasterMinionStoreSnapshot
  freeze (MasterMinionStore masters minions) =
    MasterMinionStoreSnapshot <$> freeze masters <*> freeze minions

  thaw :: MasterMinionStoreSnapshot -> ST s (MasterMinionStore s)
  thaw (MasterMinionStoreSnapshot masters minions) =
    MasterMinionStore <$> thaw masters <*> Mutable.thaw minions

instance Copyable s (MasterMinionStore s) where
  copy target source = do
    DS.copyDenseStore (mm_masters target) (mm_masters source)
    DS.copyDenseStore (mm_minions target) (mm_minions source)

registerMaster ::
  MasterMinionStore s -> EntityID 'Overseer -> Int -> CoreM s ()
registerMaster store masterID minionLimit =
  Store.insert (mm_masters store) masterID (newMasterComponent minionLimit)

registerMinion ::
  MasterMinionStore s -> EntityID 'Drone -> MinionComponent -> CoreM s ()
registerMinion store minionID minionComp@(MinionComponent masterID) = do
  Store.insert (mm_minions store) minionID minionComp
  coreM $ DS.modify (mm_masters store) masterID (addMinion minionID)

unregisterMinion ::
  MasterMinionStore s -> Time -> EntityID 'Drone -> CoreM s ()
unregisterMinion store time minionID =
  coreM (Store.lookup (mm_minions store) minionID) >>= \case
    Nothing -> pure ()
    Just (MinionComponent masterID) -> do
      Store.delete (mm_minions store) minionID
      coreM $
        DS.modify (mm_masters store) masterID (removeMinion minionID time)

unregisterMaster ::
  MasterMinionStore s -> EntityID 'Overseer -> CoreM s [EntityID 'Drone]
unregisterMaster store masterID =
  coreM (Store.lookup (mm_masters store) masterID) >>= \case
    Nothing -> pure []
    Just masterComponent -> do
      Store.delete (mm_masters store) masterID
      let minionIDs = WIS.toList $ master_minionIDs masterComponent
      mapM_ (Store.delete (mm_minions store)) minionIDs
      pure minionIDs

--------------------------------------------------------------------------------
-- Query

recruitingMasters ::
  MasterMinionStore s ->
  CombatParams ->
  Time ->
  ST s [EntityID 'Overseer]
recruitingMasters store params time =
  map fst . filter (hasReadyMinionRespawn params time . snd)
    <$> DS.toList (mm_masters store)

getMinions ::
  MasterMinionStore s ->
  EntityID 'Overseer ->
  ST s (WIS.IntSet (EntityID 'Drone))
getMinions store masterEID =
  master_minionIDs <$> Store.sureLookup (mm_masters store) masterEID

lookupMaster ::
  MasterMinionStore s ->
  EntityID 'Drone ->
  ST s (Maybe (EntityID 'Overseer))
lookupMaster store droneID =
  fmap minion_masterEID <$> Store.lookup (mm_minions store) droneID
