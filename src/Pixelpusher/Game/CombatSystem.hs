{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}

module Pixelpusher.Game.CombatSystem (
  CombatStore,
  newCombatStore,
  CombatReadStore,
  readCombatStore,
  ReadCombat (..),
  setHealthRegenType,
  applyPoisonDamage,
  CombatDeathEvent (..),
  SmiteOverseers (..),
  runCombat,
  recentDamage,
  recentDamageDecayFactorTicks,
) where

import Control.Monad (unless, when)
import Control.Monad.ST
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Writer.CPS
import Control.Monad.Writer.Class ()
import Control.Monad.Writer.Class.Custom ()
import Data.Foldable (foldl', for_, traverse_)
import Data.Generics.Product.Fields
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Maybe (catMaybes)
import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom
import System.Random.MWC

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.CombatEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )
import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core qualified as Store (
  ReadOnly,
  lookup,
  sureLookup,
 )

--------------------------------------------------------------------------------
-- Store

type CombatStore' s =
  DS.DenseStore
    s
    (SEntityID 'CombatEntity)
    (SEntityID 'CombatEntity)
    CombatComponentVec

newtype CombatStore s = CombatStore (CombatStore' s)
  deriving newtype
    ( ReadStore s (SEntityID 'CombatEntity) CombatComponent
    , Store s (SEntityID 'CombatEntity) CombatComponent
    , Mutable s (DS.DenseStoreSnapshot (SEntityID 'CombatEntity) CombatComponentVec)
    , SureReadStore s (SEntityID 'CombatEntity) CombatComponent
    , ReadCombat s
    )

instance Copyable s (CombatStore s) where
  copy (CombatStore target) (CombatStore source) =
    DS.copyDenseStore target source

newCombatStore :: ST s (CombatStore s)
newCombatStore = CombatStore <$> DS.newDenseStore C.maxNumEntities

newtype CombatReadStore s = CombatReadStore (CombatStore s)
  deriving newtype
    ( ReadStore s (SEntityID 'CombatEntity) CombatComponent
    , SureReadStore s (SEntityID 'CombatEntity) CombatComponent
    , ReadCombat s
    )

readCombatStore :: CombatStore s -> CombatReadStore s
readCombatStore = CombatReadStore

--------------------------------------------------------------------------------
-- Query

class ReadCombat s store where
  getHealth ::
    (SubEntityID i (SEntityID 'CombatEntity)) => store -> i -> ST s Fixed
  getHealthFraction ::
    (SubEntityID i (SEntityID 'CombatEntity)) => store -> i -> ST s Fixed

instance ReadCombat s (CombatStore' s) where
  getHealth combatComps = DS.sureLookup_1 combatComps vec_com_currentHealth
  {-# INLINE getHealth #-}
  getHealthFraction combatComps =
    fmap (uncurry (/))
      . DS.sureLookup_2 combatComps vec_com_currentHealth vec_com_maxHealth
  {-# INLINE getHealthFraction #-}

--------------------------------------------------------------------------------
-- Operations

setHealthRegenType ::
  (SubEntityID i (SEntityID 'CombatEntity)) =>
  CombatStore s ->
  i ->
  HealthRegenType ->
  ST s ()
setHealthRegenType (CombatStore combatComps) =
  DS.sureSet_1 combatComps vec_com_healthRegenType

-- | Interrupt an entity's health regeneration and reduce its health without
-- killing it. Bypasses invulnerability.
applyPoisonDamage ::
  (SubEntityID i (SEntityID 'CombatEntity)) =>
  CombatStore s ->
  Time ->
  i ->
  Fixed ->
  ST s ()
applyPoisonDamage (CombatStore combatComps) time i damage =
  DS.modify combatComps i (applyPoisonDamage' time damage)

--------------------------------------------------------------------------------
-- System

data SmiteOverseers = SmiteOverseers | DoNotSmiteOverseers

runCombat ::
  Gen s ->
  CombatStore s ->
  Store.ReadOnly (ActorEntityStore s) ->
  CombatParams ->
  Time ->
  SmiteOverseers ->
  [Pair (EntityIDPair 'CombatEntity) Collision] ->
  [PsiStormOverlap] ->
  [SpawnOverlap] ->
  ST s CombatEvents
runCombat
  gen
  (CombatStore combatComps)
  actorComps
  params
  time
  shouldSmiteOverseers
  collisions
  psiStormOverlaps
  spawnOverlaps =
    execWriterT $ do
      lift $
        when (gp_unlimitedSpawnInvulnerability params) $
          applySpawnInvulnerability combatComps time spawnOverlaps
      lift (regenerateHealth combatComps params time)
        >>= scribe (field @"ce_deaths")
      applyPsiStormDamage
        params
        combatComps
        actorComps
        time
        psiStormOverlaps
      traverse_ (tradeBlows combatComps actorComps time) collisions
      case shouldSmiteOverseers of
        DoNotSmiteOverseers ->
          pure ()
        SmiteOverseers ->
          lift (smiteOverseers gen combatComps)
            >>= scribe (field @"ce_deaths")

data IsKilled = Killed | NotKilled
  deriving stock (Eq)

data IsInvulnerable = Invulnerable | NotInvulnerable
  deriving stock (Eq)

tradeBlows ::
  CombatStore' s ->
  Store.ReadOnly (ActorEntityStore s) ->
  Time ->
  Pair (EntityIDPair 'CombatEntity) Collision ->
  WriterT CombatEvents (ST s) ()
tradeBlows combatComps actorComps time collision = do
  let eidPair :!: collision' = collision
      (eid1, eid2) = getEntityIDPair eidPair

  currentHealth1 <-
    lift $ DS.sureLookup_1 combatComps vec_com_currentHealth eid1
  currentHealth2 <-
    lift $ DS.sureLookup_1 combatComps vec_com_currentHealth eid2

  let damage = min currentHealth1 $ min currentHealth2 $ col_damage collision'
  when (damage > 0) $ do
    let applyDamage' =
          applyDamage InterruptRegen time damage
    (isKilled1, isInvulnerable1) <-
      lift $
        DS.sureModifyReturn combatComps eid1 applyDamage'
    (isKilled2, isInvulnerable2) <-
      lift $
        DS.sureModifyReturn combatComps eid2 applyDamage'

    when (isKilled1 == Killed) $ do
      mKillerPid2 <- lift $ Store.lookup actorComps eid2
      scribe (field @"ce_deaths") $
        pure $
          CombatDeathEvent{killed_eid = eid1, killer_mPid = mKillerPid2}
    when (isKilled2 == Killed) $ do
      mKillerPid1 <- lift $ Store.lookup actorComps eid1
      scribe (field @"ce_deaths") $
        pure $
          CombatDeathEvent{killed_eid = eid2, killer_mPid = mKillerPid1}

    participants <-
      lift $
        case (refineEntityID eid1, refineEntityID eid2) of
          (OverseerID overseerID1, OverseerID overseerID2) -> do
            CollisionDamageOverseers
              <$> Store.sureLookup actorComps overseerID1
              <*> Store.sureLookup actorComps overseerID2
          (OverseerID overseerID, DroneID _) -> do
            CollisionDamageOverseerDrone
              <$> Store.sureLookup actorComps overseerID
          (DroneID _, OverseerID overseerID) -> do
            CollisionDamageOverseerDrone
              <$> Store.sureLookup actorComps overseerID
          (DroneID _, DroneID _) ->
            pure CollisionDamageDrones

    let collisionDamage = flip fmap collision $ \Collision{..} ->
          CollisionDamage
            { cd_position = col_position
            , cd_velocity = col_velocity
            , cd_damage = damage
            , cd_direction = col_direction
            , cd_mActorID1 =
                if isInvulnerable1 == Invulnerable
                  then Nothing
                  else col_mActorID1
            , cd_mActorID2 =
                if isInvulnerable2 == Invulnerable
                  then Nothing
                  else col_mActorID2
            , cd_participants = participants
            }
    scribe (field @"ce_collisions") $ pure collisionDamage

-- Helper
data InterruptRegen = InterruptRegen | NoInterruptRegen

-- | Apply damage, but not to invulnerable entities
applyDamage ::
  InterruptRegen ->
  Time ->
  Fixed ->
  CombatComponent ->
  ((IsKilled, IsInvulnerable), CombatComponent)
applyDamage shouldInterruptRegen time rawDamage combatComp
  | isInvulnerable time combatComp = ((NotKilled, Invulnerable), combatComp)
  | otherwise =
      let (isKilled, combatComp') =
            forceApplyDamage shouldInterruptRegen time rawDamage combatComp
      in  ((isKilled, NotInvulnerable), combatComp')

-- | Apply damage regardless of invulnerability
forceApplyDamage ::
  InterruptRegen ->
  Time ->
  Fixed ->
  CombatComponent ->
  (IsKilled, CombatComponent)
forceApplyDamage shouldInterruptRegen time rawDamage combatComp =
  let currentHealth = com_currentHealth combatComp
      damage = min currentHealth rawDamage
      newHealth = currentHealth - damage
      oldDamage = recentDamage time combatComp
      setRegenInterrupt =
        case shouldInterruptRegen of
          NoInterruptRegen -> id
          InterruptRegen -> field @"com_lastRegenInterruptTime" .~ time
      combatComp' =
        combatComp
          & field @"com_currentHealth" .~ newHealth
          & field @"com_lastDamageTime" .~ time
          & field @"com_lastDamageAmount" .~ damage + oldDamage
          & setRegenInterrupt
      isKilled =
        if newHealth <= 0 && currentHealth > 0
          then Killed
          else NotKilled
  in  (isKilled, combatComp')

-- | Interrupt an entity's health regeneration and reduce its health without
-- killing it. Bypasses invulnerability.
applyPoisonDamage' ::
  Time ->
  Fixed ->
  CombatComponent ->
  CombatComponent
applyPoisonDamage' time rawDamage combatComp =
  let currentHealth = com_currentHealth combatComp
      nonLethalDamage = min (com_currentHealth combatComp - epsilon) rawDamage
       where
        epsilon = 1e-3
      newHealth = currentHealth - nonLethalDamage
      oldDamage = recentDamage time combatComp
      combatComp' =
        combatComp
          & field @"com_currentHealth" .~ newHealth
          & field @"com_lastDamageTime" .~ time
          & field @"com_lastDamageAmount" .~ nonLethalDamage + oldDamage
          & field @"com_lastRegenInterruptTime" .~ time
  in  combatComp'

-- | Returns total damage taken weighted by an exponential decay with time
recentDamage :: Time -> CombatComponent -> Fixed
recentDamage time combatComp =
  let oldTime = com_lastDamageTime combatComp
      ticksSinceDamage =
        -- 32767 = 2^15 - 1
        fromIntegral $ min 32767 $ getTicks $ time `diffTime` oldTime
      oldDamage = com_lastDamageAmount combatComp
  in  oldDamage * exp (-ticksSinceDamage * recentDamageDecayFactorTicks)

{-# INLINE recentDamageDecayFactorTicks #-}
recentDamageDecayFactorTicks :: Fractional a => a
recentDamageDecayFactorTicks = 0.35 -- half life of 2 ticks

regenerateHealth ::
  CombatStore' s -> CombatParams -> Time -> ST s [CombatDeathEvent]
regenerateHealth combatComps params time =
  if not (time `atMultipleOf` gp_healthRegenPeriodTicks params)
    then pure []
    else fmap catMaybes
      $ DS.mapWithIndexReturn_3_1
        combatComps
        vec_com_healthRegenType
        vec_com_maxHealth
        vec_com_lastRegenInterruptTime
        vec_com_currentHealth
      $ \combatID regenType maxHP regenInterruptTime hp ->
        let newHP = case regenType of
              -- Overseers
              HealthRegenQuadratic ->
                let ticksSinceInterrupt =
                      -- `min` necessary to prevent fixed-point overflow
                      min (gp_quadraticHealthFullRegenTicks params) $
                        time `diffTime` regenInterruptTime
                    periodsSinceInterrupt =
                      fromIntegral ticksSinceInterrupt
                        / fromIntegral (gp_healthRegenPeriodTicks params)
                    rateDivisor =
                      gp_quadraticHealthRegenRateDivisor params
                    hp' = hp + (maxHP / rateDivisor) * periodsSinceInterrupt
                    hpClamp = max hp maxHP
                in  min hpClamp hp'
              -- Drones
              HealthRegenExponential expRate ->
                maxHP - expRate * (maxHP - hp)
              -- Decaying drones
              HealthRegenLinear linearRate ->
                hp + maxHP * linearRate

            mDeathEvent =
              if newHP > 0
                then Nothing
                else
                  Just
                    CombatDeathEvent
                      { killed_eid = combatID
                      , killer_mPid = Nothing
                      }
        in  (newHP, mDeathEvent)

smiteOverseers :: Gen s -> CombatStore' s -> ST s [CombatDeathEvent]
smiteOverseers gen combatComps =
  catMaybes
    <$> DS.traverseWithIndexReturn_1
      combatComps
      vec_com_currentHealth
      \combatID hp ->
        case refineEntityID combatID of
          OverseerID _
            | hp > 0 ->
                uniformRM (1, C.tickRate_hz `div` 6) gen <&> \x ->
                  if x == 1
                    then
                      let combatDeathEvent =
                            CombatDeathEvent
                              { killed_eid = combatID
                              , killer_mPid = Nothing
                              }
                      in  (0, Just combatDeathEvent)
                    else (hp, Nothing)
          _ ->
            pure (hp, Nothing)

applyPsiStormDamage ::
  CombatParams ->
  CombatStore' s ->
  Store.ReadOnly (ActorEntityStore s) ->
  Time ->
  [PsiStormOverlap] ->
  WriterT CombatEvents (ST s) ()
applyPsiStormDamage
  params
  combatComps
  actorComps
  time
  (stackPsiStormOverlaps -> stackedPsiStormOverlaps) = do
    let interruptRegen =
          if gp_psiStormInterruptsRegen params
            then InterruptRegen
            else NoInterruptRegen
    for_ stackedPsiStormOverlaps $ \(combatID, psiStormOverlaps) -> do
      let !totalOverlap = foldl' (+) 0 $ fmap pso_overlap psiStormOverlaps
          !totalStackedOverlap =
            sqrt $ foldl' (+) 0 $ fmap (sq . pso_overlap) psiStormOverlaps
           where
            sq x = x * x
          damageFactor =
            case refineEntityID combatID of
              OverseerID _ -> gp_psiStormOverseerDamageFactor params
              DroneID _ -> 1
          totalDamage =
            damageFactor * totalStackedOverlap * gp_psiStormDamagePerHit params
          damageScale = totalDamage / totalOverlap
      do
        let pso :| _ = psiStormOverlaps
        isInvulnerable' <-
          lift $
            DS.sureLookup_1 combatComps vec_com_invulnerableUntil combatID
              <&> (time <=)
        unless isInvulnerable' $
          scribe (field @"ce_psiStormEffects") $
            pure
              PsiStormEffect
                { pse_mEntityActorID = pso_mEntityActorID pso
                , pse_entityPos = pso_entityPos pso
                , pse_entityVel = pso_entityVel pso
                , pse_damage = totalDamage
                }
      for_ psiStormOverlaps $ \pso -> do
        let damage = damageScale * pso_overlap pso
        (isKilled, isInvulnerable') <-
          lift $
            DS.sureModifyReturn combatComps combatID $
              applyDamage interruptRegen time damage
        when (isKilled == Killed) $ do
          mKillerPid <-
            lift $ Store.lookup actorComps $ pso_psiStormEntityID pso
          scribe (field @"ce_deaths") $
            pure
              CombatDeathEvent
                { killed_eid = combatID
                , killer_mPid = mKillerPid
                }
        when (isInvulnerable' == NotInvulnerable) $
          case refineEntityID combatID of
            OverseerID overseerID ->
              scribe (field @"ce_psiStormDamage") $
                pure
                  OverseerPsiStormDamage
                    { opsd_overseerID = overseerID
                    , opsd_damage = damage
                    , opsd_mPsiStormActorID = pso_mPsiStormActorID pso
                    }
            DroneID _ -> pure ()

stackPsiStormOverlaps ::
  [PsiStormOverlap] ->
  [(SEntityID 'CombatEntity, NonEmpty PsiStormOverlap)]
stackPsiStormOverlaps =
  WIM.toList
    . WIM.fromListWith (<>)
    . map (\psOverlap -> (pso_entityID psOverlap, pure psOverlap))

applySpawnInvulnerability :: CombatStore' s -> Time -> [SpawnOverlap] -> ST s ()
applySpawnInvulnerability store time spawnOverlaps = do
  let extendedInvulnerabilityTime =
        addTicks C.spawnInvulnerabilityCheckPeriod time
  for_ spawnOverlaps $ \(SpawnOverlap overseerID) ->
    DS.sureModify_1 store vec_com_invulnerableUntil overseerID $
      max extendedInvulnerabilityTime
