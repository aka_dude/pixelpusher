{-# LANGUAGE BlockArguments #-}

module Pixelpusher.Game.DynamicsSystem (
  DynamicsStore,
  newDynamicsStore,
  DynamicsReadStore,
  readDynamicsStore,
  ReadDynamics (..),
  modifyVelocity,
  modifyVelocityWithMass,
  stepDynamics,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Data.Foldable (traverse_)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.CombatSystem
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Dynamics.Collisions (runCollisions)
import Pixelpusher.Game.Dynamics.Controls (boundPlayerControls)
import Pixelpusher.Game.Dynamics.Independent (stepIndependentKinematics)
import Pixelpusher.Game.Dynamics.Thermostat (driftingSoftObstacleThermostat)
import Pixelpusher.Game.Dynamics.Types (DynamicsStore')
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.PsiStormSystem
import Pixelpusher.Game.TeamSystem
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )
import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core qualified as Store (
  ReadOnly,
 )

--------------------------------------------------------------------------------
-- Newtype wrapping and boilerplate

newtype DynamicsStore s = DynamicsStore (DynamicsStore' s)
  deriving newtype
    ( ReadStore s (SEntityID 'DynamicsEntity) DynamicsComponent
    , Store s (SEntityID 'DynamicsEntity) DynamicsComponent
    , Mutable s (DenseStoreSnapshot (SEntityID 'DynamicsEntity) DynamicsComponentVec)
    , SureReadStore s (SEntityID 'DynamicsEntity) DynamicsComponent
    , ReadDynamics s
    )

instance Copyable s (DynamicsStore s) where
  copy (DynamicsStore target) (DynamicsStore source) =
    DS.copyDenseStore target source

newDynamicsStore :: ST s (DynamicsStore s)
newDynamicsStore = DynamicsStore <$> DS.newDenseStore C.maxNumEntities

newtype DynamicsReadStore s = DynamicsReadStore (DynamicsStore s)
  deriving newtype
    ( ReadStore s (SEntityID 'DynamicsEntity) DynamicsComponent
    , SureReadStore s (SEntityID 'DynamicsEntity) DynamicsComponent
    , ReadDynamics s
    )

readDynamicsStore :: DynamicsStore s -> DynamicsReadStore s
readDynamicsStore = DynamicsReadStore

--------------------------------------------------------------------------------
-- Query

class ReadDynamics s store where
  -- | Returns a list of all the dynamics components and their EntityIDs,
  -- filtered by EntityID, position, and radius.
  getDynamicsComponentsFiltered ::
    store ->
    (SEntityID 'DynamicsEntity -> Fixed -> Fixed2 -> Bool) ->
    ST s [(SEntityID 'DynamicsEntity, DynamicsComponent)]
  readPos ::
    (SubEntityID i (SEntityID 'DynamicsEntity)) =>
    store ->
    i ->
    ST s Fixed2
  readPosVel ::
    (SubEntityID i (SEntityID 'DynamicsEntity)) =>
    store ->
    i ->
    ST s (Fixed2, Fixed2)
  readPosVelRadius ::
    (SubEntityID i (SEntityID 'DynamicsEntity)) =>
    store ->
    i ->
    ST s (Fixed2, Fixed2, Fixed)

instance ReadDynamics s (DynamicsStore' s) where
  getDynamicsComponentsFiltered comps =
    DS.toListFilteredWithIdx2 comps vec_dyn_radius vec_dyn_pos
  readPos comps = DS.sureLookup_1 comps vec_dyn_pos
  readPosVel comps = DS.sureLookup_2 comps vec_dyn_pos vec_dyn_vel
  readPosVelRadius comps =
    DS.sureLookup_3 comps vec_dyn_pos vec_dyn_vel vec_dyn_radius
  {-# INLINE getDynamicsComponentsFiltered #-}
  {-# INLINE readPos #-}
  {-# INLINE readPosVel #-}
  {-# INLINE readPosVelRadius #-}

--------------------------------------------------------------------------------
-- Operations

modifyVelocity ::
  (SubEntityID i (SEntityID 'DynamicsEntity)) =>
  DynamicsStore s ->
  i ->
  (Fixed2 -> Fixed2) ->
  ST s ()
modifyVelocity (DynamicsStore dynComps) = DS.sureModify_1 dynComps vec_dyn_vel

modifyVelocityWithMass ::
  (SubEntityID i (SEntityID 'DynamicsEntity)) =>
  DynamicsStore s ->
  i ->
  (Fixed -> Fixed2 -> Fixed2) ->
  ST s ()
modifyVelocityWithMass (DynamicsStore dynComps) =
  DS.sureModify_1_1 dynComps vec_dyn_mass vec_dyn_vel

--------------------------------------------------------------------------------
-- Integration

-- | Integrate entity dynamics.
stepDynamics ::
  GameParams ->
  Time ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  MasterMinionStore s ->
  CombatReadStore s ->
  PsiStormStore s ->
  DynamicsStore s ->
  ST s CollisionEvents
stepDynamics
  params
  time
  playerCtrlsOverseers
  teamStore
  actorEntityStore
  masterMinionStore
  combatStore
  psiStormStore
  (DynamicsStore dynComps) = do
    let dynParams = gp_dynamics params

    -- Soft obstacle thermostat
    driftingSoftObstacleThermostat dynParams time dynComps

    -- Bound player inputs
    dynCtrls <-
      traverse (boundPlayerControls dynParams dynComps) playerCtrlsOverseers

    -- Record positions
    -- TODO: read depending on game time instead of copying
    DS.unsafeCopy dynComps vec_dyn_pos_prev6 vec_dyn_pos_prev5
    DS.unsafeCopy dynComps vec_dyn_pos_prev5 vec_dyn_pos_prev4
    DS.unsafeCopy dynComps vec_dyn_pos_prev4 vec_dyn_pos_prev3
    DS.unsafeCopy dynComps vec_dyn_pos_prev3 vec_dyn_pos_prev2
    DS.unsafeCopy dynComps vec_dyn_pos_prev2 vec_dyn_pos_prev
    DS.unsafeCopy dynComps vec_dyn_pos_prev vec_dyn_pos

    -- Run independent kinematics
    DS.traverseWithIndex_1_2
      dynComps
      vec_dyn_radius
      vec_dyn_pos
      vec_dyn_vel
      (stepIndependentKinematics dynParams dynCtrls actorEntityStore masterMinionStore)

    -- Grow psi-storms
    getPsiStorms psiStormStore >>= traverse_ \psiStormID ->
      DS.sureModify_1 dynComps vec_dyn_radius psiStormID (growPsiStorm dynParams)

    -- Collisions
    partitionCollisionEvents
      <$> runCollisions
        params
        teamStore
        actorEntityStore
        combatStore
        time
        dynComps

growPsiStorm :: DynamicsParams -> Fixed -> Fixed
growPsiStorm dynParams radius =
  let targetRadius = gp_psiStormRadius dynParams
      initFrac = gp_psiStormInitialGrowthFraction dynParams
      currentFraction = radius / targetRadius
      scaling =
        1
          - gp_psiStormGrowthConstant dynParams
            * (initFrac + (1 - initFrac) * currentFraction)
      radius' = targetRadius - (targetRadius - radius) * scaling
  in  radius'
