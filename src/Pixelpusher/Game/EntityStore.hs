{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RecordWildCards #-}

-- | This module encapsulates the /core/ of the game logic.
--
-- This module is not well organized, and the boundary between this module and
-- "Pixelpusher.Game.GameState" is arbitrary.
module Pixelpusher.Game.EntityStore (
  -- Store type
  EntityStore,
  Core.EntityStoreSnapshot,
  initialStore,
  resetEntityStore,
  -- Queries
  viewPlayers,
  getFlags,
  -- Operations
  addPlayer,
  removePlayer,
  addSoftObstacle,
  spawnTeamBase,
  spawnBaseFlag,
  spawnSpawnArea,
  -- GameStateIntergration
  stepEntities,
  EntityStepEvents (..),
  OverseerDeathEvent (..),
  DroneDeathEvent (..),
  OverseerHitEvent (..),
  HitType (..),
  Collision (..),
  CastEvent (..),
  PsiStormCastEvent (..),
  DashCastEvent (..),
  FlagDropEvent (..),
  -- Export entity data for rendering
  getRenderingComponents,
) where

import Control.Arrow ((&&&))
import Control.Monad (join, void, when)
import Control.Monad.ST (ST)
import Data.Bifunctor (bimap, first)
import Data.Either (partitionEithers)
import Data.Foldable (for_, toList, traverse_)
import Data.IntMap.Merge.Strict qualified as IntMap
import Data.IntMap.Strict qualified as IntMap
import Data.List (foldl')
import Data.Maybe (catMaybes)
import Data.Strict.Tuple (Pair ((:!:)))
import Data.Strict.Tuple qualified as Pair
import Data.Traversable (for)
import Lens.Micro.Platform.Custom
import System.Random.MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIntSet
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View (BotView, makeBotViews)
import Pixelpusher.Game.Cast
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.CombatEvents
import Pixelpusher.Game.CombatSystem
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.DynamicsSystem
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.FlagSystem hiding (getFlags)
import Pixelpusher.Game.FlagSystem qualified as Flag
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamBase
import Pixelpusher.Game.TeamScore
import Pixelpusher.Game.TeamSystem
import Pixelpusher.Game.Time
import Pixelpusher.Game.TransitorySystem
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.EntityStore.Core (ReadOnly, readOnly)
import Pixelpusher.Game.EntityStore.Core qualified as Core
import Pixelpusher.Game.EntityStore.Rendering qualified as ES

--------------------------------------------------------------------------------
-- Store type

-- | An abstract wrapper around 'Core.EntityStore' for encapsulation purposes.
newtype EntityStore s = EntityStore (Core.EntityStore s)
  deriving newtype
    ( Mutable s Core.EntityStoreSnapshot
    , Copyable s
    )

initialStore :: ST s (EntityStore s)
initialStore = EntityStore <$> Core.initialEntityStore

resetEntityStore :: EntityStore s -> ST s ()
resetEntityStore (EntityStore store) = Core.resetEntityStore store

--------------------------------------------------------------------------------
-- Queries

viewPlayers :: EntityStore s -> ST s (WIM.IntMap ActorID PlayerView)
viewPlayers (EntityStore store) =
  Core.getPlayerStatuses store >>= traverse (viewPlayer store)

viewPlayer :: Core.EntityStore s -> PlayerStatus -> ST s PlayerView
viewPlayer store PlayerStatus{..} = do
  let Core.Stores{store_dynamics, store_combat, store_flag} =
        Core.dataStores store
  overseerView <-
    case ps_overseerStatus of
      OverseerDead deathTime respawnTime deathPos ->
        pure $
          OV_Dead
            OverseerViewDead
              { ovd_deathTime = deathTime
              , ovd_respawnTime = respawnTime
              , ovd_lastPos = deathPos
              }
      OverseerAlive overseerID _ -> do
        (pos, vel) <- readPosVel store_dynamics overseerID
        hasFlag <- flag_hasFlag <$> Core.sureLookup store_flag overseerID
        healthFrac <-
          min 1 . max 0 <$> getHealthFraction store_combat overseerID
        pure $
          OV_Alive
            OverseerViewAlive
              { ova_pos = pos
              , ova_vel = vel
              , ova_hasFlag = hasFlag
              , ova_healthFraction = healthFrac
              }
  pure
    PlayerView
      { pv_team = ps_team
      , pv_overseerView = overseerView
      , pv_cooldowns = ps_cooldowns
      }

getFlags :: EntityStore s -> ST s [(Team, Fixed2, FlagType)]
getFlags (EntityStore store) = do
  Flag.getFlags store_flag store_team' >>= traverse \(flagsID, team) -> do
    pos <- dyn_pos <$> Core.sureLookup store_dynamics flagsID
    pure (team, pos, flagType flagsID)
 where
  Core.Stores{store_flag, store_dynamics, store_team} = Core.dataStores store
  store_team' = readOnly store_team

  flagType :: SEntityID 'FlagEntity -> FlagType
  flagType flagsID =
    case refineEntityID flagsID of
      BaseFlagID _ -> BaseFlagType
      FlagID _ -> DroppedFlagType
      OverseerID _ -> OverseerFlagType
      TeamBaseID _ -> error "getFlags: team bases should not carry flags"

--------------------------------------------------------------------------------
-- Operations

addPlayer :: GameParams -> EntityStore s -> Time -> Team -> ActorID -> ST s ()
addPlayer params (EntityStore store) = Core.addPlayer params store

removePlayer :: GameParams -> EntityStore s -> Time -> ActorID -> ST s ()
removePlayer params (EntityStore store) = Core.removePlayer params store

addSoftObstacle ::
  GameParams ->
  EntityStore s ->
  Maybe Fixed ->
  Fixed2 ->
  Maybe Fixed ->
  ST s ()
addSoftObstacle params (EntityStore store) = Core.addSoftObstacle params store

spawnTeamBase :: GameParams -> EntityStore s -> Team -> ST s ()
spawnTeamBase params (EntityStore store) team =
  let pos = teamBaseLocation params team
  in  void $ Core.addTeamBase params store team pos

spawnBaseFlag :: GameParams -> EntityStore s -> Team -> ST s ()
spawnBaseFlag params (EntityStore store) = spawnBaseFlag' params store

spawnBaseFlag' :: GameParams -> Core.EntityStore s -> Team -> ST s ()
spawnBaseFlag' params store team =
  let pos = teamBaseLocation params team
  in  void $ Core.addBaseFlag params store team pos

spawnSpawnArea :: GameParams -> EntityStore s -> Team -> ST s ()
spawnSpawnArea params (EntityStore store) = Core.addSpawnArea params store

--------------------------------------------------------------------------------
-- Game state integration

data EntityStepEvents = EntityStepEvents
  { ese_overseerDeaths :: [OverseerDeathEvent]
  , ese_droneDeaths :: [DroneDeathEvent]
  , ese_overseerHits :: [OverseerHitEvent]
  , ese_collisions :: [CollisionDamage]
  , ese_neutralCollisions :: [NeutralCollision]
  , ese_dashCasts :: [DashCastEvent]
  , ese_psiStormCasts :: [PsiStormCastEvent]
  , ese_psiStormEffects :: [PsiStormEffect]
  , ese_flagEvents :: FlagEvents
  , ese_flagDropEvents :: [FlagDropEvent]
  , ese_scoreIncrement :: TeamScore
  , ese_botViews :: WIM.IntMap BotID BotView
  }

data OverseerDeathEvent = OverseerDeathEvent
  { ode_time :: Time
  , ode_pos :: Fixed2
  , ode_vel :: Fixed2
  , ode_radius :: Fixed
  , ode_owner :: ActorID
  , ode_owner_team :: Team
  , ode_killer :: Maybe ActorID
  , ode_entityID :: Int
  }

data DroneDeathEvent = DroneDeathEvent
  { dde_time :: Time
  , dde_eid :: Int
  -- ^ Weakened `EntityID`
  , dde_radius :: Fixed
  , dde_pos :: Fixed2
  , dde_vel :: Fixed2
  , dde_team :: Team
  , dde_recentDamage :: Fixed
  }

-- | An event for when an overseer gets hit, for the purposes of tracking
-- player stats
data OverseerHitEvent = OverseerHitEvent
  { ohe_attacker :: Maybe ActorID
  , ohe_defender :: ActorID
  , ohe_hitType :: HitType
  , ohe_damage :: Fixed
  }

data HitType = DroneHit | BodyHit | PsiStormHit

data FlagDropEvent = FlagDropEvent
  { fde_pos :: Fixed2
  , fde_vel :: Fixed2
  }

-- | Advance the simulation by one step. This function runs the core game logic.
stepEntities ::
  GameParams ->
  EntityStore s ->
  Gen s ->
  Time ->
  WIM.IntMap ActorID Player ->
  GamePhase ->
  ST s EntityStepEvents
stepEntities params (EntityStore store) gen time players gamePhase =
  Core.finallyRemoveEntities params time store $ \markForRemoval -> do
    Core.respawnOverseers params gen store time gamePhase
    spawnDrones params store gen time

    castEvents <-
      runPlayerCasts
        (gp_cast params)
        store
        time
        (WIM.map (player_prevControls &&& player_controls) players)

    playerStatuses <- Core.getPlayerStatuses store
    let playersAndStatuses =
          WIM.merge
            (IntMap.mapMissing (\_k _x -> error errMsg))
            (IntMap.mapMissing (\_k _x -> error errMsg))
            (IntMap.zipWithMatched (\_k a b -> (a, b)))
            players
            playerStatuses
         where
          errMsg = "Error: stepEntities: Inconsistency in player data"
        players' = WIM.map (first player_controls) playersAndStatuses

    -- TODO: merge with `FlagEvents`
    flagDropEvents <- runDropFlags params store time players'

    let Core.Stores{..} = Core.dataStores store
        readStore_team = readOnly store_team
        readStore_playerEntity = readOnly store_actorEntity
        readStore_combat = readCombatStore store_combat
        readStore_dynamics = readDynamicsStore store_dynamics

    collisionEvents <-
      stepDynamics
        params
        time
        players'
        readStore_team
        readStore_playerEntity
        store_masterMinions
        readStore_combat
        store_psiStorm
        store_dynamics

    let shouldSmiteOverseers =
          case gamePhase of
            GamePhase_Game{} -> DoNotSmiteOverseers
            GamePhase_Intermission intermissionStart _ _ _ ->
              if time `diffTime` intermissionStart > Ticks (C.tickRate_hz * 6)
                then SmiteOverseers
                else DoNotSmiteOverseers

    combatEvents <-
      runCombat
        gen
        store_combat
        readStore_playerEntity
        (gp_combat params)
        time
        shouldSmiteOverseers
        (ce_collision collisionEvents)
        (ce_psiStorm collisionEvents)
        (ce_spawn collisionEvents)

    (droneDeathEvents, overseerDeathEvents) <-
      -- deduplicate drone and overseer death events by EntityID
      bimap
        (IntMap.elems . IntMap.fromList . map (\dde -> (dde_eid dde, dde)))
        (IntMap.elems . IntMap.fromList . map (\val@(overseerID, _) -> (getIndex overseerID, val)))
        . partitionEithers
        . concat
        <$> mapM
          ( lookupDeaths
              readStore_dynamics
              readStore_combat
              store_masterMinions
              readStore_playerEntity
              readStore_team
              time
          )
          (ce_deaths combatEvents)

    overseerCollisionHits <-
      concat
        <$> mapM
          (lookupCollisionDamage readStore_playerEntity)
          (ce_collisions combatEvents)
    overseerPsiStormHits <-
      catMaybes
        <$> mapM
          (lookupPsiStormDamage readStore_playerEntity)
          (ce_psiStormDamage combatEvents)
    let overseerHitEvents = overseerCollisionHits ++ overseerPsiStormHits

    flagEvents <-
      runFlags' params store time markForRemoval (ce_flag collisionEvents)

    -- Remove entities killed in combat
    for_ (ce_deaths combatEvents) $ markForRemoval . subEntityID . killed_eid

    -- Remove expired transitory entities
    getExpiredEntities store_transitory time
      >>= traverse_ (markForRemoval . subEntityID)

    -- Create overseer death explosions
    for_ overseerDeathEvents $ \(overseerID, _) -> do
      pos <-
        dyn_pos <$> Core.sureLookup (readOnly store_dynamics) overseerID
      Core.addExplosion (gp_dynamics params) store time pos

    -- Bot logic
    flags <- getFlags (EntityStore store)
    botViews <- do
      let playerControlsStatuses =
            WIM.intersectionWith
              (,)
              (WIM.map player_controls players)
              playerStatuses
      makeBotViews
        (readDynamicsStore store_dynamics)
        (readCombatStore store_combat)
        store_masterMinions
        (readOnly store_team)
        (readOnly store_actorEntity)
        (readFlagStore store_flag)
        time
        flags
        playerControlsStatuses
        (ce_botView collisionEvents)

    -- Partition cast events for output
    let (droneDashes, psiStormCasts) =
          partitionEithers $
            map
              ( \case
                  DashCastEvent_ dash -> Left dash
                  PsiStormCastEvent_ psiStorm -> Right psiStorm
              )
              castEvents

    pure
      EntityStepEvents
        { ese_overseerDeaths = map snd overseerDeathEvents
        , ese_droneDeaths = droneDeathEvents
        , ese_overseerHits = overseerHitEvents
        , ese_collisions = map Pair.snd $ ce_collisions combatEvents
        , ese_neutralCollisions = ce_neutralCollision collisionEvents
        , ese_dashCasts = droneDashes
        , ese_psiStormCasts = psiStormCasts
        , ese_psiStormEffects = ce_psiStormEffects combatEvents
        , ese_flagEvents = flagEvents
        , ese_flagDropEvents = flagDropEvents
        , ese_scoreIncrement =
            foldl' (flip incrementTeamScore) mempty $
              map (\(FlagCapture{flagCapture_team}) -> flagCapture_team) $
                flagEvents_captures flagEvents
        , ese_botViews = botViews
        }

--------------------------------------------------------------------------------

spawnDrones :: GameParams -> Core.EntityStore s -> Gen s -> Time -> ST s ()
spawnDrones params store gen time = do
  let checkInterval = gp_droneRespawnCheckInterval $ gp_combat params
  when (time `atMultipleOf` checkInterval) $ do
    let Core.Stores{store_masterMinions} = Core.dataStores store
    recruitingMasterIDs <-
      recruitingMasters store_masterMinions (gp_combat params) time
    let invulnTicks = Ticks (-1)
        mDeathTime = Nothing
    for_ recruitingMasterIDs $ \overseerID ->
      Core.addDrone
        params
        time
        invulnTicks
        store
        gen
        overseerID
        mDeathTime
        Core.SingleDroneSpawn

--------------------------------------------------------------------------------

runDropFlags ::
  (Traversable f) =>
  GameParams ->
  Core.EntityStore s ->
  Time ->
  f (PlayerControls, PlayerStatus) ->
  ST s [FlagDropEvent]
runDropFlags params store time players
  | gp_enableFlagDrops (gp_flags params) =
      fmap catMaybes $
        for (toList players) $ \(controls, playerStatus) ->
          case ps_overseerStatus playerStatus of
            OverseerDead{} ->
              pure Nothing
            OverseerAlive overseerID _ ->
              if controls ^. to control_buttons . control_flag == FlagDrop
                then do
                  let Core.Stores{store_dynamics, store_flag, store_team} =
                        Core.dataStores store
                  tryDropOverseerFlag (gp_flags params) store_flag time overseerID
                    >>= \case
                      HadNoFlag -> pure Nothing
                      FlagDropped -> do
                        flagTeam <- oppositeTeam <$> Core.sureLookup store_team overseerID
                        (pos, vel) <- readPosVel store_dynamics overseerID
                        Core.addFlag params store time flagTeam pos
                        pure $
                          Just
                            FlagDropEvent
                              { fde_pos = pos
                              , fde_vel = vel
                              }
                else pure Nothing
  | otherwise =
      pure []

--------------------------------------------------------------------------------

-- | Note: A drone death event may be created by either the drone itself or its
-- overseer, so we must (elsewhere) account for the possibility of duplicates.
lookupDeaths ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MasterMinionStore s ->
  ReadOnly (ActorEntityStore s) ->
  ReadOnly (TeamStore s) ->
  Time ->
  CombatDeathEvent ->
  ST s [Either DroneDeathEvent (EntityID 'Overseer, OverseerDeathEvent)]
lookupDeaths
  dynamicsStore
  combatStore
  masterMinionStore
  playerStore
  teamStore
  time
  combatEvent =
    case refineEntityID (killed_eid combatEvent) of
      OverseerID overseerID -> do
        team <- Core.sureLookup teamStore overseerID
        overseerDeath <- do
          pid <- Core.sureLookup playerStore overseerID
          (pos, vel, radius) <-
            readPosVelRadius dynamicsStore overseerID
          pure
            OverseerDeathEvent
              { ode_time = time
              , ode_pos = pos
              , ode_vel = vel
              , ode_radius = radius
              , ode_owner = pid
              , ode_owner_team = team
              , ode_killer = killer_mPid combatEvent
              , ode_entityID = getIndex overseerID
              }
        droneDeaths <- do
          droneMinionIDs <-
            WIntSet.toList <$> getMinions masterMinionStore overseerID
          for droneMinionIDs $ \droneID -> do
            posVelRadius <- readPosVelRadius dynamicsStore droneID
            combatComp <- Core.sureLookup combatStore droneID
            pure $
              makeDroneDeath time team droneID posVelRadius combatComp
        pure $ (:) (Right (overseerID, overseerDeath)) (map Left droneDeaths)
      DroneID droneID -> do
        team <- Core.sureLookup teamStore droneID
        posVelRadius <- readPosVelRadius dynamicsStore droneID
        combatComp <- Core.sureLookup combatStore droneID
        pure $
          singleton $
            Left $
              makeDroneDeath time team droneID posVelRadius combatComp
   where
    singleton x = [x]

makeDroneDeath ::
  Time ->
  Team ->
  EntityID 'Drone ->
  (Fixed2, Fixed2, Fixed) ->
  CombatComponent ->
  DroneDeathEvent
makeDroneDeath time team eid (pos, vel, radius) combatComp =
  DroneDeathEvent
    { dde_time = time
    , dde_eid = getIndex eid
    , dde_radius = radius
    , dde_pos = pos
    , dde_vel = vel
    , dde_team = team
    , dde_recentDamage = recentDamage time combatComp
    }

lookupPsiStormDamage ::
  ReadOnly (ActorEntityStore s) ->
  OverseerPsiStormDamage ->
  ST s (Maybe OverseerHitEvent)
lookupPsiStormDamage playerStore psiStormDamage =
  Core.sureLookup playerStore (opsd_overseerID psiStormDamage) <&> \pid ->
    Just $
      OverseerHitEvent
        { ohe_attacker = opsd_mPsiStormActorID psiStormDamage
        , ohe_defender = pid
        , ohe_hitType = PsiStormHit
        , ohe_damage = opsd_damage psiStormDamage
        }

lookupCollisionDamage ::
  ReadOnly (ActorEntityStore s) ->
  Pair (EntityIDPair 'CombatEntity) CollisionDamage ->
  ST s [OverseerHitEvent]
lookupCollisionDamage actorComps collision = do
  let combatIDPair :!: collisionDamage = collision
      (combatID1, combatID2) = getEntityIDPair combatIDPair
      dmg = cd_damage collisionDamage
  case refineEntityID combatID1 of
    OverseerID overseerID1 -> do
      case refineEntityID combatID2 of
        OverseerID overseerID2 -> do
          pid1 <- Core.sureLookup actorComps overseerID1
          pid2 <- Core.sureLookup actorComps overseerID2
          pure
            [ OverseerHitEvent
                { ohe_attacker = Just pid1
                , ohe_defender = pid2
                , ohe_hitType = BodyHit
                , ohe_damage = dmg
                }
            , OverseerHitEvent
                { ohe_attacker = Just pid2
                , ohe_defender = pid1
                , ohe_hitType = BodyHit
                , ohe_damage = dmg
                }
            ]
        DroneID droneID2 -> do
          (: []) <$> makeDroneHit actorComps dmg droneID2 overseerID1
    DroneID droneID1 -> do
      case refineEntityID combatID2 of
        OverseerID overseerID2 ->
          (: []) <$> makeDroneHit actorComps dmg droneID1 overseerID2
        DroneID _droneID2 ->
          pure []

makeDroneHit ::
  ReadOnly (ActorEntityStore s) ->
  Fixed ->
  EntityID 'Drone ->
  EntityID 'Overseer ->
  ST s OverseerHitEvent
makeDroneHit actorComps dmg droneID overseerID = do
  attackerPID <- Core.lookup actorComps droneID
  defenderPID <- Core.sureLookup actorComps overseerID
  pure
    OverseerHitEvent
      { ohe_attacker = attackerPID
      , ohe_defender = defenderPID
      , ohe_hitType = DroneHit
      , ohe_damage = dmg
      }

--------------------------------------------------------------------------------

runFlags' ::
  GameParams ->
  Core.EntityStore s ->
  Time ->
  (SEntityID 'AnyEntity -> ST s ()) ->
  [FlagCollisionEvent] ->
  ST s FlagEvents
runFlags' params store time markForRemoval collisions = do
  let Core.Stores{store_flag, store_dynamics, store_combat, store_team, store_actorEntity} =
        Core.dataStores store
      store_dynamics' = readDynamicsStore store_dynamics
      store_team' = readOnly store_team
      store_playerEntity' = readOnly store_actorEntity

  (flagEvents, partitionedFlags) <-
    runFlags
      store_flag
      store_dynamics'
      store_team'
      store_playerEntity'
      (gp_flags params)
      time
      collisions

  let PartitionedFlags{pf_flagCarriers, pf_emptySourceFlags} = partitionedFlags

  -- Slow flag carriers
  for_ pf_flagCarriers $ \overseerID -> do
    modifyVelocityWithMass store_dynamics overseerID $ \mass vel ->
      let speed2 = Fixed.normSq vel
          factor = 1.0 - speed2 * gp_flagDrag (gp_dynamics params) / mass
      in  Fixed.map (* factor) vel

  -- Poison flag carriers
  let shouldPoison =
        gp_drainFlagCarrierHealth (gp_combat params)
          && time
          `atMultipleOf` Ticks C.tickRate_hz
  when shouldPoison $
    for_ pf_flagCarriers $ \overseerID -> do
      applyPoisonDamage store_combat time overseerID $
        gp_drainFlagCarrierHealthRate (gp_combat params)

  -- Remove empty source flags
  traverse_ (markForRemoval . subEntityID) pf_emptySourceFlags

  -- Spawn base flags from captures and reclamations
  for_ (flagEvents_captures flagEvents) $ \(FlagCapture{flagCapture_team}) ->
    spawnBaseFlag' params store (oppositeTeam flagCapture_team)
  for_ (flagEvents_recoveries flagEvents) $ \(FlagRecovery team _ _) ->
    spawnBaseFlag' params store team

  pure flagEvents

--------------------------------------------------------------------------------
-- Export entity data for rendering

getRenderingComponents ::
  GameParams ->
  EntityStore s ->
  Time ->
  Maybe ActorID ->
  WIM.IntMap ActorID Player ->
  ST s RenderingComponents
getRenderingComponents params (EntityStore store) time mActorID playerMap = do
  mViewPos <- join <$> traverse (getPlayerPosition store) mActorID
  playerStatusMap <- Core.getPlayerStatuses store
  ES.getRenderingComponents params (Core.dataStores store) time mViewPos $
    WIM.merge
      (IntMap.mapMissing (\_k _x -> error errMsg))
      (IntMap.mapMissing (\_k _x -> error errMsg))
      (IntMap.zipWithMatched (\_k a b -> (a, b)))
      playerMap
      playerStatusMap
 where
  errMsg = "Error: getRenderingComponents: Inconsistency in player data"

getPlayerPosition :: Core.EntityStore s -> ActorID -> ST s (Maybe Fixed2)
getPlayerPosition store actorID = do
  let Core.Stores{store_dynamics} = Core.dataStores store
  Core.getPlayerStatus store actorID >>= \case
    Nothing -> pure Nothing
    Just playerStatus ->
      fmap Just $
        case ps_overseerStatus playerStatus of
          OverseerDead _ _ deathPos ->
            pure deathPos
          OverseerAlive overseerID _ ->
            readPos store_dynamics overseerID

