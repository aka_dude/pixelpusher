module Pixelpusher.Game.GamePhase (
  GamePhase (..),
  inGame,
  inIntermission,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamScore
import Pixelpusher.Game.Time

-- | The current phase of the game
data GamePhase
  = GamePhase_Game
      !Time -- round start
      !TeamScore
  | GamePhase_Intermission
      !Time -- round end
      !Time -- intermission end
      !Team -- winning team
      !TeamScore -- final score
  deriving stock (Generic)
  deriving anyclass (Serialize)

inGame :: GamePhase -> Bool
inGame = \case
  GamePhase_Game{} -> True
  GamePhase_Intermission{} -> False

inIntermission :: GamePhase -> Bool
inIntermission = \case
  GamePhase_Game{} -> False
  GamePhase_Intermission{} -> True
