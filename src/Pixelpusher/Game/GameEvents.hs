module Pixelpusher.Game.GameEvents (
  GameEvents (..),
  GamePhaseTransition (..),
) where

import Pixelpusher.Game.CollisionEvents (NeutralCollision)
import Pixelpusher.Game.CombatEvents (CollisionDamage, PsiStormEffect)
import Pixelpusher.Game.EntityStore (
  DashCastEvent,
  OverseerDeathEvent,
  PsiStormCastEvent,
 )
import Pixelpusher.Game.FlagEvents (FlagCapture, FlagPickup, FlagRecovery)

data GameEvents = GameEvents
  { ge_gamePhaseTransitions :: [GamePhaseTransition]
  -- ^ For the server (game manager)
  , ge_collisions :: [CollisionDamage]
  , ge_neutralCollisions :: [NeutralCollision]
  , ge_overseerDeaths :: [OverseerDeathEvent]
  , ge_dashes :: [DashCastEvent]
  , ge_psiStormCasts :: [PsiStormCastEvent]
  , ge_psiStormEffects :: [PsiStormEffect]
  , ge_flagCaptures :: [FlagCapture]
  , ge_flagPickups :: [FlagPickup]
  , ge_flagRecoveries :: [FlagRecovery]
  }

instance Semigroup GameEvents where
  ge1 <> ge2 =
    GameEvents
      { ge_gamePhaseTransitions =
          ge_gamePhaseTransitions ge1 <> ge_gamePhaseTransitions ge2
      , ge_collisions =
          ge_collisions ge1 <> ge_collisions ge2
      , ge_neutralCollisions =
          ge_neutralCollisions ge1 <> ge_neutralCollisions ge2
      , ge_overseerDeaths =
          ge_overseerDeaths ge1 <> ge_overseerDeaths ge2
      , ge_dashes =
          ge_dashes ge1 <> ge_dashes ge2
      , ge_psiStormCasts =
          ge_psiStormCasts ge1 <> ge_psiStormCasts ge2
      , ge_psiStormEffects =
          ge_psiStormEffects ge1 <> ge_psiStormEffects ge2
      , ge_flagCaptures =
          ge_flagCaptures ge1 <> ge_flagCaptures ge2
      , ge_flagPickups =
          ge_flagPickups ge1 <> ge_flagPickups ge2
      , ge_flagRecoveries =
          ge_flagRecoveries ge1 <> ge_flagRecoveries ge2
      }

instance Monoid GameEvents where
  mempty = GameEvents [] [] [] [] [] [] [] [] [] []

data GamePhaseTransition
  = -- | The match has ended
    MatchEnd
  | -- | Timing for match end sfx (hack)
    PlayMatchEndSfx
  | -- | The game intermission is about to end
    IntermissionEnding
  | -- | The game is restarting now
    GameRestart
  deriving stock (Eq)
