{-# LANGUAGE RecordWildCards #-}

-- | Rendering specifications for individual game entities
module Pixelpusher.Game.RenderingComponent (
  RenderingComponent (..),
  EntityRendering (..),
  OverseerRendering (..),
  DroneRendering (..),
  SoftObstacleRendering (..),
  ControlPointRendering (..),
  TeamBaseRendering (..),
  FlagRendering (..),
  BaseFlagRendering (..),
  FlagCaptureAreaRendering (..),
  PsiStormRendering (..),
  SpawnAreaRendering (..),
  PlaceholderRendering (..),
  RenderingComponents (..),
  emptyRenderingComponents,
  partitionRenderingComponents,
  filterVisible,
  isInView,
  entityViewPredicate,
) where

import Data.Functor (($>))

import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.FlagComponent (FlagConditionStatus)
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

--------------------------------------------------------------------------------

data RenderingComponent a = RenderingComponent
  { rendering_entityRendering :: a
  , rendering_pos :: Float2
  , rendering_radius :: Float
  }
  deriving stock (Functor)

data EntityRendering
  = ER_Overseer OverseerRendering
  | ER_Drone DroneRendering
  | ER_SoftObstacle SoftObstacleRendering
  | ER_TeamBase TeamBaseRendering
  | ER_Flag FlagRendering
  | ER_BaseFlag BaseFlagRendering
  | ER_FlagCaptureArea FlagCaptureAreaRendering
  | ER_PsiStorm PsiStormRendering
  | ER_SpawnArea SpawnAreaRendering
  | ER_Placeholder PlaceholderRendering
  | ER_NoVisual

data OverseerRendering = OverseerRendering
  { or_team :: Team
  , or_color :: PlayerColor
  , or_healthFraction :: Float
  , or_recentDamage :: Float
  , or_actorID :: ActorID
  , or_playerName :: PlayerName
  , or_prevPos1 :: Float2
  , or_prevPos2 :: Float2
  , or_prevPos3 :: Float2
  , or_prevPos4 :: Float2
  , or_prevPos5 :: Float2
  , or_prevPos6 :: Float2
  , or_hasFlag :: Bool
  , or_psiStormCooldownUntil :: Time
  , or_droneDashCooldownUntil1 :: Time
  , or_droneDashCooldownUntil2 :: Time
  , or_isInvulnerable :: Bool
  , or_entityID :: Int
  , or_ticksSinceDash :: Maybe Ticks
  }

data DroneRendering = DroneRendering
  { dr_color :: Either Team PlayerColor
  , dr_healthFraction :: Float
  , dr_recentDamage :: Float
  , dr_actorID :: Maybe ActorID
  , dr_prevPos1 :: Float2
  , dr_prevPos2 :: Float2
  , dr_prevPos3 :: Float2
  , dr_prevPos4 :: Float2
  , dr_prevPos5 :: Float2
  , dr_prevPos6 :: Float2
  , dr_isInvulnerable :: Bool
  , dr_entityID :: Int
  , dr_team :: Team
  , dr_ticksSinceDash :: Maybe (Ticks, Float) -- Intensity
  }

newtype SoftObstacleRendering = SoftObstacleRendering Float3

data ControlPointRendering
  = ControlPointRendering_Neutral
  | ControlPointRendering_Tentative
      !Team
      !Float -- Capture progress (from 0 to 1)
      !Ticks -- Ticks since transition
  | ControlPointRendering_Captured
      !Team
      !Float -- Capture progress (from 0 to 1)
      !Ticks -- Ticks since transition

data TeamBaseRendering = TeamBaseRendering
  { tbr_team :: Team
  , tbr_flagDepositPending :: FlagConditionStatus
  }

newtype BaseFlagRendering = BaseFlagRendering {unBaseFlagRendering :: Team}

newtype FlagRendering = FlagRendering
  { fr_team :: Team
  }

data FlagCaptureAreaRendering = FlagCaptureAreaRendering
  { fcar_team :: Team
  , fcar_progress :: Float
  , fcar_recoveringFlag :: FlagConditionStatus
  }

data PsiStormRendering = PsiStormRendering
  { psr_endTime :: Time
  , psr_color :: Maybe PlayerColor
  }

newtype SpawnAreaRendering = SpawnAreaRendering
  { tsr_team :: Team
  }

newtype PlaceholderRendering = PlaceholderRendering {unPlaceholderRendering :: Float4}

--------------------------------------------------------------------------------

data RenderingComponents = RenderingComponents
  { rcs_overseers :: [RenderingComponent OverseerRendering]
  , rcs_drones :: [RenderingComponent DroneRendering]
  , rcs_softObstacles :: [RenderingComponent SoftObstacleRendering]
  , rcs_teamBases :: [RenderingComponent TeamBaseRendering]
  , rcs_flags :: [RenderingComponent FlagRendering]
  , rcs_baseFlags :: [RenderingComponent BaseFlagRendering]
  , rcs_flagCaptureAreas :: [RenderingComponent FlagCaptureAreaRendering]
  , rcs_psiStorms :: [RenderingComponent PsiStormRendering]
  , rcs_spawnAreas :: [RenderingComponent SpawnAreaRendering]
  , rcs_placeholders :: [RenderingComponent PlaceholderRendering]
  }

emptyRenderingComponents :: RenderingComponents
emptyRenderingComponents = RenderingComponents [] [] [] [] [] [] [] [] [] []

partitionRenderingComponents ::
  [RenderingComponent EntityRendering] -> RenderingComponents
partitionRenderingComponents = foldr select emptyRenderingComponents

select ::
  RenderingComponent EntityRendering ->
  RenderingComponents ->
  RenderingComponents
select rc rcs = case rendering_entityRendering rc of
  ER_Overseer x ->
    rcs{rcs_overseers = (rc $> x) : rcs_overseers rcs}
  ER_Drone x ->
    rcs{rcs_drones = (rc $> x) : rcs_drones rcs}
  ER_SoftObstacle x ->
    rcs{rcs_softObstacles = (rc $> x) : rcs_softObstacles rcs}
  ER_TeamBase x ->
    rcs{rcs_teamBases = (rc $> x) : rcs_teamBases rcs}
  ER_Flag x ->
    rcs{rcs_flags = (rc $> x) : rcs_flags rcs}
  ER_BaseFlag x ->
    rcs{rcs_baseFlags = (rc $> x) : rcs_baseFlags rcs}
  ER_FlagCaptureArea x ->
    rcs{rcs_flagCaptureAreas = (rc $> x) : rcs_flagCaptureAreas rcs}
  ER_PsiStorm x ->
    rcs{rcs_psiStorms = (rc $> x) : rcs_psiStorms rcs}
  ER_SpawnArea x ->
    rcs{rcs_spawnAreas = (rc $> x) : rcs_spawnAreas rcs}
  ER_Placeholder x ->
    rcs{rcs_placeholders = (rc $> x) : rcs_placeholders rcs}
  ER_NoVisual -> rcs -- noop

--------------------------------------------------------------------------------
-- Filtering by view radius (optimization)

-- | For filtering entities by overseer position before exporting their
-- rendering data.
entityViewPredicate :: SEntityID 'DynamicsEntity -> Bool
entityViewPredicate entityID =
  case refineEntityID entityID of
    OverseerID _ -> True -- expose overseers for minimap
    TeamBaseID _ -> True -- expose team bases for minimap
    FlagID _ -> True -- for flag capture area
    _ -> False

-- | For filtering entity rendering data when rendering the game world.
--
-- Only filters that which is not filtered by the entity filter.
filterVisible ::
  Float -> Float2 -> RenderingComponents -> RenderingComponents
filterVisible viewRadius viewCenter RenderingComponents{..} =
  RenderingComponents
    { rcs_overseers = isInCircularView' rcs_overseers -- overseers
    , rcs_drones = rcs_drones
    , rcs_softObstacles = rcs_softObstacles
    , rcs_teamBases = isInCircularView' rcs_teamBases -- team bases
    , rcs_flags = isInCircularView' rcs_flags -- flags
    , rcs_baseFlags = rcs_baseFlags
    , rcs_flagCaptureAreas = rcs_flagCaptureAreas
    , rcs_psiStorms = rcs_psiStorms
    , rcs_spawnAreas = rcs_spawnAreas
    , rcs_placeholders = rcs_placeholders
    }
 where
  isInCircularView' = filter (isInView viewRadius viewCenter)

isInView :: Float -> Float2 -> RenderingComponent a -> Bool
isInView viewRadius overseerPos renderingComp =
  let dist = Float.distance2 overseerPos (rendering_pos renderingComp)
  in  dist - rendering_radius renderingComp <= viewRadius
