module Pixelpusher.Game.Dynamics.Collisions.Entities (
  CollisionEntity (..),
  CollisionEntityPair (..),
) where

import Data.Hashable (Hashable)
import GHC.Generics (Generic)
import Pixelpusher.Game.BotID
import Pixelpusher.Store.EntityID

data CollisionEntity
  = CE_DynEntity {-# UNPACK #-} !(SEntityID 'DynamicsEntity)
  | -- | Area around non-base flags
    CE_FlagArea {-# UNPACK #-} !(EntityID 'Flag)
  | CE_BotViewArea {-# UNPACK #-} !BotID
  | CE_PsiStormDamage {-# UNPACK #-} !(EntityID 'PsiStorm)
  deriving stock (Eq, Generic)
  deriving anyclass (Hashable)

-- | Just a strict, monomorphic pair
data CollisionEntityPair
  = CollisionEntityPair !CollisionEntity !CollisionEntity
  deriving stock (Eq, Generic)
  deriving anyclass (Hashable)
