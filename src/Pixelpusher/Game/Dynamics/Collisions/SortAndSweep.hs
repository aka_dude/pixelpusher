{-# LANGUAGE RecordWildCards #-}

-- | Broad-phase collision detection on circular entities
module Pixelpusher.Game.Dynamics.Collisions.SortAndSweep (
  SortAndSweepComponent (..),
  sortAndSweep,
) where

import Control.Monad.ST (runST)
import Data.DList qualified as DL
import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict qualified as HashMap
import Data.Vector qualified as V
import Data.Word (Word16)
import Lens.Micro.Platform.Custom (view, _1)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Radix qualified as Radix
import Pixelpusher.Game.Dynamics.Collisions.Entities

--------------------------------------------------------------------------------

data SortAndSweepComponent = SortAndSweepComponent
  { ssc_entity :: CollisionEntity
  , ssc_radius :: {-# UNPACK #-} !Fixed
  , ssc_pos :: {-# UNPACK #-} !Fixed2
  }

-- | Exact collision detection of circles
areOverlapping :: SortAndSweepComponent -> SortAndSweepComponent -> Bool
areOverlapping comp1 comp2 =
  let SortAndSweepComponent _ r1 pos1 = comp1
      SortAndSweepComponent _ r2 pos2 = comp2
      radius = r1 + r2
      displacement = pos1 - pos2
  in  Fixed.normLeq displacement radius

--------------------------------------------------------------------------------

-- | Returns exact collision results
sortAndSweep ::
  Fixed2 -> [SortAndSweepComponent] -> [CollisionEntityPair]
sortAndSweep worldBounds components =
  sweep $
    radixSort $
      catPairs $
        map (axisAlignedBounds discr (view _1)) components
 where
  discr = makeDiscretization worldBounds

catPairs :: [(a, a)] -> [a]
catPairs = foldr (\(x, y) xs -> x : y : xs) []

radixSort :: (Radix.Radix a) => [a] -> V.Vector a
radixSort xs = runST $ do
  vm <- V.unsafeThaw $ V.fromList xs
  Radix.sort vm
  V.unsafeFreeze vm

--------------------------------------------------------------------------------
-- Discretizing game-world positions to `Word16` for radix sort

data Discretization = Discretization
  { discr_lbound :: {-# UNPACK #-} !Fixed
  , discr_ubound :: {-# UNPACK #-} !Fixed
  , discr_scale :: {-# UNPACK #-} !Fixed
  }

makeDiscretization :: Fixed2 -> Discretization
makeDiscretization worldBounds =
  let (Fixed2 lbound ubound) = worldBounds
      bufferSize = 64
      bufferedWorldLength = ubound - lbound + 2 * bufferSize
      scale = 4 * (16384 / bufferedWorldLength) -- avoiding overflow
      -- Set bounds conservatively to avoid errors due to rounding near bounds
      bufferedLowerBound = lbound - 0.5 * bufferSize
      bufferedUpperBound = ubound + 0.5 * bufferSize
  in  Discretization
        { discr_lbound = bufferedLowerBound
        , discr_ubound = bufferedUpperBound
        , discr_scale = scale
        }

discretize :: Discretization -> Fixed -> Word16
discretize Discretization{..} x =
  let bounded = min discr_ubound $ max discr_lbound x
  in  floor $ discr_scale * (bounded - discr_lbound)

--------------------------------------------------------------------------------
-- Axis-aligned entity bounds

data Boundary = Boundary
  { boundary_position :: {-# UNPACK #-} !Word16
  , boundary_isMinBound :: Bool
  , boundary_component :: SortAndSweepComponent
  }

axisAlignedBounds ::
  Discretization ->
  (Fixed2 -> Fixed) ->
  SortAndSweepComponent ->
  (Boundary, Boundary)
axisAlignedBounds discr projection component =
  let (SortAndSweepComponent _ radius pos) = component
      proj = projection pos
      lb =
        Boundary
          { boundary_position = discretize discr (proj - radius)
          , boundary_isMinBound = True
          , boundary_component = component
          }
      ub =
        Boundary
          { boundary_position = discretize discr (proj + radius)
          , boundary_isMinBound = False
          , boundary_component = component
          }
  in  (lb, ub)

instance Radix.Radix Boundary where
  passes = Radix.passes . boundary_position
  size = Radix.size . boundary_position
  radix n = Radix.radix n . boundary_position

--------------------------------------------------------------------------------
-- "Sweeping" through sorted boundaries to find (potential) collisions

data SweepState = SweepState
  { sweep_activeComponents :: HashMap CollisionEntity SortAndSweepComponent
  -- ^ Using `CollisionEntity` as a key assumes that `CollisionEntity`s
  -- are unique in all collections passed to functions of this module
  , sweep_entityPairs :: DL.DList CollisionEntityPair
  }

initialSweepState :: SweepState
initialSweepState =
  SweepState
    { sweep_activeComponents = HashMap.empty
    , sweep_entityPairs = DL.empty
    }

-- | Returns exact collision results
sweep :: V.Vector Boundary -> [CollisionEntityPair]
sweep = DL.toList . sweep_entityPairs . V.foldl f initialSweepState
 where
  f :: SweepState -> Boundary -> SweepState
  f (SweepState activeComponents entityPairs) boundary =
    let component = boundary_component boundary
    in  if boundary_isMinBound boundary
          then
            SweepState
              { sweep_activeComponents =
                  HashMap.insert (ssc_entity component) component activeComponents
              , sweep_entityPairs =
                  DL.append entityPairs $
                    DL.fromList $
                      map (CollisionEntityPair (ssc_entity component) . ssc_entity) $
                        filter (areOverlapping component) $
                          HashMap.elems activeComponents
              }
          else
            SweepState
              { sweep_activeComponents =
                  HashMap.delete (ssc_entity component) activeComponents
              , sweep_entityPairs = entityPairs
              }
