-- | Shared types and helper functions for the dynamics system
module Pixelpusher.Game.Dynamics.Types (
  DynamicsStore',
  projected,
  flipPositiveVelocity,
  repulsionImpulse,
) where

import Lens.Micro.Platform.Custom (Lens')

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Store.DenseStore (DenseStore)
import Pixelpusher.Store.EntityID

type DynamicsStore' s =
  DenseStore
    s
    (SEntityID 'DynamicsEntity)
    (SEntityID 'DynamicsEntity)
    DynamicsComponentVec

-- | Focus on the projection of a vector along a direction.
projected :: Fixed2 -> Lens' Fixed2 Fixed
projected direction f v = unProj <$> f proj_v
 where
  unitDirection = Fixed.normalize direction
  proj_v = Fixed.dot unitDirection v
  unProj proj = Fixed.map (* (proj - proj_v)) unitDirection + v

flipPositiveVelocity :: Fixed -> Fixed
flipPositiveVelocity v
  | v > 0 = coeffRestitution * negate v
  | otherwise = v
 where
  coeffRestitution = 0.9

repulsionImpulse :: Fixed -> Fixed -> Fixed
repulsionImpulse overlap v
  | v < maxVel = v
  | otherwise = max maxVel $ v - 0.05 * overlap
 where
  maxVel = -0.1
