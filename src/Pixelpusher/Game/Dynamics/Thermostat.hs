-- | Regulation of soft obstacles
module Pixelpusher.Game.Dynamics.Thermostat (
  driftingSoftObstacleThermostat,
) where

import Prelude hiding (lookup)

import Control.Monad (when)
import Control.Monad.ST
import Data.Foldable (for_)
import Data.List (foldl', tails)

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Dynamics.Types (DynamicsStore')
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

--------------------------------------------------------------------------------

driftingSoftObstacleThermostat ::
  DynamicsParams -> Time -> DynamicsStore' s -> ST s ()
driftingSoftObstacleThermostat params time dynComps =
  when (time `atMultipleOf` Ticks 61) $
    driftingSoftObstacleThermostat' params dynComps

-- | Regulate the total kinetic energy of the soft obstacles
driftingSoftObstacleThermostat' :: DynamicsParams -> DynamicsStore' s -> ST s ()
driftingSoftObstacleThermostat' params dynComps = do
  let isSoftObstacle dynID =
        case refineEntityID dynID of
          SoftObstacleID _ -> True
          _ -> False
  (eids, positions, velocities) <-
    unzip3
      <$> DS.toListFilteredWith_idx_2
        dynComps
        vec_dyn_pos
        vec_dyn_vel
        isSoftObstacle
        (,,)

  let kineticEnergy = (* 0.5) $ foldl' (+) 0 $ map Fixed.normSq velocities
      pairwisePositions = concatMap headZip $ tails positions
      potentialEnergy =
        (* (gp_softObstacleReplusion params * 0.5)) $
          foldl' (+) 0 $
            map (sq . (/ collisionRadius) . subtract collisionRadius . Fixed.norm) $
              filter (`Fixed.normLeq` collisionRadius) $
                map (uncurry subtract) pairwisePositions
      totalEnergy = kineticEnergy + potentialEnergy
      targetAvgEnergy = (* 0.5) $ sq $ gp_softObstacleAverageSpeed params
      targetTotalEnergy = targetAvgEnergy * fromIntegral (length positions)
      scale =
        if totalEnergy == 0 || targetTotalEnergy == 0
          then 1
          else sqrt $ targetTotalEnergy / totalEnergy

  -- Artificially limit speed (to prevent the movement soft obstacles from
  -- becoming too relevant in combat)
  for_ eids $ \eid -> do
    DS.sureModify_1_1 dynComps vec_dyn_pos vec_dyn_vel eid $ \pos vel ->
      let worldPotentialDeltaV = worldGradient params pos
          maxSpeed = 0.05
          scale' = if Fixed.normLeq vel maxSpeed then scale else scale * 0.95
      in  Fixed.map (* scale') (vel + worldPotentialDeltaV)
 where
  collisionRadius = 2 * gp_softObstacleRadius params

  headZip :: [a] -> [(a, a)]
  headZip [] = []
  headZip (x : xs) = map (x,) xs

  sq x = x * x

-- | Push soft obstacles towards the center of the world, since they are
-- usually pushed out towards the world boundary by overseers and drones.
worldGradient :: DynamicsParams -> Fixed2 -> Fixed2
worldGradient params pos =
  let (dist, dir) = Fixed.normAndNormalize pos
      distFrac = dist / gp_worldRadius params
      magnitude = 2.5e-4 * distFrac * gp_softObstacleWorldGradientFactor params
  in  Fixed.map (* magnitude) (-dir)
