-- | Independent movement of entities
module Pixelpusher.Game.Dynamics.Independent (
  stepIndependentKinematics,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Data.Maybe (fromMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Dynamics.Controls (
  DynamicsControls (..),
  zeroAccelerationControls,
 )
import Pixelpusher.Game.Dynamics.Types (
  flipPositiveVelocity,
  projected,
  repulsionImpulse,
 )
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core qualified as Store (
  ReadOnly,
  lookup,
  sureLookup,
 )

--------------------------------------------------------------------------------

-- ** Independent dynamics

-- | Run movements of entities that depend only the entity itself.
stepIndependentKinematics ::
  DynamicsParams ->
  WIM.IntMap ActorID DynamicsControls ->
  Store.ReadOnly (ActorEntityStore s) ->
  MasterMinionStore s ->
  SEntityID 'DynamicsEntity ->
  Fixed ->
  Fixed2 ->
  Fixed2 ->
  ST s (Fixed2, Fixed2)
stepIndependentKinematics
  params
  playerCtrls
  actorComps
  masterMinionComps
  dynID
  radius
  pos
  vel = case refineEntityID dynID of
    OverseerID overseerID -> do
      dynControls <-
        fromMaybe zeroAccelerationControls . (`WIM.lookup` playerCtrls)
          <$> Store.sureLookup actorComps overseerID
      pure $
        applyWorldBounds' $
          standardVelocityIntegration $
            overseerVelocityControl params dynControls
    DroneID droneID -> do
      dynCtrls <-
        lookupMaster masterMinionComps droneID >>= \case
          Nothing -> pure zeroAccelerationControls
          Just _ ->
            fromMaybe zeroAccelerationControls . ((`WIM.lookup` playerCtrls) =<<)
              <$> Store.lookup actorComps droneID
      pure $
        applyWorldBounds' $
          standardVelocityIntegration $
            droneVelocityControl params dynCtrls pos
    SoftObstacleID _ ->
      pure $ applyWorldBounds' $ standardVelocityIntegration id
    FlagID _ ->
      pure $
        applyWorldBounds' $
          standardVelocityIntegration flagVelocityControl
    TeamBaseID _ -> fixed
    PsiStormID _ -> fixed
    BaseFlagID _ -> fixed
    ExplosionID _ -> fixed
    SpawnAreaID _ -> fixed
   where
    -- Use the given velocity update function to obtain a new position and
    -- velocity from the current position and velocity.
    standardVelocityIntegration :: (Fixed2 -> Fixed2) -> (Fixed2, Fixed2)
    standardVelocityIntegration f_vel =
      let vel' = f_vel vel
          pos' = pos + Fixed.map (* 0.5) (vel + vel')
      in  (pos', vel')

    applyWorldBounds' :: (Fixed2, Fixed2) -> (Fixed2, Fixed2)
    applyWorldBounds' (pos', vel') =
      (,) pos' $ applyWorldBounds2 (gp_worldRadius params) radius pos' vel'

    fixed :: Applicative f => f (Fixed2, Fixed2)
    fixed = pure (pos, 0)

-- | Accelerate in the direction specified by the arrow keys. Apply drag.
overseerVelocityControl ::
  DynamicsParams -> DynamicsControls -> Fixed2 -> Fixed2
overseerVelocityControl params dynCtrls =
  (+ accel) . applyLinearDrag
 where
  accelMagnitude = gp_overseerAcceleration params
  accel = Fixed.map (* accelMagnitude) $ dynCtrl_arrowKeyDir dynCtrls
  overseerDrag = gp_overseerDrag params
  applyLinearDrag = Fixed.map (* (1 - overseerDrag))

-- | Accelerate towards/away from the position of the mouse cursor. Apply drag.
droneVelocityControl :: DynamicsParams -> DynamicsControls -> Fixed2 -> Fixed2 -> Fixed2
droneVelocityControl params dynCtrls position =
  (+ accel) . applyLinearDrag
 where
  accel =
    Fixed.map (* accelSize) $
      Fixed.normalize $
        dynCtrl_mousePos dynCtrls - position
  accelSize = dynCtrl_droneAccelScale dynCtrls * gp_droneAcceleration params
  droneDrag = gp_droneDrag params
  applyLinearDrag = Fixed.map (* (1 - droneDrag))

-- | Apply strong drag
flagVelocityControl :: Fixed2 -> Fixed2
flagVelocityControl = Fixed.map (* (1 - 0.1))

-- World boundary forces

-- | The world is a disc centered at the origin.
applyWorldBounds2 :: Fixed -> Fixed -> Fixed2 -> Fixed2 -> Fixed2
applyWorldBounds2 worldRadius entityRadius pos vel =
  vel & projected pos %~ applyWorldBounds1D2 worldRadius entityRadius r
 where
  r = Fixed.norm pos

applyWorldBounds1D2 :: Fixed -> Fixed -> Fixed -> Fixed -> Fixed
applyWorldBounds1D2 worldRadius entityRadius pos
  | overlap > 0 = repulsionImpulse overlap . flipPositiveVelocity
  | otherwise = id
 where
  overlap = pos + entityRadius - worldRadius
