module Pixelpusher.Game.PlayerStats (
  PlayerStats,
  newPlayerStats,
  AggregatePlayerStats,
  plusAggregateStats,
  -- Update
  incrementKills,
  incrementDeaths,
  addDroneHitDamage,
  addBodyHitDamage,
  addStormHitDamage,
  addDamageDealt,
  addDamageTaken,
  incrementFlagsCaptured,
  incrementFlagsRecovered,
  -- Access
  getAggregateStats,
  getKills,
  getDeaths,
  getStreakCurrent,
  getStreakBest,
  getDroneHitDamage,
  getBodyHitDamage,
  getStormHitDamage,
  getDamageDealt,
  getDamageTaken,
  getFlagsCaptured,
  getFlagsRecovered,
) where

import Control.Monad.Trans.State.Strict (execState)
import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import Data.Word (Word64)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed

--------------------------------------------------------------------------------

data PlayerStats = PlayerStats
  { ps_aggregated :: {-# UNPACK #-} !AggregatePlayerStats
  , ps_killStreak_current :: Word64
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | The subset of player statistics for which a (monoidal) combining action
-- makes sense. Only this subset will be preserved across rounds.
data AggregatePlayerStats = AggregatePlayerStats
  { aps_kills :: Word64
  , aps_killStreak_best :: Word64
  , aps_deaths :: Word64
  , aps_droneHitDamage :: Word64
  , aps_bodyHitDamage :: Word64
  , aps_stormHitDamage :: Word64
  , aps_damageDealt :: Word64
  , aps_damageTaken :: Word64
  , aps_captureWork :: Word64
  , aps_decaptureWork :: Word64
  , aps_flagsCaptured :: Word64
  , aps_flagsRecovered :: Word64
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------

newPlayerStats :: PlayerStats
newPlayerStats = PlayerStats mempty 0

instance Semigroup AggregatePlayerStats where
  (<>) s t =
    AggregatePlayerStats
      { aps_kills =
          aps_kills s + aps_kills t
      , aps_killStreak_best =
          max (aps_killStreak_best s) (aps_killStreak_best t)
      , aps_deaths =
          aps_deaths s + aps_deaths t
      , aps_droneHitDamage =
          aps_droneHitDamage s + aps_droneHitDamage t
      , aps_bodyHitDamage =
          aps_bodyHitDamage s + aps_bodyHitDamage t
      , aps_stormHitDamage =
          aps_stormHitDamage s + aps_stormHitDamage t
      , aps_damageDealt =
          aps_damageDealt s + aps_damageDealt t
      , aps_damageTaken =
          aps_damageTaken s + aps_damageTaken t
      , aps_captureWork =
          aps_captureWork s + aps_captureWork t
      , aps_decaptureWork =
          aps_decaptureWork s + aps_decaptureWork t
      , aps_flagsCaptured =
          aps_flagsCaptured s + aps_flagsCaptured t
      , aps_flagsRecovered =
          aps_flagsRecovered s + aps_flagsRecovered t
      }

instance Monoid AggregatePlayerStats where
  mempty =
    AggregatePlayerStats
      { aps_kills = 0
      , aps_killStreak_best = 0
      , aps_deaths = 0
      , aps_droneHitDamage = 0
      , aps_bodyHitDamage = 0
      , aps_stormHitDamage = 0
      , aps_damageDealt = 0
      , aps_damageTaken = 0
      , aps_captureWork = 0
      , aps_decaptureWork = 0
      , aps_flagsCaptured = 0
      , aps_flagsRecovered = 0
      }

plusAggregateStats :: PlayerStats -> AggregatePlayerStats -> PlayerStats
plusAggregateStats stats aggStats =
  stats & field @"ps_aggregated" <>~ aggStats

--------------------------------------------------------------------------------
-- Update

incrementKills :: PlayerStats -> PlayerStats
incrementKills = execState $ do
  field @"ps_aggregated" . field @"aps_kills" += 1
  currentStreak <- field @"ps_killStreak_current" <%= (+ 1)
  field @"ps_aggregated" . field @"aps_killStreak_best" %= max currentStreak

incrementDeaths :: PlayerStats -> PlayerStats
incrementDeaths =
  (field @"ps_aggregated" . field @"aps_deaths" +~ 1)
    . (field @"ps_killStreak_current" .~ 0)

addDamageDealt :: Fixed -> PlayerStats -> PlayerStats
addDamageDealt dmg =
  field @"ps_aggregated" . field @"aps_damageDealt" +~ fromDamage dmg

addDamageTaken :: Fixed -> PlayerStats -> PlayerStats
addDamageTaken dmg =
  field @"ps_aggregated" . field @"aps_damageTaken" +~ fromDamage dmg

addDroneHitDamage :: Fixed -> PlayerStats -> PlayerStats
addDroneHitDamage dmg =
  field @"ps_aggregated" . field @"aps_droneHitDamage" +~ fromDamage dmg

addBodyHitDamage :: Fixed -> PlayerStats -> PlayerStats
addBodyHitDamage dmg =
  field @"ps_aggregated" . field @"aps_bodyHitDamage" +~ fromDamage dmg

addStormHitDamage :: Fixed -> PlayerStats -> PlayerStats
addStormHitDamage dmg =
  field @"ps_aggregated" . field @"aps_stormHitDamage" +~ fromDamage dmg

incrementFlagsCaptured :: PlayerStats -> PlayerStats
incrementFlagsCaptured =
  field @"ps_aggregated" . field @"aps_flagsCaptured" +~ 1

incrementFlagsRecovered :: PlayerStats -> PlayerStats
incrementFlagsRecovered =
  field @"ps_aggregated" . field @"aps_flagsRecovered" +~ 1

--------------------------------------------------------------------------------
-- Access

getAggregateStats :: PlayerStats -> AggregatePlayerStats
getAggregateStats = ps_aggregated

getKills :: AggregatePlayerStats -> Int
getKills = fromIntegral . aps_kills

getDeaths :: AggregatePlayerStats -> Int
getDeaths = fromIntegral . aps_deaths

getStreakCurrent :: PlayerStats -> Int
getStreakCurrent = fromIntegral . ps_killStreak_current

getStreakBest :: AggregatePlayerStats -> Int
getStreakBest = fromIntegral . aps_killStreak_best

getDroneHitDamage :: AggregatePlayerStats -> Int
getDroneHitDamage = toDamage . aps_droneHitDamage

getBodyHitDamage :: AggregatePlayerStats -> Int
getBodyHitDamage = toDamage . aps_bodyHitDamage

getStormHitDamage :: AggregatePlayerStats -> Int
getStormHitDamage = toDamage . aps_stormHitDamage

getDamageDealt :: AggregatePlayerStats -> Int
getDamageDealt = toDamage . aps_damageDealt

getDamageTaken :: AggregatePlayerStats -> Int
getDamageTaken = toDamage . aps_damageTaken

getFlagsCaptured :: AggregatePlayerStats -> Int
getFlagsCaptured = fromIntegral . aps_flagsCaptured

getFlagsRecovered :: AggregatePlayerStats -> Int
getFlagsRecovered = fromIntegral . aps_flagsRecovered

--------------------------------------------------------------------------------
-- Helpers

fromDamage :: Fixed -> Word64
fromDamage dmg = round (damageResolutionFloat * Fixed.toFloat dmg)

toDamage :: Word64 -> Int
toDamage w = fromIntegral $ w `div` damageResolutionWord

--------------------------------------------------------------------------------
-- Constants

damageResolutionWord :: Word64
damageResolutionWord = 256

damageResolutionFloat :: Float
damageResolutionFloat = fromIntegral damageResolutionWord
