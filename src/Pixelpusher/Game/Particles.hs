{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Rudimentary particle system
module Pixelpusher.Game.Particles (
  Particle (..),
  SomeParticle (..),
  IsParticle (..),
  makeParticle,
  particleProgress,
  OverseerDeathParticle (..),
  DroneDeathParticle (..),
  OverseerRemainsParticle (..),
  CollisionDamageParticle (..),
  KillNotificationParticle (..),
  CastRippleParticle (..),
  PsiStormDamageParticle (..),
  LogMsgParticle (..),
  getFlagPickupMessage,
  FlagPickupParticle (..),
  FlagCaptureParticle (..),
  FlagRecoverParticle (..),
  FlagDropParticle (..),
  DashParticle (..),
  FlagExplodeParticle (..),
  FlagFadeParticle (..),
  ScreenshakeParticle (..),
  ParticleSystem,
  new,
  insert,
  trim,
  Particles (..),
  emptyParticles,
  getParticles,
) where

import Data.Functor (($>))
import Data.List.NonEmpty (NonEmpty ((:|)), (<|))
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed
import Pixelpusher.Custom.Serialize ()
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

data Particle a = Particle
  { particle_endTime :: Time
  , particle_startTime :: Time
  , particle_type :: a
  }
  deriving stock (Generic, Functor, Foldable, Traversable)

instance (Serialize a) => Serialize (Particle a)

data SomeParticle
  = PT_OverseerDeath OverseerDeathParticle
  | PT_DroneDeath DroneDeathParticle
  | PT_OverseerPersistentDeath OverseerRemainsParticle
  | PT_CollisionDamage CollisionDamageParticle
  | PT_KillNotification KillNotificationParticle
  | PT_CastRipple CastRippleParticle
  | PT_PsiStormDamage PsiStormDamageParticle
  | PT_LogMsg LogMsgParticle
  | PT_Dash DashParticle
  | PT_FlagExplode FlagExplodeParticle
  | PT_FlagFade FlagFadeParticle
  | PT_Screenshake ScreenshakeParticle
  deriving stock (Generic)
  deriving anyclass (Serialize)

class IsParticle a where
  animationTicks :: Ticks
  toParticle :: a -> SomeParticle

makeParticle ::
  forall a. IsParticle a => Time -> a -> Particle SomeParticle
makeParticle startTime p =
  Particle
    { particle_endTime = addTicks (animationTicks @a) startTime
    , particle_startTime = startTime
    , particle_type = toParticle p
    }
{-# INLINEABLE makeParticle #-}

particleProgress :: Time -> Particle a -> Float
particleProgress time p =
  let duration =
        getTicks $
          particle_endTime p `diffTime` particle_startTime p
      progress =
        getTicks $
          time `diffTime` particle_startTime p
  in  fromIntegral progress / fromIntegral duration

--------------------------------------------------------------------------------

data OverseerDeathParticle = OverseerDeathParticle
  { odp_color :: PlayerColor
  , odp_pos :: Fixed2
  , odp_vel :: Fixed2
  , odp_scale :: Fixed
  , odp_entityID :: Int
  , odp_actorID :: ActorID
  , odp_killedTeam :: Team
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle OverseerDeathParticle where
  animationTicks = Ticks 180
  toParticle = PT_OverseerDeath
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data DroneDeathParticle = DroneDeathParticle
  { ddp_eid :: Int
  , ddp_pos :: Fixed2
  , ddp_vel :: Fixed2
  , ddp_scale :: Fixed
  , ddp_team :: Team
  , ddp_recentDamageAtDeath :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle DroneDeathParticle where
  animationTicks = Ticks 16
  toParticle = PT_DroneDeath

data OverseerRemainsParticle = OverseerRemainsParticle
  { orp_color :: PlayerColor
  , orp_pos :: Fixed2
  , orp_scale :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle OverseerRemainsParticle where
  animationTicks = Ticks $ 8 * C.tickRate_hz
  toParticle = PT_OverseerPersistentDeath

data CollisionDamageParticle = CollisionDamageParticle
  { cdp_pos :: Fixed2
  , cdp_vel :: Fixed2
  , cdp_damage :: Fixed
  , cdp_direction :: Fixed2 -- relative position of collision participants
  , cdp_mPlayerColor1 :: Maybe PlayerColor
  , cdp_mPlayerColor2 :: Maybe PlayerColor
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle CollisionDamageParticle where
  animationTicks = Ticks C.tickRate_hz
  toParticle = PT_CollisionDamage
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data KillNotificationParticle = KillNotificationParticle
  { knp_killedName :: PlayerName
  , knp_killedTeam :: Team
  , knp_mKillerName :: Maybe PlayerName
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle KillNotificationParticle where
  animationTicks = Ticks $ C.tickRate_hz * 5
  toParticle = PT_KillNotification
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data CastRippleParticle = CastRippleParticle
  { crp_color :: Fixed4
  , crp_pos :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle CastRippleParticle where
  animationTicks = Ticks $ C.tickRate_hz `div` 2
  toParticle = PT_CastRipple
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data PsiStormDamageParticle = PsiStormDamageParticle
  { psdp_pos :: Fixed2
  , psdp_vel :: Fixed2
  , psdp_mPlayerColor :: Maybe PlayerColor
  , psdp_damage :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle PsiStormDamageParticle where
  animationTicks = Ticks C.tickRate_hz
  toParticle = PT_PsiStormDamage
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data LogMsgParticle
  = LogMsg_FlagPickup FlagPickupParticle
  | LogMsg_FlagCapture FlagCaptureParticle
  | LogMsg_FlagRecover FlagRecoverParticle
  | LogMsg_AnyMsg String
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle LogMsgParticle where
  animationTicks = Ticks $ C.tickRate_hz * 5
  toParticle = PT_LogMsg
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data FlagPickupParticle = FlagPickupParticle
  { fpp_team :: Team
  , fpp_pos :: Fixed2
  , fpp_name :: PlayerName
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

getFlagPickupMessage :: LogMsgParticle -> Maybe FlagPickupParticle
getFlagPickupMessage (LogMsg_FlagPickup x) = Just x
getFlagPickupMessage _ = Nothing

data FlagCaptureParticle = FlagCaptureParticle
  { fcp_team :: Team
  , fcp_name :: PlayerName
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data FlagRecoverParticle = FlagRecoverParticle
  { frp_team :: Team
  , frp_name :: Maybe PlayerName
  , frp_pos :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data FlagDropParticle = FlagDropParticle
  { fdp_pos :: Fixed2
  , fdp_vel :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data DashParticle = DashParticle
  { dp_pos :: Fixed2
  , dp_vel :: Fixed2
  , dp_radius :: Fixed
  , dp_direction :: Fixed2
  , dp_magnitudeFraction :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle DashParticle where
  animationTicks = Ticks (2 * C.tickRate_hz)
  toParticle = PT_Dash
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data FlagExplodeParticle = FlagExplodeParticle
  { fep_team :: Team
  -- ^ The team owning the flag
  , fep_pos :: Fixed2
  , fep_vel :: Fixed2
  , fep_radius :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle FlagExplodeParticle where
  animationTicks = Ticks 180
  toParticle = PT_FlagExplode
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data FlagFadeParticle
  = FlagFade_Recover FlagRecoverParticle
  | FlagFade_Pickup FlagPickupParticle
  | FlagFade_Drop FlagDropParticle
  | FlagFade_Respawn Team
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle FlagFadeParticle where
  animationTicks = Ticks $ C.tickRate_hz `div` 2
  toParticle = PT_FlagFade
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

data ScreenshakeParticle = ScreenshakeParticle
  { sp_damage :: Fixed
  , sp_direction :: Fixed2 -- relative position of collision participants
  , sp_actorID :: ActorID
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle ScreenshakeParticle where
  animationTicks = Ticks $ 2 * C.tickRate_hz
  toParticle = PT_Screenshake
  {-# INLINE animationTicks #-}
  {-# INLINE toParticle #-}

--------------------------------------------------------------------------------

newtype ParticleSystem
  = ParticleSystem (Map Time (NonEmpty (Particle SomeParticle)))
  deriving newtype (Serialize)

new :: ParticleSystem
new = ParticleSystem Map.empty

insert :: Particle SomeParticle -> ParticleSystem -> ParticleSystem
insert particle (ParticleSystem m) =
  let deathTime = particle_endTime particle
      addParticle = \case
        Nothing -> Just $ particle :| []
        Just particles -> Just $ particle <| particles
  in  ParticleSystem $ Map.alter addParticle deathTime m

-- | Removes particles that die at or before the provided time
trim :: Time -> ParticleSystem -> ParticleSystem
trim time (ParticleSystem m) = ParticleSystem $ snd $ Map.split time m

data Particles = Particles
  { particles_overseerDeath :: [Particle OverseerDeathParticle]
  , particles_droneDeath :: [Particle DroneDeathParticle]
  , particles_overseerRemains :: [Particle OverseerRemainsParticle]
  , particles_collisionDamage :: [Particle CollisionDamageParticle]
  , particles_killNotification :: [Particle KillNotificationParticle]
  , particles_castRipple :: [Particle CastRippleParticle]
  , particles_psiStormDamage :: [Particle PsiStormDamageParticle]
  , particles_logMsg :: [Particle LogMsgParticle]
  , particles_dash :: [Particle DashParticle]
  , particles_flagExplode :: [Particle FlagExplodeParticle]
  , particles_flagFade :: [Particle FlagFadeParticle]
  , particles_screenshake :: [Particle ScreenshakeParticle]
  }

emptyParticles :: Particles
emptyParticles = Particles [] [] [] [] [] [] [] [] [] [] [] []

getParticles :: ParticleSystem -> Particles
getParticles (ParticleSystem m) =
  partitionParticles $ concatMap NE.toList $ Map.elems m

partitionParticles :: [Particle SomeParticle] -> Particles
partitionParticles = foldr select emptyParticles

select :: Particle SomeParticle -> Particles -> Particles
select p ps = case particle_type p of
  PT_OverseerDeath x ->
    ps{particles_overseerDeath = (p $> x) : particles_overseerDeath ps}
  PT_DroneDeath x ->
    ps{particles_droneDeath = (p $> x) : particles_droneDeath ps}
  PT_OverseerPersistentDeath x ->
    ps{particles_overseerRemains = (p $> x) : particles_overseerRemains ps}
  PT_CollisionDamage x ->
    ps{particles_collisionDamage = (p $> x) : particles_collisionDamage ps}
  PT_KillNotification x ->
    ps{particles_killNotification = (p $> x) : particles_killNotification ps}
  PT_CastRipple x ->
    ps{particles_castRipple = (p $> x) : particles_castRipple ps}
  PT_PsiStormDamage x ->
    ps{particles_psiStormDamage = (p $> x) : particles_psiStormDamage ps}
  PT_LogMsg x ->
    ps{particles_logMsg = (p $> x) : particles_logMsg ps}
  PT_Dash x ->
    ps{particles_dash = (p $> x) : particles_dash ps}
  PT_FlagExplode x ->
    ps{particles_flagExplode = (p $> x) : particles_flagExplode ps}
  PT_FlagFade x ->
    ps{particles_flagFade = (p $> x) : particles_flagFade ps}
  PT_Screenshake x ->
    ps{particles_screenshake = (p $> x) : particles_screenshake ps}
