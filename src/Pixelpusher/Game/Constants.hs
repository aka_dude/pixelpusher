{-# HLINT ignore "Use underscore" #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

module Pixelpusher.Game.Constants where

import Data.Int (Int32)
import Data.SemVer qualified as SV
import Data.Word (Word16, Word8)

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Time (Ticks)

--------------------------------------------------------------------------------
-- Version

version :: SV.Version
version = SV.version 0 29 0 [] []

versionString :: String
versionString = SV.toString version

--------------------------------------------------------------------------------
-- Protocol

defaultPort :: Int
defaultPort = 30303

connTimeLimit_ns :: Int
connTimeLimit_ns = 5_000_000_000

--------------------------------------------------------------------------------
-- Simulation

frameDelay_ns :: Int
frameDelay_ns = 16_666_667

tickRate_hz :: Int32
tickRate_hz = 120

tickDelay_ns :: Int
tickDelay_ns = 8_333_333

{-# INLINE tickDelay_sec #-}
tickDelay_sec :: Floating a => a
tickDelay_sec = 1 / 120

maxNumEntities :: Word16
maxNumEntities = 768

--------------------------------------------------------------------------------
-- Game

-- Counts

maxPlayersPerGame :: Word8
maxPlayersPerGame = 32

maxBotsPerGame :: Word8
maxBotsPerGame = 16

maxActorsPerGame :: Word8
maxActorsPerGame = maxPlayersPerGame + maxBotsPerGame

minGameSize :: Word16
minGameSize = 6

maxGameSize :: Word16
maxGameSize = 16

maxSoftObstacles :: Word16
maxSoftObstacles = 64

maxPsiStorms :: Word16
maxPsiStorms = fromIntegral maxActorsPerGame * 2

-- Dynamics

-- | The value of this constant is intended to make the overseer mass close to
-- 1.
sqrtDensity :: Fixed
sqrtDensity = 1 / 16

-- Mass per unit area, for entities without health
density :: Fixed
density = sqrtDensity * sqrtDensity

-- Mass per unit health, for entities with health
massPerMaxHealth :: Fixed
massPerMaxHealth = 1 / 64

-- Spawn invulnerability

spawnInvulnerabilityCheckPeriod :: Ticks
spawnInvulnerabilityCheckPeriod = 6

viewRadius :: Fixed
viewRadius = fromIntegral viewRadiusInt

viewRadiusInt :: Int32
viewRadiusInt = 425

--------------------------------------------------------------------------------
-- Effects

matchEndSfxDelayTicks :: Ticks
matchEndSfxDelayTicks = 164 -- 88 bpm, 2 beats
