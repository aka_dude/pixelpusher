{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.FlagComponent (
  FlagComponent (..),
  FlagComponentVec (..),
  markFlagConditionSatisfied,
  FlagConditionStatus (..),
  flagConditionStatus,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Game.Time
import Pixelpusher.Store.VecStore

--------------------------------------------------------------------------------

-- | Entities that carry flags
data FlagComponent = FlagComponent
  { flag_hasFlag :: Bool
  , flag_time :: Time
  -- ^ This field has different meaning depending on the entity type:
  -- * BaseFlag: no meaning
  -- * Flag: time since the flag was dropped
  -- * Overseer: time until which the carrier cannot carry flags
  -- * TeamBase: no meaning
  , flag_ticks :: Ticks
  -- ^ This field has different meaning depending on the entity type:
  -- * BaseFlag: no meaning
  -- * Flag: number of ticks for which an allied overseer has been present
  -- * Overseer: no meaning
  -- * TeamBase: no meaning
  , flag_conditionStart :: Time
  -- ^ The most recent time at which a condition *began* to be satisfied. The
  -- specific condition depends on the entity type:
  -- * BaseFlag: no meaning
  -- * Flag: the flag is being recovering by an allied overseer
  -- * Overseer: no meaning
  -- * TeamBase: the team base is in contact with an overseer carrying an
  --   enemy flag
  -- Used with 'flag_conditionMostRecent'
  , flag_conditionLast :: Time
  -- ^ The last time at which a condition was satisfied. The specific
  -- condition depends on the entity type:
  -- * BaseFlag: no meaning
  -- * Flag: the flag is being recovering by an allied overseer
  -- * Overseer: no meaning
  -- * TeamBase: the team base is in contact with an overseer carrying an
  --   enemy flag
  -- Used with 'flag_conditionStart'
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeVecStore ''FlagComponent

--------------------------------------------------------------------------------
-- Flag conditions

markFlagConditionSatisfied :: Time -> Time -> Time -> (Time, Time)
markFlagConditionSatisfied currentTime startTime lastTime
  | currentTime == lastTime || currentTime == nextTick lastTime =
      (startTime, currentTime)
  | otherwise =
      (currentTime, currentTime)

data FlagConditionStatus
  = FlagConditionSatisfied
      !Time -- Satisified since
  | FlagConditionUnsatisfied
      !Time -- Previously satisified since
      !Time -- Unsatisfied since

flagConditionStatus :: Time -> FlagComponent -> FlagConditionStatus
flagConditionStatus time FlagComponent{flag_conditionStart, flag_conditionLast}
  | time == flag_conditionLast =
      FlagConditionSatisfied flag_conditionStart
  | otherwise =
      FlagConditionUnsatisfied
        flag_conditionStart
        (nextTick flag_conditionLast)
