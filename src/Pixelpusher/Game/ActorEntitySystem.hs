{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.ActorEntitySystem (
  ActorEntityStore,
  newActorEntityStore,
  getPlayerEntities,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore (..))

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the player that owns the entity
newtype ActorEntityStore s
  = ActorEntityStore
      ( DenseStore
          s
          (SEntityID 'MaybeActorEntity)
          (SEntityID 'ActorEntity)
          (StorableVecStore ActorID)
      )
  deriving newtype
    ( ReadStore s (SEntityID 'MaybeActorEntity) ActorID
    , Store s (SEntityID 'MaybeActorEntity) ActorID
    , Mutable s (DenseStoreSnapshot (SEntityID 'MaybeActorEntity) (StorableVecStore ActorID))
    , SureReadStore s (SEntityID 'ActorEntity) ActorID
    )

instance Copyable s (ActorEntityStore s) where
  copy (ActorEntityStore target) (ActorEntityStore source) =
    DS.copyDenseStore target source

newActorEntityStore :: ST s (ActorEntityStore s)
newActorEntityStore = ActorEntityStore <$> DS.newDenseStore C.maxNumEntities

--------------------------------------------------------------------------------

getPlayerEntities ::
  ActorEntityStore s -> ActorID -> ST s [SEntityID 'MaybeActorEntity]
getPlayerEntities (ActorEntityStore store) actorID =
  DS.toEntityIDListFiltered_1 store getStorableMVec (== actorID)
