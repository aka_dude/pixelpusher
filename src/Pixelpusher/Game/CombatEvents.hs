module Pixelpusher.Game.CombatEvents (
  CombatEvents (..),
  CollisionDamage (..),
  CollisionDamageParticipants (..),
  CombatDeathEvent (..),
  OverseerPsiStormDamage (..),
  PsiStormEffect (..),
) where

import Data.Strict.Tuple (Pair)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Store.EntityID

data CombatEvents = CombatEvents
  { ce_collisions :: [Pair (EntityIDPair 'CombatEntity) CollisionDamage]
  , ce_deaths :: [CombatDeathEvent]
  , ce_psiStormDamage :: [OverseerPsiStormDamage]
  , ce_psiStormEffects :: [PsiStormEffect]
  }
  deriving stock (Generic)

instance Semigroup CombatEvents where
  {-# INLINE (<>) #-}
  (<>) ce1 ce2 =
    CombatEvents
      { ce_collisions = ce_collisions ce1 <> ce_collisions ce2
      , ce_deaths = ce_deaths ce1 <> ce_deaths ce2
      , ce_psiStormDamage = ce_psiStormDamage ce1 <> ce_psiStormDamage ce2
      , ce_psiStormEffects = ce_psiStormEffects ce1 <> ce_psiStormEffects ce2
      }

instance Monoid CombatEvents where
  {-# INLINE mempty #-}
  mempty =
    CombatEvents
      { ce_collisions = []
      , ce_deaths = []
      , ce_psiStormDamage = []
      , ce_psiStormEffects = []
      }

data CollisionDamage = CollisionDamage
  { cd_position :: Fixed2
  , cd_velocity :: Fixed2
  , cd_damage :: Fixed
  , cd_direction :: Fixed2
  , cd_mActorID1 :: Maybe ActorID
  , cd_mActorID2 :: Maybe ActorID
  , cd_participants :: CollisionDamageParticipants
  }
  deriving stock (Generic)

data CollisionDamageParticipants
  = CollisionDamageOverseers ActorID ActorID
  | CollisionDamageDrones
  | CollisionDamageOverseerDrone ActorID

data CombatDeathEvent = CombatDeathEvent
  { killed_eid :: SEntityID 'CombatEntity
  , killer_mPid :: Maybe ActorID
  }

-- | For player statistics
data OverseerPsiStormDamage = OverseerPsiStormDamage
  { opsd_overseerID :: EntityID 'Overseer
  -- ^ The entity in the psi-storm
  , opsd_damage :: Fixed
  , opsd_mPsiStormActorID :: Maybe ActorID
  -- ^ The owner of the psi-storm, if any
  }

-- | For VFX and SFX
data PsiStormEffect = PsiStormEffect
  { pse_mEntityActorID :: Maybe ActorID
  -- ^ The owner of the entity, if any
  , pse_entityPos :: Fixed2
  -- ^ Position of the entity
  , pse_entityVel :: Fixed2
  -- ^ Velocity of the entity
  , pse_damage :: Fixed
  -- ^ Total psi-storm damage
  }
