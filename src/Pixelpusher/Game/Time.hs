-- | In-game time
module Pixelpusher.Game.Time (
  Time,
  getTime,
  Ticks (..),
  initialTime,
  diffTime,
  nextTick,
  prevTick,
  addTicks,
  subtractTicks,
  atMultipleOf,
  timeRange,
) where

import Data.Int (Int32)
import Data.List (unfoldr)
import Data.Serialize (Serialize)
import Foreign.Storable

import Pixelpusher.Custom.WrappedIntMap qualified as WIM

--------------------------------------------------------------------------------

-- Using an `Int32` rather than an `Int` to save a bit of bandwidth. At 120
-- ticks per second, an Int32 would take over half a year to overflow.
newtype Time = Time Int32
  deriving newtype (Eq, Ord, Serialize, Show, Storable)

getTime :: Time -> Int32
getTime (Time i) = i

instance WIM.IsInt Time where
  toInt (Time n) = WIM.makeIdentity $ fromIntegral n
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . Time . fromIntegral
  {-# INLINE fromInt #-}

newtype Ticks = Ticks {getTicks :: Int32}
  deriving newtype (Eq, Ord, Num, Real, Enum, Integral, Serialize)

initialTime :: Time
initialTime = Time 0

diffTime :: Time -> Time -> Ticks
diffTime (Time n1) (Time n2) = Ticks $ n1 - n2

nextTick :: Time -> Time
nextTick (Time n) = Time $ n + 1

prevTick :: Time -> Time
prevTick (Time n) = Time $ n - 1

addTicks :: Ticks -> Time -> Time
addTicks (Ticks ticks) (Time t) = Time $ t + ticks
{-# INLINE addTicks #-}

subtractTicks :: Ticks -> Time -> Time
subtractTicks (Ticks ticks) (Time t) = Time $ t - ticks
{-# INLINE subtractTicks #-}

atMultipleOf :: Time -> Ticks -> Bool
atMultipleOf (Time t) (Ticks ticks) = t `rem` ticks == 0
{-# INLINE atMultipleOf #-}

-- | Helper function
timeRange :: Time -> Time -> [Time]
timeRange firstTime lastTime =
  unfoldr
    (\t -> if t > lastTime then Nothing else Just (t, nextTick t))
    firstTime
