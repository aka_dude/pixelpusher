{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UndecidableInstances #-}

-- |
-- We represent the game parameters in the following three ways:
-- 1. 'TomlGameParams'
-- 2. 'BaseGameParams'
-- 3. 'GameParams'
--
-- 'TomlGameParams'
-- * User-facing, exported as TOML
-- * Organized for editing by users
-- * Uses 'Double's for their nice textual representation
--
-- 'BaseGameParams'
-- * Internal
-- * Variant of 'TomlGameParams' that uses 'Fixed' for cross-platform
-- compatibility
--
-- 'GameParams'
-- * Internal
-- * Organized for consumption by the game logic
-- * Uses 'Fixed' for cross-platform compatibility
--
-- 'BaseGameParams' is derived from 'TomlGameParams' by converting the
-- 'Double's to 'Fixed'. 'GameParams' is derived from the combination of
-- 'BaseGameParams' and the number of players.
--
-- Currently, the number of players is represented by the number of "control
-- points", an old game mechanic that no longer exists.
module Pixelpusher.Game.Parameters (
  TomlGameParams,
  defaultTomlGameParams,
  BaseGameParams,
  BaseGameParams' (..),
  GameParams (..),
  makeBaseGameParams,
  makeGameParams,
  defaultBaseGameParams,
  resizeGameParams,
  rebaseGameParams,
  defaultGameParams,
  -- Re-exports
  BotParams (..),
  CastParams (..),
  CombatParams (..),
  DynamicsParams (..),
  FlagParams (..),
  AudioParams (..),
  TestParams (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Audio
import Pixelpusher.Game.Parameters.Base.Bots
import Pixelpusher.Game.Parameters.Base.Damage
import Pixelpusher.Game.Parameters.Base.Dash
import Pixelpusher.Game.Parameters.Base.Dynamics
import Pixelpusher.Game.Parameters.Base.Flag
import Pixelpusher.Game.Parameters.Base.GameScaling
import Pixelpusher.Game.Parameters.Base.Misc
import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.Regen
import Pixelpusher.Game.Parameters.Base.SoftObstacle
import Pixelpusher.Game.Parameters.Base.Test

import Pixelpusher.Game.Parameters.Audio
import Pixelpusher.Game.Parameters.Bots
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.Parameters.Flag
import Pixelpusher.Game.Parameters.Test

--------------------------------------------------------------------------------

-- | User-specified game parameters. The type parameter 'a' is intended to be
-- substituted with either 'Double' or 'Fixed'.
data BaseGameParams' a = BaseGameParams'
  { bgp_gameScalingParams :: BaseGameScalingParams a
  , bgp_softObstacleParams :: BaseSoftObstacleParams a
  , bgp_dynamicsParams :: BaseDynamicsParams a
  , bgp_damageParams :: BaseDamageParams a
  , bgp_regenParams :: BaseRegenParams a
  , bgp_dashParams :: BaseDashParams a
  , bgp_psiStormParams :: BasePsiStormParams a
  , bgp_flagParams :: BaseFlagParams a
  , bgp_audioParams :: BaseAudioParams a
  , bgp_botParams :: BaseBotParams a
  , bgp_miscParams :: BaseMiscParams a
  , bgp_testParams :: BaseTestParams a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

-- | User-specified game parameters, suitable for representation in a .toml
-- file.
--
-- This form of the parameters represents non-integral numbers using 'Double'
-- (instead of 'Fixed') in order to obtain a nicer textual representation for
-- the numbers in the .toml file. The textual representations of 'Fixed' values
-- are long (e.g. 0.4 becomes "0.3999939"), making the parameters file hard to
-- read, while those of 'Double' values are nicely rounded.
newtype TomlGameParams = TomlGameParams (BaseGameParams' Double)
  deriving newtype (Toml.ToTable, Toml.ToValue)

-- Manual instance since newtype deriving doesn't work
instance Toml.FromValue TomlGameParams where
  fromValue =
    fmap TomlGameParams . Toml.parseTableFromValue Toml.genericParseTable

-- | User-specified game parameters, suitable for transmission to clients on
-- different platforms.
--
-- This form of the parameters represents non-integral numbers using 'Fixed'
-- (instead of 'Double') in order to prevent inconsistency between platforms
-- (e.g. operating systems and CPUs) due to differences in floating-point
-- arithmetic. Strictly speaking, transmitting values as 'Fixed' may not be
-- necessary for cross-platform consistency, but it is sufficient, and I just
-- want to a simple way to avoid any cross-platform issues.
type BaseGameParams = BaseGameParams' Fixed

-- | Convert a 'TomlGameParams' into a 'BaseGameParams', making it
-- cross-platform compatible.
makeBaseGameParams :: TomlGameParams -> BaseGameParams
makeBaseGameParams (TomlGameParams params) =
  fmap Fixed.fromDouble params

defaultBaseGameParams :: BaseGameParams
defaultBaseGameParams = makeBaseGameParams defaultTomlGameParams

instance Toml.FromValue (BaseGameParams' Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseGameParams' Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseGameParams' Double) where
  toTable = Toml.genericToTable

defaultTomlGameParams :: TomlGameParams
defaultTomlGameParams =
  TomlGameParams $
    BaseGameParams'
      { bgp_gameScalingParams = defaultGameScalingParams
      , bgp_softObstacleParams = defaultSoftObstacleParams
      , bgp_dynamicsParams = defaultDynamicsParams
      , bgp_damageParams = defaultDamageParams
      , bgp_regenParams = defaultRegenParams
      , bgp_dashParams = defaultDashParams
      , bgp_psiStormParams = defaultPsiStormParams
      , bgp_flagParams = defaultFlagParams
      , bgp_audioParams = defaultAudioParams
      , bgp_botParams = defaultBotParams
      , bgp_miscParams = defaultMiscParams
      , bgp_testParams = defaultTestParams
      }

--------------------------------------------------------------------------------

-- | Game parameters used by the game logic and also for effects. Derived from
-- the combination of `BaseGameParams` and the number of players.
data GameParams = GameParams
  -- The inputs from which the rest are derived
  { gp_gameParamsRaw :: BaseGameParams' Fixed
  , gp_numPlayers :: Int
  , -- Derived parameters (structured)
    gp_dynamics :: DynamicsParams
  , gp_cast :: CastParams
  , gp_combat :: CombatParams
  , gp_flags :: FlagParams
  , gp_audio :: AudioParams
  , gp_bots :: BotParams
  , gp_test :: TestParams
  , -- Derived parameters (miscellaneous)
    gp_gameSize :: Int
  , gp_numSoftObstacles :: Int
  , gp_minIntermissionTicks :: Ticks
  , gp_maxIntermissionTicks :: Ticks
  , gp_flagsToWin :: Int
  , gp_requestFogOfWar :: Bool
  , gp_disassociatedDroneDecayFactor :: Fixed
  , gp_startSpawnDelayTicks :: Ticks
  , gp_teamBaseSpacing :: Fixed
  , gp_showFlagsOnMinimap :: Bool
  , gp_overseerDroneLimit :: Int
  , gp_spawnLocationAngle :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Derive game parameters from 'BaseGameParams' and the desired number of
-- control points. Any number of control points is acceptable because the value
-- is internally clamped.
makeGameParams :: Int -> BaseGameParams -> GameParams
makeGameParams nPlayers gpr@BaseGameParams'{..} =
  GameParams
    { -- The inputs from which the rest are derived
      gp_gameParamsRaw = gpr
    , gp_numPlayers = nPlayers
    , -- Derived parameters (structured)
      gp_dynamics = dynamicsParams
    , gp_cast =
        makeCastParams
          bgp_dynamicsParams
          bgp_psiStormParams
          bgp_dashParams
    , gp_combat = makeCombatParams bgp_regenParams bgp_psiStormParams
    , gp_flags = makeFlagParams bgp_flagParams
    , gp_audio = makeAudioParams bgp_audioParams
    , gp_bots = botParams
    , gp_test = makeTestParams bgp_testParams
    , -- Derived parameters (miscellaneous)
      gp_gameSize = gameSize
    , gp_numSoftObstacles =
        min (fromIntegral C.maxSoftObstacles) $
          round $
            bgp_softObstaclesPerSize * fromIntegral gameSize
    , gp_minIntermissionTicks = fromIntegral bgp_minIntermissionTicks
    , gp_maxIntermissionTicks = fromIntegral bgp_maxIntermissionTicks
    , gp_flagsToWin = bgp_flagsToWin
    , gp_requestFogOfWar = bgp_requestFogOfWar
    , gp_disassociatedDroneDecayFactor =
        let ticksUntilDeath =
              fromIntegral bgp_disassociatedDroneLifetimeTicks
                / fromIntegral bgp_healthRegenPeriodTicks
        in  recip ticksUntilDeath
    , gp_startSpawnDelayTicks = fromIntegral bgp_startSpawnDelayTicks
    , gp_teamBaseSpacing = teamBaseSpacing
    , gp_showFlagsOnMinimap = bgp_showFlagsOnMinimap
    , gp_overseerDroneLimit = bgp_overseerDroneLimit
    , gp_spawnLocationAngle = bgp_spawnLocationAngleDegrees * (pi / 180)
    }
 where
  BaseGameScalingParams{..} = bgp_gameScalingParams
  BaseRegenParams{..} = bgp_regenParams
  BaseMiscParams{..} = bgp_miscParams

  gameSize =
    let teamSize = (nPlayers + 1) `div` 2 + gp_minBotsPerTeam botParams
    in  max (fromIntegral C.minGameSize) $
          min (fromIntegral C.maxGameSize) teamSize
  teamBaseSpacing =
    (* bgp_teamBaseSpacingPerSqrtSize) $ sqrt $ fromIntegral gameSize
  botParams = makeBotParams bgp_botParams
  dynamicsParams =
    makeDynamicsParams
      gameSize
      bgp_gameScalingParams
      bgp_dynamicsParams
      bgp_damageParams
      bgp_flagParams
      bgp_softObstacleParams
      bgp_psiStormParams

-- | Re-derive game parameters using a different number of control points.
resizeGameParams :: Int -> GameParams -> GameParams
resizeGameParams nPlayers params =
  makeGameParams nPlayers $ gp_gameParamsRaw params

-- | Re-derive game parameters using different base parameters.
rebaseGameParams :: BaseGameParams -> GameParams -> GameParams
rebaseGameParams baseParams oldParams =
  makeGameParams (gp_numPlayers oldParams) baseParams

defaultGameParams :: GameParams
defaultGameParams = makeGameParams 0 defaultBaseGameParams
