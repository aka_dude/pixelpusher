{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.PsiStormSystem (
  PsiStormStore,
  newPsiStormStore,
  getPsiStorms,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (UnitVecStore)

import {-# SOURCE #-} Pixelpusher.Game.EntityStore.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the time at which entities should expire.
newtype PsiStormStore s
  = PsiStormStore
      ( DenseStore
          s
          (EntityID 'PsiStorm)
          (EntityID 'PsiStorm)
          UnitVecStore
      )
  deriving newtype
    ( ReadStore s (EntityID 'PsiStorm) ()
    , Store s (EntityID 'PsiStorm) ()
    , Mutable s (DenseStoreSnapshot (EntityID 'PsiStorm) UnitVecStore)
    , SureReadStore s (EntityID 'PsiStorm) ()
    )

instance Copyable s (PsiStormStore s) where
  copy (PsiStormStore target) (PsiStormStore source) =
    DS.copyDenseStore target source

newPsiStormStore :: ST s (PsiStormStore s)
newPsiStormStore = PsiStormStore <$> DS.newDenseStore C.maxNumEntities

--------------------------------------------------------------------------------

getPsiStorms :: PsiStormStore s -> ST s [EntityID 'PsiStorm]
getPsiStorms (PsiStormStore store) = DS.indices store
