module Pixelpusher.Game.TeamBase (
  teamBaseLocation,
  spawnAreaCenter,
) where

import Pixelpusher.Custom.Fixed (Fixed2 (Fixed2))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Team

teamBaseLocation :: GameParams -> Team -> Fixed2
teamBaseLocation params = \case
  TeamNW -> Fixed2 lbound 0
  TeamSE -> Fixed2 ubound 0
 where
  buffer = gp_teamBaseRadius (gp_dynamics params) + gp_teamBaseSpacing params
  x = gp_worldRadius (gp_dynamics params) - buffer
  ubound = x
  lbound = -x

spawnAreaCenter :: GameParams -> Team -> Fixed2
spawnAreaCenter params = \case
  TeamNW -> westLoc
  TeamSE -> eastLoc
 where
  spawnAreaRadius = gp_spawnAreaRadius (gp_dynamics params)
  r = gp_worldRadius (gp_dynamics params) - buffer - spawnAreaRadius
  buffer = 8
  angle = gp_spawnLocationAngle params
  eastLoc = Fixed2 (r * cos angle) (r * sin angle)
  westLoc = -eastLoc
