-- | Scorekeeping
module Pixelpusher.Game.TeamScore (
  TeamScore (..),
  zeroTeamScore,
  incrementTeamScore,
  pointTeamNW,
  pointTeamSE,
  scaleTeamScore,
) where

import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Game.Team

--------------------------------------------------------------------------------

data TeamScore = TeamScore
  { teamScore_se :: Int
  , teamScore_nw :: Int
  }
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

instance Semigroup TeamScore where
  (<>) (TeamScore a1 b1) (TeamScore a2 b2) = TeamScore (a1 + a2) (b1 + b2)

instance Monoid TeamScore where
  mempty = TeamScore 0 0

zeroTeamScore :: TeamScore
zeroTeamScore = TeamScore 0 0

pointTeamNW :: TeamScore
pointTeamNW =
  TeamScore
    { teamScore_nw = 1
    , teamScore_se = 0
    }

pointTeamSE :: TeamScore
pointTeamSE =
  TeamScore
    { teamScore_nw = 0
    , teamScore_se = 1
    }

incrementTeamScore :: Team -> TeamScore -> TeamScore
incrementTeamScore team score = case team of
  TeamNW -> score & field @"teamScore_nw" +~ 1
  TeamSE -> score & field @"teamScore_se" +~ 1

scaleTeamScore :: Int -> TeamScore -> TeamScore
scaleTeamScore factor score =
  TeamScore
    { teamScore_se = factor * teamScore_se score
    , teamScore_nw = factor * teamScore_nw score
    }
