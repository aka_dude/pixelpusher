{-# LANGUAGE OverloadedStrings #-}

module Pixelpusher.Game.PlayerName (
  PlayerName,
  getPlayerName,
  makePlayerName,
  makePlayerNameText,
  nullPlayerName,
  makeBotName,
  maxNameLength,
  isNameChar,
) where

import Data.ByteString.Char8 qualified as B
import Data.Char (isAsciiLower, isAsciiUpper, isDigit)
import Data.Serialize (Serialize)
import Data.Serialize.Text ()
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.Encoding qualified as Text

--------------------------------------------------------------------------------

newtype PlayerName = PlayerName B.ByteString
  deriving newtype (Eq, Serialize)

nullPlayerName :: PlayerName
nullPlayerName = PlayerName B.empty

getPlayerName :: PlayerName -> B.ByteString
getPlayerName (PlayerName bs) = bs

makePlayerName :: B.ByteString -> Either Text PlayerName
makePlayerName rawName
  | B.null rawName = Left "Please enter a name"
  | B.length rawName > maxNameLength =
      Left $
        "Name too long (max "
          <> Text.pack (show maxNameLength)
          <> " chars)"
  | not $ B.all isNameChar rawName =
      Left "Only letters (a-z, A-Z) and numbers (0-9) are allowed in names"
  | otherwise = Right $ PlayerName rawName

makePlayerNameText :: Text -> Either Text PlayerName
makePlayerNameText = makePlayerName . Text.encodeUtf8

-- TODO: Make a separate type for actor names
makeBotName :: String -> PlayerName
makeBotName n = PlayerName $ B.pack $ "#" <> n

--------------------------------------------------------------------------------
-- Name requirements

maxNameLength :: Int
maxNameLength = 16

isNameChar :: Char -> Bool
isNameChar c = isDigit c || isAsciiLower c || isAsciiUpper c
