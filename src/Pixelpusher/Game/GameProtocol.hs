-- | Game data transferred between server and client
module Pixelpusher.Game.GameProtocol (
  ServerGameMsg (..),
  ServerGameData (..),
  GameStateUpdate (..),
  ClientGameMsg (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.SmallList (SmallList)
import Pixelpusher.Game.GameCommand (GameCommand)
import Pixelpusher.Game.GameState (GameStateSnapshot)
import Pixelpusher.Game.PlayerControls (PlayerControlsTransport)
import Pixelpusher.Game.PlayerID (PlayerID, PlayerIDMap32)
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

-- | Messages from servers to clients
newtype ServerGameMsg = ServerGameData {unServerGameMsg :: ServerGameData}
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ServerGameData
  = ServerSnapshot
      !PlayerID
      !Int -- Room ID
      !GameStateSnapshot
  | ServerDeltaUpdate !GameStateUpdate
  deriving stock (Generic)
  deriving anyclass (Serialize)

data GameStateUpdate = GameStateUpdate
  { deltaUpdate_time :: Time
  , deltaUpdate_playerControls :: PlayerIDMap32 PlayerControlsTransport
  , deltaUpdate_gameCommands :: SmallList GameCommand
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Messages from clients to the servers
newtype ClientGameMsg
  = ClientControls PlayerControlsTransport
  deriving stock (Generic)
  deriving anyclass (Serialize)
