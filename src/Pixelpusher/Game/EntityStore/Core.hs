{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE UndecidableInstances #-}

-- | This module encapsulates the logic for creating and destroying the data
-- components of game entities. It is responsible for maintaining the
-- consistency of the collection of all data components across all stores of
-- data components.
--
-- We aim to maintain the following relationships:
--
-- * The status of each player's overseer is tracked by their 'OverseerStatus'.
-- * Each entity has the data components prescribed by its type.
module Pixelpusher.Game.EntityStore.Core (
  -- Store interface
  Store (..),
  ReadStore (..),
  SureReadStore (..),
  -- Read-only store interface
  ReadOnly,
  readOnly,
  -- Store action context
  CoreM,
  coreM,
  -- Collection of all stores
  EntityStore,
  dataStores,
  Stores (..),
  initialEntityStore,
  resetEntityStore,
  EntityStoreSnapshot,
  -- Queries
  getPlayerStatuses,
  getPlayerStatus,
  -- Mutations
  traversePlayerCooldownsWithResult,
  -- Store operations
  addPlayer,
  removePlayer,
  finallyRemoveEntities,
  respawnOverseers,
  DroneSpawnType (..),
  addDrone,
  addSoftObstacle,
  addPsiStorm,
  addFlag,
  addBaseFlag,
  addTeamBase,
  addExplosion,
  addSpawnArea,
) where

import Prelude hiding (lookup)

import Control.Monad (replicateM_, void, when)
import Control.Monad.ST (ST)
import Control.Monad.Trans.Writer
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields
import Data.Maybe (fromMaybe)
import Data.STRef (STRef, modifySTRef', newSTRef, readSTRef, writeSTRef)
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.Util (
  expectJust,
  uniformUnitDisc,
 )
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.CombatSystem
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.DynamicsSystem
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.FlagSystem hiding (getFlags)
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.PsiStormSystem
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamBase (spawnAreaCenter)
import Pixelpusher.Game.TeamSystem
import Pixelpusher.Game.Time
import Pixelpusher.Game.TransitorySystem
import Pixelpusher.Store.DenseStore (DenseStoreSnapshot)
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore, UnitVecStore)

--------------------------------------------------------------------------------
-- Store interfaces

-- | An interface through which a data store can allow for the creation and
-- deletion of data entries for a specific entity. All such operations should
-- be restricted to the 'CoreM' context.
class Store s idx elem store | store -> idx, store -> elem where
  -- | Add a data component to the store at the given index. This function
  -- should be the only way to add entries to a store.
  insert :: (SubEntityID i idx) => store -> i -> elem -> CoreM s ()

  -- | Remove the data component in the store at the given index. This function
  -- should be the only way to remove entries to a store, other than 'clear'.
  delete :: (SubEntityID i idx) => store -> i -> CoreM s ()

  -- | A convenience function for removing all data from the store.
  clear :: store -> CoreM s ()

-- | An interface through which one can retrieve data about an entity from a
-- data store.
class ReadStore s idx elem store | store -> idx, store -> elem where
  lookup :: (SubEntityID i idx) => store -> i -> ST s (Maybe elem)

-- | An interface for data stores through which data about an entity can be
-- retrieved when the target data is known to be present in the store.
class SureReadStore s sidx elem store | store -> sidx, store -> elem where
  -- | Like 'lookup', but we know that the store is has an entry at the given
  -- index.
  sureLookup :: (SubEntityID i sidx) => store -> i -> ST s elem

--------------------------------------------------------------------------------
-- Read-only store wrapper

-- | A wrapper for entity stores that exposes only their 'ReadStore' instances
newtype ReadOnly store = ReadOnly store

instance
  ReadStore s idx elem store =>
  ReadStore s idx elem (ReadOnly store)
  where
  lookup :: (SubEntityID i idx) => ReadOnly store -> i -> ST s (Maybe elem)
  lookup (ReadOnly store) = lookup store

instance
  SureReadStore s sidx elem store =>
  SureReadStore s sidx elem (ReadOnly store)
  where
  sureLookup :: (SubEntityID i sidx) => ReadOnly store -> i -> ST s elem
  sureLookup (ReadOnly store) = sureLookup store

-- | Wrap an entity store, exposing only its 'ReadStore' instance.
readOnly :: store -> ReadOnly store
readOnly = ReadOnly

--------------------------------------------------------------------------------
-- Store action context

-- | 'CoreM' is a wrapper around 'ST' that can only be unwrapped by the code
-- in this module. We want all operations on entity stores that create or
-- delete entries to be encapsulated in this module so that we can more easily
-- maintain invariants across data stores. To achieve encapsulation, the
-- implementation of each individual entity store /must/ ensure that they use
-- 'CoreM' to wrap all operations that create or delete entries.
newtype CoreM s a = CoreM {runCoreM :: ST s a}
  deriving newtype (Functor, Applicative, Monad)

type role CoreM nominal representational

-- | Wrap an 'ST' action, labelling it as a store action.
coreM :: ST s a -> CoreM s a
coreM = CoreM

--------------------------------------------------------------------------------
-- Collection of all stores

-- | An opaque type containing the collection of all stores.
data EntityStore s = EntityStore
  { entityStore_entityIDPool :: EntityIDPool s
  , entityStore_actors :: STRef s (WIM.IntMap ActorID PlayerStatus)
  , entityStore_dataStores :: Stores s
  }
  deriving stock (Generic)

dataStores :: EntityStore s -> Stores s
dataStores = entityStore_dataStores

-- | All the stores accessible to the game logic.
data Stores s = Stores
  { store_team :: TeamStore s
  , store_dynamics :: DynamicsStore s
  , store_combat :: CombatStore s
  , store_masterMinions :: MasterMinionStore s
  , store_transitory :: TransitoryStore s
  , store_flag :: FlagStore s
  , store_actorEntity :: ActorEntityStore s
  , store_psiStorm :: PsiStormStore s
  }
  deriving stock (Generic)

initialEntityStore :: ST s (EntityStore s)
initialEntityStore =
  EntityStore
    <$> newEntityIDPool
    <*> newSTRef WIM.empty
    <*> initialStores

initialStores :: ST s (Stores s)
initialStores =
  Stores
    <$> newTeamStore
    <*> newDynamicsStore
    <*> newCombatStore
    <*> newMasterMinionStore
    <*> newTransitoryStore
    <*> newFlagStore
    <*> newActorEntityStore
    <*> newPsiStormStore

data EntityStoreSnapshot = EntityStoreSnapshot
  { entityStoreSnapshot_entityIDPool :: EntityIDPoolSnapshot
  , entityStoreSnapshot_overseers :: WIM.IntMap ActorID PlayerStatus
  , entityStoreSnapshot_stores :: StoresSnapshot
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data StoresSnapshot = StoresSnapshot
  { storeSnapshot_team ::
      DenseStoreSnapshot (SEntityID 'TeamEntity) (StorableVecStore Team)
  , storeSnapshot_dynamics ::
      DenseStoreSnapshot (SEntityID 'DynamicsEntity) DynamicsComponentVec
  , storeSnapshot_combat ::
      DenseStoreSnapshot (SEntityID 'CombatEntity) CombatComponentVec
  , storeSnapshot_masterMinions ::
      MasterMinionStoreSnapshot
  , storeSnapshot_transitory ::
      DenseStoreSnapshot (SEntityID 'TransitoryEntity) (StorableVecStore Time)
  , storeSnapshot_flag ::
      DenseStoreSnapshot (SEntityID 'FlagEntity) FlagComponentVec
  , storeSnapshot_playerEntity ::
      DenseStoreSnapshot (SEntityID 'MaybeActorEntity) (StorableVecStore ActorID)
  , storeSnapshot_psiStorm ::
      DenseStoreSnapshot (EntityID 'PsiStorm) UnitVecStore
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s EntityStoreSnapshot (EntityStore s) where
  freeze store =
    EntityStoreSnapshot
      <$> freeze (entityStore_entityIDPool store)
      <*> readSTRef (entityStore_actors store)
      <*> freeze (entityStore_dataStores store)
  thaw snapshot =
    EntityStore
      <$> thaw (entityStoreSnapshot_entityIDPool snapshot)
      <*> newSTRef (entityStoreSnapshot_overseers snapshot)
      <*> thaw (entityStoreSnapshot_stores snapshot)

instance Mutable s StoresSnapshot (Stores s) where
  freeze store =
    StoresSnapshot
      <$> freeze (store_team store)
      <*> freeze (store_dynamics store)
      <*> freeze (store_combat store)
      <*> freeze (store_masterMinions store)
      <*> freeze (store_transitory store)
      <*> freeze (store_flag store)
      <*> freeze (store_actorEntity store)
      <*> freeze (store_psiStorm store)
  thaw snapshot =
    Stores
      <$> thaw (storeSnapshot_team snapshot)
      <*> thaw (storeSnapshot_dynamics snapshot)
      <*> thaw (storeSnapshot_combat snapshot)
      <*> thaw (storeSnapshot_masterMinions snapshot)
      <*> thaw (storeSnapshot_transitory snapshot)
      <*> thaw (storeSnapshot_flag snapshot)
      <*> thaw (storeSnapshot_playerEntity snapshot)
      <*> thaw (storeSnapshot_psiStorm snapshot)

instance Copyable s (EntityStore s)
instance Copyable s (Stores s)

-- | Clear all entities and `EntityID`s from the store, setting it a state
-- equivalent to that of `initialEntityStore`. This can be used to reset the
-- game state between game rounds.
--
-- Note: We perform a pattern match on the constructor to make it harder to
-- forget a component.
resetEntityStore :: EntityStore s -> ST s ()
resetEntityStore (EntityStore eidPool playerOverseers stores) = do
  clearEntityIDPool eidPool
  writeSTRef playerOverseers WIM.empty
  resetStores stores

-- Note: We perform a pattern match on the constructor to make it harder to
-- forget a component.
resetStores :: Stores s -> ST s ()
resetStores (Stores team dynamics combat minion transitory flag playerEntity psiStorm) =
  runCoreM $ do
    clear team
    clear dynamics
    clear combat
    clearMasterMinionStore minion
    clear transitory
    clear flag
    clear playerEntity
    clear psiStorm

--------------------------------------------------------------------------------
-- Queries

getPlayerStatuses :: EntityStore s -> ST s (WIM.IntMap ActorID PlayerStatus)
getPlayerStatuses store = readSTRef $ entityStore_actors store

getPlayerStatus :: EntityStore s -> ActorID -> ST s (Maybe PlayerStatus)
getPlayerStatus store actorID =
  fmap (WIM.lookup actorID) $ readSTRef $ entityStore_actors store

--------------------------------------------------------------------------------
-- Mutations

traversePlayerCooldownsWithResult ::
  Monoid w =>
  EntityStore s ->
  (ActorID -> PlayerStatus -> ST s (PlayerCooldowns, w)) ->
  ST s w
traversePlayerCooldownsWithResult store f = do
  players <- readSTRef playersRef
  (newPlayers, w) <-
    runWriterT $
      flip WIM.traverseWithKey players $
        \actorID playerStatus -> do
          newCooldowns <- WriterT $ f actorID playerStatus
          pure playerStatus{ps_cooldowns = newCooldowns}
  writeSTRef playersRef newPlayers
  pure w
 where
  playersRef = entityStore_actors store

--------------------------------------------------------------------------------
-- Coordinated store operations

-- Adding and removing players

addPlayer :: GameParams -> EntityStore s -> Time -> Team -> ActorID -> ST s ()
addPlayer params store time team aid = do
  let fakeDeathTime = addTicks (Ticks (-C.tickRate_hz * 60)) time
      respawnTime = addTicks (gp_startSpawnDelayTicks params) time
      playerStatus =
        PlayerStatus
          { ps_team = team
          , ps_overseerStatus =
              OverseerDead
                fakeDeathTime
                respawnTime
                (spawnAreaCenter params team)
          , ps_cooldowns = initialCooldowns time
          }
  modifySTRef' (entityStore_actors store) $ at aid ?~ playerStatus

-- Should be idempotent
removePlayer :: GameParams -> EntityStore s -> Time -> ActorID -> ST s ()
removePlayer params store time actorID = do
  let Stores{store_actorEntity} = entityStore_dataStores store

  -- Dissociate player entities
  getPlayerEntities store_actorEntity actorID
    >>= traverse_ (dissociateActorEntity params store time)

  modifySTRef' (entityStore_actors store) $ at actorID .~ Nothing

-- Entity removal

-- | Run an ST action with the ability to mark entities for removal from the
-- given `EntityStore`. The marked entities will be removed at the end of the
-- action. This function is the only way to remove entities from an
-- 'EntityStore`.
--
-- 'EntityID's act as references to entities. If the entity referred to by an
-- 'EntityID' is removed, continued use of the 'EntityID' will likely yield
-- nonsensical results. Our goal is to prevent such "invalidation" of
-- 'EntityID's; to this end, we aim to maintain the following invariants, which
-- should be sufficient:
--
-- (1) We do not retain any 'EntityID's between iterations of the game loop.
-- (2) We never remove entities within an iteration of the game loop; instead,
-- they are removed at the very end of each iteration, after all uses of any
-- 'EntityID's.
--
-- We manually enforce the first invariant. This function helps us enforce the
-- second invariant. In the game loop, this function should be called exactly
-- once, and all uses of 'EntityID's should take place in the action given to
-- this function.
finallyRemoveEntities ::
  GameParams ->
  Time ->
  EntityStore s ->
  ((SEntityID 'AnyEntity -> ST s ()) -> ST s a) ->
  ST s a
finallyRemoveEntities params time store action = do
  -- Using a set ensures that each entity is removed at most once (assuming
  -- that the above-mentioned invariants hold).
  toDelete <- newSTRef WIS.empty
  let markForRemoval = modifySTRef' toDelete . WIS.insert
  result <- action markForRemoval
  readSTRef toDelete
    >>= traverse_ (removeEntity params store time) . WIS.toList
  pure result

removeEntity ::
  GameParams -> EntityStore s -> Time -> SEntityID 'AnyEntity -> ST s ()
removeEntity params store time eid =
  case refineEntityID eid of
    OverseerID overseerID -> removeOverseer params store time overseerID
    DroneID droneID -> removeDrone store time droneID
    SoftObstacleID softObstacleID -> removeSoftObstacle store softObstacleID
    PsiStormID psiStormID -> removePsiStorm store psiStormID
    FlagID flagID -> removeFlag store flagID
    TeamBaseID teamBaseID -> removeTeamBase store teamBaseID
    BaseFlagID baseFlagID -> removeBaseFlag store baseFlagID
    ExplosionID explosionID -> removeExplosion store explosionID
    SpawnAreaID spawnAreaID -> removeSpawnArea store spawnAreaID

-- | Remove all `ActorID`s from the data associated with an entity, either by
-- modifying its data or removing the entity outright. Should be idempotent.
dissociateActorEntity ::
  GameParams ->
  EntityStore s ->
  Time ->
  SEntityID 'MaybeActorEntity ->
  ST s ()
dissociateActorEntity params store time playerEntityID =
  case refineEntityID playerEntityID of
    OverseerID overseerID ->
      -- Dissociation not implemented yet, but we need to get rid of the
      -- `ActorID` reference, so overseers currently cannot exist without a
      -- controlling actor
      removeOverseer params store time overseerID
    DroneID droneID -> dissociateActorDrone params store time droneID
    PsiStormID psiStormID -> dissociateActorPsiStorm store psiStormID

-- Overseer entities

-- | This function should be the only way to create overseer entities.
respawnOverseers ::
  GameParams ->
  MWC.Gen s ->
  EntityStore s ->
  Time ->
  GamePhase ->
  ST s ()
respawnOverseers params gen store time gamePhase =
  -- Check for respawns only periodically
  when (time `atMultipleOf` Ticks 6) $ do
    case gamePhase of
      GamePhase_Intermission{} ->
        pure ()
      GamePhase_Game{} -> do
        let ovRef = entityStore_actors store
        pids <- WIM.keys <$> readSTRef ovRef
        traverse_ (addOverseer params time store gen) pids

-- This function should only be used with 'respawnOverseers'.
addOverseer ::
  GameParams -> Time -> EntityStore s -> Gen s -> ActorID -> ST s ()
addOverseer params time store gen actorID = do
  PlayerStatus team overseerStatus _cooldonws <-
    expectJust "addOverseer: ActorID not found" . WIM.lookup actorID
      <$> readSTRef (entityStore_actors store)
  case overseerStatus of
    OverseerAlive{} -> pure ()
    OverseerDead deathTime respawnTime _ ->
      when (time >= respawnTime) $ do
        overseerID <- borrowEntityID @'Overseer (entityStore_entityIDPool store)

        modifySTRef' (entityStore_actors store) $
          ix actorID . field @"ps_overseerStatus"
            .~ OverseerAlive overseerID time

        let droneLimit = gp_overseerDroneLimit params

        runCoreM $ do
          let Stores{..} = entityStore_dataStores store
          insert store_team overseerID team
          CoreM (randomOverseerSpawnPosition params gen team) >>= \pos ->
            insert store_dynamics overseerID $
              overseer_newDynamicsComponent params pos
          insert store_combat overseerID $
            overseer_newCombatComponent params time deathTime
          registerMaster store_masterMinions overseerID droneLimit
          insert store_flag overseerID $ overseer_newFlagComponent time
          insert store_actorEntity overseerID actorID

        -- Create drones
        replicateM_ droneLimit $
          addDrone
            params
            time
            (gp_playerRespawnDroneInvulnerabilityTicks (gp_combat params))
            store
            gen
            overseerID
            (Just deathTime)
            GroupDroneSpawn

-- Should be idempotent
removeOverseer ::
  GameParams -> EntityStore s -> Time -> EntityID 'Overseer -> ST s ()
removeOverseer params store time overseerID = do
  let Stores{..} = entityStore_dataStores store
  -- We must check for the absence of a 'ActorID', since the overseer may
  -- already be deleted.
  mActorID <- lookup store_actorEntity overseerID
  for_ mActorID $ \pid -> do
    -- Get components before removal
    hasFlag <- flag_hasFlag <$> sureLookup store_flag overseerID
    position <- dyn_pos <$> sureLookup store_dynamics overseerID
    team <- sureLookup store_team overseerID

    -- Remove components
    droneIDs <- runCoreM $ do
      delete store_team overseerID
      delete store_dynamics overseerID
      delete store_combat overseerID
      droneIDs <- unregisterMaster store_masterMinions overseerID
      delete store_flag overseerID
      delete store_actorEntity overseerID
      pure droneIDs

    -- Drop flag
    when hasFlag $ do
      -- Overseers carry the flags of the opposite team
      let flagTeam = oppositeTeam team
      void $ addFlag params store time flagTeam position

    -- Set overseer status
    modifySTRef' (entityStore_actors store) $
      over (ix pid . field @"ps_overseerStatus") $ \case
        OverseerAlive{} ->
          let respawnDelay = gp_overseerRespawnDelay (gp_combat params)
              respawnTime = addTicks respawnDelay time
          in  OverseerDead time respawnTime position
        ovDead@OverseerDead{} -> ovDead

    -- Destroy drones
    traverse_ (dissociateOverseerDrone params store time) droneIDs

    returnEntityID (entityStore_entityIDPool store) (subEntityID overseerID)

overseer_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
overseer_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = radius
      , pdyn_mass = C.massPerMaxHealth * gp_overseerMaxHealth (gp_combat params)
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
 where
  radius = gp_overseerRadius $ gp_dynamics params

overseer_newCombatComponent ::
  GameParams -> Time -> Time -> CombatComponent
overseer_newCombatComponent params time deathTime =
  CombatComponent
    { com_currentHealth = min maxHealth $ maxHealth * respawnHealthFraction
    , com_maxHealth = maxHealth
    , com_healthRegenType = HealthRegenQuadratic
    , com_invulnerableUntil =
        flip addTicks time $
          gp_playerRespawnOverseerInvulnerabilityTicks (gp_combat params)
    , com_lastRegenInterruptTime = deathTime
    , com_lastDamageTime = deathTime
    , com_lastDamageAmount = 0
    }
 where
  maxHealth = gp_overseerMaxHealth (gp_combat params)
  respawnHealthFraction =
    let baseFraction =
          gp_playerRespawnOverseerBonusHealthFraction (gp_combat params)
        regenFraction =
          sq $
            fromIntegral (time `diffTime` deathTime)
              / fromIntegral (gp_quadraticHealthFullRegenTicks (gp_combat params))
        sq x = x * x
    in  min 1 $ baseFraction + regenFraction

overseer_newFlagComponent :: Time -> FlagComponent
overseer_newFlagComponent time =
  FlagComponent
    { flag_hasFlag = False
    , flag_time = time
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- Drone entities

data DroneSpawnType = SingleDroneSpawn | GroupDroneSpawn

addDrone ::
  GameParams ->
  Time ->
  Ticks ->
  EntityStore s ->
  Gen s ->
  EntityID 'Overseer ->
  Maybe Time ->
  DroneSpawnType ->
  ST s ()
addDrone params time invulnTicks store gen overseerID mDeathTime spawnType = do
  let Stores{..} = entityStore_dataStores store
  -- Inherit overseer attributes
  overseerTeam <- sureLookup store_team overseerID
  overseerDynamicsComp <- sureLookup store_dynamics overseerID
  pid <- sureLookup store_actorEntity overseerID

  -- Get new EntityID
  droneID <- borrowEntityID @'Drone (entityStore_entityIDPool store)

  -- Register team component
  runCoreM $ insert store_team droneID overseerTeam

  -- Register dynamics component
  offsetDir <- Fixed.angle . Fixed.fromDouble <$> uniformR @Double (0, 2 * pi) gen
  let spawnRadius = 8 -- smaller spawn radius --> more drone overlap --> more repulsion at overseer spawn
      pos = dyn_pos overseerDynamicsComp + Fixed.map (* spawnRadius) offsetDir
      vel =
        case spawnType of
          SingleDroneSpawn -> dyn_vel overseerDynamicsComp
          GroupDroneSpawn -> dyn_vel overseerDynamicsComp + Fixed.map (* 2.5) offsetDir
  runCoreM $
    insert store_dynamics droneID (drone_newDynamicsComponent params pos vel)

  runCoreM $ do
    -- Register combat component
    insert store_combat droneID $
      drone_newCombatComponent
        params
        time
        invulnTicks
        mDeathTime

    -- Register minion component
    registerMinion
      store_masterMinions
      droneID
      (MinionComponent overseerID)

    -- Register actor entity component
    insert store_actorEntity droneID pid

-- Should be idempotent
removeDrone :: EntityStore s -> Time -> EntityID 'Drone -> ST s ()
removeDrone store time droneID = do
  let Stores{..} = entityStore_dataStores store
      eid = subEntityID droneID
  runCoreM $ do
    delete store_team droneID
    delete store_dynamics droneID
    delete store_combat droneID
    unregisterMinion store_masterMinions time droneID
    delete store_actorEntity droneID

  returnEntityID (entityStore_entityIDPool store) eid

-- Modify a drone when its actor leaves the game. Should be idempotent.
dissociateActorDrone ::
  GameParams -> EntityStore s -> Time -> EntityID 'Drone -> ST s ()
dissociateActorDrone params store time droneID = do
  dissociateOverseerDrone params store time droneID
  let Stores{..} = entityStore_dataStores store
  -- Remove actor entity component
  runCoreM $ delete store_actorEntity droneID

-- Modify a drone when its overseer dies. Should be idempotent.
dissociateOverseerDrone ::
  GameParams -> EntityStore s -> Time -> EntityID 'Drone -> ST s ()
dissociateOverseerDrone params store time droneID = do
  let Stores{..} = entityStore_dataStores store
  -- Unregister as a minion
  runCoreM $ unregisterMinion store_masterMinions time droneID
  -- Set health decay
  setHealthRegenType store_combat droneID $
    HealthRegenLinear $
      negate $
        gp_disassociatedDroneDecayFactor params -- set negative health regen

drone_newDynamicsComponent ::
  GameParams -> Fixed2 -> Fixed2 -> DynamicsComponent
drone_newDynamicsComponent params pos vel =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = radius
      , pdyn_mass = C.massPerMaxHealth * gp_droneMaxHealth (gp_combat params)
      , pdyn_pos = pos
      , pdyn_vel = vel
      }
 where
  radius = gp_droneRadius $ gp_dynamics params

drone_newCombatComponent ::
  GameParams ->
  Time ->
  Ticks ->
  Maybe Time ->
  CombatComponent
drone_newCombatComponent params time invulnTicks mDeathTime =
  CombatComponent
    { com_currentHealth = min maxHealth respawnHealth
    , com_maxHealth = maxHealth
    , com_healthRegenType =
        HealthRegenExponential $
          gp_droneHealthRegenPeriodFactor (gp_combat params)
    , com_invulnerableUntil = addTicks invulnTicks time
    , com_lastRegenInterruptTime = initialTime
    , com_lastDamageTime = initialTime
    , com_lastDamageAmount = 0
    }
 where
  maxHealth = gp_droneMaxHealth (gp_combat params)
  respawnHealth = case mDeathTime of
    Nothing -> gp_droneRespawnHealth (gp_combat params)
    Just deathTime ->
      let combatParams = gp_combat params
          timeConstantTicks = gp_droneHealthRegenTimeConstantTicks combatParams
          ticksSinceDeath = fromIntegral $ time `diffTime` deathTime
      in  gp_droneMaxHealth combatParams
            * (1 - exp (ticksSinceDeath / timeConstantTicks))

-- Soft obstacle entities

addSoftObstacle ::
  GameParams ->
  EntityStore s ->
  Maybe Fixed ->
  Fixed2 ->
  Maybe Fixed ->
  ST s ()
addSoftObstacle params store velAngle pos mRadius = do
  let Stores{..} = entityStore_dataStores store
  obstacleID <- borrowEntityID @'SoftObstacle (entityStore_entityIDPool store)

  runCoreM $
    insert store_dynamics obstacleID $
      softObstacle_newDynamicsComponent params pos velAngle mRadius

-- Should be idempotent
removeSoftObstacle :: EntityStore s -> EntityID 'SoftObstacle -> ST s ()
removeSoftObstacle store obstacleID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ delete store_dynamics obstacleID

  returnEntityID (entityStore_entityIDPool store) (subEntityID obstacleID)

softObstacle_newDynamicsComponent ::
  GameParams ->
  Fixed2 ->
  Maybe Fixed ->
  Maybe Fixed ->
  DynamicsComponent
softObstacle_newDynamicsComponent params pos mVelAngle mRadius =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = obstacleRadius
      , pdyn_mass =
          square (obstacleRadius * C.sqrtDensity) * softObstacleDenstyFactor
      , pdyn_pos = pos
      , pdyn_vel =
          case mVelAngle of
            Nothing -> 0
            Just velAngle ->
              Fixed.map (* gp_softObstacleAverageSpeed (gp_dynamics params)) $
                Fixed2 (cos velAngle) (sin velAngle)
      }
 where
  obstacleRadius =
    fromMaybe (gp_softObstacleRadius (gp_dynamics params)) mRadius
  softObstacleDenstyFactor =
    gp_softObstacleDensityFactor (gp_dynamics params)

-- Flag entities

addFlag :: GameParams -> EntityStore s -> Time -> Team -> Fixed2 -> ST s ()
addFlag params store time team pos = do
  flagID <- borrowEntityID @'Flag (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_team flagID team
    insert store_dynamics flagID $
      flag_newDynamicsComponent params pos
    insert store_flag flagID $ flag_newFlagComponent time

-- Should be idempotent
removeFlag :: EntityStore s -> EntityID 'Flag -> ST s ()
removeFlag store flagID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_team flagID
    delete store_dynamics flagID
    delete store_flag flagID

  returnEntityID (entityStore_entityIDPool store) (subEntityID flagID)

flag_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
flag_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_flagRadius (gp_dynamics params)
      , pdyn_mass = square $ gp_flagRadius (gp_dynamics params) * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }

flag_newFlagComponent :: Time -> FlagComponent
flag_newFlagComponent time =
  FlagComponent
    { flag_hasFlag = True
    , flag_time = time
    , flag_ticks = 0
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- Base flag entities

addBaseFlag :: GameParams -> EntityStore s -> Team -> Fixed2 -> ST s ()
addBaseFlag params store team pos = do
  flagID <- borrowEntityID @'BaseFlag (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_team flagID team
    insert store_dynamics flagID $
      baseFlag_newDynamicsComponent params pos
    insert store_flag flagID baseFlag_newFlagComponent

-- Should be idempotent
removeBaseFlag :: EntityStore s -> EntityID 'BaseFlag -> ST s ()
removeBaseFlag store flagID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_team flagID
    delete store_dynamics flagID
    delete store_flag flagID

  returnEntityID (entityStore_entityIDPool store) (subEntityID flagID)

baseFlag_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
baseFlag_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = radius
      , pdyn_mass = square $ radius * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
 where
  radius = gp_flagRadius $ gp_dynamics params

baseFlag_newFlagComponent :: FlagComponent
baseFlag_newFlagComponent =
  FlagComponent
    { flag_hasFlag = True
    , flag_time = initialTime -- dummy value
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- TeamBase entities

addTeamBase :: GameParams -> EntityStore s -> Team -> Fixed2 -> ST s ()
addTeamBase params store team pos = do
  teamBaseID <- borrowEntityID @'TeamBase (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_team teamBaseID team
    insert store_dynamics teamBaseID $
      teamBase_newDynamicsComponent params pos
    insert store_flag teamBaseID teamBase_newFlagComponent

-- Should be idempotent
removeTeamBase :: EntityStore s -> EntityID 'TeamBase -> ST s ()
removeTeamBase store teamBaseID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_team teamBaseID
    delete store_dynamics teamBaseID
    delete store_flag teamBaseID

  returnEntityID (entityStore_entityIDPool store) (subEntityID teamBaseID)

teamBase_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
teamBase_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = baseRadius
      , pdyn_mass = square $ baseRadius * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
 where
  baseRadius = gp_teamBaseRadius (gp_dynamics params)

teamBase_newFlagComponent :: FlagComponent
teamBase_newFlagComponent =
  FlagComponent
    { flag_hasFlag = False
    , flag_time = initialTime -- dummy value
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- Psi storm entities

addPsiStorm ::
  CastParams ->
  EntityStore s ->
  Time ->
  Maybe ActorID ->
  Fixed2 ->
  ST s ()
addPsiStorm params store time mActorID targetPos = do
  psiStormID <- borrowEntityID @'PsiStorm (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_dynamics psiStormID $ psiStorm_newDynamicsComponent targetPos
    insert store_transitory psiStormID $
      addTicks (gp_psiStormLifetimeTicks params) time
    for_ mActorID $ insert store_actorEntity psiStormID
    insert store_psiStorm psiStormID ()

-- Should be idempotent
removePsiStorm :: EntityStore s -> EntityID 'PsiStorm -> ST s ()
removePsiStorm store psiStormID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_dynamics psiStormID
    delete store_transitory psiStormID
    delete store_actorEntity psiStormID
    delete store_psiStorm psiStormID

  returnEntityID (entityStore_entityIDPool store) (subEntityID psiStormID)

-- Should be idempotent
dissociateActorPsiStorm :: EntityStore s -> EntityID 'PsiStorm -> ST s ()
dissociateActorPsiStorm store psiStormID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ delete store_actorEntity psiStormID

psiStorm_newDynamicsComponent :: Fixed2 -> DynamicsComponent
psiStorm_newDynamicsComponent pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = 0 -- initial radius
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }

-- Explosion entities

addExplosion ::
  DynamicsParams ->
  EntityStore s ->
  Time ->
  Fixed2 ->
  ST s ()
addExplosion params store time pos = do
  explosionID <- borrowEntityID @'Explosion (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_dynamics explosionID $
      explosion_newDynamicsComponent params pos
    insert store_transitory explosionID $
      addTicks (gp_overseerExplosionDuration params) time

-- Should be idempotent
removeExplosion :: EntityStore s -> EntityID 'Explosion -> ST s ()
removeExplosion store explosionID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_dynamics explosionID
    delete store_transitory explosionID

  returnEntityID (entityStore_entityIDPool store) (subEntityID explosionID)

explosion_newDynamicsComponent :: DynamicsParams -> Fixed2 -> DynamicsComponent
explosion_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_overseerExplosionRadius params
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }

-- Team spawn entities

addSpawnArea ::
  GameParams ->
  EntityStore s ->
  Team ->
  ST s ()
addSpawnArea params store team = do
  spawnAreaID <- borrowEntityID @'SpawnArea (entityStore_entityIDPool store)

  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    insert store_dynamics spawnAreaID $
      spawnArea_newDynamicsComponent params team
    insert store_team spawnAreaID team

-- Should be idempotent
removeSpawnArea :: EntityStore s -> EntityID 'SpawnArea -> ST s ()
removeSpawnArea store spawnAreaID = do
  let Stores{..} = entityStore_dataStores store
  runCoreM $ do
    delete store_dynamics spawnAreaID
    delete store_team spawnAreaID

  returnEntityID (entityStore_entityIDPool store) (subEntityID spawnAreaID)

spawnArea_newDynamicsComponent :: GameParams -> Team -> DynamicsComponent
spawnArea_newDynamicsComponent params team =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_spawnAreaRadius (gp_dynamics params)
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }
 where
  pos = spawnAreaCenter params team

--------------------------------------------------------------------------------
-- Overseer respawn helpers

-- | Return a random spawn position for a new overseer based on its team.
randomOverseerSpawnPosition :: GameParams -> MWC.Gen s -> Team -> ST s Fixed2
randomOverseerSpawnPosition params gen team =
  (+ spawnAreaCenter params team) . Fixed.map (* bufferedSpawnAreaRadius)
    <$> uniformUnitDisc gen
 where
  spawnAreaRadius = gp_spawnAreaRadius (gp_dynamics params)
  buffer = 1 + gp_overseerRadius (gp_dynamics params)
  bufferedSpawnAreaRadius = spawnAreaRadius - buffer

--------------------------------------------------------------------------------
-- Misc. helpers

square :: Num a => a -> a
square x = x * x
