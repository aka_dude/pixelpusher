-- | Export entity data for rendering
module Pixelpusher.Game.EntityStore.Rendering (
  getRenderingComponents,
  renderingComponent,
) where

import Control.Monad.ST (ST)
import Data.List qualified as List
import Data.Maybe (fromMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float3 (Float3))
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.CombatSystem
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.DynamicsSystem
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.FlagSystem hiding (getFlags)
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.TeamSystem
import Pixelpusher.Game.Time
import Pixelpusher.Game.TransitorySystem
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.EntityStore.Core (Stores (..))
import Pixelpusher.Game.EntityStore.Core qualified as Store

--------------------------------------------------------------------------------

-- | Export entity data for rendering.
--
-- Filters data by entity type and view position.
getRenderingComponents ::
  GameParams ->
  Stores s ->
  Time ->
  Maybe Fixed2 ->
  WIM.IntMap ActorID (Player, PlayerStatus) ->
  ST s RenderingComponents
getRenderingComponents params stores time mViewPos playersMap =
  let viewPredicate' =
        case mViewPos of
          Nothing -> \_ _ _ -> True
          Just viewPos -> \entityID radius pos ->
            entityViewPredicate entityID
              || isInOverseerView C.viewRadius viewPos radius pos
  in  fmap (partitionRenderingComponents . concat) $
        traverse renderingComponent'
          =<< getDynamicsComponentsFiltered
            (store_dynamics stores)
            viewPredicate'
 where
  renderingComponent' =
    renderingComponent
      params
      (Store.readOnly (store_team stores))
      (Store.readOnly (store_combat stores))
      (Store.readOnly (store_transitory stores))
      (readFlagStore (store_flag stores))
      (Store.readOnly (store_actorEntity stores))
      (store_masterMinions stores)
      time
      playersMap

isInOverseerView :: Fixed -> Fixed2 -> Fixed -> Fixed2 -> Bool
isInOverseerView viewRadius overseerPos objRadius objPos =
  Fixed.normLeq (overseerPos - objPos) (viewRadius + objRadius)

renderingComponent ::
  GameParams ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (CombatStore s) ->
  Store.ReadOnly (TransitoryStore s) ->
  FlagReadStore s ->
  Store.ReadOnly (ActorEntityStore s) ->
  MasterMinionStore s ->
  Time ->
  WIM.IntMap ActorID (Player, PlayerStatus) ->
  (SEntityID 'DynamicsEntity, DynamicsComponent) ->
  ST s [RenderingComponent EntityRendering]
renderingComponent
  params
  teamComps
  combatComps
  transitoryComps
  flagComps
  actorComps
  masterMinionComps
  time
  playersMap
  (dynID, dynamicsComp) =
    case refineEntityID dynID of
      OverseerID overseerID -> do
        actorID <- Store.sureLookup actorComps overseerID
        combatComp <- Store.sureLookup combatComps overseerID
        hasFlag <- readHasFlag flagComps overseerID
        let (player, playerStatus) =
              expectJust "renderingComponent: actorID not found" $
                WIM.lookup actorID playersMap
            playerColor =
              PlayerColor
                (player ^. player_team)
                (player ^. player_color)
                actorID
            ticksSinceDash =
              let dashCooldownTicks =
                    gp_overseerDashCooldownTicks $ gp_cast params
                  cooldownUntil =
                    pc_overseerDashCooldownUntil1 $ ps_cooldowns playerStatus
                  castTime = subtractTicks dashCooldownTicks cooldownUntil
              in  time `diffTime` castTime
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_Overseer $
            OverseerRendering
              { or_team = view player_team player
              , or_color = playerColor
              , or_healthFraction = Fixed.toFloat $ healthFraction combatComp
              , or_recentDamage = Fixed.toFloat $ recentDamage time combatComp
              , or_actorID = actorID
              , or_playerName = view player_name player
              , or_prevPos1 = Fixed.toFloats $ dyn_pos_prev dynamicsComp
              , or_prevPos2 = Fixed.toFloats $ dyn_pos_prev2 dynamicsComp
              , or_prevPos3 = Fixed.toFloats $ dyn_pos_prev3 dynamicsComp
              , or_prevPos4 = Fixed.toFloats $ dyn_pos_prev4 dynamicsComp
              , or_prevPos5 = Fixed.toFloats $ dyn_pos_prev5 dynamicsComp
              , or_prevPos6 = Fixed.toFloats $ dyn_pos_prev6 dynamicsComp
              , or_hasFlag = hasFlag
              , or_psiStormCooldownUntil =
                  pc_psiStormCooldownUntil $ ps_cooldowns playerStatus
              , or_droneDashCooldownUntil1 =
                  pc_overseerDashCooldownUntil1 $ ps_cooldowns playerStatus
              , or_droneDashCooldownUntil2 =
                  pc_overseerDashCooldownUntil2 $ ps_cooldowns playerStatus
              , or_isInvulnerable = isInvulnerable time combatComp
              , or_entityID = getIndex overseerID
              , or_ticksSinceDash = Just ticksSinceDash
              }
      DroneID droneID -> do
        mActorID <- Store.lookup actorComps droneID
        combatComp <- Store.sureLookup combatComps droneID
        team <- Store.sureLookup teamComps droneID
        mOverseerID <- lookupMaster masterMinionComps droneID
        let mPlayerAndStatus =
              mActorID <&> \actorID ->
                expectJust "renderingComponent: actorID not found" $
                  WIM.lookup actorID playersMap
            colorRep =
              fromMaybe (Left team) $ do
                _ <- mOverseerID -- Use generic team color if drone has no overseer
                (player, _) <- mPlayerAndStatus
                pure $
                  Right $
                    PlayerColor
                      (player ^. player_team)
                      (player ^. player_color)
                      (player ^. player_actorID)
            mLastDash =
              mPlayerAndStatus <&> \(_, playerStatus) ->
                let (castTime, intensity) =
                      pc_lastDroneDash (ps_cooldowns playerStatus)
                    ticksSinceDash = time `diffTime` castTime
                in  (ticksSinceDash, Fixed.toFloat intensity)
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_Drone $
            DroneRendering
              { dr_color = colorRep
              , dr_healthFraction = Fixed.toFloat $ healthFraction combatComp
              , dr_recentDamage = Fixed.toFloat $ recentDamage time combatComp
              , dr_actorID = mActorID
              , dr_prevPos1 = Fixed.toFloats $ dyn_pos_prev dynamicsComp
              , dr_prevPos2 = Fixed.toFloats $ dyn_pos_prev2 dynamicsComp
              , dr_prevPos3 = Fixed.toFloats $ dyn_pos_prev3 dynamicsComp
              , dr_prevPos4 = Fixed.toFloats $ dyn_pos_prev4 dynamicsComp
              , dr_prevPos5 = Fixed.toFloats $ dyn_pos_prev5 dynamicsComp
              , dr_prevPos6 = Fixed.toFloats $ dyn_pos_prev6 dynamicsComp
              , dr_isInvulnerable = isInvulnerable time combatComp
              , dr_entityID = getIndex droneID
              , dr_team = team
              , dr_ticksSinceDash = mLastDash
              }
      SoftObstacleID _softObstacleID -> do
        let color = Float3 1 1 1
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_SoftObstacle $
            SoftObstacleRendering color
      PsiStormID psiStormID -> do
        endTime <- Store.sureLookup transitoryComps psiStormID
        mActorID <- Store.lookup actorComps psiStormID
        let mPlayerColor = do
              actorID <- mActorID
              (player, _playerStatus) <- WIM.lookup actorID playersMap
              pure $
                PlayerColor
                  (player ^. player_team)
                  (player ^. player_color)
                  actorID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_PsiStorm $
            PsiStormRendering endTime mPlayerColor
      FlagID flagID -> do
        team <- Store.sureLookup teamComps flagID
        flagComp <- Store.sureLookup flagComps flagID
        pure $
          let flagParams = gp_flags params
              flagRecoveryTicks = gp_flagRecoveryTicks flagParams
              progressTicks =
                min flagRecoveryTicks $
                  accumulatedFlagTicks flagParams time flagComp
              progressFraction =
                fromIntegral progressTicks / fromIntegral flagRecoveryTicks
              flagRendering =
                defaultRenderingComponent dynamicsComp $
                  ER_Flag $
                    FlagRendering{fr_team = team}
              flagCaptureAreaRendering =
                RenderingComponent
                  { rendering_entityRendering =
                      ER_FlagCaptureArea $
                        FlagCaptureAreaRendering
                          { fcar_team = team
                          , fcar_progress = progressFraction
                          , fcar_recoveringFlag =
                              flagConditionStatus time flagComp
                          }
                  , rendering_pos = Fixed.toFloats $ dyn_pos dynamicsComp
                  , rendering_radius =
                      Fixed.toFloat $
                        gp_flagRecoveryRadius $
                          gp_dynamics params
                  }
          in  [flagRendering, flagCaptureAreaRendering]
      TeamBaseID teamBaseID -> do
        flagComp <- Store.sureLookup flagComps teamBaseID
        team <- Store.sureLookup teamComps teamBaseID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_TeamBase $
            TeamBaseRendering
              { tbr_team = team
              , tbr_flagDepositPending = flagConditionStatus time flagComp
              }
      BaseFlagID baseFlagID ->
        Store.sureLookup teamComps baseFlagID <&> \team ->
          List.singleton . defaultRenderingComponent dynamicsComp $
            ER_BaseFlag (BaseFlagRendering team)
      ExplosionID _explosionID ->
        pure $ List.singleton $ defaultRenderingComponent dynamicsComp ER_NoVisual
      SpawnAreaID spawnAreaID -> do
        team <- Store.sureLookup teamComps spawnAreaID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_SpawnArea (SpawnAreaRendering team)

defaultRenderingComponent :: DynamicsComponent -> a -> RenderingComponent a
defaultRenderingComponent dynamicsComp a =
  RenderingComponent
    { rendering_entityRendering = a
    , rendering_pos = Fixed.toFloats $ dyn_pos dynamicsComp
    , rendering_radius = Fixed.toFloat $ dyn_radius dynamicsComp
    }
