{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE UndecidableInstances #-}

module Pixelpusher.Game.EntityStore.Core where

import Control.Monad.ST (ST)
import Data.Kind (Type)
import Pixelpusher.Store.EntityID

--------------------------------------------------------------------------------
-- Store action context

type CoreM :: Type -> Type -> Type
data CoreM s a
type role CoreM nominal representational

instance Functor (CoreM s)
instance Applicative (CoreM s)
instance Monad (CoreM s)

coreM :: ST s a -> CoreM s a

--------------------------------------------------------------------------------
-- Store interface

class Store s idx elem store | store -> idx , store -> elem where
  insert :: (SubEntityID i idx) => store -> i -> elem -> CoreM s ()
  delete :: (SubEntityID i idx) => store -> i -> CoreM s ()
  clear :: store -> CoreM s ()

class ReadStore s idx elem store | store -> idx, store -> elem where
  lookup :: (SubEntityID i idx) => store -> i -> ST s (Maybe elem)

class SureReadStore s sidx elem store | store -> sidx, store -> elem where
  sureLookup :: (SubEntityID i sidx) => store -> i -> ST s elem

--------------------------------------------------------------------------------
-- Read-only store interface

type ReadOnly :: Type -> Type
data ReadOnly store

readOnly :: store -> ReadOnly store

instance (ReadStore s idx elem store) => ReadStore s idx elem (ReadOnly store)

instance
  (SureReadStore s sidx elem store) =>
  SureReadStore s sidx elem (ReadOnly store)
