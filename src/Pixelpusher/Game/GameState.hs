{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

-- | This module encapsulates the game logic.
--
-- This module is not well organized, and the boundary between this module and
-- "Pixelpusher.Game.EntityStore" is arbitrary.
module Pixelpusher.Game.GameState (
  Player,
  player_controls,
  player_stats,
  player_aggregateStats,
  player_name,
  player_team,
  player_actorID,
  GameState,
  initializeGameState,
  initializeGameStateWithGen,
  getGameTime,
  getGamePlayers,
  getGameScene,
  GameStateSnapshot,
  getSnapshotTime,
  integrateGameState,
) where

import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.ST
import Control.Monad.State.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict (StateT (StateT), runStateT)
import Data.Bifunctor (bimap, second)
import Data.Either (partitionEithers)
import Data.Foldable (foldl', for_, toList)
import Data.Generics.Product.Fields
import Data.Generics.Sum.Constructors
import Data.Int (Int64)
import Data.IntMap.Merge.Strict qualified as IntMap
import Data.Maybe (catMaybes, fromMaybe, mapMaybe, maybeToList)
import Data.STRef
import Data.Serialize (Serialize)
import Data.Vector qualified as V
import Data.Vector.Unboxed qualified as VU
import Data.Word (Word32)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC
import System.Random.MWC.Distributions qualified as MWC

import Pixelpusher.Custom.DelayQueue
import Pixelpusher.Custom.Fixed (Fixed, Fixed2, Fixed4 (Fixed4))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.Util (expectJust, uniformUnitDisc)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.BotID
import Pixelpusher.Game.Bots.AI
import Pixelpusher.Game.Bots.View (BotView)
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.CombatEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.EntityStore qualified as ES
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.GameCommand
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles qualified as P
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerID (PlayerID)
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStats
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Stats
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamScore
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------
-- Game state types

-- | The entirety of the game state. Not deriving a `Generic` instance to
-- preserve the abstraction.
data GameState s = GameState
  { game_pure :: STRef s GameStatePure
  , game_store :: ES.EntityStore s
  , game_prng :: MWC.Gen s
  }

-- | The pure portion of the game state.
data GameStatePure = GameStatePure
  { gamePure_params :: GameParams
  , gamePure_time :: Time
  , gamePure_roundStart :: Time
  , gamePure_phase :: GamePhase
  , gamePure_players :: WIM.IntMap ActorID Player
  , gamePure_particles :: P.ParticleSystem
  , gamePure_botIDPool :: BotIDPool
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- Extra `GameStatePure` lenses

gamePure_status_players ::
  Lens' GameStatePure (GamePhase, WIM.IntMap ActorID Player)
gamePure_status_players =
  unsafeLensProduct (field @"gamePure_phase") (field @"gamePure_players")

unsafeLensProduct :: Lens' s a -> Lens' s b -> Lens' s (a, b)
unsafeLensProduct l r = lens getter setter
 where
  getter = view l &&& view r
  setter s (a, b) = s & set l a . set r b

--------------------------------------------------------------------------------
-- Game state initialization

-- | Initialize the game state, seeding the PRNG with the system's PRNG.
initializeGameState :: BaseGameParams -> IO (GameState RealWorld)
initializeGameState params =
  MWC.createSystemRandom >>= stToIO . initializeGameStateWithGen params

-- | Initialize the game state with given a PRNG.
initializeGameStateWithGen :: BaseGameParams -> MWC.Gen s -> ST s (GameState s)
initializeGameStateWithGen baseParams gen = do
  let params = makeGameParams 0 baseParams
  entityStore <- ES.initialStore
  let initialState = initialGameStatePure params
  ref <- newSTRef initialState
  spawnInitialEntities params gen entityStore
  pure $
    GameState
      { game_pure = ref
      , game_store = entityStore
      , game_prng = gen
      }

initialGameStatePure :: GameParams -> GameStatePure
initialGameStatePure params =
  GameStatePure
    { gamePure_params = params
    , gamePure_time = t0
    , gamePure_roundStart = initialTime
    , gamePure_phase = GamePhase_Game t0 zeroTeamScore
    , gamePure_players = WIM.empty
    , gamePure_particles = P.new
    , gamePure_botIDPool = newBotIDPool
    }
 where
  t0 = initialTime

spawnInitialEntities :: GameParams -> MWC.Gen s -> ES.EntityStore s -> ST s ()
spawnInitialEntities params gen store = do
  spawnDriftingSoftObstacles params gen store
  spawnTeamBasesAndFlags params store

spawnDriftingSoftObstacles ::
  GameParams -> MWC.Gen s -> ES.EntityStore s -> ST s ()
spawnDriftingSoftObstacles params gen store = do
  let (nPairs, nCenterObstacles) = gp_numSoftObstacles params `divMod` 2
      buffer = 1.5 * gp_softObstacleRadius (gp_dynamics params)
      spawnRadius = max 1 $ gp_worldRadius (gp_dynamics params) - buffer
  replicateM_ nCenterObstacles $ do
    let pos = 0
        velAngle = Nothing
    void $ ES.addSoftObstacle params store velAngle pos Nothing
  replicateM_ nPairs $ do
    pos <- Fixed.map (* spawnRadius) <$> uniformUnitDisc gen
    velAngle <- MWC.uniformR (0, 2 * pi) gen
    void $ ES.addSoftObstacle params store (Just velAngle) pos Nothing
    void $ ES.addSoftObstacle params store (Just (velAngle + pi)) (-pos) Nothing

spawnTeamBasesAndFlags :: GameParams -> ES.EntityStore s -> ST s ()
spawnTeamBasesAndFlags params store = do
  ES.spawnTeamBase params store TeamNW
  ES.spawnTeamBase params store TeamSE
  ES.spawnBaseFlag params store TeamNW
  ES.spawnBaseFlag params store TeamSE
  ES.spawnSpawnArea params store TeamNW
  ES.spawnSpawnArea params store TeamSE

--------------------------------------------------------------------------------
-- Game state accessors

getPureGameState :: GameState s -> ST s GameStatePure
getPureGameState = readSTRef . game_pure

getGameTime :: GameState s -> ST s Time
getGameTime = fmap gamePure_time . getPureGameState

getGamePlayers :: GameState s -> ST s (WIM.IntMap ActorID Player)
getGamePlayers = fmap gamePure_players . getPureGameState

getGameScene :: GameState s -> Maybe ActorID -> ST s GameScene
getGameScene gameState mActorID = do
  gameStatePure <- readSTRef $ game_pure gameState
  flags <- getFlags gameState
  renderingComps <- getRenderingComponents gameState mActorID
  playerViews <- ES.viewPlayers $ game_store gameState
  pure $
    GameScene
      { scene_gameParams = gamePure_params gameStatePure
      , scene_time = gamePure_time gameStatePure
      , scene_roundStart = gamePure_roundStart gameStatePure
      , scene_renderingComps = renderingComps
      , scene_gamePhase = gamePure_phase gameStatePure
      , scene_particles = P.getParticles $ gamePure_particles gameStatePure
      , scene_players =
          let errMsg = "getGameScene: Players and PlayerViews do not align"
          in  WIM.merge
                (IntMap.mapMissing (const (const (error errMsg))))
                (IntMap.mapMissing (const (const (error errMsg))))
                (IntMap.zipWithMatched (const (,)))
                (gamePure_players gameStatePure)
                playerViews
      , scene_flags = flags
      }

getFlags :: GameState s -> ST s [(Team, Fixed2, FlagType)]
getFlags gameState = ES.getFlags $ game_store gameState

getRenderingComponents ::
  GameState s -> Maybe ActorID -> ST s RenderingComponents
getRenderingComponents gameState mActorID = do
  gameStatePure <- readSTRef $ game_pure gameState
  ES.getRenderingComponents
    (gamePure_params gameStatePure)
    (game_store gameState)
    (gamePure_time gameStatePure)
    mActorID
    (gamePure_players gameStatePure)

--------------------------------------------------------------------------------
-- Game state serialization

data GameStateSnapshot = GameStateSnapshot
  { gameSnapshot_pure :: GameStatePure
  , gameSnapshot_store :: ES.EntityStoreSnapshot
  , gameSnapshot_prng :: VU.Vector Word32
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

getSnapshotTime :: GameStateSnapshot -> Time
getSnapshotTime = gamePure_time . gameSnapshot_pure

instance Mutable s GameStateSnapshot (GameState s) where
  freeze gameState =
    GameStateSnapshot
      <$> freeze (game_pure gameState)
      <*> freeze (game_store gameState)
      <*> freeze (game_prng gameState)
  thaw snapshot =
    GameState
      <$> thaw (gameSnapshot_pure snapshot)
      <*> thaw (gameSnapshot_store snapshot)
      <*> thaw (gameSnapshot_prng snapshot)

-- Not derived because we're avoiding a `Generic` instance on `GameState`.
-- Pattern-matching to make it harder to forget to copy a field.
instance Copyable s (GameState s) where
  copy
    (GameState target_pure target_store target_prng)
    (GameState source_pure source_store source_prng) =
      do
        copy target_pure source_pure
        copy target_store source_store
        copy target_prng source_prng

--------------------------------------------------------------------------------
-- Game state integration

-- | Step the game state to the next time step.
integrateGameState ::
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  GameState s ->
  ST s GameEvents
integrateGameState newPlayerControls gameCmds gameState =
  withSTRef (game_pure gameState) $
    integrateGameState'
      (game_prng gameState)
      (game_store gameState)
      newPlayerControls
      gameCmds

withSTRef :: STRef s a -> (a -> ST s (b, a)) -> ST s b
withSTRef ref f =
  readSTRef ref >>= f >>= \(b, a) -> seq a (writeSTRef ref a) >> pure b

integrateGameState' ::
  MWC.Gen s ->
  ES.EntityStore s ->
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  GameStatePure ->
  ST s (GameEvents, GameStatePure)
integrateGameState' gen store newPlayerControls gameCmds gameStatePure =
  flip runStateT gameStatePure $
    integrateGameState'' gen store newPlayerControls gameCmds

integrateGameState'' ::
  MWC.Gen s ->
  ES.EntityStore s ->
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  StateT GameStatePure (ST s) GameEvents
integrateGameState'' gen store newPlayerControls gameCmds = do
  time <- field @"gamePure_time" <%= nextTick
  params <- gets gamePure_params

  -- Apply game commands
  paramsChangedEvents <-
    catMaybes <$> mapM (runGameCommand gen store time) gameCmds

  -- Add/remove bots
  when (time `atMultipleOf` Ticks 60) $
    maintainBotNumbers params gen store time

  -- Update player controls (and maybe also apply bot controls??)
  players <-
    zoom (field @"gamePure_players") $
      modifyReturn' $
        updateWith'
          ( \newControls player ->
              player
                & set (field @"player_controls") newControls
                  . set (field @"player_prevControls") (player_controls player)
          )
          (WIM.mapKeys PlayerActor newPlayerControls)

  -- Step simulation
  ES.EntityStepEvents{..} <- do
    gamePhase <- gets gamePure_phase
    lift $
      ES.stepEntities
        params
        store
        gen
        time
        players
        gamePhase

  -- Update bot controls
  zoom (field @"gamePure_players") $
    StateT $
      fmap ((),) . updateBotControls params gen ese_botViews

  -- Update particles
  do
    gamePhase <- gets gamePure_phase
    zoom (field @"gamePure_particles") $
      updateParticles
        params
        gamePhase
        time
        players
        ese_overseerDeaths
        ese_droneDeaths
        ese_collisions
        ese_dashCasts
        ese_psiStormCasts
        ese_psiStormEffects
        ese_flagEvents
        ese_flagDropEvents
        paramsChangedEvents

  -- Update score
  field @"gamePure_phase"
    . _Ctor @"GamePhase_Game"
    . _2
    %= (ese_scoreIncrement <>)

  -- Record stats
  -- Note: Stats should only be updated while the game is in session, that is,
  -- not during intermission. Maybe I should better enforce this ...
  gets gamePure_phase >>= \case
    GamePhase_Intermission{} -> pure ()
    GamePhase_Game{} ->
      zoom (field @"gamePure_players") $
        recordStats
          ese_overseerDeaths
          ese_overseerHits
          ese_flagEvents

  -- Game status transitions
  -- Note: Game status transitions may change the game parameters. As such,
  -- they performed at the end of the step to avoid inconsistency related to
  -- changing game parameters part way through a step.
  mGamePhaseTransition <-
    zoom gamePure_status_players $
      transitionGamePhase params time
  for_ mGamePhaseTransition $ \case
    MatchEnd -> pure ()
    PlayMatchEndSfx -> pure ()
    IntermissionEnding -> pure ()
    GameRestart -> newRound gen store

  pure
    GameEvents
      { ge_gamePhaseTransitions = maybeToList mGamePhaseTransition
      , ge_collisions = ese_collisions
      , ge_neutralCollisions = ese_neutralCollisions
      , ge_overseerDeaths = ese_overseerDeaths
      , ge_dashes = ese_dashCasts
      , ge_psiStormCasts = ese_psiStormCasts
      , ge_psiStormEffects = ese_psiStormEffects
      , ge_flagCaptures = flagEvents_captures ese_flagEvents
      , ge_flagPickups = flagEvents_pickups ese_flagEvents
      , ge_flagRecoveries = flagEvents_recoveries ese_flagEvents
      }

--------------------------------------------------------------------------------
-- Game status transitions

transitionGamePhase ::
  (Monad m) =>
  GameParams ->
  Time ->
  StateT (GamePhase, WIM.IntMap ActorID Player) m (Maybe GamePhaseTransition)
transitionGamePhase params time =
  use _1 >>= \case
    GamePhase_Game roundStartTime teamScore -> do
      let scoreNW = teamScore_nw teamScore
          scoreSE = teamScore_se teamScore
          requiredScore = gp_flagsToWin params
      let mWinningTeam
            | scoreNW >= requiredScore = Just TeamNW
            | scoreSE >= requiredScore = Just TeamSE
            | otherwise = Nothing
      case mWinningTeam of
        Nothing -> pure Nothing
        Just winningTeam ->
          let minIntermissionTicks =
                fromIntegral (gp_minIntermissionTicks params) :: Fixed
              maxIntermissionTicks =
                fromIntegral (gp_maxIntermissionTicks params)
              shortMatchSeconds = 90 -- 1:30
              longMatchSeconds = 60 * 6 -- 6:00
              matchDurationSeconds =
                fromIntegral (time `diffTime` roundStartTime)
                  `div` C.tickRate_hz
              lengthFactor =
                max 0 . min 1 $
                  (fromIntegral matchDurationSeconds - shortMatchSeconds)
                    / (longMatchSeconds - shortMatchSeconds)
              intermissionTicks =
                round $
                  minIntermissionTicks
                    + lengthFactor * (maxIntermissionTicks - minIntermissionTicks)
              nextRoundTime = addTicks (Ticks intermissionTicks) time
          in  do
                _1
                  .= GamePhase_Intermission
                    time
                    nextRoundTime
                    winningTeam
                    teamScore
                pure $ Just MatchEnd
    GamePhase_Intermission roundEndTime nextRoundTime _ _ -> do
      let -- Emit an end-of-intermission event some time before the actual end
          -- of intermission to allow the game manager to act before the start
          -- of the next round
          intermissionTicks = nextRoundTime `diffTime` roundEndTime
          endOfIntermission =
            addTicks (4 * intermissionTicks `div` 5) roundEndTime
          matchEndSfxTime = addTicks C.matchEndSfxDelayTicks roundEndTime
      if
          | time == nextRoundTime -> do
              _2 . traverse %= \player ->
                player
                  & field @"player_aggregateStats"
                    <>~ getAggregateStats (player_stats player)
              pure $ Just GameRestart
          | time == endOfIntermission ->
              pure $ Just IntermissionEnding
          | time == matchEndSfxTime ->
              pure $ Just PlayMatchEndSfx
          | otherwise ->
              pure Nothing

--------------------------------------------------------------------------------
-- Game commands

data GameCommandEvent = ParamsChanged

-- | Run a game command.
--
-- There are probably situations where switching game params causes some
-- inconsistency. This is okay for now, since we only plan to switch game
-- parameters during playtests for experimentation purposes.
runGameCommand ::
  MWC.Gen s ->
  ES.EntityStore s ->
  Time ->
  GameCommand ->
  StateT GameStatePure (ST s) (Maybe GameCommandEvent)
runGameCommand gen store time cmd = case cmd of
  PlayerJoin pid name mTeam -> do
    params <- gets gamePure_params
    let actorID = PlayerActor pid
        emptyStats = mempty
    zoom (field @"gamePure_players") $
      addPlayer params store gen time mTeam actorID name emptyStats
    pure Nothing
  PlayerLeave pid -> do
    params <- gets gamePure_params
    let actorID = PlayerActor pid
    removePlayer params store time actorID
    pure Nothing
  SwitchGameParams newRawParams -> do
    field @"gamePure_params" %= rebaseGameParams newRawParams
    pure $ Just ParamsChanged

--------------------------------------------------------------------------------
-- Add/remove players

addPlayer ::
  GameParams ->
  ES.EntityStore s ->
  MWC.Gen s ->
  Time ->
  Maybe Team ->
  ActorID ->
  PlayerName ->
  AggregatePlayerStats ->
  StateT (WIM.IntMap ActorID Player) (ST s) ()
addPlayer params store gen time mTeam pid name aggStats = do
  -- Assign new players to the smaller team
  team <- case mTeam of
    Just team -> pure team
    Nothing ->
      get >>= \actors ->
        let players =
              map snd $
                filter (\(actorID, _) -> isActorPlayer actorID) $
                  WIM.toList actors
        in  case smallerTeam (map (view player_team) players) of
              Just team -> pure team
              Nothing -> do
                bool <- MWC.uniform gen
                if bool then pure TeamNW else pure TeamSE

  -- Choose player color to be the least common color on the team
  color <-
    gets $
      leastCommonColorVariant
        . map (view player_color)
        . filter ((== team) . view player_team)
        . WIM.elems

  let attrs = PlayerAttributes pid name team color
      player =
        Player
          attrs
          defaultControls
          defaultControls
          Nothing
          emptyDelayQueue
          defaultControls
          newPlayerStats
          aggStats
  -- Add player record
  at pid .= Just player
  -- Add store data
  lift $ ES.addPlayer params store time team pid

removePlayer ::
  GameParams ->
  ES.EntityStore s ->
  Time ->
  ActorID ->
  StateT GameStatePure (ST s) ()
removePlayer params store time pid = do
  -- Remove player record
  field @"gamePure_players" . at pid .= Nothing
  -- Return BotID
  case pid of
    PlayerActor _ -> pure ()
    BotActor botID ->
      field @"gamePure_botIDPool" %= returnBotID botID
  -- Remove store data
  lift $ ES.removePlayer params store time pid

--------------------------------------------------------------------------------
-- Player controls

-- | The expression `updateWith' f ma mb` updates those values of `mb` with
-- keys also in `ma` using the the update function `f`.
updateWith' ::
  (a -> b -> b) -> WIM.IntMap k a -> WIM.IntMap k b -> WIM.IntMap k b
updateWith' f =
  WIM.merge
    IntMap.dropMissing
    IntMap.preserveMissing
    (IntMap.zipWithMatched (\_k a b -> f a b))

--------------------------------------------------------------------------------
-- Particles

updateParticles ::
  GameParams ->
  GamePhase ->
  Time ->
  WIM.IntMap ActorID Player ->
  [ES.OverseerDeathEvent] ->
  [ES.DroneDeathEvent] ->
  [CollisionDamage] ->
  [DashCastEvent] ->
  [PsiStormCastEvent] ->
  [PsiStormEffect] ->
  FlagEvents ->
  [ES.FlagDropEvent] ->
  [GameCommandEvent] ->
  StateT P.ParticleSystem (ST s) ()
updateParticles
  params
  gamePhase
  time
  players
  overseerDeaths
  droneDeaths
  collisions
  dashCasts
  psiStormCasts
  groupedPsiStormEffectEvents
  flagEvents
  flagDropEvents
  paramsChangedEvents =
    do
      let dynamicsParams = gp_dynamics params

      -- Remove old particles
      modify' $ P.trim (addTicks (Ticks (-1)) time)

      -- Create death particles
      forM_ overseerDeaths $ \ovDeathEvent ->
        case WIM.lookup (ES.ode_owner ovDeathEvent) players of
          Nothing ->
            error "Cannot create overseer death particle: Non-existent ActorID"
          Just player -> do
            let teamColor =
                  PlayerColor
                    (player ^. player_team)
                    (player ^. player_color)
                    (player ^. player_actorID)
            let particle =
                  P.makeParticle time $
                    P.OverseerDeathParticle
                      teamColor
                      (ES.ode_pos ovDeathEvent)
                      (ES.ode_vel ovDeathEvent)
                      (ES.ode_radius ovDeathEvent)
                      (ES.ode_entityID ovDeathEvent)
                      (ES.ode_owner ovDeathEvent)
                      (ES.ode_owner_team ovDeathEvent)
                particle2 =
                  P.makeParticle time $
                    P.OverseerRemainsParticle
                      teamColor
                      (ES.ode_pos ovDeathEvent)
                      (ES.ode_radius ovDeathEvent)
            modify' $ P.insert particle . P.insert particle2

      forM_ droneDeaths $ \drDeathEvent ->
        let particle =
              P.makeParticle time $
                P.DroneDeathParticle
                  (ES.dde_eid drDeathEvent)
                  (ES.dde_pos drDeathEvent)
                  (ES.dde_vel drDeathEvent)
                  (ES.dde_radius drDeathEvent)
                  (ES.dde_team drDeathEvent)
                  (ES.dde_recentDamage drDeathEvent)
        in  modify' $ P.insert particle

      -- Create collision damage particles
      forM_ collisions $ \collision ->
        let particle =
              P.makeParticle time $
                P.CollisionDamageParticle
                  (cd_position collision)
                  (cd_velocity collision)
                  (cd_damage collision)
                  (cd_direction collision)
                  (lookupColor $ cd_mActorID1 collision)
                  (lookupColor $ cd_mActorID2 collision)
        in  modify' $ P.insert particle

      -- Create kill/death notification particles
      when (inGame gamePhase) $
        forM_ overseerDeaths $ \ovDeathEvent ->
          case WIM.lookup (ES.ode_owner ovDeathEvent) players of
            Nothing ->
              error "Cannot create kill notification particle: Non-existent ActorID"
            Just killedPlayer ->
              let killedName = killedPlayer ^. player_name
                  killedTeam = killedPlayer ^. player_team
                  mKillerName = do
                    killerPid <- ES.ode_killer ovDeathEvent
                    killerPlayer <- WIM.lookup killerPid players
                    guard $ view player_team killerPlayer /= killedTeam
                    pure $ view player_name killerPlayer
                  particle =
                    P.makeParticle time $
                      P.KillNotificationParticle killedName killedTeam mKillerName
              in  modify' $ P.insert particle

      -- Create psi-storm cast particles
      forM_ psiStormCasts $ \psiStormCastEvent ->
        let color = Fixed4 1.0 1.0 0.0 0.85
            particle =
              P.makeParticle time $
                P.CastRippleParticle color (ES.psce_pos psiStormCastEvent)
        in  modify' $ P.insert particle

      -- Create dash cast particles
      forM_ dashCasts $ \dashEvent ->
        let particle =
              P.makeParticle time $
                P.DashParticle
                  { P.dp_pos = dce_pos dashEvent
                  , P.dp_vel = dce_vel dashEvent
                  , P.dp_radius = dce_entityRadius dashEvent
                  , P.dp_direction = dce_direction dashEvent
                  , P.dp_magnitudeFraction = dce_effectsMagnitudeFrac dashEvent
                  }
        in  modify' $ P.insert particle

      -- Create psi-storm damage particles
      forM_ groupedPsiStormEffectEvents $
        \PsiStormEffect
          { pse_entityPos
          , pse_entityVel
          , pse_mEntityActorID
          , pse_damage
          } ->
            let mColor = lookupColor pse_mEntityActorID
                particle =
                  P.makeParticle time $
                    P.PsiStormDamageParticle
                      pse_entityPos
                      pse_entityVel
                      mColor
                      pse_damage
            in  modify' $ P.insert particle

      -- Note: The game phase restriction was intended to only apply to
      -- notifications, but since the overseers won't have much time to live
      -- after the game ends, it doesn't really matter that we also "silence"
      -- the other flag-related particles.
      when (inGame gamePhase) $ do
        -- Create flag pickup particles
        forM_ (flagEvents_pickups flagEvents) $ \pickup ->
          let team = flagPickup_carrierTeam pickup
              pos = flagPickup_position pickup
              name =
                view player_name $
                  expectJust "updateParticles: flagPickup: no actorID" $
                    WIM.lookup (flagPickup_actor pickup) players
              particle = P.FlagPickupParticle team pos name
              insertLogParticle =
                if flagPickup_type pickup == BaseFlagType
                  then
                    P.insert $
                      P.makeParticle time $
                        P.LogMsg_FlagPickup particle
                  else id
              insertDisappearParticle =
                P.insert $
                  P.makeParticle time $
                    P.FlagFade_Pickup particle
          in  modify' $ insertLogParticle . insertDisappearParticle

        -- Create flag score particles
        forM_ (flagEvents_captures flagEvents) $
          \(FlagCapture scoringTeam actorID pos vel) -> do
            let name =
                  view player_name $
                    expectJust "updateParticles: flagPickup: no actorID" $
                      WIM.lookup actorID players
                particle =
                  P.makeParticle time $
                    P.LogMsg_FlagCapture $
                      P.FlagCaptureParticle scoringTeam name
             in modify' $ P.insert particle

            let flagTeam = oppositeTeam scoringTeam
                radius = gp_flagRadius dynamicsParams
                particle =
                  P.makeParticle time $
                    P.FlagExplodeParticle flagTeam pos vel radius
             in modify' $ P.insert particle

            let flagTeam = oppositeTeam scoringTeam
                particle = P.makeParticle time $ P.FlagFade_Respawn flagTeam
             in modify' $ P.insert particle

        -- Create flag recovery particles
        forM_ (flagEvents_recoveries flagEvents) $
          \(FlagRecovery team mActorID pos) ->
            let mName =
                  mActorID <&> \actorID ->
                    view player_name $
                      expectJust "updateParticles: flagPickup: no actorID" $
                        WIM.lookup actorID players
                particle = P.FlagRecoverParticle team mName pos
                disappearParticle =
                  P.makeParticle time $
                    P.FlagFade_Recover particle
                appearParticle =
                  P.makeParticle time $
                    P.FlagFade_Respawn team
                logParticle =
                  P.makeParticle time $
                    P.LogMsg_FlagRecover particle
            in  modify' $
                  P.insert logParticle
                    . P.insert disappearParticle
                    . P.insert appearParticle

      -- Create flag drop particles
      forM_ flagDropEvents $
        \(ES.FlagDropEvent pos vel) ->
          modify' $
            P.insert $
              P.makeParticle time $
                P.FlagFade_Drop (P.FlagDropParticle pos vel)

      -- Create params changed notification particles
      forM_ paramsChangedEvents $ \ParamsChanged ->
        let particle =
              P.makeParticle time $
                P.LogMsg_AnyMsg "Game parameters updated"
        in  modify' $ P.insert particle

      -- Create screenshake particles
      forM_ collisions $ \CollisionDamage{..} -> do
        case cd_participants of
          CollisionDamageOverseers actorID1 actorID2 -> do
            let particle1 =
                  P.makeParticle time $
                    P.ScreenshakeParticle
                      { sp_damage = cd_damage
                      , sp_direction = cd_direction
                      , sp_actorID = actorID1
                      }
                particle2 =
                  P.makeParticle time $
                    P.ScreenshakeParticle
                      { sp_damage = cd_damage
                      , sp_direction = cd_direction
                      , sp_actorID = actorID2
                      }
            modify' $ P.insert particle1
            modify' $ P.insert particle2
          CollisionDamageDrones -> do
            pure ()
          CollisionDamageOverseerDrone actorID -> do
            let particle =
                  P.makeParticle time $
                    P.ScreenshakeParticle
                      { sp_damage = cd_damage
                      , sp_direction = cd_direction
                      , sp_actorID = actorID
                      }
            modify' $ P.insert particle
   where
    lookupColor :: Maybe ActorID -> Maybe PlayerColor
    lookupColor mPid = do
      pid <- mPid
      player <- WIM.lookup pid players
      pure $
        PlayerColor
          (player ^. player_team)
          (player ^. player_color)
          (player ^. player_actorID)

--------------------------------------------------------------------------------
-- Round reset

newRound ::
  MWC.Gen s ->
  ES.EntityStore s ->
  StateT GameStatePure (ST s) ()
newRound gen store = do
  -- Save IDs, names, and (aggregated) stats of players.
  -- Only player information is saved; bots are excluded.
  pidNameStats <- do
    players <-
      gets gamePure_players <&> \actors ->
        V.fromList $
          flip mapMaybe (WIM.elems actors) $ \actor ->
            case actor ^. player_actorID of
              BotActor _ -> Nothing
              PlayerActor playerID ->
                Just
                  ( playerID
                  , actor ^. player_name
                  , player_aggregateStats actor
                  )
    MWC.uniformShuffle players gen -- randomize order to randomize teams

  -- Determine new parameter set from number of players
  let nPlayers = length pidNameStats
  newParams <- field @"gamePure_params" <%= resizeGameParams nPlayers

  -- Reset
  time <- gets gamePure_time

  field @"gamePure_roundStart" .= time
  field @"gamePure_phase" .= GamePhase_Game time zeroTeamScore
  field @"gamePure_players" .= WIM.empty
  field @"gamePure_botIDPool" .= newBotIDPool
  field @"gamePure_particles" .= P.new

  -- Add initial entities, re-add players
  lift $ do
    ES.resetEntityStore store
    spawnInitialEntities newParams gen store
  zoom (field @"gamePure_players") $
    for_ pidNameStats $ \(pid, name, aggStats) ->
      addPlayer
        newParams
        store
        gen
        time
        Nothing
        (PlayerActor pid)
        name
        aggStats

--------------------------------------------------------------------------------
-- Bots

updateBotControls ::
  GameParams ->
  MWC.Gen s ->
  WIM.IntMap BotID BotView ->
  WIM.IntMap ActorID Player ->
  ST s (WIM.IntMap ActorID Player)
updateBotControls params gen botViews players =
  flip WIM.traverseWithKey players $ \actorID player ->
    case actorID of
      PlayerActor _ -> pure player
      BotActor botID ->
        -- Bots should have a bot view exactly when `shouldControlBot` is true
        -- for the current frame
        case WIM.lookup botID botViews of
          Nothing ->
            pure
              player
                { player_controls =
                    updateControlWorldPos
                      (player_controls player)
                      (player_botTargetControls player)
                , player_prevControls = player_controls player
                }
          Just botView -> do
            let botState = fromMaybe defaultBotState $ player_botState player
            (!controls, !newBotState) <-
              let delayedControls =
                    toList $ player_botControlQueue player
               in botControl params gen botID botView botState delayedControls
            let (delayedTargetControlMaybe, newControlQueue) =
                  pushDelayQueue controlQueueSize controls $
                    player_botControlQueue player
                newTargetControls =
                  fromMaybe (player_controls player) delayedTargetControlMaybe
            pure $
              player
                { player_botState = Just newBotState
                , player_botControlQueue = newControlQueue
                , player_controls =
                    updateControlWorldPos
                      (player_controls player)
                      newTargetControls
                , player_prevControls = player_controls player
                , player_botTargetControls = newTargetControls
                }
 where
  controlQueueSize = gp_botControlQueueLength (gp_bots params)

-- Update old controls by (1) taking buttons of target control and (2)
-- interpolating mouse position towards that of target controls
updateControlWorldPos :: PlayerControls -> PlayerControls -> PlayerControls
updateControlWorldPos controlsOld controlsTarget =
  let newMousePos =
        interpolateMouse
          (control_worldPos controlsOld)
          (control_worldPos controlsTarget)
  in  PlayerControls
        { control_buttons = control_buttons controlsTarget
        , control_worldPos = newMousePos
        }

maintainBotNumbers ::
  GameParams ->
  MWC.Gen s ->
  ES.EntityStore s ->
  Time ->
  StateT GameStatePure (ST s) ()
maintainBotNumbers params gen store time = do
  UnimportantBots{..} <- lift $ findUnimportantBots <$> ES.viewPlayers store
  PlayerCounts{..} <- gets (countPlayers . gamePure_players)
  let BotParams{gp_minBotsPerTeam, gp_minTeamSize} = gp_bots params
      nwActorsTotal = pc_nwHumans + pc_nwBots
      seActorsTotal = pc_seHumans + pc_seBots
      minNwBots = max gp_minBotsPerTeam $ gp_minTeamSize - pc_nwHumans
      minSeBots = max gp_minBotsPerTeam $ gp_minTeamSize - pc_seHumans
      minNwTeamSize = pc_nwHumans + minNwBots
      minSeTeamSize = pc_seHumans + minSeBots
      targetTeamSize = max minNwTeamSize minSeTeamSize
      targetNwBots = targetTeamSize - pc_nwHumans
      targetSeBots = targetTeamSize - pc_seHumans
      totalBots = pc_nwBots + pc_seBots
      maxBots = fromIntegral C.maxBotsPerGame
  -- Strategy:
  -- 1. Always maintain sufficient bot numbers
  -- 2. Quickly balance teams by spawning bots if necessary
  -- 3. Prune pairs of excess bots only when their removal would have low impact on the game
  if
      | (pc_nwBots < targetNwBots || pc_seBots < targetSeBots) && totalBots < maxBots -> do
          -- Maintain minimum bot numbers on each team by spawning new bots.
          -- The check on the total number of bots prevents this logic from
          -- becoming stuck attempting to spawn new bots while being unable to
          -- due to the bot limit.
          let numNwSpawns = targetNwBots - pc_nwBots
              numSeSpawns = targetSeBots - pc_seBots
              -- Interleave bot spawns so that one team's bots aren't
              -- guaranteed to have a big latency advantage over the other.
              -- If there are two bots, A and B, on opposing teams, and bot A's
              -- logic always runs the tick after that of bot B, then bot A has
              -- an advantage over bot B; this is because bot A can more
              -- quickly respond to the moves of bot B than bot B can respond
              -- to bot A's moves.
              --
              -- This situation can arise because (1) we stagger the computation
              -- of the bot logic for different bots across different ticks in
              -- order to reduce the maximum computation required in a tick,
              -- (2) the current way we choose which frame to run a bot's logic
              -- is by taking the bot's id modulo the control delay, in ticks,
              -- and (3) bots spawned at the start of the game are assigned bot
              -- ids in ascending order.
              spawns =
                if numNwSpawns < numSeSpawns
                  then
                    interleave
                      (replicate numSeSpawns TeamSE)
                      (replicate numNwSpawns TeamNW)
                  else
                    interleave
                      (replicate numNwSpawns TeamNW)
                      (replicate numSeSpawns TeamSE)
          for_ spawns $ \team ->
            spawnBot gen store team
      | nwActorsTotal < seActorsTotal -> do
          -- Try to remove SE team bots
          let difference = seActorsTotal - nwActorsTotal
              toRemove = min difference (pc_seBots - targetSeBots)
              seBots = take toRemove ub_seUnimportant
          for_ seBots $ \seBotID ->
            removePlayer params store time (BotActor seBotID)
          -- Spawn bots for NW team
          let remainingDifference = difference - length seBots
          replicateM_ remainingDifference $ spawnBot gen store TeamNW
      | nwActorsTotal > seActorsTotal -> do
          -- Try to remove NW team bots
          let difference = nwActorsTotal - seActorsTotal
              toRemove = min difference (pc_nwBots - targetNwBots)
              nwBots = take toRemove ub_nwUnimportant
          for_ nwBots $ \nwBotID ->
            removePlayer params store time (BotActor nwBotID)
          -- Spawn bots for SE team
          let remainingDifference = difference - length nwBots
          replicateM_ remainingDifference $ spawnBot gen store TeamSE
      | pc_nwBots > targetNwBots && pc_seBots > targetSeBots -> do
          -- Remove excess bots
          let numPairsToRemove =
                min (pc_nwBots - targetNwBots) (pc_seBots - targetSeBots)
              botPairsToRemove =
                take numPairsToRemove $ zip ub_nwUnimportant ub_seUnimportant
          for_ botPairsToRemove $ \(botID1, botID2) -> do
            removePlayer params store time (BotActor botID1)
            removePlayer params store time (BotActor botID2)
      | otherwise -> pure ()

-- | Interleave the elements of two lists, starting with the list on the left.
interleave :: [a] -> [a] -> [a]
interleave (x : xs) (y : ys) = x : y : interleave xs ys
interleave xs ys = xs ++ ys

data UnimportantBots = UnimportantBots
  { ub_nwUnimportant :: [BotID]
  , ub_seUnimportant :: [BotID]
  }

findUnimportantBots :: WIM.IntMap ActorID PlayerView -> UnimportantBots
findUnimportantBots playerViews =
  let
    -- Filter to bots
    bots = (mapMaybe . _1) matchBotID (WIM.toList playerViews)

    -- Partition by team
    (nwBots, seBots) =
      let teamOverseerView playerView =
            case pv_team playerView of
              TeamNW -> Left $ pv_overseerView playerView
              TeamSE -> Right $ pv_overseerView playerView
      in  partitionEithers $ map (distribute . second teamOverseerView) bots

    -- Partition by liveness
    partitionByLiveness overseerViews =
      let flagLivenessView = \case
            OV_Alive OverseerViewAlive{ova_pos, ova_hasFlag} ->
              Left (ova_pos, ova_hasFlag)
            OV_Dead{} ->
              Right ()
      in  partitionEithers $
            map (distribute . second flagLivenessView) overseerViews
    (nwLiveBots, map fst -> nwDeadBots) = partitionByLiveness nwBots
    (seLiveBots, map fst -> seDeadBots) = partitionByLiveness seBots

    -- Filter by flags
    filterByFlag (pos, hasFlag) = if hasFlag then Nothing else Just pos
    nwFlaglessBots = (mapMaybe . _2) filterByFlag nwLiveBots
    seFlaglessBots = (mapMaybe . _2) filterByFlag seLiveBots

    -- Positions
    nwPositions = map (fst . snd) nwLiveBots
    sePositions = map (fst . snd) seLiveBots

    -- Proximities to enemies
    enemyProximity enemyPositions pos =
      foldl' min maxBound $
        map (\pos' -> Fixed.normSqApprox (pos' - pos)) enemyPositions
    nwProximitiesSq =
      (map . second) (enemyProximity sePositions) nwFlaglessBots
    seProximitiesSq =
      (map . second) (enemyProximity nwPositions) seFlaglessBots

    -- Partition by enemy visibility
    partitionByVisibility proximitiesSq =
      let visibility (botID, distSq) =
            let viewRadius' = fromIntegral C.viewRadiusInt :: Int64
            in  if fromIntegral distSq >= viewRadius' * viewRadius'
                  then Left botID
                  else Right botID
      in  partitionEithers $ map visibility proximitiesSq
    (nwNotInViewBots, _nwVisibleBots) = partitionByVisibility nwProximitiesSq
    (seNotInViewBots, _seVisibleBots) = partitionByVisibility seProximitiesSq
  in
    -- TODO: Could try to assign priorities to not-in-view bots
    UnimportantBots
      { ub_nwUnimportant = nwDeadBots ++ nwNotInViewBots
      , ub_seUnimportant = seDeadBots ++ seNotInViewBots
      }

-- Helper
distribute :: (a, Either b c) -> Either (a, b) (a, c)
distribute (a, e) = bimap (a,) (a,) e

-- Helper
data PlayerCounts = PlayerCounts
  { pc_nwHumans :: Int
  , pc_seHumans :: Int
  , pc_nwBots :: Int
  , pc_seBots :: Int
  }

emptyPlayerCounts :: PlayerCounts
emptyPlayerCounts =
  PlayerCounts
    { pc_nwHumans = 0
    , pc_seHumans = 0
    , pc_nwBots = 0
    , pc_seBots = 0
    }

-- Helper
countPlayers :: (Foldable f) => f Player -> PlayerCounts
countPlayers = foldl' f emptyPlayerCounts
 where
  f counts player =
    case (view player_team player, view player_actorID player) of
      (TeamNW, PlayerActor _) -> counts{pc_nwHumans = pc_nwHumans counts + 1}
      (TeamNW, BotActor _) -> counts{pc_nwBots = pc_nwBots counts + 1}
      (TeamSE, PlayerActor _) -> counts{pc_seHumans = pc_seHumans counts + 1}
      (TeamSE, BotActor _) -> counts{pc_seBots = pc_seBots counts + 1}

spawnBot ::
  MWC.Gen s -> ES.EntityStore s -> Team -> StateT GameStatePure (ST s) ()
spawnBot gen store team = do
  params <- gets gamePure_params
  time <- gets gamePure_time
  botIDMaybe <- zoom (field @"gamePure_botIDPool") $ state borrowBotID
  for_ botIDMaybe $ \botID ->
    zoom (field @"gamePure_players") $
      addPlayer
        params
        store
        gen
        time
        (Just team)
        (BotActor botID)
        (makeBotName "bot")
        mempty

--------------------------------------------------------------------------------
-- Helpers

modifyReturn' :: Monad m => (s -> s) -> StateT s m s
modifyReturn' f = do
  s <- gets f
  put s
  seq s $ pure s
