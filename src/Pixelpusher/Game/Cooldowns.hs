module Pixelpusher.Game.Cooldowns (
  PlayerCooldowns (..),
  initialCooldowns,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed

import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time

-- TODO: Rename to `PlayerControlState`?
data PlayerCooldowns = PlayerCooldowns
  { pc_psiStormCooldownUntil :: Time
  , pc_overseerDashCooldownUntil1 :: Time
  -- ^ Earlier of the two dash cooldowns
  , pc_overseerDashCooldownUntil2 :: Time
  -- ^ Later of the two dash cooldowns
  , pc_lastDroneCommand :: DroneCommand
  , pc_lastDroneCommandInitialTime :: Time
  -- ^ Time since the last move command has been held
  , pc_lastDroneDash :: (Time, Fixed)
  -- ^ Time and magnitude (between 0 and 1) of the last drone dash
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- All cooldowns ready
initialCooldowns :: Time -> PlayerCooldowns
initialCooldowns time =
  PlayerCooldowns
    { pc_psiStormCooldownUntil = time
    , pc_overseerDashCooldownUntil1 = time
    , pc_overseerDashCooldownUntil2 = time
    , pc_lastDroneCommand = DroneNeutral
    , pc_lastDroneCommandInitialTime = time
    , pc_lastDroneDash = (initialTime, 0)
    }
