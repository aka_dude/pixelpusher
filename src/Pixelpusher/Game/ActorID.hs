{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}

module Pixelpusher.Game.ActorID (
  ActorID (PlayerActor, BotActor),
  matchPlayerID,
  matchBotID,
  isActorPlayer,
  isActorBot,
) where

import Data.Bits
import Data.Function ((&))
import Data.Serialize (Serialize)
import Data.Word (Word16, Word8)
import Foreign.Storable (Storable)

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.BotID.Internal
import Pixelpusher.Game.PlayerID.Internal

--------------------------------------------------------------------------------

newtype ActorID = ActorID {_getActorID :: Word16}
  deriving newtype (Eq, Ord, Serialize, Storable)

pattern PlayerActor :: PlayerID -> ActorID
pattern PlayerActor playerID <- (matchPlayerID -> Just playerID)
  where
    PlayerActor = fromPlayerID

pattern BotActor :: BotID -> ActorID
pattern BotActor botID <- (matchBotID -> Just botID)
  where
    BotActor = fromBotID

{-# COMPLETE PlayerActor, BotActor :: ActorID #-}

--------------------------------------------------------------------------------

playerIDTag :: Word8
playerIDTag = 0

botIDTag :: Word8
botIDTag = 1

fromPlayerID :: PlayerID -> ActorID
fromPlayerID = fromID playerIDTag getPlayerID

fromBotID :: BotID -> ActorID
fromBotID = fromID botIDTag _getBotID

fromID :: Word8 -> (a -> Word8) -> a -> ActorID
fromID idTag idAccessor x =
  ActorID $ shiftL (fromIntegral idTag) 8 .|. fromIntegral (idAccessor x)

matchPlayerID :: ActorID -> Maybe PlayerID
matchPlayerID = matchID playerIDTag PlayerID

matchBotID :: ActorID -> Maybe BotID
matchBotID = matchID botIDTag BotID

matchID :: Word8 -> (Word8 -> a) -> ActorID -> Maybe a
matchID idTag idConstructor (ActorID w16) =
  splitWord16 w16 & \(tag, w8) ->
    if tag == idTag
      then Just (idConstructor w8)
      else Nothing

splitWord16 :: Word16 -> (Word8, Word8)
splitWord16 w16 = (fromIntegral (shiftR w16 8), fromIntegral w16)

--------------------------------------------------------------------------------

instance WIS.IsInt ActorID where
  toInt (ActorID i) = WIS.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIS.makeIdentity . ActorID . fromIntegral
  {-# INLINE fromInt #-}

instance WIM.IsInt ActorID where
  toInt (ActorID i) = WIM.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . ActorID . fromIntegral
  {-# INLINE fromInt #-}

isActorPlayer :: ActorID -> Bool
isActorPlayer = \case
  PlayerActor _ -> True
  BotActor _ -> False

isActorBot :: ActorID -> Bool
isActorBot = \case
  PlayerActor _ -> False
  BotActor _ -> True
