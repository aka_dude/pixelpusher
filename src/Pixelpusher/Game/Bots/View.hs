{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.View (
  BotView (..),
  BotViewEntities (..),
  OverseerBotView (..),
  DroneBotView (..),
  SoftObstacleBotView (..),
  PsiStormBotView (..),
  FlagBotView (..),
  makeBotViews,
) where

import Control.Monad (foldM)
import Control.Monad.ST (ST)
import Control.Monad.State.Strict (execStateT, modify')
import Control.Monad.Trans.Class (lift)
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.List qualified as List
import Data.Maybe (fromMaybe, isNothing)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorEntitySystem
import Pixelpusher.Game.ActorID (ActorID (BotActor))
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.CollisionEvents (BotViewCollisionEvent (..))
import Pixelpusher.Game.CombatSystem
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.DynamicsSystem
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.FlagSystem
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamSystem
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.EntityStore.Core qualified as Store

--------------------------------------------------------------------------------

data BotView = BotView
  { bv_nwTeamFlag :: FlagBotView
  , bv_seTeamFlag :: FlagBotView
  , bv_entities :: BotViewEntities
  , bv_alliesAlive :: Int
  , bv_enemiesAlive :: Int
  , bv_alliedOverseerPositions :: [Fixed2]
  , bv_visibleEnemyOverseerPositions :: [Fixed2]
  }
  deriving stock (Generic)

data BotViewEntities = BotViewEntities
  { bve_overseers :: [OverseerBotView]
  , bve_drones :: [DroneBotView]
  , bve_softObstacles :: [SoftObstacleBotView]
  , bve_psiStorms :: [PsiStormBotView]
  }
  deriving stock (Generic)

emptyBotViewEntities :: BotViewEntities
emptyBotViewEntities =
  BotViewEntities
    { bve_overseers = []
    , bve_drones = []
    , bve_softObstacles = []
    , bve_psiStorms = []
    }

data OverseerBotView = OverseerBotView
  { obv_actorID :: ActorID
  , obv_team :: Team
  , obv_pos :: Fixed2
  , obv_vel :: Fixed2
  , -- Unit vector
    obv_accelDir :: Fixed2
  , obv_hasFlag :: Bool
  , obv_health :: Fixed
  }
  deriving stock (Generic)

data DroneBotView = DroneBotView
  { dbv_actorID :: Maybe ActorID
  , dbv_orphaned :: Bool
  , dbv_team :: Team
  , dbv_pos :: Fixed2
  , dbv_vel :: Fixed2
  , -- Unit vector
    dbv_accelDir :: Fixed2
  , dbv_chargeTicks :: Ticks
  }
  deriving stock (Generic)

data SoftObstacleBotView = SoftObstacleBotView
  { sobv_pos :: Fixed2
  , sobv_vel :: Fixed2
  }
  deriving stock (Generic)

newtype PsiStormBotView = PsiStormBotView
  { psbv_pos :: Fixed2
  }
  deriving stock (Generic)

data FlagBotView = FlagBotView
  { fbv_pos :: Fixed2
  , fbv_type :: FlagType
  }

--------------------------------------------------------------------------------

makeBotViews ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MasterMinionStore s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  FlagReadStore s ->
  Time ->
  [(Team, Fixed2, FlagType)] ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  [BotViewCollisionEvent] ->
  ST s (WIM.IntMap BotID BotView)
makeBotViews
  dynComps
  combatComps
  minionComps
  teamComps
  actorComps
  flagComps
  time
  flags
  playerControlsStatuses
  botViewCollisionEvents = do
    overseerPositions' <-
      overseerPositions dynComps (WIM.map snd playerControlsStatuses)
    WIM.traverseWithKey
      ( makeBotView
          dynComps
          combatComps
          minionComps
          teamComps
          actorComps
          flagComps
          time
          flags
          overseerPositions'
          playerControlsStatuses
      )
      (groupBotViewEvents botViewCollisionEvents)

groupBotViewEvents ::
  [BotViewCollisionEvent] ->
  WIM.IntMap BotID (WIS.IntSet (SEntityID 'DynamicsEntity))
groupBotViewEvents =
  WIM.fromListWith WIS.union
    . map
      ( \(BotViewCollisionEvent botID dynID) ->
          (botID, WIS.singleton dynID)
      )

makeBotView ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MasterMinionStore s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  FlagReadStore s ->
  Time ->
  [(Team, Fixed2, FlagType)] ->
  OverseerPositions ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  BotID ->
  WIS.IntSet (SEntityID 'DynamicsEntity) ->
  ST s BotView
makeBotView
  dynComps
  combatComps
  minionComps
  teamComps
  actorComps
  flagComps
  time
  flags
  overseerPositions'
  playerControlsStatuses
  botID
  entities = do
    bv_entities <-
      flip execStateT emptyBotViewEntities $
        for_ (WIS.toList entities) $ \dynID ->
          case refineEntityID dynID of
            OverseerID overseerID -> do
              overseerView <- lift $ do
                obv_actorID <- Store.sureLookup actorComps overseerID
                (obv_pos, obv_vel) <- readPosVel dynComps overseerID
                obv_health <- getHealth combatComps overseerID
                obv_team <- Store.sureLookup teamComps overseerID
                obv_hasFlag <- readHasFlag flagComps overseerID
                let obv_accelDir =
                      maybe 0 (arrowKeyDirection . fst) $
                        WIM.lookup obv_actorID playerControlsStatuses
                pure OverseerBotView{..}
              modify' $ over (field @"bve_overseers") $ (:) overseerView
            DroneID droneID -> do
              droneView <- lift $ do
                dbv_actorID <- Store.lookup actorComps droneID
                (dbv_pos, dbv_vel) <- readPosVel dynComps droneID
                dbv_orphaned <-
                  isNothing <$> lookupMaster minionComps droneID
                dbv_team <- Store.sureLookup teamComps droneID
                let (dbv_accelDir, dbv_chargeTicks) =
                      fromMaybe (0, 0) $ do
                        actorID <- dbv_actorID
                        (controls, status) <- WIM.lookup actorID playerControlsStatuses
                        let mousePos = control_worldPos controls
                            droneControl =
                              controls ^. to control_buttons . control_drone
                            accelScale =
                              case droneControl of
                                DroneNeutral -> 0
                                DroneAttraction -> 1
                                DroneRepulsion -> -1
                            accelDir =
                              Fixed.map (* accelScale) $
                                Fixed.normalize (mousePos - dbv_pos)
                        let chargeTicks =
                              case ps_overseerStatus status of
                                OverseerDead{} -> 0
                                OverseerAlive _ spawnTime ->
                                  case pc_lastDroneCommand (ps_cooldowns status) of
                                    DroneNeutral ->
                                      diffTime time $
                                        max spawnTime $
                                          pc_lastDroneCommandInitialTime $
                                            ps_cooldowns status
                                    _ -> 0
                        pure (accelDir, chargeTicks)
                pure DroneBotView{..}
              modify' $ over (field @"bve_drones") $ (:) droneView
            SoftObstacleID softObstacleID -> do
              softObstacleView <- lift $ do
                (sobv_pos, sobv_vel) <- readPosVel dynComps softObstacleID
                pure SoftObstacleBotView{..}
              modify' $ over (field @"bve_softObstacles") $ (:) softObstacleView
            PsiStormID psiStormID -> do
              psiStormView <- lift $ do
                psbv_pos <- readPos dynComps psiStormID
                pure PsiStormBotView{..}
              modify' $ over (field @"bve_psiStorms") $ (:) psiStormView
            _ -> pure ()
    let (_, playerStatus) =
          expectJust "makeBotView: could not find BotID" $
            WIM.lookup (BotActor botID) playerControlsStatuses
        ( bv_alliesAlive
          , bv_enemiesAlive
          , bv_alliedOverseerPositions
          , bv_visibleEnemyOverseerPositions
          ) =
            let OverseerPositions{..} = overseerPositions'
            in  case ps_team playerStatus of
                  TeamNW ->
                    ( op_nwPlayersAlive
                    , op_sePlayersAlive
                    , op_nwOverseerPositions
                    , op_seVisibleOverseerPositions
                    )
                  TeamSE ->
                    ( op_sePlayersAlive
                    , op_nwPlayersAlive
                    , op_seOverseerPositions
                    , op_nwVisibleOverseerPositions
                    )
        (nwFlags, seFlags) =
          List.partition (\(team, _, _) -> team == TeamNW) flags
        bv_nwTeamFlag =
          case nwFlags of
            [(TeamNW, pos, flagType)] -> FlagBotView pos flagType
            _ -> error "makeBotView: nw team flag missing"
        bv_seTeamFlag =
          case seFlags of
            [(TeamSE, pos, flagType)] -> FlagBotView pos flagType
            _ -> error "makeBotView: se team flag missing"
    pure BotView{..}

data OverseerPositions = OverseerPositions
  { op_nwPlayersAlive :: Int
  , op_sePlayersAlive :: Int
  , op_nwOverseerPositions :: [Fixed2]
  , op_seOverseerPositions :: [Fixed2]
  , op_nwVisibleOverseerPositions :: [Fixed2]
  , op_seVisibleOverseerPositions :: [Fixed2]
  }

overseerPositions ::
  DynamicsReadStore s ->
  WIM.IntMap ActorID PlayerStatus ->
  ST s OverseerPositions
overseerPositions dynComps playerStatuses = do
  ( op_nwPlayersAlive
    , op_sePlayersAlive
    , op_nwOverseerPositions
    , op_seOverseerPositions
    ) <- do
    let countOverseer
          input@(!nwPlayersAlive, !sePlayersAlive, nwPositions, sePositions)
          playerStatus = do
            case ps_overseerStatus playerStatus of
              OverseerDead{} ->
                pure input
              OverseerAlive overseerID _ -> do
                pos <- readPos dynComps overseerID
                pure $
                  case ps_team playerStatus of
                    TeamNW ->
                      ( nwPlayersAlive + 1
                      , sePlayersAlive
                      , pos : nwPositions
                      , sePositions
                      )
                    TeamSE ->
                      ( nwPlayersAlive
                      , sePlayersAlive + 1
                      , nwPositions
                      , pos : sePositions
                      )
    foldM countOverseer (0, 0, [], []) playerStatuses
  let (op_nwVisibleOverseerPositions, op_seVisibleOverseerPositions) =
        inViewOverseers id (op_nwOverseerPositions, op_seOverseerPositions)
  pure $! OverseerPositions{..}

-- | Helper: Filter each of two sets of overseers for the overseers within view
-- radius of an overseer in the other set. Naive implementation.
inViewOverseers :: (a -> Fixed2) -> ([a], [a]) -> ([a], [a])
inViewOverseers getPos (overseers1, overseers2) =
  let positions1 = map getPos overseers1
      positions2 = map getPos overseers2
      isInViewOf positions overseer =
        any
          (\pos -> Fixed.normLeq (getPos overseer - pos) C.viewRadius)
          positions
  in  ( filter (isInViewOf positions2) overseers1
      , filter (isInViewOf positions1) overseers2
      )
