{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.MoveTarget (
  MoveTarget (..),
  BroadMoveTarget (..),
  MoveUrgency (..),
  computeMoveGoal,
) where

import Data.Foldable (minimumBy)
import Data.Function (on)
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamBase

import Pixelpusher.Game.Bots.AI.GeometryUtil (lineDistance)
import Pixelpusher.Game.Bots.AI.RefinedView

--------------------------------------------------------------------------------
-- New bot state transition

data MoveTarget = MoveTarget
  { mt_broad :: BroadMoveTarget
  , mt_requiresPrecision :: Bool
  , mt_urgency :: MoveUrgency
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data BroadMoveTarget = BroadMoveTarget
  { bmt_pos :: Fixed2
  , bmt_radius :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data MoveUrgency = LowUrgency | MediumUrgency | HighUrgency | ReallyHighUrgency
  deriving stock (Generic)
  deriving anyclass (Serialize)

computeMoveGoal :: GameParams -> RefinedBotView -> MoveTarget
computeMoveGoal params botView@RefinedBotView{..}
  | obv_hasFlag rbv_myOverseer =
      flagCarrier params botView
  | otherwise =
      nonFlagCarrier params botView

nonFlagCarrier :: GameParams -> RefinedBotView -> MoveTarget
nonFlagCarrier params botView@RefinedBotView{..} =
  case (alliedFlagTarget params botView, enemyFlagTarget params botView) of
    (Nothing, Just enemyFlag) ->
      -- Allied flag at home base
      case eft_type enemyFlag of
        OverseerFlagType ->
          case globalTeamAdvantage botView of
            AlliedTeamAdvantage -> seekFights escortFlagCarrier params botView
            EnemyTeamAdvantage -> escortFlagCarrier params botView
            NeutralTeamAdvantage -> escortFlagCarrier params botView
        BaseFlagType ->
          case globalTeamAdvantage botView of
            AlliedTeamAdvantage -> captureEnemyFlag params botView
            EnemyTeamAdvantage -> retreat params botView
            NeutralTeamAdvantage -> seekFights captureEnemyFlag params botView
        DroppedFlagType ->
          let distanceToFlag =
                Fixed.norm $ obv_pos rbv_myOverseer - fbv_pos rbv_enemyFlag
              teamAdvantage =
                if distanceToFlag < C.viewRadius
                  then localTeamAdvantage botView
                  else globalTeamAdvantage botView
          in  case teamAdvantage of
                AlliedTeamAdvantage -> captureEnemyFlag params botView
                EnemyTeamAdvantage -> retreat params botView
                NeutralTeamAdvantage -> captureEnemyFlag params botView
    (Just alliedFlag, Just enemyFlag) ->
      -- Allied flag stolen
      let myPos = obv_pos rbv_myOverseer
          distAllied = Fixed.norm (myPos - fbv_pos rbv_alliedFlag)
          distEnemy = Fixed.norm (myPos - fbv_pos rbv_enemyFlag)
      in  case (aft_type alliedFlag, eft_type enemyFlag) of
            (StolenOverseerFlagType, OverseerFlagType) ->
              -- Be more aggressive to prevent boring stalemates
              case globalTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  pursueEnemyFlagCarrier params botView
                EnemyTeamAdvantage ->
                  escortFlagCarrier params botView
                NeutralTeamAdvantage ->
                  if distAllied < distEnemy
                    then pursueEnemyFlagCarrier params botView
                    else case localTeamAdvantage botView of
                      AlliedTeamAdvantage ->
                        pursueEnemyFlagCarrier params botView
                      EnemyTeamAdvantage ->
                        escortFlagCarrier params botView
                      NeutralTeamAdvantage ->
                        seekFights escortFlagCarrier params botView
            (StolenOverseerFlagType, BaseFlagType) ->
              case globalTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  if distAllied < distEnemy
                    then pursueEnemyFlagCarrier params botView
                    else captureEnemyFlag params botView
                EnemyTeamAdvantage ->
                  let localAdvantage =
                        length rbv_alliedOverseers >= length rbv_enemyOverseers
                  in  if distAllied < C.viewRadius && localAdvantage
                        then pursueEnemyFlagCarrier params botView
                        else retreat params botView
                NeutralTeamAdvantage ->
                  if distAllied < distEnemy
                    then pursueEnemyFlagCarrier params botView
                    else captureEnemyFlag params botView
            (StolenOverseerFlagType, DroppedFlagType) ->
              case globalTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  if distAllied < distEnemy
                    then pursueEnemyFlagCarrier params botView
                    else captureEnemyFlag params botView
                EnemyTeamAdvantage ->
                  case localTeamAdvantage botView of
                    EnemyTeamAdvantage ->
                      retreat params botView
                    AlliedTeamAdvantage ->
                      if
                          | distAllied < C.viewRadius ->
                              pursueEnemyFlagCarrier params botView
                          | distEnemy < C.viewRadius ->
                              captureEnemyFlag params botView
                          | otherwise ->
                              retreat params botView
                    NeutralTeamAdvantage ->
                      if distEnemy < C.viewRadius
                        then captureEnemyFlag params botView
                        else retreat params botView
                NeutralTeamAdvantage ->
                  if distAllied < distEnemy
                    then pursueEnemyFlagCarrier params botView
                    else captureEnemyFlag params botView
            (StolenDroppedFlagType, OverseerFlagType) ->
              case globalTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  if distAllied < distEnemy
                    then recoverAlliedFlag params botView
                    else seekFights escortFlagCarrier params botView
                EnemyTeamAdvantage ->
                  if
                      | distAllied < 500 -> -- arbitrary
                          recoverAlliedFlag params botView
                      | distEnemy < 500 -> -- arbitrary
                          escortFlagCarrier params botView
                      | otherwise ->
                          retreat params botView
                NeutralTeamAdvantage ->
                  if distAllied < distEnemy
                    then recoverAlliedFlag params botView
                    else escortFlagCarrier params botView
            (StolenDroppedFlagType, BaseFlagType) ->
              case globalTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  if distAllied < distEnemy
                    then recoverAlliedFlag params botView
                    else captureEnemyFlag params botView
                EnemyTeamAdvantage ->
                  if distAllied < C.viewRadius
                    then case localTeamAdvantage botView of
                      EnemyTeamAdvantage ->
                        retreat params botView
                      AlliedTeamAdvantage ->
                        recoverAlliedFlag params botView
                      NeutralTeamAdvantage ->
                        recoverAlliedFlag params botView
                    else retreat params botView
                NeutralTeamAdvantage ->
                  recoverAlliedFlag params botView
            (StolenDroppedFlagType, DroppedFlagType) ->
              if distAllied < distEnemy
                then recoverAlliedFlag params botView
                else captureEnemyFlag params botView
    (Nothing, Nothing) ->
      -- Allied flag at home base, enemy flag near home base. We're probably
      -- about to score.
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage -> advance params botView
        EnemyTeamAdvantage -> retreat params botView
        NeutralTeamAdvantage -> seekFights advance params botView
    (Just alliedFlag, Nothing) ->
      -- Allied flag stolen, enemy flag near home base. Likely a flag
      -- stalemate.
      case globalTeamAdvantage botView of
        NeutralTeamAdvantage ->
          case aft_type alliedFlag of
            StolenDroppedFlagType ->
              recoverAlliedFlag params botView
            StolenOverseerFlagType ->
              seekFights recoverAlliedFlag params botView
        AlliedTeamAdvantage ->
          recoverAlliedFlag params botView
        EnemyTeamAdvantage ->
          retreat params botView

data TeamAdvantage
  = AlliedTeamAdvantage
  | EnemyTeamAdvantage
  | NeutralTeamAdvantage

globalTeamAdvantage :: RefinedBotView -> TeamAdvantage
globalTeamAdvantage RefinedBotView{..}
  | rbv_alliesAlive * 4 < rbv_enemiesAlive * 3 = EnemyTeamAdvantage
  | rbv_alliesAlive * 3 > rbv_enemiesAlive * 4 = AlliedTeamAdvantage
  | otherwise = NeutralTeamAdvantage

localTeamAdvantage :: RefinedBotView -> TeamAdvantage
localTeamAdvantage RefinedBotView{..}
  | alliedOverseers < enemyOverseers = EnemyTeamAdvantage
  | alliedOverseers > enemyOverseers = AlliedTeamAdvantage
  | otherwise = NeutralTeamAdvantage
 where
  alliedOverseers = length rbv_alliedOverseers + 1 -- count self
  enemyOverseers = length rbv_enemyOverseers

newtype EnemyFlagTarget = EnemyFlagTarget {eft_type :: FlagType}

newtype AlliedFlagTarget = AlliedFlagTarget {aft_type :: StolenFlagType}

data StolenFlagType = StolenDroppedFlagType | StolenOverseerFlagType

alliedFlagTarget :: GameParams -> RefinedBotView -> Maybe AlliedFlagTarget
alliedFlagTarget params RefinedBotView{..} =
  let myTeam = obv_team rbv_myOverseer
      myBase = teamBaseLocation params myTeam
      flagPos = fbv_pos rbv_alliedFlag
  in  -- Only stolen flags
      case fbv_type rbv_alliedFlag of
        BaseFlagType ->
          Nothing
        DroppedFlagType ->
          let enemyBase = teamBaseLocation params (oppositeTeam myTeam)
              distToBase = Fixed.norm (flagPos - myBase)
          in  if
                  | distToBase < 180 -> -- arbitrary
                      Nothing
                  | Fixed.norm (flagPos - enemyBase) > Fixed.norm (myBase - enemyBase) ->
                      Nothing
                  | otherwise ->
                      Just $ AlliedFlagTarget StolenDroppedFlagType
        OverseerFlagType ->
          Just $ AlliedFlagTarget StolenOverseerFlagType

enemyFlagTarget :: GameParams -> RefinedBotView -> Maybe EnemyFlagTarget
enemyFlagTarget params RefinedBotView{..} =
  let myTeam = obv_team rbv_myOverseer
      myBase = teamBaseLocation params myTeam
      flagPos = fbv_pos rbv_enemyFlag
  in  case fbv_type rbv_enemyFlag of
        BaseFlagType ->
          Just $ EnemyFlagTarget BaseFlagType
        DroppedFlagType ->
          Just $
            EnemyFlagTarget DroppedFlagType
        OverseerFlagType ->
          -- Ally carrying enemy flag
          if Fixed.normLeq (flagPos - myBase) 240 -- arbitrary
            then Nothing -- Ally already at base
            else
              List.NonEmpty.nonEmpty rbv_enemyOverseers >>= \enemyOverseers' ->
                let enemyDistToFlagCarrier =
                      minimum $
                        fmap (Fixed.norm . subtract flagPos . obv_pos) enemyOverseers'
                in  if enemyDistToFlagCarrier < 500 -- arbitrary
                      then Nothing
                      else Just $ EnemyFlagTarget OverseerFlagType

flagCarrier :: GameParams -> RefinedBotView -> MoveTarget
flagCarrier params botView@RefinedBotView{..}
  | fbv_type rbv_alliedFlag == BaseFlagType =
      -- can score
      let collisionRadius = gp_overseerRadius dynParams + gp_teamBaseRadius dynParams
          urgency =
            if Fixed.normLeq (myPos - myBase) (collisionRadius + 180) -- arbitrary
              then ReallyHighUrgency
              else HighUrgency
      in  MoveTarget
            { mt_broad =
                withAllies
                  botView
                  BroadMoveTarget
                    { bmt_pos = myBase
                    , bmt_radius =
                        gp_overseerRadius dynParams
                          + gp_teamBaseRadius dynParams
                    }
            , mt_requiresPrecision = True
            , mt_urgency = urgency
            }
  | otherwise =
      -- cannot score
      if
          | rbv_alliesAlive >= rbv_enemiesAlive
          , length rbv_alliedOverseers >= length rbv_enemyOverseers ->
              -- Flag stalemate, no team disadvantage, no local disadvantage:
              -- advance to center
              MoveTarget
                { mt_broad =
                    BroadMoveTarget
                      { bmt_pos = 0 -- map enter
                      , bmt_radius = 240 -- arbitrary
                      }
                , mt_requiresPrecision = False
                , mt_urgency = LowUrgency
                }
          | Fixed.normLeq (obv_pos rbv_myOverseer - myBase) 240 -- arbitrary
          , length rbv_alliedOverseers < length rbv_enemyOverseers ->
              -- Pressured at base: retreat to spawn
              MoveTarget
                { mt_broad =
                    BroadMoveTarget
                      { bmt_pos = mySpawn
                      , bmt_radius = 240 -- arbitrary
                      }
                , mt_requiresPrecision = False
                , mt_urgency = LowUrgency
                }
          | otherwise ->
              MoveTarget
                { mt_broad =
                    withAllies
                      botView
                      BroadMoveTarget
                        { bmt_pos = myBase
                        , bmt_radius =
                            gp_overseerRadius dynParams
                              + gp_teamBaseRadius dynParams
                        }
                , mt_requiresPrecision = False
                , mt_urgency = LowUrgency
                }
 where
  myPos = obv_pos rbv_myOverseer
  myTeam = obv_team rbv_myOverseer
  myBase = teamBaseLocation params myTeam
  mySpawn = spawnAreaCenter params myTeam
  dynParams = gp_dynamics params

retreat :: GameParams -> RefinedBotView -> MoveTarget
retreat params botView =
  MoveTarget
    { mt_broad = retreat' params botView
    , mt_requiresPrecision = False
    , mt_urgency = LowUrgency
    }

retreat' :: GameParams -> RefinedBotView -> BroadMoveTarget
retreat' params botView@RefinedBotView{..}
  | lineDistance myBase mySpawn myPos < radius
      && length rbv_alliedOverseers + 1 < length rbv_enemyOverseers =
      withAllies
        botView
        BroadMoveTarget
          { bmt_pos = mySpawn
          , bmt_radius = gp_spawnAreaRadius (gp_dynamics params)
          }
  | otherwise =
      withAllies
        botView
        BroadMoveTarget
          { bmt_pos = myBase
          , bmt_radius = radius
          }
 where
  myPos = obv_pos rbv_myOverseer
  myTeam = obv_team rbv_myOverseer
  myBase = teamBaseLocation params myTeam
  mySpawn = spawnAreaCenter params myTeam
  radius = 240 -- arbitrary

advance :: GameParams -> RefinedBotView -> MoveTarget
advance params botView =
  MoveTarget
    { mt_broad =
        withAllies botView $
          BroadMoveTarget
            { bmt_pos = teamBaseLocation params (oppositeTeam myTeam)
            , bmt_radius = 360 -- arbitrary
            }
    , mt_requiresPrecision = False
    , mt_urgency = LowUrgency
    }
 where
  myTeam = obv_team (rbv_myOverseer botView)

seekFights ::
  (GameParams -> RefinedBotView -> MoveTarget) ->
  GameParams ->
  RefinedBotView ->
  MoveTarget
seekFights next params botView@RefinedBotView{..} =
  -- Go to nearest enemy
  let myPos = obv_pos rbv_myOverseer
      nearestEnemyPosMaybe =
        minimumBy (compare `on` (Fixed.norm . subtract myPos))
          <$> List.NonEmpty.nonEmpty rbv_visibleEnemyOverseerPositions
  in  case nearestEnemyPosMaybe of
        Nothing ->
          next params botView
        Just nearestEnemyPos ->
          if Fixed.normLeq (nearestEnemyPos - myPos) 600 -- arbitrary
            then
              MoveTarget
                { mt_broad =
                    BroadMoveTarget
                      { bmt_pos = nearestEnemyPos
                      , bmt_radius = 120 -- arbitrary
                      }
                , mt_requiresPrecision = False
                , mt_urgency = LowUrgency
                }
            else next params botView

escortFlagCarrier :: GameParams -> RefinedBotView -> MoveTarget
escortFlagCarrier _params botView =
  MoveTarget
    { mt_broad =
        withAllies botView $
          BroadMoveTarget
            { bmt_pos = fbv_pos (rbv_enemyFlag botView)
            , bmt_radius = 240 -- arbitrary
            }
    , mt_requiresPrecision = False
    , mt_urgency = LowUrgency
    }

captureEnemyFlag :: GameParams -> RefinedBotView -> MoveTarget
captureEnemyFlag params botView =
  MoveTarget
    { mt_broad =
        withAllies botView $
          BroadMoveTarget
            { bmt_pos = fbv_pos (rbv_enemyFlag botView)
            , bmt_radius =
                gp_overseerRadius (gp_dynamics params)
                  + gp_flagRadius (gp_dynamics params)
                  + 30 -- arbitrary
            }
    , mt_requiresPrecision = True
    , mt_urgency = MediumUrgency
    }

recoverAlliedFlag :: GameParams -> RefinedBotView -> MoveTarget
recoverAlliedFlag params botView =
  MoveTarget
    { mt_broad =
        withAllies botView $
          BroadMoveTarget
            { bmt_pos = fbv_pos (rbv_alliedFlag botView)
            , bmt_radius =
                gp_flagRecoveryRadius (gp_dynamics params)
                  + gp_overseerRadius (gp_dynamics params)
            }
    , mt_requiresPrecision = False
    , mt_urgency = HighUrgency
    }

pursueEnemyFlagCarrier :: GameParams -> RefinedBotView -> MoveTarget
pursueEnemyFlagCarrier params botView =
  MoveTarget
    { mt_broad =
        withAllies botView $
          BroadMoveTarget
            { bmt_pos =
                let flagPos = fbv_pos (rbv_alliedFlag botView)
                    enemyBasePos =
                      teamBaseLocation params $
                        oppositeTeam $
                          obv_team (rbv_myOverseer botView)
                    toEnemyBase = Fixed.normalize (enemyBasePos - flagPos)
                in  flagPos + Fixed.map (* 200) toEnemyBase
            , bmt_radius = 200 -- arbitrary
            }
    , mt_requiresPrecision = False
    , mt_urgency = MediumUrgency
    }

withAllies :: RefinedBotView -> BroadMoveTarget -> BroadMoveTarget
withAllies RefinedBotView{..} bmt@BroadMoveTarget{..}
  | distanceTo <= allyClosenessRadius = bmt
  | otherwise =
      let targetDir = Fixed.normalize $ bmt_pos - myPos
          projectionOnTargetDir v =
            Fixed.dot targetDir (Fixed.normalize v)
          forwardAllies =
            filter
              ( \allyPos ->
                  projectionOnTargetDir (allyPos - myPos) > 0.7 -- ~ 45 deg
                    && Fixed.normGt (allyPos - myPos) allyClosenessRadius
              )
              rbv_alliedOverseerPositions
          (withAlly, targetPos) =
            minimumBy
              (compare `on` (Fixed.norm . subtract myPos . snd))
              ((WithoutAlly, bmt_pos) :| map (WithAlly,) forwardAllies)
          radius =
            case withAlly of
              WithAlly -> allyClosenessRadius
              WithoutAlly -> bmt_radius
      in  BroadMoveTarget
            { bmt_pos = targetPos
            , bmt_radius = radius
            }
 where
  myPos = obv_pos rbv_myOverseer
  distanceTo = Fixed.norm (myPos - bmt_pos)
  allyClosenessRadius = 360 -- arbitrary

data WithAlly = WithAlly | WithoutAlly
