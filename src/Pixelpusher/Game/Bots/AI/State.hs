module Pixelpusher.Game.Bots.AI.State (
  BotTargetState (..),
  TargetingMode (..),
  transitionDroneTarget,
) where

import Control.Monad.ST
import Data.Foldable (minimumBy)
import Data.Function (on)
import Data.List.NonEmpty qualified as List (NonEmpty)
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Bots.View

--------------------------------------------------------------------------------
-- Bot state and state transitions

data BotTargetState
  = NoTarget
  | OverseerTarget ActorID TargetingMode
  deriving stock (Generic)
  deriving anyclass (Serialize)

data TargetingMode
  = DirectTargeting
  | LeadTargeting
  deriving stock (Generic)
  deriving anyclass (Serialize)

transitionDroneTarget ::
  MWC.Gen s ->
  OverseerBotView ->
  Maybe Fixed2 ->
  [OverseerBotView] ->
  BotTargetState ->
  ST s BotTargetState
transitionDroneTarget
  gen
  myOverseer
  maybeMedianDronePos
  enemyOverseers
  botTargetState =
    case (maybeMedianDronePos, List.NonEmpty.nonEmpty enemyOverseers) of
      (Just medianDronePos, Just enemyOverseersNE) ->
        let newTarget =
              chooseDroneTarget myOverseer medianDronePos enemyOverseersNE
        in  case botTargetState of
              NoTarget ->
                pure $! OverseerTarget (obv_actorID newTarget) DirectTargeting
              OverseerTarget _actorID targetingMode -> do
                newTargetingMode <-
                  case targetingMode of
                    DirectTargeting -> do
                      roll <- Fixed.fromFloat <$> MWC.uniformRM @Float (0, 1) gen
                      pure $
                        if roll < 0.1
                          then LeadTargeting
                          else DirectTargeting
                    LeadTargeting -> do
                      roll <- Fixed.fromFloat <$> MWC.uniformRM @Float (0, 1) gen
                      pure $
                        if roll < 0.1
                          then DirectTargeting
                          else LeadTargeting
                pure $
                  OverseerTarget (obv_actorID newTarget) newTargetingMode
      _ -> pure NoTarget

chooseDroneTarget ::
  OverseerBotView ->
  Fixed2 ->
  List.NonEmpty OverseerBotView ->
  OverseerBotView
chooseDroneTarget myOverseer medianDronePos =
  fst
    . minimumBy (compare `on` snd)
    . fmap (\ov -> (ov, scoreDroneTarget myOverseer medianDronePos ov))

-- Returns a score representing target priority; a lower score means higher
-- priority.
scoreDroneTarget ::
  OverseerBotView ->
  Fixed2 ->
  OverseerBotView ->
  Fixed
scoreDroneTarget myOverseer medianDronePos targetOverseer =
  -- TODO: We could also consider velocities
  let distFromOverseer =
        Fixed.norm $ obv_pos targetOverseer - obv_pos myOverseer
      distFromDrones =
        Fixed.norm $ obv_pos targetOverseer - medianDronePos
      targetHasFlag = obv_hasFlag targetOverseer
      targetHealth = obv_health targetOverseer
  in  -- Arbitrary constants
      distFromOverseer
        + 0.5 * distFromDrones
        + 3 * targetHealth
        + if targetHasFlag then -120 else 0
