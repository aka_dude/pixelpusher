{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.DroneMovement (
  droneMovement,
) where

import Data.Foldable (find, foldl', minimumBy)
import Data.Function (on)
import Data.List.NonEmpty qualified as List.NonEmpty
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Parameters

import Pixelpusher.Game.Bots.AI.Kinematics
import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.State

--------------------------------------------------------------------------------
-- Drone movement

droneMovement :: GameParams -> RefinedBotView -> BotTargetState -> Maybe Fixed2
droneMovement params botView@RefinedBotView{..} targetState =
  rbv_myDrones <&> \(dronesMedianPos, myDrones) ->
    let
      -- Weigh velolcities by proximity to median position
      aggregateVel =
        (\(totalVel, totalWeight) -> Fixed.map (/ totalWeight) totalVel) $
          foldl' f (0, 0) myDrones
       where
        f :: (Fixed2, Fixed) -> DroneBotView -> (Fixed2, Fixed)
        f (!velAcc, !weightAcc) drone =
          let scaling dist
                | dist < 0.01 = 100
                | otherwise = recip dist
              weight = scaling $ Fixed.norm $ dbv_pos drone - dronesMedianPos
              vel = Fixed.map (* weight) (dbv_vel drone)
          in  (velAcc + vel, weightAcc + weight)
      moveCandidates =
        droneMoveCandidates
          params
          (obv_pos rbv_myOverseer)
          dronesMedianPos
          aggregateVel
      totalPotential pos =
        targetingPotential (targetDronePosition params botView targetState) pos
          + psiStormAvoidancePotential params rbv_psiStorms pos
          + obstacleAvoidancePotential params rbv_softObstacles pos
      mousePos =
        dmc_mousePos
          . snd
          . minimumBy (compare `on` fst)
          . fmap (\mc -> (totalPotential (dmc_pos mc), mc))
          $ moveCandidates
    in
      mousePos

--------------------------------------------------------------------------------

targetingPotential :: Fixed2 -> Fixed2 -> Fixed
targetingPotential targetPos testPos = Fixed.norm (testPos - targetPos)

-- Heuristic for choosing a drone position for attacking an overseer target
targetDronePosition :: GameParams -> RefinedBotView -> BotTargetState -> Fixed2
targetDronePosition params RefinedBotView{..} newBotTargetState
  | OverseerTarget actorID targetingMode <- newBotTargetState
  , Just OverseerBotView{..} <-
      find ((== actorID) . obv_actorID) rbv_enemyOverseers =
      let stepTicks =
            case targetingMode of
              DirectTargeting -> predictionTicks
              LeadTargeting -> 2 * predictionTicks
          (projOverseerPos, projOverseerVel) =
            forwardSimulateKinematics
              (overseerDragParams params)
              obv_accelDir
              stepTicks
              (obv_pos, obv_vel)
          ovLeadingPos = projOverseerPos + Fixed.map (* predictionTicks) projOverseerVel
      in  ovLeadingPos
  | otherwise =
      obv_pos rbv_myOverseer
        + Fixed.map (* predictionTicks) (obv_vel rbv_myOverseer)
 where
  predictionTicks = 20

--------------------------------------------------------------------------------

-- TODO: take into account drone health
psiStormAvoidancePotential ::
  GameParams -> [PsiStormBotView] -> Fixed2 -> Fixed
psiStormAvoidancePotential params psiStorms pos =
  let
    -- Compute psi-storm damage to cluster of drones centered at position
    psiStormRadius = gp_psiStormRadius (gp_dynamics params)
    droneRadius4 = 4 * gp_droneRadius (gp_dynamics params)
    (overlapFracs, distances) =
      unzip $
        flip map psiStorms $ \psiStorm ->
          let distance = Fixed.norm (psbv_pos psiStorm - pos)
              overlap =
                max 0 . min 1 $
                  (psiStormRadius + droneRadius4 - distance)
                    / (2 * droneRadius4)
          in  (overlap, distance)
    overlapsSqSum = sqrt $ foldl' (+) 0 $ map (\x -> x * x) overlapFracs
    damage = gp_psiStormDamagePerHit (gp_combat params) * overlapsSqSum

    damagePotential = damage * 144
    movementIncentivePotential =
      case List.NonEmpty.nonEmpty distances of
        Nothing -> 0
        Just distances' ->
          -min (psiStormRadius + droneRadius4) (minimum distances')
            * damage
  in
    damagePotential + movementIncentivePotential

--------------------------------------------------------------------------------

obstacleAvoidancePotential ::
  GameParams -> [SoftObstacleBotView] -> Fixed2 -> Fixed
obstacleAvoidancePotential params obstacles pos =
  let softObstacleRadius = gp_softObstacleRadius (gp_dynamics params)
      droneRadius3 = 3 * gp_droneRadius (gp_dynamics params)
      avoidanceRadius = max 0 $ softObstacleRadius - droneRadius3
      minDistance =
        foldl' min maxBound $
          map (\obstacle -> Fixed.norm (sobv_pos obstacle - pos)) obstacles
      maxPenalty = softObstacleRadius * 2 -- arbitrary multiplier
  in  if
          | minDistance > softObstacleRadius -> 0
          | minDistance > avoidanceRadius ->
              ( (minDistance - avoidanceRadius)
                  / (softObstacleRadius - avoidanceRadius)
              )
                * maxPenalty
          | otherwise ->
              maxPenalty

--------------------------------------------------------------------------------

droneMoveCandidates ::
  GameParams ->
  Fixed2 ->
  Fixed2 ->
  Fixed2 ->
  [DroneMoveCandidate]
droneMoveCandidates params overseerPos initialPos initialVel =
  flip map moveOffets $ \moveOffset ->
    let rawMousePos = initialPos + moveOffset
        dmc_mousePos = boundPosition overseerPos controlRadius rawMousePos
        steps = 45
        (dmc_pos, dmc_vel) =
          forwardSimulateKinematics'
            droneParams
            (Fixed.normalize (dmc_mousePos - initialPos))
            steps
            (initialPos, initialVel)
    in  DroneMoveCandidate{..}
 where
  droneParams = droneDragParams params
  controlRadius = gp_controlRadius (gp_dynamics params)

boundPosition :: Fixed2 -> Fixed -> Fixed2 -> Fixed2
boundPosition center radius pos
  | normDiff <= radius = pos
  | otherwise = center + Fixed.map (* (radius / normDiff)) diff
 where
  diff = pos - center
  normDiff = Fixed.norm diff

moveOffets :: [Fixed2]
moveOffets =
  flip map [1 :: Int .. nPoints] $ \n ->
    let frac = fromIntegral n / nPoints'
        ang = 2 * pi * frac
    in  Fixed.map (* r) (Fixed.angle ang)
 where
  r = 150
  nPoints = 24
  nPoints' = fromIntegral nPoints

data DroneMoveCandidate = DroneMoveCandidate
  { dmc_pos :: Fixed2
  , dmc_vel :: Fixed2
  , dmc_mousePos :: Fixed2
  }
  deriving stock (Show)
