{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Eta reduce" #-}

module Pixelpusher.Game.Bots.AI.OverseerMovement (
  overseerMovement,
) where

import Control.Arrow ((&&&))
import Data.Foldable (foldl', maximumBy, minimumBy)
import Data.Function (on)
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe (fromMaybe, mapMaybe, maybeToList)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls

import Pixelpusher.Game.Bots.AI.GeometryUtil (lineDistance)
import Pixelpusher.Game.Bots.AI.Kinematics
import Pixelpusher.Game.Bots.AI.MoveTarget
import Pixelpusher.Game.Bots.AI.RefinedView

--------------------------------------------------------------------------------

overseerMovement :: GameParams -> RefinedBotView -> MoveTarget -> MoveCommand
overseerMovement params botView@RefinedBotView{..} moveTarget =
  let (neutralMoveCandiate, nonNeutralMoveCandidates) =
        makeMoveCandidates
          (overseerDragParams params)
          (obv_pos rbv_myOverseer)
          (obv_vel rbv_myOverseer)
      generalMoveCandidates = neutralMoveCandiate : nonNeutralMoveCandidates
      preciseMoveCandidate =
        let BroadMoveTarget{..} = mt_broad moveTarget
            neutralPos = mc_pos neutralMoveCandiate
        in  maybeToList $
              if mt_requiresPrecision moveTarget
                && Fixed.normLeq (bmt_pos - neutralPos) bmt_radius
                then
                  Just $
                    let direction =
                          bmt_pos - mc_pos neutralMoveCandiate
                    in  MoveCandidate
                          { mc_pos = bmt_pos
                          , mc_vel = Nothing
                          , mc_command =
                              directionToNonNeutralMoveCommand direction
                          }
                else Nothing
      enemyProximity =
        foldl' min maxBound $
          map (\pos -> Fixed.norm (pos - obv_pos rbv_myOverseer)) $
            map obv_pos rbv_enemyOverseers ++ map dbv_pos rbv_enemyDrones
      filteredMoveCandidates =
        filter
          ( uncurry (obstructionMask params rbv_softObstacles enemyProximity)
              . (mc_pos &&& (fromMaybe 0 . mc_vel))
          )
          (preciseMoveCandidate ++ generalMoveCandidates)
      totalPotential pos =
        movementPotential moveTarget pos
          + collisionAvoidancePotential
            params
            botView
            (obv_health rbv_myOverseer)
            pos
          + psiStormAvoidancePotential params rbv_myOverseer rbv_psiStorms pos
          + alliedOverseerAvoidancePotential botView pos
  in  maybe
        MoveNeutral
        ( mc_command
            . snd
            . minimumBy (compare `on` fst)
            . fmap (\mc -> (totalPotential (mc_pos mc), mc))
        )
        (List.NonEmpty.nonEmpty filteredMoveCandidates)

-- Brute-force
directionToNonNeutralMoveCommand :: Fixed2 -> MoveCommand
directionToNonNeutralMoveCommand dir =
  fst $
    maximumBy
      (compare `on` Fixed.dot dir . snd)
      [ (MoveE, Fixed2 1 0)
      , (MoveNE, Fixed2 invSqrt2 invSqrt2)
      , (MoveN, Fixed2 0 1)
      , (MoveNW, Fixed2 (-invSqrt2) invSqrt2)
      , (MoveW, Fixed2 (-1) 0)
      , (MoveSW, Fixed2 (-invSqrt2) (-invSqrt2))
      , (MoveS, Fixed2 0 (-1))
      , (MoveSE, Fixed2 invSqrt2 (-invSqrt2))
      ]

invSqrt2 :: Fixed
invSqrt2 = sqrt 0.5

--------------------------------------------------------------------------------
-- Overseer move candidates

data MoveCandidate = MoveCandidate
  { mc_pos :: Fixed2
  , mc_vel :: Maybe Fixed2
  -- ^ Currently only used for filtering out obstructed move candidates
  , mc_command :: MoveCommand
  }
  deriving stock (Show)

makeMoveCandidates ::
  LinearDragParams ->
  Fixed2 ->
  Fixed2 ->
  (MoveCandidate, [MoveCandidate])
makeMoveCandidates dragParams initialPos initialVel =
  let makeMove :: (MoveCommand, [MoveCommand]) -> [MoveCandidate]
      makeMove (moveCommand1, moveCommands2) =
        let (pos1, vel1) =
              step1 moveCommand1 (initialPos, initialVel)
            moveCandidates =
              flip map moveCommands2 $ \moveCommand2 ->
                let (pos2, vel2) = step2 moveCommand2 (pos1, vel1)
                in  MoveCandidate
                      { mc_pos = pos2
                      , mc_vel = Just vel2
                      , mc_command = moveCommand1
                      }
        in  moveCandidates
      neutralMoveCandidate =
        let (pos2, vel2) =
              step2 MoveNeutral $ step1 MoveNeutral (initialPos, initialVel)
        in  MoveCandidate
              { mc_pos = pos2
              , mc_vel = Just vel2
              , mc_command = MoveNeutral
              }
  in  ( neutralMoveCandidate
      , concatMap makeMove nonNeutralMoveSequences
      )
 where
  stepTicks = 5
  step time moveCommand =
    forwardSimulateKinematics' dragParams (arrowKeyDirection' moveCommand) time
  step1 moveCommand1 = step (5 * stepTicks) moveCommand1
  step2 moveCommand2 = step (4 * stepTicks) moveCommand2

nonNeutralMoveSequences :: [(MoveCommand, [MoveCommand])]
nonNeutralMoveSequences =
  [ (MoveE, [MoveNE, MoveE, MoveSE])
  , (MoveNE, [MoveN, MoveNE, MoveE])
  , (MoveN, [MoveNW, MoveN, MoveNE])
  , (MoveNW, [MoveW, MoveNW, MoveN])
  , (MoveW, [MoveSW, MoveW, MoveNW])
  , (MoveSW, [MoveW, MoveSW, MoveS])
  , (MoveS, [MoveSW, MoveS, MoveSE])
  , (MoveSE, [MoveS, MoveSE, MoveE])
  ]

--------------------------------------------------------------------------------

obstructionMask ::
  GameParams ->
  [SoftObstacleBotView] ->
  Fixed ->
  Fixed2 ->
  Fixed2 ->
  Bool
obstructionMask params softObstacles enemyProximity testPos testVel
  | Fixed.normLeq testPos (gp_worldRadius - collisionDetectionBuffer) =
      -- Away from world boundary
      case softObstaclePositions of
        [] ->
          True
        [obstaclePos] ->
          let (distance, intoObstacle) =
                Fixed.normAndNormalize $ obstaclePos - testPos
              velIntoObstacle = Fixed.dot intoObstacle testVel
              modifiedRadius =
                gp_softObstacleRadius + velIntoObstacle * velWeight
          in  distance > modifiedRadius
        [obstaclePos1, obstaclePos2] ->
          let (distance1, intoObstacle1) =
                Fixed.normAndNormalize $ obstaclePos1 - testPos
              (distance2, intoObstacle2) =
                Fixed.normAndNormalize $ obstaclePos2 - testPos
              velIntoObstacle1 = Fixed.dot intoObstacle1 testVel
              velIntoObstacle2 = Fixed.dot intoObstacle2 testVel
              obstacleOverlap =
                2 * gp_softObstacleRadius
                  - Fixed.norm (obstaclePos1 - obstaclePos2)
              modifiedRadius1 =
                gp_softObstacleRadius
                  - radiusAttenuation obstacleOverlap
                  + velIntoObstacle1 * velWeight
              modifiedRadius2 =
                gp_softObstacleRadius
                  - radiusAttenuation obstacleOverlap
                  + velIntoObstacle2 * velWeight
          in  distance1 > modifiedRadius1 && distance2 > modifiedRadius2
        _ ->
          False
  | otherwise =
      -- Near world boundary
      case softObstaclePositions of
        [] ->
          let (distanceFromCenter, intoWorldBoundary) =
                Fixed.normAndNormalize testPos
              distanceFromBoundary = gp_worldRadius - distanceFromCenter
              velIntoWorldBoundary = Fixed.dot intoWorldBoundary testVel
              boundaryBufferRadius =
                gp_overseerRadius + velIntoWorldBoundary * velWeight
          in  distanceFromBoundary > boundaryBufferRadius
        [obstaclePos] ->
          let (distanceObstacle, intoObstacle) =
                Fixed.normAndNormalize $ obstaclePos - testPos
              velIntoObstacle = Fixed.dot intoObstacle testVel
              virtualOverlap =
                gp_softObstacleRadius
                  + gp_overseerRadius -- world boundary "radius"
                  - (gp_worldRadius - Fixed.norm obstaclePos)
              modifiedObstacleRadius =
                gp_softObstacleRadius
                  - radiusAttenuation virtualOverlap
                  + velIntoObstacle * velWeight
              (distanceFromCenter, intoWorldBoundary) =
                Fixed.normAndNormalize testPos
              distanceFromBoundary = gp_worldRadius - distanceFromCenter
              velIntoWorldBoundary = Fixed.dot intoWorldBoundary testVel
              boundaryBufferRadius =
                gp_overseerRadius
                  - 0.5 * radiusAttenuation virtualOverlap
                  + velIntoWorldBoundary * velWeight
          in  distanceObstacle > modifiedObstacleRadius
                && distanceFromBoundary > boundaryBufferRadius
        _ ->
          False
 where
  DynamicsParams{..} = gp_dynamics params
  velWeight =
    1.5
      * gp_overseerRadius
      * (gp_overseerDrag / gp_overseerAcceleration)
  collisionDetectionBuffer = 1.5 * gp_overseerRadius

  softObstaclePositions :: [Fixed2]
  softObstaclePositions =
    flip mapMaybe softObstacles $ \SoftObstacleBotView{..} ->
      if Fixed.normLeq
        (sobv_pos - testPos)
        (gp_softObstacleRadius + collisionDetectionBuffer)
        then Just sobv_pos
        else Nothing

  -- Using (0.5*) because it the attenuation is applied to both obstacles.
  -- Using (+ overlap) effectively sets the the overlap to zero, which is a
  -- good reference point for conceptualizing the effective gap between the
  -- obstacles after this radius attenuation.
  radiusAttenuation :: Fixed -> Fixed
  radiusAttenuation overlap
    | overlap < maxOverlap =
        let pathWidth =
              widePathWidth
                - (widePathWidth - narrowPathWidth) * overlap / maxOverlap
        in  0.5 * (overlap + pathWidth)
    | otherwise =
        -- Beyond the maximum overlap, _increase_ effective radius to prevent
        -- overseers from getting stuck in the cleft between two obstacles
        (* 0.5) $
          max (-gp_overseerRadius) $
            narrowPathWidth + overlap - 16 * (overlap - maxOverlap)
   where
    maxOverlap
      | enemyProximity > farDist = highMaxOverlap
      | enemyProximity > nearDist =
          highMaxOverlap
            - (highMaxOverlap - lowMaxOverlap)
              * (enemyProximity - nearDist)
              / (farDist - nearDist)
      | otherwise = lowMaxOverlap
    lowMaxOverlap = 40
    highMaxOverlap = 60
    farDist = 240
    nearDist = 60
    widePathWidth = 64 -- easy to maneouver through
    narrowPathWidth = 32 -- difficult to maneouver through

--------------------------------------------------------------------------------
-- Overseer movement goal

movementPotential :: MoveTarget -> Fixed2 -> Fixed
movementPotential MoveTarget{..} pos =
  let BroadMoveTarget{..} = mt_broad
      steepness =
        case mt_urgency of
          LowUrgency -> 0.1 -- arbitrary
          MediumUrgency -> 0.3 -- arbitrary
          HighUrgency -> 0.5 -- arbitrary
          ReallyHighUrgency -> 0.8 -- arbitrary
  in  (* steepness) $ max 0 $ Fixed.norm (bmt_pos - pos) - bmt_radius

--------------------------------------------------------------------------------
-- Overseer enemy collision avoidance

collisionAvoidancePotential :: GameParams -> RefinedBotView -> Fixed -> Fixed2 -> Fixed
collisionAvoidancePotential params RefinedBotView{..} myHealth pos =
  negate $
    foldl'
      min
      avoidanceRange
      [ droneMinDistance params 1 rbv_enemyDrones pos
      , let steepness = 4 -- probably will need to change if the avoidance range
        in  droneMinDistance params steepness rbv_enemyOrphanedDrones pos
      , enemyOverseerMinDistance params 1 rbv_enemyOverseers myHealth pos
      ]
 where
  avoidanceRange = 120

droneMinDistance :: GameParams -> Fixed -> [DroneBotView] -> Fixed2 -> Fixed
droneMinDistance params steepness enemyDrones myPos =
  foldl' min maxBound $
    map ((* steepness) . droneDistance params myPos) enemyDrones

droneDistance :: GameParams -> Fixed2 -> DroneBotView -> Fixed
droneDistance params testPos enemyDrone =
  let dronePos = dbv_pos enemyDrone
      droneVel = dbv_vel enemyDrone
      predictionTicks = 48
      dronePos' = dronePos + Fixed.map (* predictionTicks) droneVel
      chargeVelocity =
        (* gp_droneDashDeltaVFactor (gp_cast params)) $
          fromIntegral $
            min
              (dbv_chargeTicks enemyDrone)
              (gp_maxDroneDashChargeTicks (gp_cast params))
      chargeRadius =
        -- 0.5 is an arbitrary choice from [0, 1]
        0.5 * predictionTicks * chargeVelocity
  in  max 0 $ lineDistance dronePos dronePos' testPos - chargeRadius

enemyOverseerMinDistance ::
  GameParams ->
  Fixed ->
  [OverseerBotView] ->
  Fixed ->
  Fixed2 ->
  Fixed
enemyOverseerMinDistance params steepness enemyOverseers myHealth myPos =
  foldl' min maxBound $
    map ((* steepness) . overseerDistance myPos) $
      filter
        (\ov -> obv_health ov + 0.33 * maxOverseerHealth > myHealth)
        enemyOverseers
 where
  maxOverseerHealth = gp_overseerMaxHealth (gp_combat params)

overseerDistance :: Fixed2 -> OverseerBotView -> Fixed
overseerDistance testPos enemyOverseer =
  let overseerPos = obv_pos enemyOverseer
      overseerVel = obv_vel enemyOverseer
      predictionTicks = 48
      overseerPos' = overseerPos + Fixed.map (* predictionTicks) overseerVel
  in  lineDistance overseerPos overseerPos' testPos

--------------------------------------------------------------------------------
-- Overseer allied overseer avoidance

alliedOverseerAvoidancePotential :: RefinedBotView -> Fixed2 -> Fixed
alliedOverseerAvoidancePotential RefinedBotView{..} pos =
  (* 0.2) $ -- arbitrary factor
    negate $
      min avoidanceRange $
        let steepness = 2
        in  alliedOverseerMinDistance steepness rbv_alliedOverseers pos
 where
  avoidanceRange = 60

alliedOverseerMinDistance :: Fixed -> [OverseerBotView] -> Fixed2 -> Fixed
alliedOverseerMinDistance steepness alliedOverseers myPos =
  foldl' min maxBound $
    map ((* steepness) . overseerDistance myPos) alliedOverseers

--------------------------------------------------------------------------------
-- Overseer psi-storm avoidance

psiStormAvoidancePotential ::
  GameParams -> OverseerBotView -> [PsiStormBotView] -> Fixed2 -> Fixed
psiStormAvoidancePotential params myOverseer psiStorms pos =
  let psiStormRadius = gp_psiStormRadius (gp_dynamics params)
      overseerRadius = gp_overseerRadius (gp_dynamics params)
      (overlapFracs, distances) =
        unzip $
          flip map psiStorms $ \psiStorm ->
            let distance = Fixed.norm (psbv_pos psiStorm - pos)
                overlap =
                  max 0 . min 1 $
                    (psiStormRadius + overseerRadius - distance)
                      / (2 * overseerRadius)
            in  (overlap, distance)
      overlapsSqSum = sqrt $ foldl' (+) 0 $ map (\x -> x * x) overlapFracs
      myHealth = obv_health myOverseer
      maxHealth = gp_overseerMaxHealth (gp_combat params)
      damage = gp_psiStormDamagePerHit (gp_combat params) * overlapsSqSum
      damagePotentialFactor
        | myHealth > maxHealth - damage = 0
        | myHealth > 2 * damage = damage / (myHealth - damage)
        | otherwise = 5
      damagePotential = damagePotentialFactor * psiStormRadius
      movementIncentivePotential =
        case List.NonEmpty.nonEmpty distances of
          Nothing -> 0
          Just distances' ->
            -min (psiStormRadius + overseerRadius) (minimum distances')
              * damagePotentialFactor
  in  damagePotential + movementIncentivePotential
