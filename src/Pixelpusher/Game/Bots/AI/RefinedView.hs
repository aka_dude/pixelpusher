module Pixelpusher.Game.Bots.AI.RefinedView (
  RefinedBotView (..),
  refineBotView,
) where

import Data.Foldable (foldl')
import Data.List qualified as List
import Data.List.NonEmpty qualified as List (NonEmpty)
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Semigroup.Foldable.Class (Foldable1)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Dynamics.Collisions
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time (Ticks)

import Pixelpusher.Game.Bots.AI.Kinematics

--------------------------------------------------------------------------------

refineBotView ::
  GameParams ->
  BotID ->
  BotView ->
  [PlayerControls] ->
  Maybe RefinedBotView
refineBotView params botID botView delayedControls =
  List.NonEmpty.nonEmpty myOverseers <&> \(myOverseer List.NonEmpty.:| _) ->
    let (myDrones, otherDrones) =
          List.partition
            (\dr -> Just (BotActor botID) == dbv_actorID dr)
            (bve_drones entities)
        myTeam = obv_team myOverseer
        (alliedDrones, allEnemyDrones) =
          List.partition (\dr -> myTeam == dbv_team dr) otherDrones
        (enemyOrphanedDrones, enemyDrones) =
          List.partition dbv_orphaned allEnemyDrones
        (alliedOverseers, enemyOverseers) =
          List.partition (\ov -> myTeam == obv_team ov) otherOverseers
    in  RefinedBotView
          { rbv_alliedFlag =
              case myTeam of
                TeamNW -> bv_nwTeamFlag botView
                TeamSE -> bv_seTeamFlag botView
          , rbv_enemyFlag =
              case myTeam of
                TeamNW -> bv_seTeamFlag botView
                TeamSE -> bv_nwTeamFlag botView
          , rbv_myOverseer =
              stepSelfOverseerKinematics
                params
                (bve_softObstacles entities)
                delayedControls
                myOverseer
          , rbv_myDrones =
              List.NonEmpty.nonEmpty
                (fmap (stepSelfDroneKinematics params delayedControls) myDrones)
                <&> \drones ->
                  (geometricMedian (fmap dbv_pos drones), drones)
          , rbv_alliedOverseers =
              fmap (stepOverseerKinematics params) alliedOverseers
          , rbv_alliedDrones =
              fmap (stepDroneKinematics params) alliedDrones
          , rbv_enemyOverseers =
              fmap (stepOverseerKinematics params) enemyOverseers
          , rbv_enemyDrones =
              fmap (stepDroneKinematics params) enemyDrones
          , rbv_enemyOrphanedDrones =
              fmap (stepDroneKinematics params) enemyOrphanedDrones
          , rbv_softObstacles =
              fmap (stepSoftObstacleKinematics params) (bve_softObstacles entities)
          , rbv_psiStorms = bve_psiStorms entities
          , rbv_alliesAlive = bv_alliesAlive botView
          , rbv_enemiesAlive = bv_enemiesAlive botView
          , rbv_alliedOverseerPositions = bv_alliedOverseerPositions botView
          , rbv_visibleEnemyOverseerPositions = bv_visibleEnemyOverseerPositions botView
          }
 where
  entities = bv_entities botView
  (myOverseers, otherOverseers) =
    List.partition
      (\ov -> BotActor botID == obv_actorID ov)
      (bve_overseers entities)

-- | For factorization of the bot logic.
--
-- A refined version of 'BotView' that takes into account the bot's control
-- history for more accurate forward simulation of the kinematics of the bot's
-- entities.
data RefinedBotView = RefinedBotView
  { rbv_alliedFlag :: FlagBotView
  , rbv_enemyFlag :: FlagBotView
  , rbv_myOverseer :: OverseerBotView
  , rbv_myDrones :: Maybe (Fixed2, List.NonEmpty DroneBotView)
  , rbv_alliedOverseers :: [OverseerBotView]
  , rbv_alliedDrones :: [DroneBotView]
  , rbv_enemyOverseers :: [OverseerBotView]
  , rbv_enemyDrones :: [DroneBotView]
  , rbv_enemyOrphanedDrones :: [DroneBotView]
  , rbv_softObstacles :: [SoftObstacleBotView]
  , rbv_psiStorms :: [PsiStormBotView]
  , rbv_alliesAlive :: Int
  , rbv_enemiesAlive :: Int
  , rbv_alliedOverseerPositions :: [Fixed2]
  , rbv_visibleEnemyOverseerPositions :: [Fixed2]
  }

-- | Predict the future position of a bot's overseer using the bot's queued
-- controls.
stepSelfOverseerKinematics ::
  GameParams ->
  [SoftObstacleBotView] ->
  [PlayerControls] ->
  OverseerBotView ->
  OverseerBotView
stepSelfOverseerKinematics params softObstacles delayedControls overseer =
  fst $
    foldl'
      ( \(overseer', softObstacles') (stepTicks, delayedControl) ->
          let ambientAccel =
                softObstacleAccel
                  (gp_dynamics params)
                  softObstacles'
                  overseer
              softObstacles'' =
                fmap
                  (stepSoftObstacleKinematics' (fromIntegral stepTicks))
                  softObstacles'
              overseer'' =
                forwardSimulateKinematicsWithAccel
                  (overseerDragParams params)
                  ambientAccel
                  (arrowKeyDirection delayedControl)
                  (fromIntegral stepTicks)
                  overseer'
          in  (overseer'', softObstacles'')
      )
      (overseer, softObstacles)
      (takePredictionControls params delayedControls)

softObstacleAccel ::
  DynamicsParams -> [SoftObstacleBotView] -> OverseerBotView -> Fixed2
softObstacleAccel params softObstacles overseer =
  let ovPos = obv_pos overseer
      obstaclePositions = map sobv_pos softObstacles
      ovRadius = gp_overseerRadius params
      obstacleRadius = gp_softObstacleRadius params
  in  foldl' (+) 0 $
        flip map obstaclePositions $ \obstaclePos ->
          let (dist, dir) = Fixed.normAndNormalize (ovPos - obstaclePos)
              magnitude =
                softObstacleReplusion params obstacleRadius ovRadius dist
          in  Fixed.map (* magnitude) dir

-- | Predict the future position of a bot's drones using the bot's queued
-- controls.
--
-- This function is only a rough approximation because it uses a fixed
-- acceleration direction for each control interval.
stepSelfDroneKinematics ::
  GameParams ->
  [PlayerControls] ->
  DroneBotView ->
  DroneBotView
stepSelfDroneKinematics params delayedControls drone =
  foldl'
    ( \drone' (stepTicks, delayedControl) ->
        let accelSign =
              case view control_drone (control_buttons delayedControl) of
                DroneNeutral -> 0
                DroneAttraction -> 1
                DroneRepulsion -> 1
            accelDir =
              Fixed.map (* accelSign) $
                Fixed.normalize $
                  -- To be accurate more, we would need to change the
                  -- acceleration direction as the drone position changes
                  control_worldPos delayedControl - dbv_pos drone
        in  forwardSimulateKinematics
              (droneDragParams params)
              accelDir
              (fromIntegral stepTicks)
              drone'
    )
    drone
    (takePredictionControls params delayedControls)

-- | Take only as many queued controls as allowed by the prediction limit
takePredictionControls :: GameParams -> [a] -> [(Ticks, a)]
takePredictionControls params = go totalTicks
 where
  totalTicks = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  stepTicks = gp_botControlFrequencyTicks (gp_bots params)
  go ticksRemaining (x : xs)
    | ticksRemaining > stepTicks =
        (stepTicks, x) : go (ticksRemaining - stepTicks) xs
    | otherwise = [(ticksRemaining, x)]
  go _ [] = []

-- | Predict the future position of an overseer using dead reckoning
stepOverseerKinematics :: GameParams -> OverseerBotView -> OverseerBotView
stepOverseerKinematics params overseer =
  let accelDir = obv_accelDir overseer
      duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  forwardSimulateKinematics
        (overseerDragParams params)
        accelDir
        duration
        overseer

-- | Predict the future position of a drone using dead reckoning
stepDroneKinematics :: GameParams -> DroneBotView -> DroneBotView
stepDroneKinematics params drone =
  let accelDir = dbv_accelDir drone
      duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  forwardSimulateKinematics
        (droneDragParams params)
        accelDir
        duration
        drone

-- | Predict the future position of a soft obstacle using dead reckoning
stepSoftObstacleKinematics ::
  GameParams -> SoftObstacleBotView -> SoftObstacleBotView
stepSoftObstacleKinematics params softObstacle =
  let duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  stepSoftObstacleKinematics' duration softObstacle

-- | Predict the future position of a soft obstacle using dead reckoning
stepSoftObstacleKinematics' ::
  Fixed -> SoftObstacleBotView -> SoftObstacleBotView
stepSoftObstacleKinematics' duration softObstacle =
  softObstacle
    { sobv_pos =
        sobv_pos softObstacle + Fixed.map (* duration) (sobv_vel softObstacle)
    }

--------------------------------------------------------------------------------

-- Approximate
geometricMedian :: Foldable1 t => t Fixed2 -> Fixed2
geometricMedian positions =
  let (posSum, posCount) =
        let f (!sumAcc, !countAcc) pos = (sumAcc + pos, countAcc + 1)
        in  foldl' f (0, 0 :: Int) positions
      avgPos = Fixed.map (/ fromIntegral posCount) posSum
  in  geometricMedianIter positions $
        geometricMedianIter positions $
          geometricMedianIter positions avgPos

-- https://en.wikipedia.org/wiki/Geometric_median
-- Weiszfeld's algorithm
geometricMedianIter :: Foldable t => t Fixed2 -> Fixed2 -> Fixed2
geometricMedianIter positions candidatePos =
  let f (!numAcc, !denomAcc) pos =
        let dist = Fixed.norm (pos - candidatePos)
            recipDist = if dist < 0.04 then 25 else recip dist
        in  (numAcc + Fixed.map (* recipDist) pos, denomAcc + recipDist)
      (num, denom) = foldl' f (0, 0) positions
  in  if denom < 1e-3 then candidatePos else Fixed.map (/ denom) num
