{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Eta reduce" #-}

module Pixelpusher.Game.Bots.AI.Kinematics (
  LinearDragParams (..),
  makeLinearDragParams,
  overseerDragParams,
  droneDragParams,
  forwardSimulateKinematics,
  forwardSimulateKinematicsWithAccel,
  forwardSimulateKinematics',
) where

import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Parameters

--------------------------------------------------------------------------------
-- Linear drag

data LinearDragParams = LinearDragParams
  { ldp_terminalVelocity :: Fixed
  , ldp_tickDrag :: Fixed
  , ldp_expRate :: Fixed
  }

makeLinearDragParams :: Fixed -> Fixed -> LinearDragParams
makeLinearDragParams accel tickDrag =
  let expRate = -log (1 - tickDrag)
  in  LinearDragParams
        { ldp_terminalVelocity = accel / tickDrag
        , ldp_tickDrag = tickDrag
        , ldp_expRate = expRate
        }

overseerDragParams :: GameParams -> LinearDragParams
overseerDragParams params =
  let dynParams = gp_dynamics params
  in  makeLinearDragParams
        (gp_overseerAcceleration dynParams)
        (gp_overseerDrag dynParams)

droneDragParams :: GameParams -> LinearDragParams
droneDragParams params =
  let dynParams = gp_dynamics params
  in  makeLinearDragParams
        (gp_droneAcceleration dynParams)
        (gp_droneDrag dynParams)

{-# INLINEABLE forwardSimulateKinematics #-}
forwardSimulateKinematics ::
  HasKinematics a => LinearDragParams -> Fixed2 -> Fixed -> a -> a
forwardSimulateKinematics dragParams accelDir duration a =
  over posVel (forwardSimulateKinematics' dragParams accelDir duration) a

{-# INLINEABLE forwardSimulateKinematicsWithAccel #-}
forwardSimulateKinematicsWithAccel ::
  HasKinematics a => LinearDragParams -> Fixed2 -> Fixed2 -> Fixed -> a -> a
forwardSimulateKinematicsWithAccel dragParams ambientAccel accelDir duration a =
  over
    posVel
    (forwardSimulateKinematics'' dragParams ambientAccel accelDir duration)
    a

forwardSimulateKinematics' ::
  LinearDragParams ->
  Fixed2 ->
  Fixed ->
  (Fixed2, Fixed2) ->
  (Fixed2, Fixed2)
forwardSimulateKinematics' dragParams accelDir duration a =
  over posVel (forwardSimulateKinematics'' dragParams 0 accelDir duration) a

-- With ambient acceleration
forwardSimulateKinematics'' ::
  LinearDragParams ->
  Fixed2 ->
  Fixed2 ->
  Fixed ->
  (Fixed2, Fixed2) ->
  (Fixed2, Fixed2)
forwardSimulateKinematics''
  LinearDragParams{..}
  ambientAccel
  accelDir
  duration
  (pos_init, v_initial) =
    let t = duration
        v0 = v_initial
        vt =
          Fixed.map (* ldp_terminalVelocity) accelDir
            + Fixed.map (/ ldp_tickDrag) ambientAccel
        exp' = Fixed.approxExp (-ldp_expRate * t)
        displacement =
          Fixed.map (* t) vt
            + Fixed.map (\dv -> (dv / ldp_expRate) * (1 - exp')) (v0 - vt)
        velocity = vt + Fixed.map (* exp') (v0 - vt)
    in  (pos_init + displacement, velocity)

-- Helper class
class HasKinematics a where
  posVel :: Lens' a (Fixed2, Fixed2)

instance HasKinematics OverseerBotView where
  posVel f obv =
    f (obv_pos obv, obv_vel obv) <&> \(pos, vel) ->
      obv{obv_pos = pos, obv_vel = vel}

instance HasKinematics DroneBotView where
  posVel f dbv =
    f (dbv_pos dbv, dbv_vel dbv) <&> \(pos, vel) ->
      dbv{dbv_pos = pos, dbv_vel = vel}

instance HasKinematics (Fixed2, Fixed2) where
  posVel = id
