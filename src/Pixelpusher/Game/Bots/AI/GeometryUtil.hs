module Pixelpusher.Game.Bots.AI.GeometryUtil (
  lineDistance,
) where

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed

lineDistance :: Fixed2 -> Fixed2 -> Fixed2 -> Fixed
lineDistance endpoint1 endpoint2 testPoint
  | lineLength < 1e-3 =
      -- (Approximately) degenerate line
      Fixed.norm $ testPoint - Fixed.map (* 0.5) (endpoint1 + endpoint2) -- circular sdf
  | proj < 0 =
      Fixed.norm $ testPoint - endpoint1
  | proj > lineLength =
      Fixed.norm $ testPoint - endpoint2
  | otherwise =
      let Fixed2 dx dy = lineDir
          perpDirLine = Fixed2 (-dy) dx
      in  abs $ Fixed.dot perpDirLine (testPoint - endpoint1)
 where
  (lineLength, lineDir) = Fixed.normAndNormalize (endpoint2 - endpoint1)
  proj = Fixed.dot lineDir (testPoint - endpoint1)
