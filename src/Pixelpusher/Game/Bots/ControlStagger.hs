module Pixelpusher.Game.Bots.ControlStagger (
  shouldControlBot,
) where

import Pixelpusher.Game.BotID
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Time

-- | Whether the bot should run its control logic this frame.
--
-- For the purposes of staggering the execution of bot control logic across
-- multiple frames, in order to save on our per-frame cpu budget. (Exceeding
-- this budget is currently not a concern, but may become a concern later if
-- the control logic becomes more expensive, or if we decide to run more bots
-- in a game.)
shouldControlBot :: GameParams -> Time -> BotID -> Bool
shouldControlBot params time botID =
  atMultipleOf
    (addTicks (fromIntegral (getBotID botID)) time)
    (gp_botControlFrequencyTicks (gp_bots params))
