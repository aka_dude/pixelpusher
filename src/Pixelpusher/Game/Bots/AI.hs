{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Eta reduce" #-}

module Pixelpusher.Game.Bots.AI (
  BotState,
  defaultBotState,
  botControl,
  interpolateMouse,
) where

import Control.Monad.ST
import Data.Maybe (fromMaybe)
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls

import Pixelpusher.Game.Bots.AI.DroneMovement
import Pixelpusher.Game.Bots.AI.MoveTarget
import Pixelpusher.Game.Bots.AI.OverseerMovement
import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.State

--------------------------------------------------------------------------------

botControl ::
  GameParams ->
  MWC.Gen s ->
  BotID ->
  BotView ->
  BotState ->
  [PlayerControls] ->
  ST s (PlayerControls, BotState)
botControl params gen botID botView botState delayedControls =
  case refineBotView params botID botView delayedControls of
    Nothing -> pure (defaultControls, botState)
    Just refinedBotView -> botControl2 params gen botState refinedBotView

data BotState = BotState
  { bs_target :: BotTargetState
  , bs_targetMousePos :: Fixed2
  -- ^ Desired mouse pos
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

defaultBotState :: BotState
defaultBotState = BotState NoTarget 0

botControl2 ::
  GameParams ->
  MWC.Gen s ->
  BotState ->
  RefinedBotView ->
  ST s (PlayerControls, BotState)
botControl2 params gen botState botView@RefinedBotView{..} = do
  newBotTargetState <-
    transitionDroneTarget
      gen
      rbv_myOverseer
      (fmap fst rbv_myDrones)
      rbv_enemyOverseers
      (bs_target botState)
  let !moveTarget = computeMoveGoal params botView
      !rawMove = overseerMovement params botView moveTarget
  move <-
    case rawMove of
      MoveNeutral ->
        -- Randomly move overseers when "stuck"
        MWC.uniformM gen
      otherMove -> pure otherMove
  let targetMousePos =
        fromMaybe (bs_targetMousePos botState) $
          droneMovement params botView newBotTargetState

      controls =
        PlayerControls
          { control_buttons =
              defaultButtonStates
                & control_move .~ move
                & control_drone .~ DroneAttraction
          , control_worldPos = targetMousePos
          }
      newBotState =
        BotState
          { bs_target = newBotTargetState
          , bs_targetMousePos = targetMousePos
          }
  pure (controls, newBotState)

--------------------------------------------------------------------------------
-- Mouse movement interpolation
--
-- To make the bot mouse movement more human-like

interpolateMouse :: Fixed2 -> Fixed2 -> Fixed2
interpolateMouse oldPos targetPos =
  let (distance, direction) = Fixed.normAndNormalize (targetPos - oldPos)
      moveDistance =
        min distance $
          min maxSpeed $
            baseSpeed + distFraction * distance
  in  oldPos + Fixed.map (* moveDistance) direction
 where
  baseSpeed = 0.5
  distFraction = 0.05
  maxSpeed = 50
