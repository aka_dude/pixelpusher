{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.DynamicsComponent (
  DynamicsComponent (..),
  DynamicsComponentVec (..),
  ProtoDynamicsComponent (..),
  newDynamicsComponent,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed
import Pixelpusher.Store.VecStore

--------------------------------------------------------------------------------

-- | Entities that have a spatial presence
data DynamicsComponent = DynamicsComponent
  { dyn_radius :: {-# UNPACK #-} !Fixed
  , dyn_mass :: {-# UNPACK #-} !Fixed
  , dyn_pos :: {-# UNPACK #-} !Fixed2
  , dyn_vel :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev2 :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev3 :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev4 :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev5 :: {-# UNPACK #-} !Fixed2
  , dyn_pos_prev6 :: {-# UNPACK #-} !Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeVecStore ''DynamicsComponent

-- | For making new dynamics components
data ProtoDynamicsComponent = ProtoDynamicsComponent
  { pdyn_radius :: {-# UNPACK #-} !Fixed
  , pdyn_mass :: {-# UNPACK #-} !Fixed
  , pdyn_pos :: {-# UNPACK #-} !Fixed2
  , pdyn_vel :: {-# UNPACK #-} !Fixed2
  }

newDynamicsComponent :: ProtoDynamicsComponent -> DynamicsComponent
newDynamicsComponent ProtoDynamicsComponent{..} =
  DynamicsComponent
    { dyn_radius = pdyn_radius
    , dyn_mass = pdyn_mass
    , dyn_pos = pdyn_pos
    , dyn_vel = pdyn_vel
    , dyn_pos_prev = pdyn_pos
    , dyn_pos_prev2 = pdyn_pos
    , dyn_pos_prev3 = pdyn_pos
    , dyn_pos_prev4 = pdyn_pos
    , dyn_pos_prev5 = pdyn_pos
    , dyn_pos_prev6 = pdyn_pos
    }
