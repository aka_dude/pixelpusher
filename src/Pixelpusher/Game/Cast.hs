{-# LANGUAGE DuplicateRecordFields #-}

module Pixelpusher.Game.Cast (
  runPlayerCasts,
) where

import Control.Monad (guard, void)
import Control.Monad.ST (ST)
import Control.Monad.State.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State.Strict (runStateT)
import Data.Generics.Product.Fields
import Data.Maybe (fromMaybe, maybeToList)
import Data.Traversable (for)
import Data.Tuple (swap)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.DynamicsSystem
import Pixelpusher.Game.EntityStore.Core qualified as Core
import Pixelpusher.Game.MasterMinionSystem
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

-- TODO: Separate cast events into PsiStormCast and DroneDashCast events
runPlayerCasts ::
  CastParams ->
  Core.EntityStore s ->
  Time ->
  WIM.IntMap ActorID (PlayerControls, PlayerControls) ->
  ST s [CastEvent]
runPlayerCasts params store time playerControls =
  Core.traversePlayerCooldownsWithResult store $ \actorID playerStatus -> do
    let cooldowns = ps_cooldowns playerStatus
        noop = pure (cooldowns, mempty)
    case ps_overseerStatus playerStatus of
      OverseerDead{} -> noop
      OverseerAlive overseerID overseerSpawnTime ->
        case WIM.lookup actorID playerControls of
          Nothing -> noop
          Just controls ->
            runPlayerCast
              params
              store
              time
              actorID
              controls
              cooldowns
              overseerID
              overseerSpawnTime

runPlayerCast ::
  CastParams ->
  Core.EntityStore s ->
  Time ->
  ActorID ->
  (PlayerControls, PlayerControls) ->
  PlayerCooldowns ->
  EntityID 'Overseer ->
  Time ->
  ST s (PlayerCooldowns, [CastEvent])
runPlayerCast params store time actorID (prevControls, controls) cooldowns overseerID overseerSpawnTime = do
  let Core.Stores{store_dynamics, store_masterMinions} = Core.dataStores store
      buttons = control_buttons controls
      prevButtons = control_buttons prevControls
  fmap swap $
    flip runStateT cooldowns $ do
      mPsiStorm <- zoom (field @"pc_psiStormCooldownUntil") $
        runMaybeT $ do
          guard $ buttons ^. control_psiStorm == PsiStormOn
          cooldownUntil <- get
          guard $ time >= cooldownUntil
          put $! gp_psiStormCastCooldownTicks params `addTicks` time
          lift $
            lift $
              PsiStormCastEvent_
                <$> runPsiStorm params store time actorID controls overseerID
      overseerDashes <- do
        -- TODO: cleanup
        cooldownUntil1 <- gets pc_overseerDashCooldownUntil1
        cooldownUntil2 <- gets pc_overseerDashCooldownUntil2
        let dashCooldownTicks = gp_overseerDashCooldownTicks params
            ticksSinceLastDash =
              time `diffTime` cooldownUntil2 + dashCooldownTicks
            doubleDashDelayReady =
              not (gp_enableOverseerDoubleDash params)
                || ticksSinceLastDash >= gp_overseerDoubleDashDelayTicks params
                || prevButtons ^. control_overseerDash /= OverseerDashOn
            shouldDash =
              time >= cooldownUntil1
                && buttons ^. control_overseerDash == OverseerDashOn
                && doubleDashDelayReady
            updateCooldown =
              if gp_enableOverseerDoubleDash params
                then do
                  field @"pc_overseerDashCooldownUntil1" .= cooldownUntil2
                  field @"pc_overseerDashCooldownUntil2"
                    .= dashCooldownTicks
                    `addTicks` time
                else
                  field @"pc_overseerDashCooldownUntil1"
                    .= dashCooldownTicks
                    `addTicks` time
        if not shouldDash
          then pure []
          else do
            mOverseerDash <-
              runMaybeT $ do
                guard $ gp_enableOverseerDash params
                guard $ (MoveNeutral /=) $ view control_move buttons
                updateCooldown
                lift . lift $
                  runOverseerDash params store_dynamics controls overseerID
            pure $ maybeToList mOverseerDash
      mDroneDashes <-
        runMaybeT $ do
          let currentDroneCmd = controls ^. to control_buttons . control_drone
              lastDroneCmd = pc_lastDroneCommand cooldowns
          guard $ lastDroneCmd /= currentDroneCmd
          lift $ field @"pc_lastDroneCommand" .= currentDroneCmd
          lift $ field @"pc_lastDroneCommandInitialTime" .= time
          guard $ lastDroneCmd == DroneNeutral && gp_enableDroneDash params
          let
            lastCommandTime =
              max overseerSpawnTime (pc_lastDroneCommandInitialTime cooldowns)
            chargeTicks =
              min (gp_maxDroneDashChargeTicks params) $
                time `diffTime` lastCommandTime
            baseDeltaV =
              gp_droneDashDeltaVFactor params * fromIntegral chargeTicks
            effectIntensityFrac =
              fromIntegral chargeTicks
                / fromIntegral (gp_maxDroneDashChargeTicks params)
          lift $ do
            field @"pc_lastDroneDash" .= (time, effectIntensityFrac)
            lift $
              runDroneDash
                params
                store_dynamics
                store_masterMinions
                controls
                overseerID
                baseDeltaV
                effectIntensityFrac
      pure $
        maybeToList mPsiStorm ++ overseerDashes ++ fromMaybe [] mDroneDashes

runPsiStorm ::
  CastParams ->
  Core.EntityStore s ->
  Time ->
  ActorID ->
  PlayerControls ->
  EntityID 'Overseer ->
  ST s PsiStormCastEvent
runPsiStorm params store time actorID controls overseerID = do
  let Core.Stores{store_dynamics} = Core.dataStores store
  overseerPos <- readPos store_dynamics overseerID
  let castRange = gp_castRange params
      cursorPos = control_worldPos controls
      deltaPos = cursorPos - overseerPos
      normDeltaPos = Fixed.norm deltaPos
      scale =
        if normDeltaPos <= castRange
          then 1.0
          else castRange / normDeltaPos
      targetPos = Fixed.map (* scale) deltaPos + overseerPos
  void $ Core.addPsiStorm params store time (Just actorID) targetPos
  pure $ PsiStormCastEvent targetPos

runDroneDash ::
  CastParams ->
  DynamicsStore s ->
  MasterMinionStore s ->
  PlayerControls ->
  EntityID 'Overseer ->
  Fixed ->
  Fixed ->
  ST s [CastEvent]
runDroneDash
  params
  dynamicsStore
  masterMinionStore
  controls
  overseerID
  baseDeltaV
  effectIntensityFrac = do
    overseerPos <- readPos dynamicsStore overseerID
    let droneAccelScale =
          case controls ^. to control_buttons . control_drone of
            DroneNeutral -> 0
            DroneAttraction -> 1
            DroneRepulsion -> (-1)
        mousePos = control_worldPos controls
        range = gp_castRange params
        diff = mousePos - overseerPos
        boundedMousePos =
          if Fixed.normLeq diff range
            then mousePos
            else
              let scale = range / Fixed.norm diff
              in  Fixed.map (* scale) diff + overseerPos
    minions <- getMinions masterMinionStore overseerID
    for (WIS.toList minions) $ \droneID -> do
      (pos, vel, radius) <- readPosVelRadius dynamicsStore droneID
      let direction =
            Fixed.normalize $
              Fixed.map (* droneAccelScale) $
                boundedMousePos - pos
          deltaV = Fixed.map (* baseDeltaV) direction
      modifyVelocity dynamicsStore droneID $ (+) deltaV
      pure $
        DashCastEvent_
          DashCastEvent
            { dce_pos = pos
            , dce_vel = vel
            , dce_entityRadius = radius
            , dce_direction = direction
            , dce_dashDeltaV = baseDeltaV
            , dce_effectsMagnitudeFrac = effectIntensityFrac
            , dce_dashType = DroneDash
            }

runOverseerDash ::
  CastParams ->
  DynamicsStore s ->
  PlayerControls ->
  EntityID 'Overseer ->
  ST s CastEvent
runOverseerDash params dynamicsStore controls overseerID = do
  (pos, vel, radius) <- readPosVelRadius dynamicsStore overseerID
  let direction = arrowKeyDirection controls
      baseDeltaV = gp_overseerDashDeltaV params
      deltaV = Fixed.map (* baseDeltaV) direction
  modifyVelocity dynamicsStore overseerID $ (+) deltaV
  pure $
    DashCastEvent_
      DashCastEvent
        { dce_pos = pos
        , dce_vel = vel
        , dce_entityRadius = radius
        , dce_direction = direction
        , dce_dashDeltaV = baseDeltaV
        , dce_effectsMagnitudeFrac = 1
        , dce_dashType = OverseerDash
        }
