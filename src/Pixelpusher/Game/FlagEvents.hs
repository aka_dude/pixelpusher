module Pixelpusher.Game.FlagEvents (
  FlagEvents (..),
  FlagPickup (..),
  FlagCapture (..),
  FlagRecovery (..),
  FlagType (..),
) where

import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Team

data FlagEvents = FlagEvents
  { flagEvents_pickups :: [FlagPickup]
  , flagEvents_captures :: [FlagCapture]
  , flagEvents_recoveries :: [FlagRecovery]
  }
  deriving (Generic)

data FlagPickup = FlagPickup
  { flagPickup_carrierTeam :: Team
  , flagPickup_actor :: ActorID
  , flagPickup_position :: Fixed2
  -- ^ The position of the flag being picked up
  , flagPickup_type :: FlagType
  }
  deriving (Generic)

data FlagCapture = FlagCapture
  { flagCapture_team :: Team
  -- ^ The team that scored
  , flagCapture_actorID :: ActorID
  , flagCapture_position :: Fixed2
  -- ^ The position of the overseer capturing the flag
  , flagCapture_velocity :: Fixed2
  -- ^ The velocity of the overseer capturing the flag
  }
  deriving (Generic)

data FlagRecovery = FlagRecovery
  { flagRecovery_team :: Team
  -- ^ The team that recovered their flag
  , flagRecovery_actorID :: Maybe ActorID
  -- ^ The player that recovered the flag, if any
  , flagRecovery_position :: Fixed2
  -- ^ The position of the flag being recovered
  }
  deriving (Generic)

instance Semigroup FlagEvents where
  (<>) (FlagEvents a1 a2 a3) (FlagEvents b1 b2 b3) =
    FlagEvents (a1 <> b1) (a2 <> b2) (a3 <> b3)

instance Monoid FlagEvents where
  mempty = FlagEvents mempty mempty mempty

data FlagType
  = BaseFlagType
  | DroppedFlagType
  | -- | Flag being carried by overseer
    OverseerFlagType
  deriving stock (Eq)
