module Pixelpusher.Game.BotID (
  BotID,
  getBotID,
  BotIDPool,
  newBotIDPool,
  borrowBotID,
  returnBotID,
) where

import Data.Serialize (Serialize)
import Data.Word (Word8)

import Pixelpusher.Custom.SmallBitSet (SmallBitSet)
import Pixelpusher.Custom.SmallBitSet qualified as SmallBitSet
import Pixelpusher.Game.BotID.Internal (BotID (..))
import Pixelpusher.Game.Constants qualified as Constants

getBotID :: BotID -> Word8
getBotID = _getBotID

newtype BotIDPool = BotIDPool SmallBitSet
  deriving newtype (Serialize)

newBotIDPool :: BotIDPool
newBotIDPool = BotIDPool $ SmallBitSet.new Constants.maxBotsPerGame

borrowBotID :: BotIDPool -> (Maybe BotID, BotIDPool)
borrowBotID (BotIDPool bitSet) =
  let (mWord8, bitSet') = SmallBitSet.takeSmallestBit bitSet
  in  (BotID <$> mWord8, BotIDPool bitSet')

returnBotID :: BotID -> BotIDPool -> BotIDPool
returnBotID (BotID word8) (BotIDPool bitSet) =
  BotIDPool $ SmallBitSet.returnBit word8 bitSet
