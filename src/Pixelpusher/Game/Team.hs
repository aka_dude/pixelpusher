{-# LANGUAGE PatternSynonyms #-}

-- | Player teams and team power-scaling
module Pixelpusher.Game.Team (
  Team (TeamNW, TeamSE),
  smallerTeam,
  oppositeTeam,
) where

import Data.Bifunctor (bimap)
import Data.Foldable (toList)
import Data.List (partition)
import Data.Serialize (Serialize)
import Data.Word (Word8)
import Foreign.Storable

--------------------------------------------------------------------------------

newtype Team = Team Word8
  deriving newtype (Eq, Serialize, Storable)

-- TODO: Based on the current spawn positions, the teams should rather be TeamNE
-- and TeamSW

{-# COMPLETE TeamNW, TeamSE :: Team #-}
pattern TeamNW :: Team
pattern TeamNW = Team 0
pattern TeamSE :: Team
pattern TeamSE = Team 1

oppositeTeam :: Team -> Team
oppositeTeam TeamNW = TeamSE
oppositeTeam TeamSE = TeamNW

-- | Returns (n_nw, n_se)
countMembers :: [Team] -> (Int, Int)
countMembers = bimap length length . partition (== TeamNW)

smallerTeam :: Foldable f => f Team -> Maybe Team
smallerTeam teams
  | n_nw > n_se = Just TeamSE
  | n_se > n_nw = Just TeamNW
  | otherwise = Nothing
 where
  (n_nw, n_se) = countMembers $ toList teams
