module Pixelpusher.Game.CollisionEvents (
  CollisionEvent (..),
  FlagCollisionEvent (..),
  BotViewCollisionEvent (..),
  CollisionEvents (..),
  partitionCollisionEvents,
  Collision (..),
  updateCollisionDamage,
  NeutralCollision (..),
  CollisionParticipants (..),
  PsiStormOverlap (..),
  SpawnOverlap (..),
) where

import Data.Serialize (Serialize)
import Data.Strict.Tuple (Pair)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Store.EntityID

--------------------------------------------------------------------------------

data CollisionEvent
  = CE_Collision !(Pair (EntityIDPair 'CombatEntity) Collision)
  | CE_NeutralCollision !NeutralCollision
  | CE_Flag !FlagCollisionEvent
  | CE_PsiStorm !PsiStormOverlap
  | CE_Spawn !SpawnOverlap
  | CE_BotView !BotViewCollisionEvent

data FlagCollisionEvent
  = FlagOverlap !(SEntityID 'FlagEntity) !(SEntityID 'FlagEntity)
  | FlagAreaOverlap !(EntityID 'Flag) !(EntityID 'Overseer)

data BotViewCollisionEvent
  = BotViewCollisionEvent !BotID !(SEntityID 'DynamicsEntity)

data CollisionEvents = CollisionEvents
  { ce_collision :: [Pair (EntityIDPair 'CombatEntity) Collision]
  , ce_neutralCollision :: [NeutralCollision]
  , ce_flag :: [FlagCollisionEvent]
  , ce_psiStorm :: [PsiStormOverlap]
  , ce_spawn :: [SpawnOverlap]
  , ce_botView :: [BotViewCollisionEvent]
  }

emptyCollisionEvents :: CollisionEvents
emptyCollisionEvents = CollisionEvents [] [] [] [] [] []

partitionCollisionEvents :: [CollisionEvent] -> CollisionEvents
partitionCollisionEvents = foldr selectCollisionEvent emptyCollisionEvents

selectCollisionEvent :: CollisionEvent -> CollisionEvents -> CollisionEvents
selectCollisionEvent event events = case event of
  CE_Collision x ->
    events{ce_collision = x : ce_collision events}
  CE_NeutralCollision x ->
    events{ce_neutralCollision = x : ce_neutralCollision events}
  CE_Flag x ->
    events{ce_flag = x : ce_flag events}
  CE_PsiStorm x ->
    events{ce_psiStorm = x : ce_psiStorm events}
  CE_Spawn x ->
    events{ce_spawn = x : ce_spawn events}
  CE_BotView x ->
    events{ce_botView = x : ce_botView events}

--------------------------------------------------------------------------------

data Collision = Collision
  { col_position :: Fixed2
  , col_velocity :: Fixed2
  , col_damage :: Fixed
  , col_direction :: Fixed2
  , col_mActorID1 :: Maybe ActorID
  , col_mActorID2 :: Maybe ActorID
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

updateCollisionDamage :: (Fixed -> Fixed) -> Collision -> Collision
updateCollisionDamage f collision =
  collision{col_damage = f $ col_damage collision}

data NeutralCollision = NeutralCollision
  { nce_position :: Fixed2
  , nce_damageEquivalent :: Fixed
  , nce_participants :: CollisionParticipants
  }

-- This type is pretty specific, but it works for now.
data CollisionParticipants
  = CollisionOverseers
  | CollisionDrones
  | CollisionOverseerDrone

data PsiStormOverlap = PsiStormOverlap
  { pso_entityID :: SEntityID 'CombatEntity
  -- ^ The entity in the psi-storm
  , pso_psiStormEntityID :: EntityID 'PsiStorm
  -- ^ The EntityID of the psi-storm
  , pso_mEntityActorID :: Maybe ActorID
  -- ^ The owner of the entity, if any
  , pso_overlap :: Fixed
  -- ^ The fraction of the entity in the psi-storm
  , pso_entityPos :: Fixed2
  -- ^ Position of the entity
  , pso_entityVel :: Fixed2
  -- ^ Velocity of the entity
  , pso_mPsiStormActorID :: Maybe ActorID
  -- ^ The owner of the psi-storm, if any
  }

newtype SpawnOverlap = SpawnOverlap {so_entityID :: EntityID 'Overseer}
