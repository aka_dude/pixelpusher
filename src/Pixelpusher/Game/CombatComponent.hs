{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.CombatComponent (
  CombatComponent (..),
  CombatComponentVec (..),
  CombatComponentVecSnapshot,
  HealthRegenType (..),
  healthFraction,
  isInvulnerable,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed
import Pixelpusher.Game.Time
import Pixelpusher.Store.VecStore

--------------------------------------------------------------------------------

-- | Entities that can die from accumulating too much damage
data CombatComponent = CombatComponent
  { com_currentHealth :: Fixed
  , com_maxHealth :: Fixed
  , com_healthRegenType :: HealthRegenType
  , com_invulnerableUntil :: Time
  , com_lastRegenInterruptTime :: Time
  , com_lastDamageTime :: Time
  , com_lastDamageAmount :: Fixed
  }
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

data HealthRegenType
  = HealthRegenQuadratic
  | HealthRegenExponential !Fixed
  | HealthRegenLinear !Fixed
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

healthFraction :: CombatComponent -> Fixed
healthFraction comp =
  com_currentHealth comp / com_maxHealth comp

isInvulnerable :: Time -> CombatComponent -> Bool
isInvulnerable time comp = time <= com_invulnerableUntil comp

makeVecStore ''CombatComponent
