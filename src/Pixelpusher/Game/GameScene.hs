-- | Rendering specification
module Pixelpusher.Game.GameScene (
  GameScene (..),
) where

import Pixelpusher.Custom.Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.FlagEvents (FlagType)
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

-- | The part of the rendering specification that is derived purely from the
-- game state.
data GameScene = GameScene
  { scene_gameParams :: GameParams
  , scene_time :: Time
  , scene_roundStart :: Time
  , scene_renderingComps :: RenderingComponents
  , scene_gamePhase :: GamePhase
  , scene_particles :: Particles
  , scene_players :: WIM.IntMap ActorID (Player, PlayerView)
  , scene_flags :: [(Team, Fixed2, FlagType)]
  }
