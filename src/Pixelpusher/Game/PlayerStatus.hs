module Pixelpusher.Game.PlayerStatus (
  PlayerStatus (..),
  OverseerStatus (..),
  PlayerView (..),
  OverseerView (..),
  OverseerViewAlive (..),
  OverseerViewDead (..),
  overseerViewPosition,
  overseerViewHasFlag,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Game.Cooldowns
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

--------------------------------------------------------------------------------

-- | Player-related game state.
data PlayerStatus = PlayerStatus
  { ps_team :: Team
  , ps_overseerStatus :: OverseerStatus
  , ps_cooldowns :: PlayerCooldowns
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | The state of a player's overseer.
data OverseerStatus
  = OverseerAlive
      !(EntityID 'Overseer)
      !Time -- the time at which the overseer spawned
  | OverseerDead
      !Time -- the time at which the overseer died
      !Time -- the time at which the overseer should respawn
      !Fixed2 -- the position at which the overseer died
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | A view of the game state of a player.
data PlayerView = PlayerView
  { pv_team :: Team
  , pv_overseerView :: OverseerView
  , pv_cooldowns :: PlayerCooldowns
  }

-- | A view of a player's overseer status for rendering purposes.
data OverseerView
  = OV_Alive OverseerViewAlive
  | OV_Dead OverseerViewDead

data OverseerViewAlive = OverseerViewAlive
  { ova_pos :: Fixed2
  , ova_vel :: Fixed2
  , ova_hasFlag :: Bool
  , ova_healthFraction :: Fixed
  }

data OverseerViewDead = OverseerViewDead
  { -- The time at which the overseer died
    ovd_deathTime :: Time
  , -- The time at which the overseer should respawn
    ovd_respawnTime :: Time
  , -- Last position while alive
    ovd_lastPos :: Fixed2
  }

overseerViewPosition :: OverseerView -> Fixed2
overseerViewPosition = \case
  OV_Alive OverseerViewAlive{ova_pos} -> ova_pos
  OV_Dead OverseerViewDead{ovd_lastPos} -> ovd_lastPos

overseerViewHasFlag :: OverseerView -> Bool
overseerViewHasFlag = \case
  OV_Alive OverseerViewAlive{ova_hasFlag} -> ova_hasFlag
  OV_Dead{} -> False
