-- | This module specifies the command line interface to the server.
module Server.Init (
  ServerCommand (..),
  StaticServerConfig (..),
  parseArgs,
) where

import Options.Applicative

import Server.Config.Static

data ServerCommand
  = -- | User-provided parameters may be inconsistent, in which case
    -- we return an error message for the user.
    RunServer (Either String StaticServerConfig)
  | WriteDefaultConfigs

parseArgs :: IO ServerCommand
parseArgs =
  execParser $
    info (helper <*> pServer) $
      header "=<< Pixelpusher Game Server >>="

pServer :: Parser ServerCommand
pServer = pWriteDefaultConfigs <|> pRunServer

pWriteDefaultConfigs :: Parser ServerCommand
pWriteDefaultConfigs =
  flag' WriteDefaultConfigs $
    mconcat
      [ long "write-defaults"
      , short 'd'
      , help "Write out the default configuration files and exit"
      ]

pRunServer :: Parser ServerCommand
pRunServer = RunServer <$> pStaticServerConfig
