{-
The /client send filter/ is a thread through which all outgoing messages to
clients are funneled.

It also handles the server's role in the server-to-client UDP protocol.

Note: The logic in this module is non-explicit, interdependent, and relies on
the internals of the "ClientSendFilterTypes" module. In order to understand
this module, you need to consider it as a whole. Apologies.
-}
{-# LANGUAGE BlockArguments #-}

module Server.Network.ClientSendFilter (
  runClientSendFilter,
) where

import Control.Arrow ((&&&))
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Monad (forM_, forever)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.HashMap.Strict qualified as HM
import Data.HashPSQ (HashPSQ)
import Data.HashPSQ qualified as HashPSQ
import Data.Hashable (Hashable)
import Data.Int (Int16)
import Data.List (foldl')
import Data.Maybe (catMaybes)
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Traversable (for)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import Network.Socket (SockAddr, Socket)
import Network.Socket.ByteString (sendAllTo)

import Pixelpusher.Custom.Serialize
import Pixelpusher.Custom.Util (caseJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Network.Protocol

import Server.Network.ClientSendFilterTypes
import Server.Network.Interface
import Server.Network.Internal
import Server.Network.UserID

--------------------------------------------------------------------------------

data Client send = Client
  { cl_sockAddr :: SockAddr
  -- ^ The client's UDP socket address
  , cl_cmdQueue :: UC.InChan (TCPClientCommand send)
  -- ^ A queue for sending the client commands over TCP
  , cl_protocolState :: ProtocolState send
  }
  deriving stock (Generic)

-- | State for the server's role in the server-to-client UDP protocol.
data ProtocolState send = ProtocolState
  { ps_nextMsgNo :: MsgNo
  -- ^ Increments on each new transmission request
  , ps_nextSendSeqNum :: SeqNum
  -- ^ Like `ps_NextMsgNo` but wraps around
  , ps_msgsPendingAcks :: HM.HashMap SeqNum (MsgNo, Serialized send)
  -- ^ Messages sent to the client awaiting acknowledgment
  , ps_lastAck :: Acks
  -- ^ The most recent set of acknowledgments from the client
  , ps_transmissionQueue :: HashPSQ QueueMsgKey MsgNo (Serialized send)
  -- ^ A queue for outbound messages
  }
  deriving stock (Generic)

initialProtocolState :: ProtocolState send
initialProtocolState =
  ProtocolState
    { ps_nextMsgNo = initialMsgNo
    , ps_nextSendSeqNum = firstSeqNum
    , ps_msgsPendingAcks = HM.empty
    , ps_lastAck = nullAck
    , ps_transmissionQueue = HashPSQ.empty
    }

--------------------------------------------------------------------------------

-- | Run a thread for processing all outgoing messages to clients. Maintains
-- protocol state for sending messages to each client.
runClientSendFilter ::
  (Serialize send) =>
  UC.OutChan (SendFilterCommand send) ->
  Socket ->
  IO a
runClientSendFilter sendQueue udpSocket =
  let initClients = WIM.empty :: WIM.IntMap UserID (Client send)
  in  flip evalStateT initClients $
        forever $
          liftIO (UC.readChan sendQueue) >>= clientSendFilter udpSocket

clientSendFilter ::
  (Serialize send) =>
  Socket ->
  SendFilterCommand send ->
  StateT (WIM.IntMap UserID (Client send)) IO ()
clientSendFilter udpSocket = \case
  Send_OutgoingMsg outgoingMsg -> case outgoingMsg of
    TCPSendTo userID clientCmd ->
      gets (WIM.lookup userID) >>= caseJust \client ->
        liftIO $ UC.writeChan (cl_cmdQueue client) clientCmd
    UDPBroadcast userIDs payload ->
      let binaryPayload = serialize payload
      in  forM_ userIDs $ \userID -> zoom (ix userID) $ do
            sockAddr <- gets cl_sockAddr
            zoom (field @"cl_protocolState") $ do
              -- Send the latest message right away
              field @"ps_nextMsgNo" <%= incrementMsgNo >>= \msgNo ->
                sendUDP udpSocket sockAddr msgNo binaryPayload
              -- Dequeue up to `maxExtraTransmissionsPerRequest` messages and
              -- send them as well
              msgs <-
                (fmap . map) (view _2 &&& view _3) $
                  field @"ps_transmissionQueue"
                    %%= popMinN maxExtraTransmissionsPerRequest
              for_ msgs $ uncurry $ sendUDP udpSocket sockAddr
  Send_ClientAcks userID ack -> zoom (ix userID . field @"cl_protocolState") $ do
    -- Get acknowledged sequence numbers, as well as those that could have
    -- been acknowledged
    lastAck <- field @"ps_lastAck" <<.= ack
    let ackedSeqNums = diffAck lastAck ack
        observedSeqNums = betweenAcks lastAck ack
    -- Forget acknowledged transmissions
    field @"ps_msgsPendingAcks" %= \pending ->
      foldl' (flip HM.delete) pending ackedSeqNums
    field @"ps_transmissionQueue" %= \psq ->
      let f queue seqNum = HashPSQ.delete (retransmissionKey seqNum) queue
      in  foldl' f psq ackedSeqNums
    -- Check for (potentially) dropped transmissions ...
    -- (this could be implemented more efficiently ...)
    droppedMessages <- zoom (field @"ps_msgsPendingAcks") $
      fmap catMaybes $
        for observedSeqNums $ \obsSeqNum -> do
          let expiredSeqNum = shiftSeqNum (-retransmissionTolerance) obsSeqNum
          (fmap . fmap) (expiredSeqNum,) $ at expiredSeqNum <<.= Nothing
    -- and queue their retransmission.
    for_ droppedMessages $ \(seqNum, (msgNo, msg)) ->
      field @"ps_transmissionQueue"
        %= HashPSQ.insert (retransmissionKey seqNum) msgNo msg
  Send_ConnectionUpdate connUpdate -> case connUpdate of
    ClientJoin userID _ _ sockAddr snapshotQueue -> do
      let newClient = Client sockAddr snapshotQueue initialProtocolState
      modify' $ WIM.insert userID newClient
    ClientLeave userID _ ->
      modify' $ WIM.delete userID

sendUDP ::
  (Serialize send) =>
  Socket ->
  SockAddr ->
  MsgNo ->
  Serialized send ->
  StateT (ProtocolState send) IO ()
sendUDP udpSocket sockAddr msgNo msg = do
  sendSeqNum <- field @"ps_nextSendSeqNum" <<%= incrementSeqNum
  field @"ps_msgsPendingAcks" %= HM.insert sendSeqNum (msgNo, msg)
  let bytes = Serialize.encode $ ServerUDPMsg sendSeqNum msg
  liftIO $ sendAllTo udpSocket bytes sockAddr

--------------------------------------------------------------------------------
-- Constants

maxExtraTransmissionsPerRequest :: Int
maxExtraTransmissionsPerRequest = 2

retransmissionTolerance :: Int16
retransmissionTolerance = 3 -- arbitrary

--------------------------------------------------------------------------------
-- Helpers

popMinN ::
  (Hashable k, Ord k, Ord p) =>
  Int ->
  HashPSQ k p v ->
  ([(k, p, v)], HashPSQ k p v)
popMinN n psq =
  if n <= 0
    then ([], psq)
    else case HashPSQ.minView psq of
      Nothing -> ([], psq)
      Just (k, p, v, psq') ->
        let (rs, psq'') = popMinN (n - 1) psq'
            r = (k, p, v)
        in  (r : rs, psq'')
