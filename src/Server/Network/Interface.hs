{-
Types for interacting with the server's networking functionality
-}

module Server.Network.Interface (
  IncomingMsg (..),
  OutgoingMsg (..),
  TCPClientCommand (..),
) where

import Data.Text (Text)

import Pixelpusher.Game.PlayerName

import Server.Network.UserID (UserID)

--------------------------------------------------------------------------------

data IncomingMsg recv
  = NewConnection !UserID !PlayerName !(Maybe Int)
  | Disconnection !UserID
  | IncomingMessage !UserID recv

data OutgoingMsg send
  = TCPSendTo !UserID (TCPClientCommand send)
  | UDPBroadcast ![UserID] send

data TCPClientCommand send
  = Client_TCPSend send
  | Client_Kick !Text
  | Client_Rejoin !Text
  | Client_ReturnPing !Int
