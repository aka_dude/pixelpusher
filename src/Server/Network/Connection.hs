{-
This module contains the logic for handling incoming TCP connections and
serving them.

All clients join the server by first establishing a TCP connection.
-}
{-# LANGUAGE OverloadedStrings #-}

module Server.Network.Connection (
  ConnectionEnv (..),
  handleConnection,
) where

import Control.Concurrent.Async (async, race, waitAnyCancel)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Concurrent.MVar
import Control.Exception (bracket_, onException)
import Control.Monad (forever, when)
import Data.Functor (void)
import Data.IORef
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Text.Encoding (decodeUtf8)
import Network.Socket
import System.Metrics.Prometheus.Gauge qualified as Gauge

import Pixelpusher.Custom.Clock (getMonotonicTimeNSecInt, threadDelayNSec)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Version qualified as Ver
import Pixelpusher.Network.Constants qualified as NC
import Pixelpusher.Network.Protocol
import Pixelpusher.Network.TCPProtocol (
  ReconnectFlag (..),
  TCPConnection,
  tcpRecvData,
  tcpSendClose,
  tcpSendData,
 )

import Server.Metrics

import Server.Network.Authenticate
import Server.Network.Interface
import Server.Network.Internal
import Server.Network.UserID (UserID, UserIDPool)
import Server.Network.UserID qualified as UserID

--------------------------------------------------------------------------------

data ConnectionEnv recv send = ConnectionEnv
  { connEnv_userIDPool :: UserIDPool
  , connEnv_authStore :: AuthStore SockAddr
  , connEnv_clientInputQueue :: UC.InChan (InputFilterCommand recv send)
  }

-- | Handle incoming connections. This is the entry point through which clients
-- join the server.
handleConnection ::
  (Serialize recv, Serialize send) =>
  ServerMetrics ->
  ConnectionEnv recv send ->
  TCPConnection ->
  IO ()
handleConnection metricsRefs env tcpConn =
  checkVersion tcpConn $
    withClientUDPEndpoint (connEnv_authStore env) tcpConn $ \sockAddr ->
      withUserID (connEnv_userIDPool env) tcpConn $ \userID ->
        Serialize.decode <$> tcpRecvData tcpConn >>= \case
          Left _ -> sendClose "Received corrupted player name and room ID"
          Right (rawPlayerName, mRoomID) ->
            case makePlayerName rawPlayerName of
              Left playerNameErrMsg -> sendClose playerNameErrMsg
              Right playerName ->
                announceConnection env metricsRefs userID playerName mRoomID sockAddr $
                  \cmdQueues -> serveClient env tcpConn userID cmdQueues
 where
  sendClose = tcpSendClose tcpConn DoNotReconnect

-- | Reject connections that don't present a matching version.
checkVersion :: TCPConnection -> IO () -> IO ()
checkVersion tcpConn action = do
  versionMsg <- tcpRecvData tcpConn
  if fmap (Ver.areCompatible C.version) (Ver.fromByteString versionMsg)
    == Right True
    then action
    else
      tcpSendClose tcpConn DoNotReconnect $
        "Version mismatch: expecting " <> decodeUtf8 (Ver.toByteString C.version)

-- | Associate the websocket connection with a remote UDP socket address.
withClientUDPEndpoint ::
  AuthStore SockAddr -> TCPConnection -> (SockAddr -> IO ()) -> IO ()
withClientUDPEndpoint authStore tcpConn action = do
  (sockAddrMVar, token) <- newToken authStore
  let invalidateToken' = invalidateToken authStore token
  result <- flip onException invalidateToken' $ do
    -- Send an authentication token to the client over TCP ...
    tcpSendData tcpConn $ Serialize.encode @AuthToken token
    -- ... and expect its prompt return over UDP
    race
      (threadDelayNSec C.connTimeLimit_ns >> invalidateToken')
      (takeMVar sockAddrMVar)
  either (const sendClose) action result
 where
  sendClose =
    tcpSendClose tcpConn DoNotReconnect "Could not establish UDP connection"

-- | Obtain a `UserID` from the `UserIDPool`.
withUserID :: UserIDPool -> TCPConnection -> (UserID -> IO ()) -> IO ()
withUserID userIDPool tcpConn action =
  UserID.withUserID userIDPool (maybe sendClose action)
 where
  sendClose = tcpSendClose tcpConn DoNotReconnect "Server full"

-- | Send notifications for the connection and disconnection of the client,
-- providing the client's UDP socket address and a means to send the client
-- messages over TCP.
announceConnection ::
  ConnectionEnv recv send ->
  ServerMetrics ->
  UserID ->
  PlayerName ->
  Maybe Int -> -- Room
  SockAddr ->
  ( (UC.InChan (TCPClientCommand send), UC.OutChan (TCPClientCommand send)) ->
    IO ()
  ) ->
  IO ()
announceConnection
  env
  ServerMetrics{sm_connections}
  userID
  playerName
  mRoomID
  sockAddr
  action = do
    cmdQueues@(cmdQueueIn, _) <- UC.newChan
    let notifyConnection =
          UC.writeChan (connEnv_clientInputQueue env) $
            InputFilter_Connection $
              ClientJoin userID playerName mRoomID sockAddr cmdQueueIn
        notifyDisconnection =
          UC.writeChan (connEnv_clientInputQueue env) $
            InputFilter_Connection $
              ClientLeave userID sockAddr
    bracket_ (Gauge.inc sm_connections) (Gauge.dec sm_connections) $
      bracket_ notifyConnection notifyDisconnection $
        action cmdQueues

--------------------------------------------------------------------------------

serveClient ::
  (Serialize recv, Serialize send) =>
  ConnectionEnv recv send ->
  TCPConnection ->
  UserID ->
  (UC.InChan (TCPClientCommand send), UC.OutChan (TCPClientCommand send)) ->
  IO ()
serveClient env tcpConn userID (inCmdQueue, outCmdQueue) = do
  -- We expect regular messages from the client over TCP to show that it is
  -- still alive. We could also take into account UDP messages.
  initLastAliveTime_ns <- getMonotonicTimeNSecInt
  lastAliveTimeRef_ns <- newIORef initLastAliveTime_ns
  let clientInputQueue = connEnv_clientInputQueue env
      threads =
        [ receiveTCP
            clientInputQueue
            inCmdQueue
            tcpConn
            userID
            lastAliveTimeRef_ns
        , sendTCP outCmdQueue tcpConn
        , checkPulse lastAliveTimeRef_ns (initLastAliveTime_ns + 1)
        ]
  void $ waitAnyCancel =<< traverse async threads

-- Note: We must be listening on the TCP connection in order to detect when the
-- client closes their half of the connection.
receiveTCP ::
  (Serialize recv) =>
  UC.InChan (InputFilterCommand recv send) ->
  UC.InChan (TCPClientCommand send) ->
  TCPConnection ->
  UserID ->
  IORef Int ->
  IO ()
receiveTCP clientInputQueue cmdQueue tcpConn userID lastAliveTimeRef_ns =
  forever $
    Serialize.decode <$> tcpRecvData tcpConn >>= \case
      Left _ ->
        UC.writeChan cmdQueue $
          Client_Kick "Received corrupted message over TCP"
      Right clientTCPMsg -> case clientTCPMsg of
        ClientStillAlivePing time -> do
          getMonotonicTimeNSecInt >>= writeIORef lastAliveTimeRef_ns
          UC.writeChan cmdQueue $ Client_ReturnPing time
        ClientTCPMsg_Data msg ->
          UC.writeChan clientInputQueue $ InputFilter_TCPData userID msg

-- | An action that loops as long the client regularly tells us that it is
-- still alive.
checkPulse :: IORef Int -> Int -> IO ()
checkPulse lastAliveTimeRef_ns lastCheckTime_ns = do
  threadDelayNSec NC.stillAliveCheckInterval_ns
  checkTime_ns <- getMonotonicTimeNSecInt
  lastAliveTime_ns <- readIORef lastAliveTimeRef_ns
  when (lastCheckTime_ns <= lastAliveTime_ns) $
    checkPulse lastAliveTimeRef_ns checkTime_ns

sendTCP ::
  forall send.
  (Serialize send) =>
  UC.OutChan (TCPClientCommand send) ->
  TCPConnection ->
  IO ()
sendTCP cmdQueue tcpConn =
  UC.readChan cmdQueue >>= \case
    Client_TCPSend payload -> do
      tcpSendData tcpConn $ Serialize.encode $ ServerTCPMsg_Data payload
      sendTCP cmdQueue tcpConn
    Client_Kick reason ->
      tcpSendClose tcpConn DoNotReconnect reason
    Client_Rejoin reason ->
      tcpSendClose tcpConn Reconnect reason
    Client_ReturnPing time -> do
      tcpSendData tcpConn $ Serialize.encode $ ServerReturnPing @send time
      sendTCP cmdQueue tcpConn
