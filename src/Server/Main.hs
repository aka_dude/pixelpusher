{-# LANGUAGE OverloadedStrings #-}

-- | The entry point of the server
module Server.Main (
  main,
) where

import Control.Concurrent.Async (async, waitAnyCancel, withAsync)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Exception (SomeException, finally, handle, throwIO)
import Control.Monad (forever, unless, void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except
import Data.IORef
import Data.List (intercalate)
import Data.SemVer qualified as SV
import Data.Text.IO qualified as Text
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.Exit (die)
import System.FilePath (takeDirectory)
import System.IO
import System.Log.FastLogger
import System.Log.FastLogger qualified as FastLogger
import System.Metrics.Prometheus qualified as Prometheus
import Toml qualified

import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameEvents (GamePhaseTransition)
import Pixelpusher.Game.GameProtocol (ServerGameMsg)
import Pixelpusher.Game.Parameters

import Server.Config.Dynamic
import Server.Config.Static
import Server.GameManager (GameManagerEnv (..), runGameManager)
import Server.GameManagerCommand
import Server.GameServerID (GameServerID)
import Server.Init
import Server.Logging
import Server.Metrics
import Server.Network

--------------------------------------------------------------------------------

main :: IO ()
main = do
  getFormattedTime <- FastLogger.newTimeCache FastLogger.simpleTimeFormat
  let logType =
        LogFile
          FileLogSpec
            { log_file = "pixelpusher-server.log"
            , log_file_size = 4000 * 1024
            , log_backup_number = 4
            }
          FastLogger.defaultBufSize
  withTimedFastLogger getFormattedTime logType $ \logger ->
    flip finally (writeLog logger "Exiting pixelpusher-server.") $ do
      writeLog logger "Starting pixelpusher-server."
      hSetBuffering stdout NoBuffering
      Text.putStrLn $ "pixelpusher-server " <> SV.toText C.version
      parseArgs >>= \case
        WriteDefaultConfigs -> writeDefaultConfigs
        RunServer mServerParams ->
          case mServerParams of
            Left errMsg -> putStrLn errMsg
            Right staticConfig -> runServer logger staticConfig

--------------------------------------------------------------------------------

writeDefaultConfigs :: IO ()
writeDefaultConfigs = do
  -- TODO: Avoid overwriting existing files
  withWriteFileViaTemp gameParamsFilePath $ \h ->
    Prettyprinter.renderIO h $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode defaultTomlGameParams
  putStrLn $ "Default game parameters written to " <> gameParamsFilePath
  withWriteFileViaTemp dynamicServerConfigFilePath $ \h ->
    Prettyprinter.renderIO h $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode defaultDynamicServerConfigRaw
  putStrLn $
    "Default dynamic server config written to " <> dynamicServerConfigFilePath

gameParamsFilePath :: FilePath
gameParamsFilePath = "pixelpusher-params.toml"

--------------------------------------------------------------------------------

runServer ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO ()
runServer logger staticConfig = do
  -- Create replay folder
  case ssc_replayFile staticConfig of
    Nothing -> pure ()
    Just replayFilePath -> do
      let replayDir = takeDirectory replayFilePath
      createDirectoryIfMissing True replayDir

  -- Load dynamic game parameters
  dynamicConfigRef <-
    newIORef =<< either die pure =<< loadDynamicConfig logger staticConfig

  -- Parse game parameters
  baseGameParams <- either die pure =<< loadGameParameters logger staticConfig

  -- Initialize metrics
  metricsStore <- Prometheus.newStore
  metricsRefs <- initializeServerMetrics metricsStore

  -- Serve prometheus metrics
  let restrictedMetricsStore =
        Prometheus.subset Prometheus.emptyOf metricsStore
  withAsync (serveMetrics restrictedMetricsStore) $ \_ -> do
    -- Spawn threads and set up communication between them
    (incomingMsgQueueIn, incomingMsgQueueOut) <- UC.newChan

    let putIncomingMsg = UC.writeChan incomingMsgQueueIn . GMC_ClientMsg
    (networkServerAsync, putOutgoingMsg) <-
      spawnNetworkServer
        (makeNetworkServerConfig staticConfig dynamicConfigRef)
        metricsRefs
        putIncomingMsg

    let putGameMsg =
          (fmap . fmap) (UC.writeChan incomingMsgQueueIn) GMC_GameServerMsg
        managerEnv =
          makeGameManagerEnv
            logger
            staticConfig
            (readIORef dynamicConfigRef)
            putOutgoingMsg
            putGameMsg
    gameManagerAsync <-
      async $
        runGameManager
          managerEnv
          (readIORef dynamicConfigRef)
          incomingMsgQueueOut
          baseGameParams

    let setGameParams = UC.writeChan incomingMsgQueueIn . GMC_SwitchGameParams
    consoleAsync <-
      async $
        console
          logger
          staticConfig
          (atomicWriteIORef dynamicConfigRef)
          setGameParams

    void $
      reportException logger $
        waitAnyCancel [networkServerAsync, gameManagerAsync, consoleAsync]

loadDynamicConfig ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO (Either String DynamicServerConfig)
loadDynamicConfig logger staticConfig = do
  filePath <-
    case ssc_dynamicConfigFile staticConfig of
      Nothing -> do
        writeLog logger $
          "Using default dynamic config filepath: "
            <> dynamicServerConfigFilePath
        pure dynamicServerConfigFilePath
      Just filePath -> do
        writeLog logger $
          "Using custom dynamic config filepath: " <> toLogStr filePath
        pure filePath

  filePathExists <- doesFileExist filePath
  unless filePathExists $ do
    writeLog
      logger
      "No file found at dynamic config filepath. Writing default configuration to filepath."
    withWriteFileViaTemp filePath $ \h ->
      Prettyprinter.renderIO h $
        Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
          Toml.encode defaultDynamicServerConfigRaw

  parseResult <- Toml.decode <$> readFile' filePath
  case parseResult of
    Toml.Failure errors -> do
      writeLog logger $
        "Error reading dynamic configuration: "
          <> toLogStr (intercalate "; " errors)
      pure $ Left "Error reading dynamic configuration, see logs."
    Toml.Success warnings dynConfigRaw -> do
      unless (null warnings) $
        writeLog logger $
          "Warnings encountered while reading dynamic configuration: "
            <> toLogStr (intercalate "; " warnings)
      pure $! validateDynamicConfig dynConfigRaw

loadGameParameters ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO (Either String BaseGameParams)
loadGameParameters logger staticConfig =
  (fmap . fmap) makeBaseGameParams $ case ssc_gameParamsFile staticConfig of
    Nothing -> do
      -- TODO: Maybe instead look for the game parameters in the default
      -- filepath?
      writeLog logger "Using default game parameters"
      pure $ Right defaultTomlGameParams
    Just filePath ->
      doesFileExist filePath >>= \case
        False -> pure $ Left $ "Could not find/open " <> filePath
        True -> runExceptT $ do
          params <-
            ExceptT $ do
              parseResult <- Toml.decode <$> readFile' filePath
              case parseResult of
                Toml.Failure errors -> do
                  writeLog logger $
                    "Error reading game parameters: "
                      <> toLogStr (intercalate "; " errors)
                  pure $ Left "Error reading game parameters, see logs."
                Toml.Success warnings gameParams -> do
                  unless (null warnings) $ do
                    writeLog logger $
                      "Warnings encountered while reading game parameters: "
                        <> toLogStr (intercalate "; " warnings)
                  pure $ Right gameParams
          liftIO $ putStrLn ("Using game parameters from " <> filePath)
          pure params

-- | A minimal console
console ::
  TimedFastLogger ->
  StaticServerConfig ->
  (DynamicServerConfig -> IO ()) ->
  (BaseGameParams -> IO ()) ->
  IO a
console logger staticConfig setDynamicConfig setGameParams =
  forever $
    getLine >>= \case
      "rgp" -> do
        -- Reload Game Parameters
        loadGameParameters logger staticConfig >>= \case
          Left errMsg -> do
            putStrLn "Could not reload game parameters"
            putStrLn errMsg
          Right gameParams -> do
            setGameParams gameParams
            putStrLn "Reloaded game parameters"
      "rc" -> do
        -- Reload Config
        loadDynamicConfig logger staticConfig >>= \case
          Left errMsg -> do
            putStrLn "Could not reload dynamic configuration"
            putStrLn errMsg
          Right dynConfig -> do
            setDynamicConfig dynConfig
            putStrLn "Reloaded server config"
      _ ->
        putStrLn "Unknown command"

makeNetworkServerConfig ::
  StaticServerConfig -> IORef DynamicServerConfig -> NetworkConfig
makeNetworkServerConfig staticConfig dynamicConfigRef =
  NetworkConfig
    { nc_port = ssc_port staticConfig
    , nc_getMaxConnections = dsc_maxTotalPlayers <$> readIORef dynamicConfigRef
    }

makeGameManagerEnv ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO DynamicServerConfig ->
  (OutgoingMsg ServerGameMsg -> IO ()) ->
  (GameServerID -> Either SomeException [GamePhaseTransition] -> IO ()) ->
  GameManagerEnv
makeGameManagerEnv
  logger
  staticConfig
  getDynamicConfig
  putOutgoingMsg
  putGameMsg =
    GameManagerEnv
      { gme_logger = logger
      , gme_mReplayFilePath = ssc_replayFile staticConfig
      , gme_putOutgoingMsg = putOutgoingMsg
      , gme_putGameMsg = putGameMsg
      , gme_getMaxIdleSeconds = dsc_maxIdleSeconds <$> getDynamicConfig
      }

-- Helper
reportException :: TimedFastLogger -> IO a -> IO a
reportException logger =
  handle $ \(e :: SomeException) -> do
    writeLog logger $ "Unexpected exception: " <> toLogStr (show e)
    throwIO e
