module Server.GameServerCommand (
  GameServerCommand (..),
) where

import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerName (PlayerName)

import Server.Network.UserID (UserID)

data GameServerCommand
  = ServerPlayerJoin !UserID !PlayerName
  | ServerPlayerLeave !UserID
  | -- | Just for playtesting
    ServerSwitchParams BaseGameParams
