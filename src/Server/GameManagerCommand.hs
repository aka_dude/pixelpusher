module Server.GameManagerCommand (
  GameManagerCommand (..),
) where

import Control.Exception (SomeException)

import Pixelpusher.Game.GameEvents (GamePhaseTransition)
import Pixelpusher.Game.GameProtocol (ClientGameMsg)
import Pixelpusher.Game.Parameters (BaseGameParams)

import Server.GameServerID (GameServerID)
import Server.Network.Interface (IncomingMsg)

data GameManagerCommand
  = GMC_ClientMsg (IncomingMsg ClientGameMsg)
  | GMC_GameServerMsg GameServerID (Either SomeException [GamePhaseTransition])
  | -- | For tweaking parameters during playtesting
    GMC_SwitchGameParams BaseGameParams
