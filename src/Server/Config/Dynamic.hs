{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Server.Config.Dynamic (
  DynamicServerConfig (..),
  DynamicServerConfigRaw (..),
  defaultDynamicServerConfigRaw,
  validateDynamicConfig,
  dynamicServerConfigFilePath,
) where

import Data.String (IsString)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Toml qualified
import Toml.FromValue qualified as Toml
import Toml.FromValue.Generic qualified as Toml
import Toml.ToValue qualified as Toml
import Toml.ToValue.Generic qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

-- | Parameters from a reloadable config file
data DynamicServerConfig = DynamicServerConfig
  { dsc_maxTotalPlayers :: Int
  , dsc_maxGameSize :: Word8
  , dsc_targetMaxGameSize :: Word8
  , dsc_targetMinGameSize :: Word8
  , dsc_maxIdleSeconds :: Int
  }

-- | Raw parameters from a reloadable config file
--
-- Note: We parse integral parameters using `Int` and not their final type
-- (e.g. `Word8`) in order to avoid overflow.
data DynamicServerConfigRaw = DynamicServerConfigRaw
  { dscr_maxTotalPlayers :: Int
  , dscr_maxGameSize :: Int
  , dscr_targetMaxGameSize :: Int
  , dscr_targetMinGameSize :: Int
  , dscr_maxIdleSeconds :: Int
  }
  deriving stock (Generic)

instance Toml.FromValue DynamicServerConfigRaw where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue DynamicServerConfigRaw where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable DynamicServerConfigRaw where
  toTable = Toml.genericToTable

defaultDynamicServerConfigRaw :: DynamicServerConfigRaw
defaultDynamicServerConfigRaw =
  DynamicServerConfigRaw
    { -- \| Maximum number of connections
      dscr_maxTotalPlayers = maxPlayersPerGame
    , -- \| Maximum number of players per game (hard limit)
      dscr_maxGameSize = maxPlayersPerGame
    , -- \| Preferred maximum number of players per game
      dscr_targetMaxGameSize = maxPlayersPerGame
    , -- \| Preferred minimum number of players per game
      dscr_targetMinGameSize = 8
    , -- \| Kick players if idle for this many seconds
      dscr_maxIdleSeconds = 60
    }
 where
  maxPlayersPerGame = fromIntegral C.maxPlayersPerGame

validateDynamicConfig ::
  DynamicServerConfigRaw -> Either String DynamicServerConfig
validateDynamicConfig DynamicServerConfigRaw{..} = do
  let maxPlayersPerGame' = fromIntegral C.maxPlayersPerGame
  maxTotalPlayers <-
    validate
      dscr_maxTotalPlayers
      (> 0)
      "maxTotalPlayers must be positive"
  maxGameSize <-
    validate
      dscr_maxGameSize
      (\n -> 0 < n && n <= maxPlayersPerGame')
      ( "maxGameSize must be positive and cannot be more than "
          ++ show C.maxPlayersPerGame
      )
  targetMaxGameSize <-
    validate
      dscr_targetMaxGameSize
      (\n -> 0 < n && n <= maxGameSize)
      "targetMaxGameSize must be positive and cannot be more than maxGameSize"
  targetMinGameSize <-
    validate
      dscr_targetMinGameSize
      (\n -> 0 < n && n <= targetMaxGameSize)
      "targetMinGameSize must be positive and cannot be more than targetMaxGameSize"
  maxIdleSeconds <-
    validate
      dscr_maxIdleSeconds
      (> 0)
      "maxIdleSeconds must be positive"
  pure $!
    DynamicServerConfig
      { dsc_maxTotalPlayers = maxTotalPlayers
      , dsc_maxGameSize = fromIntegral maxGameSize
      , dsc_targetMaxGameSize = fromIntegral targetMaxGameSize
      , dsc_targetMinGameSize = fromIntegral targetMinGameSize
      , dsc_maxIdleSeconds = maxIdleSeconds
      }
 where
  validate :: a -> (a -> Bool) -> String -> Either String a
  validate inputValue isValid errorMessage =
    if isValid inputValue
      then Right inputValue
      else Left $ "Invalid parameters: " ++ errorMessage

dynamicServerConfigFilePath :: IsString a => a
dynamicServerConfigFilePath = "pixelpusher-server-config.toml"
