{-
A /player input buffer/ receives and buffers all player inputs directed to a
game server and releases the inputs when the game server polls for them.

When the buffer receives more than one input set (i.e. `PlayerControls`) for a
player, the newer set is retained and the old set is dropped.

The buffer also implements basic functionality for detecting idle players.
-}

module Server.PlayerInputBuffer (
  PlayerInputBuffer,
  newPlayerInputBuffer,
  takePlayerInputs,
  putPlayerInput,
  addGameServerCommand,
  getIdlePlayers,
) where

import Data.Function (on)
import Data.Generics.Product.Fields
import Data.IORef
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Game.GameProtocol
import Pixelpusher.Game.PlayerControls (
  PlayerControlsTransport,
  defaultControlsTransport,
  getControlsTransportButtons,
 )
import Pixelpusher.Game.Time

import Server.GameServerCommand (GameServerCommand (..))
import Server.Network.UserID (UserID)

--------------------------------------------------------------------------------

newtype PlayerInputBuffer = PlayerInputBuffer (IORef PlayerInputBuffer')

data PlayerInputBuffer' = PlayerInputBuffer'
  { pib_generation :: Generation
  , pib_controlsMap :: Map UserID (Generation, PlayerControlsTransport)
  , pib_joinLeaveCommands :: [GameServerCommand]
  }
  deriving stock (Generic)

type Generation = Int

emptyPlayerInputBuffer' :: PlayerInputBuffer'
emptyPlayerInputBuffer' =
  PlayerInputBuffer'
    { pib_generation = 0
    , pib_controlsMap = Map.empty
    , pib_joinLeaveCommands = []
    }

newPlayerInputBuffer :: IO PlayerInputBuffer
newPlayerInputBuffer = PlayerInputBuffer <$> newIORef emptyPlayerInputBuffer'

takePlayerInputs ::
  PlayerInputBuffer ->
  IO ([GameServerCommand], Map UserID PlayerControlsTransport)
takePlayerInputs (PlayerInputBuffer ref) = do
  buffer <- atomicModifyIORef' ref $ \oldBuffer ->
    let newBuffer =
          PlayerInputBuffer'
            { pib_generation = succ $ pib_generation oldBuffer
            , pib_controlsMap = pib_controlsMap oldBuffer
            , pib_joinLeaveCommands = []
            }
    in  (newBuffer, oldBuffer)
  pure
    ( reverse (pib_joinLeaveCommands buffer)
    , Map.map snd $ pib_controlsMap buffer
    )

putPlayerInput :: PlayerInputBuffer -> UserID -> ClientGameMsg -> IO ()
putPlayerInput (PlayerInputBuffer ref) userID (ClientControls newCtrls) =
  atomicModifyIORef'_ ref $ \buf ->
    buf & field @"pib_controlsMap" . ix userID %~ f (pib_generation buf)
 where
  f gen (oldGen, oldCtrls) =
    if ((==) `on` getControlsTransportButtons) oldCtrls newCtrls
      then (oldGen, newCtrls)
      else (gen, newCtrls)

addGameServerCommand :: PlayerInputBuffer -> GameServerCommand -> IO ()
addGameServerCommand (PlayerInputBuffer ref) cmd =
  atomicModifyIORef'_ ref $ \buf ->
    buf
      & field @"pib_joinLeaveCommands" %~ (cmd :)
      & updateControls (pib_generation buf)
 where
  updateControls gen = case cmd of
    ServerPlayerJoin uid _ ->
      field @"pib_controlsMap" %~ Map.insert uid (gen, defaultControlsTransport)
    ServerPlayerLeave uid ->
      field @"pib_controlsMap" %~ Map.delete uid
    ServerSwitchParams _params ->
      id

getIdlePlayers :: PlayerInputBuffer -> Ticks -> IO [UserID]
getIdlePlayers (PlayerInputBuffer ref) (Ticks idleGenerations) =
  readIORef ref <&> \buf ->
    let oldGen = pib_generation buf - fromIntegral idleGenerations
    in  Map.keys $ Map.filter ((<= oldGen) . fst) $ pib_controlsMap buf

-- Helpers

atomicModifyIORef'_ :: IORef a -> (a -> a) -> IO ()
atomicModifyIORef'_ ref f = atomicModifyIORef' ref $ (,()) . f
