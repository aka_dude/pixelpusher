{-
This module sets up and runs the networking facilities of the server.
-}

module Server.Network (
  NetworkConfig (..),
  spawnNetworkServer,
  -- Re-exports
  IncomingMsg (..),
  OutgoingMsg (..),
  TCPClientCommand (..),
) where

import Control.Concurrent.Async (Async, async, waitAnyCancel)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Monad (forever)
import Data.Functor (void)
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Network.Socket
import Network.Socket.ByteString

import Pixelpusher.Network.Protocol
import Pixelpusher.Network.TCPProtocol

import Server.Metrics

import Server.Network.Authenticate
import Server.Network.ClientInputFilter
import Server.Network.ClientSendFilter
import Server.Network.Connection
import Server.Network.Interface
import Server.Network.Internal
import Server.Network.UserID

--------------------------------------------------------------------------------

data NetworkConfig = NetworkConfig
  { nc_port :: Int
  , nc_getMaxConnections :: IO Int
  }

spawnNetworkServer ::
  forall recv send.
  (Serialize recv, Serialize send) =>
  NetworkConfig ->
  ServerMetrics ->
  (IncomingMsg recv -> IO ()) -> -- Action for delivering incoming messages
  IO
    ( Async () -- Reference to the server thread
    , OutgoingMsg send -> IO () -- Action for accepting outgoing messages
    )
spawnNetworkServer config metricsRefs putIncomingMsg = do
  clientSendQueues <- UC.newChan @(SendFilterCommand send)
  serverAsync <-
    async $ runServer config metricsRefs putIncomingMsg clientSendQueues
  let (clientSendQueueIn, _) = clientSendQueues
      putOutgoingMsg = UC.writeChan clientSendQueueIn . Send_OutgoingMsg
  pure (serverAsync, putOutgoingMsg)

runServer ::
  forall recv send.
  (Serialize recv, Serialize send) =>
  NetworkConfig ->
  ServerMetrics ->
  (IncomingMsg recv -> IO ()) ->
  (UC.InChan (SendFilterCommand send), UC.OutChan (SendFilterCommand send)) ->
  IO ()
runServer config metricsRefs putIncomingMsg clientSendQueues = do
  let port = nc_port config

  udpSocket <- bindUDPSocket port
  userIDPool <- newUserIDPool (nc_getMaxConnections config)
  authStore <- newAuthenticationStore
  (clientInputQueueIn, clientInputQueueOut) <-
    UC.newChan @(InputFilterCommand recv send)

  let connEnv =
        ConnectionEnv
          { connEnv_userIDPool = userIDPool
          , connEnv_authStore = authStore
          , connEnv_clientInputQueue = clientInputQueueIn
          }

  let (clientSendQueueIn, clientSendQueueOut) = clientSendQueues
  asyncs <-
    traverse
      async
      [ runReceiveUDP udpSocket authStore clientInputQueueIn
      , runClientInputFilter
          clientInputQueueOut
          clientSendQueueIn
          putIncomingMsg
      , runClientSendFilter clientSendQueueOut udpSocket
      , runTCPServer Nothing (show port) $ handleConnection metricsRefs connEnv
      ]
  void $ waitAnyCancel asyncs

bindUDPSocket :: Int -> IO Socket
bindUDPSocket port = do
  let hints =
        defaultHints
          { addrSocketType = Datagram
          , addrFamily = AF_INET
          , addrFlags = [AI_PASSIVE]
          }
  addr <- head <$> getAddrInfo (Just hints) Nothing (Just (show port))
  udpSocket <-
    socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
  bind udpSocket (addrAddress addr)
  pure udpSocket

-- | Receive client messages over a UDP socket
runReceiveUDP ::
  forall a recv send.
  Serialize recv =>
  Socket ->
  AuthStore SockAddr ->
  UC.InChan (InputFilterCommand recv send) ->
  IO a
runReceiveUDP udpSocket authStore clientInputQueue = forever $ do
  (payload, sockAddr) <- recvFrom udpSocket 512

  case Serialize.decode @(ClientUDPMsg recv) payload of
    Left _ -> putStrLn $ "Bad msg: " <> show payload
    Right clientMsg -> case clientMsg of
      ClientUDPMsg_AuthToken token ->
        consumeToken authStore token sockAddr
      ClientUDPMsg_Data clientUDPData ->
        UC.writeChan clientInputQueue $
          InputFilter_UDPData sockAddr clientUDPData
