{-# LANGUAGE OverloadedStrings #-}

module Server.Logging (
  writeLog,
) where

import System.Log.FastLogger

-- Default log format
writeLog :: TimedFastLogger -> LogStr -> IO ()
writeLog logger logStr =
  logger $ \timeStr -> toLogStr timeStr <> " " <> logStr <> "\n"
