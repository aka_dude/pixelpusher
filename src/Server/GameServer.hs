{-
A /game server/ is a thread that runs an isolated game session.

It collects and, in effect, timestamps player inputs and broadcasts collection
of inputs to all players.

It also maintains a copy of the game state in order to allow new players to
join after a game has started.

Information external to the game (e.g. player communication) is handled without
informing the game logic.

Also, the game server has a rudimentary implementation for saving replays of
games.
-}
{-# LANGUAGE OverloadedStrings #-}

module Server.GameServer (
  GameServerEnv (..),
  GameServerHandle,
  gsh_close,
  gsh_putPlayerInput,
  gsh_issueCommand,
  spawnGameServer,
) where

import Control.Concurrent (forkIO)
import Control.Exception (
  Exception,
  IOException,
  SomeException,
  handle,
  throwIO,
  throwTo,
 )
import Control.Monad (forM_, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.ST
import Control.Monad.Trans.State.Strict
import Data.ByteString qualified as BS
import Data.Generics.Product.Fields
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
import Data.Maybe (catMaybes, mapMaybe)
import Data.Monoid (Endo (..))
import Data.Serialize qualified as Serialize
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory)
import System.IO
import System.Log.FastLogger

import Pixelpusher.Custom.Clock (runClock)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.SmallList (makeSmallList)
import Pixelpusher.Custom.Util (whenJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameCommand (GameCommand (..))
import Pixelpusher.Game.GameEvents (
  GameEvents (ge_gamePhaseTransitions),
  GamePhaseTransition,
 )
import Pixelpusher.Game.GameProtocol
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls (fromControlsTransport)
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.Time

import Server.GameServerCommand (GameServerCommand (..))
import Server.Logging
import Server.Network.Interface (OutgoingMsg (..), TCPClientCommand (..))
import Server.Network.UserID (UserID)
import Server.PlayerInputBuffer

--------------------------------------------------------------------------------
-- Interface types

data GameServerEnv = GameServerEnv
  { gse_logger :: TimedFastLogger
  , gse_gameParams :: BaseGameParams
  -- ^ These game parameters should only used for game state initialization.
  , gse_putOutgingMsg :: OutgoingMsg ServerGameMsg -> IO ()
  , gse_mReplayFile :: Maybe FilePath
  , gse_getMaxIdleSeconds :: IO Int
  }

data GameServerHandle = GameServerHandle
  { _gsh_close :: IO ()
  , _gsh_putPlayerInput :: UserID -> ClientGameMsg -> IO ()
  , _gsh_issueCommand :: GameServerCommand -> IO ()
  }

gsh_close :: GameServerHandle -> IO ()
gsh_close = _gsh_close

gsh_putPlayerInput :: GameServerHandle -> UserID -> ClientGameMsg -> IO ()
gsh_putPlayerInput = _gsh_putPlayerInput

gsh_issueCommand :: GameServerHandle -> GameServerCommand -> IO ()
gsh_issueCommand = _gsh_issueCommand

data GameServerException = GSE_TooManyGameCommands
  deriving (Show)

instance Exception GameServerException

--------------------------------------------------------------------------------
-- Internal types

-- | An exception for killing game servers
data GameCancelled = GameCancelled
  deriving stock (Show)

instance Exception GameCancelled

-- | Game server internal state
data GameServerState = GameServerState
  { gss_playerIDs :: WIM.IntMap UserID PlayerID
  , gss_playerIDPool :: PlayerIDPool
  }
  deriving stock (Generic)

initialGameServerState :: GameServerState
initialGameServerState =
  GameServerState
    { gss_playerIDs = WIM.empty
    , gss_playerIDPool = newPlayerIDPool C.maxPlayersPerGame
    }

-- | Process `GameServerCommand`s into `GameCommand`s, reporting new players.
processServerCommands ::
  (Monad m) =>
  [GameServerCommand] ->
  StateT GameServerState m ([GameCommand], [(UserID, PlayerID)])
processServerCommands gameServerCmds = do
  (gameCmds, userIDChanges) <-
    unzip . catMaybes <$> mapM processServerCommand gameServerCmds
  -- Simulate the addition and removal of players
  let newPlayers =
        WIM.toList $
          -- Note: The order of composition is crucial. We want simulate the
          -- execution of the game commands in the order they are given to us.
          foldr (\(Endo f) acc -> acc . f) id userIDChanges WIM.empty
  pure (gameCmds, newPlayers)

-- Note: This function should not be used directly.
processServerCommand ::
  (Monad m) =>
  GameServerCommand ->
  StateT
    GameServerState
    m
    ( Maybe
        ( GameCommand
        , Endo (WIM.IntMap UserID PlayerID) -- update to the userIDs
        )
    )
processServerCommand cmd = case cmd of
  ServerPlayerJoin uid name ->
    field @"gss_playerIDPool" %%= borrowPlayerID >>= \case
      Nothing ->
        -- This should not happen. We currently ignore the error, though we
        -- could instead kick the user and log the error.
        pure Nothing
      Just pid -> do
        field @"gss_playerIDs" %= WIM.insert uid pid
        let playerJoin = PlayerJoin pid name Nothing
        pure $ Just (playerJoin, Endo $ WIM.insert uid pid)
  ServerPlayerLeave uid ->
    field @"gss_playerIDs" . at uid <<.= Nothing >>= \case
      Nothing ->
        -- This should not happen. We currently ignore the error.
        pure Nothing
      Just pid -> do
        field @"gss_playerIDPool" %= returnPlayerID pid
        pure $ Just (PlayerLeave pid, Endo $ WIM.delete uid)
  ServerSwitchParams newParams ->
    pure $ Just (SwitchGameParams newParams, Endo id)

--------------------------------------------------------------------------------

spawnGameServer ::
  GameServerEnv ->
  Int -> -- Room ID
  (Either SomeException [GamePhaseTransition] -> IO ()) ->
  IO GameServerHandle
spawnGameServer env roomID putGameMsg = do
  playerInputBuffer <- newPlayerInputBuffer
  threadId <-
    forkIO $
      handle @SomeException (putGameMsg . Left) $ -- Report game server crashes
        handle (\GameCancelled -> pure ()) $ -- Swallow game-cancelling exceptions
          runGameServer env roomID playerInputBuffer (putGameMsg . Right)
  pure $
    GameServerHandle
      { _gsh_close = throwTo threadId GameCancelled
      , _gsh_putPlayerInput = putPlayerInput playerInputBuffer
      , _gsh_issueCommand = addGameServerCommand playerInputBuffer
      }

runGameServer ::
  GameServerEnv ->
  Int -> -- Room ID
  PlayerInputBuffer ->
  ([GamePhaseTransition] -> IO ()) ->
  IO a
runGameServer env roomID playerInputBuffer putGameEvent = do
  withReplayFileHandle (gse_logger env) (gse_mReplayFile env) WriteMode $
    \mReplayHandle -> do
      -- Intialize
      gameState <- initializeGameState $ gse_gameParams env
      -- Record initial state
      whenJust mReplayHandle $ \h ->
        stToIO (freeze gameState) >>= BS.hPut h . Serialize.encode
      -- Loop
      flip evalStateT initialGameServerState $
        runClock C.tickDelay_ns $
          gameServer
            (gse_logger env)
            roomID
            playerInputBuffer
            putGameEvent
            (gse_putOutgingMsg env)
            gameState
            mReplayHandle
            (gse_getMaxIdleSeconds env)

withReplayFileHandle ::
  TimedFastLogger ->
  Maybe FilePath ->
  IOMode ->
  (Maybe Handle -> IO r) ->
  IO r
withReplayFileHandle logger mReplayFilePath mode action =
  case mReplayFilePath of
    Nothing -> action Nothing
    Just filePath ->
      let handler (_ :: IOException) = do
            writeLog logger $
              toLogStr $
                concat
                  [ "Warning: Could not open file '"
                  , filePath
                  , "' for writing replay"
                  ]
            action Nothing
      in  handle handler $ do
            let dir = takeDirectory filePath
            createDirectoryIfMissing True dir
            withFile filePath mode (action . Just)

gameServer ::
  TimedFastLogger ->
  Int -> -- Room ID
  PlayerInputBuffer ->
  ([GamePhaseTransition] -> IO ()) ->
  (OutgoingMsg ServerGameMsg -> IO ()) ->
  GameState RealWorld ->
  Maybe Handle ->
  IO Int ->
  StateT GameServerState IO ()
gameServer
  logger
  roomID
  playerInputBuffer
  putGameEvent
  putMsg
  gameState
  mReplayHandle
  getMaxIdleSeconds = do
    -- Poll for inputs
    (gameServerCmds, userCommands) <-
      liftIO $ takePlayerInputs playerInputBuffer

    -- Process game server commands
    (serverGameCmds, newPlayers) <- processServerCommands gameServerCmds

    userIDMap <- gets gss_playerIDs
    let playerCommands =
          WIM.fromList
            $ mapMaybe -- ignoring errors
              (\(uid, ctrls) -> (,ctrls) <$> WIM.lookup uid userIDMap)
            $ Map.toList userCommands

    -- Drop down to IO for the rest ...
    liftIO $ do
      -- Send delta-updates first to minimize latency
      smallServerGameCmds <-
        maybe (throwIO GSE_TooManyGameCommands) pure $
          makeSmallList serverGameCmds
      oldTime <- stToIO $ getGameTime gameState
      let update =
            GameStateUpdate
              { deltaUpdate_time = nextTick oldTime
              , deltaUpdate_playerControls = makePlayerIDMap32 playerCommands
              , deltaUpdate_gameCommands = smallServerGameCmds
              }
      putMsg $
        UDPBroadcast (WIM.keys userIDMap) $
          ServerGameData $
            ServerDeltaUpdate update

      -- Record delta-update
      whenJust mReplayHandle $ \h -> BS.hPut h $ Serialize.encode update

      -- Step game state, forward game events to manager
      let playerCommands' = WIM.map fromControlsTransport playerCommands
      stToIO (integrateGameState playerCommands' serverGameCmds gameState)
        >>= putGameEvent . ge_gamePhaseTransitions

      -- Send game state snapshots to new players
      whenJust (NE.nonEmpty newPlayers) $ \newPlayers' -> do
        snapshot <- stToIO $ freeze gameState
        forM_ newPlayers' $ \(userID, playerID) ->
          putMsg $
            TCPSendTo userID $
              Client_TCPSend $
                ServerGameData $
                  ServerSnapshot playerID roomID snapshot

      -- Periodically check for idle players
      when (oldTime `atMultipleOf` Ticks (5 * C.tickRate_hz)) $ do
        maxIdleSeconds <- getMaxIdleSeconds
        let idleTicks = Ticks $ C.tickRate_hz * fromIntegral maxIdleSeconds
        mIdlePlayers <-
          NE.nonEmpty <$> getIdlePlayers playerInputBuffer idleTicks
        whenJust mIdlePlayers $ \idlePlayers ->
          forM_ idlePlayers $ \userID -> do
            putMsg $ TCPSendTo userID $ Client_Kick "Player idle"
            writeLog logger $
              "Kicked idle user (userID: " <> toLogStr (show userID) <> ")."
