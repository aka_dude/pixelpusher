{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Server.Metrics (
  ServerMetricsSpec (..),
  ServerMetrics (..),
  initializeServerMetrics,
  serveMetrics,
  prometheusPort,
) where

import Data.ByteString.Builder qualified as Builder
import Data.Kind (Type)
import GHC.TypeLits (Symbol)
import Network.HTTP.Types qualified as HTTP
import Network.Wai qualified as Wai
import Network.Wai.Handler.Warp qualified as Warp
import System.Metrics.Prometheus
import System.Metrics.Prometheus.Export (sampleToPrometheus)
import System.Metrics.Prometheus.Gauge (Gauge)

type ServerMetricsSpec ::
  Symbol -> -- Name
  Symbol -> -- Help
  MetricType ->
  Type -> -- Labels
  Type
data ServerMetricsSpec name help metricType labelType where
  Connections :: ServerMetricsSpec "connections" "" 'GaugeType ()

newtype ServerMetrics = ServerMetrics
  { sm_connections :: Gauge
  }

initializeServerMetrics :: Store ServerMetricsSpec -> IO ServerMetrics
initializeServerMetrics store = do
  sm_connections <- createGauge Connections () store
  pure ServerMetrics{..}

-- Run a web server serving Prometheus metrics
serveMetrics :: Store EmptyMetrics -> IO ()
serveMetrics store =
  Warp.run prometheusPort $ \request respond ->
    if Wai.requestMethod request == HTTP.methodGet
      && Wai.pathInfo request == ["metrics"]
      then do
        prometheusSample <-
          Builder.toLazyByteString . sampleToPrometheus <$> sampleAll store
        respond $
          Wai.responseLBS
            HTTP.status200
            [(HTTP.hContentType, "text/plain; version=0.0.4")]
            prometheusSample
      else
        respond $
          Wai.responseLBS
            HTTP.status404
            [(HTTP.hContentType, "text/plain")]
            "404"

prometheusPort :: Int
prometheusPort = 30300
