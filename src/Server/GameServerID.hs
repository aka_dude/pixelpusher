module Server.GameServerID (
  GameServerID,
  getGameServerID,
  newGameServerID,
) where

import Data.IORef
import System.IO.Unsafe (unsafePerformIO)

import Pixelpusher.Custom.WrappedIntMap qualified as WIM

--------------------------------------------------------------------------------

newtype GameServerID = GameServerID {getGameServerID' :: Int}
  deriving newtype (Eq, Ord)

getGameServerID :: GameServerID -> Int
getGameServerID = getGameServerID'

instance Show GameServerID where
  show (GameServerID i) = show i

instance WIM.IsInt GameServerID where
  toInt = WIM.makeIdentity . getGameServerID'
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . GameServerID
  {-# INLINE fromInt #-}

-- | `GameServerID`s are (practically) unique. Based on the `Data.Unique`
-- module.
newGameServerID :: IO GameServerID
newGameServerID = fmap GameServerID $
  atomicModifyIORef' uniqSource $
    \x -> let z = x + 1 in (z, z)

uniqSource :: IORef Int
uniqSource = unsafePerformIO (newIORef 0)
{-# NOINLINE uniqSource #-}
