module Lens.Micro.Platform.Custom (
  module Lens.Micro.Platform,
  module Lens.Micro.Platform.Custom,
) where

import Control.Monad.State.Class as State
import Control.Monad.Writer.Class as Writer
import Data.Functor.Const
import Lens.Micro
import Lens.Micro.Platform hiding (
  assign,
  modifying,
  (%=),
  (*=),
  (+=),
  (-=),
  (.=),
  (//=),
  (<%=),
  (<.=),
  (<<%=),
  (<<.=),
  (<?=),
  (<~),
  (?=),
 )

-- An inefficient version of `Control.Lens.alongside` ... inefficient because I
-- couldn't understand how Control.Lens does it in one pass.
alongside ::
  Lens s t a b ->
  Lens s' t' a' b' ->
  Lens (s, s') (t, t') (a, a') (b, b')
alongside l l' f (s, s') =
  let a = getConst $ l Const s
      a' = getConst $ l' Const s'
  in  f (a, a') <&> \(b, b') ->
        let t = set l b s
            t' = set l' b' s'
        in  (t, t')
{-# INLINE alongside #-}

--------------------------------------------------------------------------------
-- Copies of stuff from Lens.Micro.Mtl, but with more strictness

-- .=

(.=) :: MonadState s m => ASetter s s a b -> b -> m ()
l .= x = State.modify' (l .~ x)
{-# INLINE (.=) #-}

infix 4 .=

assign :: MonadState s m => ASetter s s a b -> b -> m ()
assign l x = l .= x
{-# INLINE assign #-}

(?=) :: MonadState s m => ASetter s s a (Maybe b) -> b -> m ()
l ?= b = l .= Just b
{-# INLINE (?=) #-}

infix 4 ?=

(<~) :: MonadState s m => ASetter s s a b -> m b -> m ()
l <~ mb = mb >>= (l .=)
{-# INLINE (<~) #-}

infixr 2 <~

-- %=

(%=) :: (MonadState s m) => ASetter s s a b -> (a -> b) -> m ()
l %= f = State.modify' (l %~ f)
{-# INLINE (%=) #-}

infix 4 %=

modifying :: (MonadState s m) => ASetter s s a b -> (a -> b) -> m ()
modifying l f = l %= f
{-# INLINE modifying #-}

(+=) :: (MonadState s m, Num a) => ASetter s s a a -> a -> m ()
l += x = l %= (+ x)
{-# INLINE (+=) #-}

infix 4 +=

(-=) :: (MonadState s m, Num a) => ASetter s s a a -> a -> m ()
l -= x = l %= subtract x
{-# INLINE (-=) #-}

infix 4 -=

(*=) :: (MonadState s m, Num a) => ASetter s s a a -> a -> m ()
l *= x = l %= (* x)
{-# INLINE (*=) #-}

infix 4 *=

(//=) :: (MonadState s m, Fractional a) => ASetter s s a a -> a -> m ()
l //= x = l %= (/ x)
{-# INLINE (//=) #-}

infix 4 //=

-- %%=

(%%=) :: MonadState s m => LensLike ((,) r) s s a b -> (a -> (r, b)) -> m r
l %%= f = State.state $ \s -> let (r, !t) = l f s in (r, t)
{-# INLINE (%%=) #-}

infix 4 %%=

(<%=) :: MonadState s m => LensLike ((,) b) s s a b -> (a -> b) -> m b
l <%= f = l %%= (\a -> (a, a)) . f
{-# INLINE (<%=) #-}

infix 4 <%=

(<<%=) :: MonadState s m => LensLike ((,) a) s s a b -> (a -> b) -> m a
l <<%= f = l %%= (\a -> (a, f a))
{-# INLINE (<<%=) #-}

infix 4 <<%=

(<<.=) :: MonadState s m => LensLike ((,) a) s s a b -> b -> m a
l <<.= b = l %%= (\a -> (a, b))
{-# INLINE (<<.=) #-}

infix 4 <<.=

(<.=) :: MonadState s m => LensLike ((,) b) s s a b -> b -> m b
l <.= b = l <%= const b
{-# INLINE (<.=) #-}

infix 4 <.=

(<?=) :: MonadState s m => LensLike ((,) b) s s a (Maybe b) -> b -> m b
l <?= b = l %%= const (b, Just b)
{-# INLINE (<?=) #-}

infix 4 <?=

-- Writer

scribe :: (MonadWriter t m, Monoid s) => ASetter s t a b -> b -> m ()
scribe l b = tell (set l b mempty)
{-# INLINE scribe #-}
