{-# OPTIONS_GHC -Wno-orphans #-}

module Control.Monad.Writer.Class.Custom (
  ) where

import Control.Monad.Trans.Writer.CPS qualified as CPS
import Control.Monad.Writer.Class

-- | Orphan MonadWriter instance not yet present in the "mtl" package
instance (Monoid w, Monad m) => MonadWriter w (CPS.WriterT w m) where
  writer = CPS.writer
  tell = CPS.tell
  listen = CPS.listen
  pass = CPS.pass
