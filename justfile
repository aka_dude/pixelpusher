format:
    #!/usr/bin/env sh
    set -uo pipefail
    find src ! -name "$(printf "*\n*")" -name '*.hs' |
    while IFS= read -r file
    do
      if ! fourmolu --mode check "$file"; then
        fourmolu -i "$file"
      fi
    done

build2:
    stack install --ghc-options "-O2 -j2 +RTS -A128m -n2m -RTS"

build1:
    stack install --ghc-options "-O1 -j2 +RTS -A128m -n2m -RTS"

build0:
    stack install --ghc-options "-O0 -j2 +RTS -A128m -n2m -RTS"

bench:
    stack bench --ghc-options "-O2 -j2 +RTS -A128m -n2m -RTS"
