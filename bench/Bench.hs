{-# LANGUAGE OverloadedStrings #-}

module Bench (
  main,
) where

import Control.Monad (replicateM, replicateM_)
import Control.Monad.ST
import Control.Monad.Trans.State.Strict
import Criterion.Main
import Data.Maybe (catMaybes)
import Data.Vector.Unboxed qualified as VU
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed2 (Fixed2))
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameCommand
import Pixelpusher.Game.GameScene
import Pixelpusher.Game.GameState
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import System.Random.MWC qualified as MWC

--------------------------------------------------------------------------------

main :: IO ()
main = do
  let playerIDs =
        catMaybes $
          flip evalState (newPlayerIDPool C.maxPlayersPerGame) $
            replicateM 10 (state borrowPlayerID)
  let gameCmds = makeInitialGameCommands playerIDs
      playerControls = makePlayerControls playerIDs
      run = runGame defaultBaseGameParams gameCmds playerControls
  let seconds = 10
      testIters = seconds * fromIntegral C.tickRate_hz
  defaultMain
    [bench "game" $ nf (\iters -> runST $ run iters) testIters]

--------------------------------------------------------------------------------

runGame ::
  BaseGameParams ->
  [GameCommand] ->
  WIM.IntMap PlayerID PlayerControls ->
  Int ->
  ST s Float
runGame baseParams initGameCmds playerControls iters = do
  gen <-
    MWC.initialize $
      VU.fromList
        [149871, 9285, 98753229, 711266, 9185341, 817359827]
  gameState <- initializeGameStateWithGen baseParams gen
  _ <- integrateGameState playerControls initGameCmds gameState
  replicateM_ (iters - 1) $ integrateGameState playerControls [] gameState
  maxCoordinate gameState

-- | An arbitrary function of the game state, used to ensure that the game
-- state is actually computed.
maxCoordinate :: GameState s -> ST s Float
maxCoordinate gameState = do
  gameScene <- getGameScene gameState Nothing
  pure $
    sum $
      toListOf (folded . to rendering_pos . _1) $
        rcs_drones $
          scene_renderingComps gameScene

--------------------------------------------------------------------------------

makeInitialGameCommands :: [PlayerID] -> [GameCommand]
makeInitialGameCommands playerIDs =
  flip map playerIDs $ \playerID ->
    PlayerJoin playerID nullPlayerName (Just TeamSE)

makePlayerControls :: [PlayerID] -> WIM.IntMap PlayerID PlayerControls
makePlayerControls playerIDs =
  WIM.fromList $ zip playerIDs (repeat playerControl)
 where
  playerControl =
    PlayerControls
      { control_buttons =
          defaultButtonStates & control_drone .~ DroneAttraction
      , control_worldPos = Fixed2 0 0
      }
