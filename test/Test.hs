
module Test (
  main,
) where

import Data.Serialize qualified as Serialize
import Data.Word (Word8)
import Test.Hspec as Hspec
import Test.QuickCheck

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))

--------------------------------------------------------------------------------

main :: IO ()
main =
  Hspec.hspec $ do
    describe "PlayerIDMap32" $ do
      it "roundtrips for encode-decode (random)" $
        forAll genPlayerIDMap32 $ \pidMap32 ->
          Right pidMap32 == Serialize.decode (Serialize.encode pidMap32)

genPlayerIDMap32 :: Gen (PlayerIDMap32 Word8)
genPlayerIDMap32 =
  makePlayerIDMap32 . WIM.fromList <$> listOf ((,) <$> genKey <*> genVal)
 where
  genKey = PlayerID . fromIntegral <$> chooseInt (0, 31)
  genVal = arbitrary @Word8
