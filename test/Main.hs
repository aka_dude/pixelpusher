
module Main where

import Test qualified (main)

main :: IO ()
main = Test.main
