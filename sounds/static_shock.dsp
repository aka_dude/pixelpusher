import("stdfaust.lib");
import("zita.dsp");

static_shock(var) =
	sparse_noise(2*60, varNoise)
	: *(en.asr(0, 1, fadeSec, ba.pulsen(ma.SR * durationSec, ma.SR*foreverSec)))
	: click
	: *(0.33)
	<: reverb

	with{
		click(trigger) =
			no.noise * en.ar(attackSec, releaseSec, trigger)
			with{
				attackSec = 0.001;
				releaseSec = attackSec;
			};

		durationSec = 0.0166;
		fadeSec = 0.0166;
		foreverSec = 100;

		// Manually selecting variations. These sounds depend on the implementation
		// of no.noises, so I guess they might change with updates to Faust.
		varNoise = no.noises(6, ba.take(var+1, (0, 1, 4, 5)));

		// Modified no.sparse_noise, taking a no.noise source as an argument
		sparse_noise(f0, noise) = sn
		with {
				saw = os.lf_sawpos(f0);
				sawdiff = saw - saw';
				e = float(noise); // float() keeps 4.656613e-10f scaling here instead of later
				eHeld = e : ba.latch(sawdiff);
				eHeldPos = 0.5 + 0.5 * eHeld;
				crossed = (saw >= eHeldPos) * (saw' < eHeldPos);
				sn = e' * float(crossed);
		};
	};
