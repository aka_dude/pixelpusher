import("stdfaust.lib");
import("zita.dsp");

freq_low = 40;
freq_high = 60;

small_whoosh(baseFreq, var) =
	varNoise
	: fi.bandpass(1, freqLow, freqHigh)
	: *(intensityEnvelope)
	: *(6)
	<: reverb

	with{
		varNoise = no.noises(4, var);

		freqLow = baseFreq + baseFreq*intensityEnvelope;
		freqHigh = freqLow * 1.5;

		intensityEnvelope = en.ar(attackSec, releaseSec, 1)
			with{
				attackSec = 0.05;
				releaseSec = 0.05;
			};
	};
