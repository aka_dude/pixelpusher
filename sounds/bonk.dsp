import("stdfaust.lib");
import("zita.dsp");

freq_low = 200;
freq_high = freq_low*1.5;

style_clear = 0;
style_muffled = 1;

t60s_standard = 1.65;
t60s_short = 0.25;

bonk(
	var,
	type, // 0 for clear, 1 for muffled
	freq
	) =
	strike
	: annulus_resonate(var, freq, t60s)
	: *(envelope)
	: *(0.5)
	: conditionalLowpass
	<: reverb

	with{

	t60s = ba.selector(type, 2, t60s_standard, t60s_short);

	conditionalLowpass(x) = ba.selector(type, 2, x, lowpass(x))
		with{
			lowpass = fi.lowpass(2, freq_low) : *(muffledGain);
			muffledGain = 4.0;
		};

	strike =
		1 :
		ba.impulsify :
		pm.strikeModel(highpassCutoff, lowpassCutoff, sharpness, gain)

		with{
			highpassCutoff = 10;
			lowpassCutoff = 8000;
			sharpness = 0.10;
			gain = 1;
		};

	// to remove the click at the start
	envelope = en.asr(attackSec, sustainLevel, releaseSec, trigger)
		with {
			attackSec = 0.001;
			sustainLevel = 1;
			releaseSec = 0; // irrelevant
			trigger = 1; // no release
		};
};

annulus_resonate_particle(freq) =
	annulus_resonate(0, freq, t60s_standard);

annulus_resonate(var, freq, t60s) =
  _ <:
  par(
		i,
		nModes,
		pm.modeFilter(modesFreqs(i), modesT60s(i), modesGainsNormalized(var, i))
		) :>
  _

  with{
    nModes = 7;

    modesFreqs(i) = freq * modesFreqsRaw(i) / modesFreqsRaw(0);
    modesFreqsRaw(i) = ba.take(i+1, (21, 45, 52, 55, 72, 96, 125));

    modesT60s(i) = t60s * (refFreq / modesFreqs(i)); // for lack of information
    refFreq = 200;

    modesGainsBase(i) = ba.db2linear(80 - ba.take(i+1, (67, 45, 65, 80, 62, 85, 77)));
		modesGainModifiers(var, i) =
			ba.selector(
				var,
				nVariations,
				ba.take(i+1, (1, 1, 1, 0.5, 1, 1, 1)),
				ba.take(i+1, (1, 0.5, 1, 1, 0.5, 1, 1)),
				ba.take(i+1, (0.5, 0.5, 1, 1, 1, 0.5, 1)),
				ba.take(i+1, (1, 0.5, 0.5, 1, 1, 1, 0.5))
				);
		nVariations = 4;
		modesGains(var, i) = modesGainsBase(i) * modesGainModifiers(var, i);
    modesGainsNormalized(var, i) = modesGains(var, i) / gainSum;
    gainSum = sum(i,nModes,modesGains(var, i));
  };
