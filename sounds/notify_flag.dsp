import("stdfaust.lib");

notify_score(chord_var) =
		1 : ba.impulsify :
		chord_var(b_b) :
		*(1.00) <:
		freeverb_demo(0.5, 0.8, 0.8, 0.5);

positive_score(freq, trig) =
		( strike_score(trig, freq)
		+ strike_score(trig, freq*major_second*2)
		+ strike_score(trig, freq*perfect_fifth)
		) / 3;

negative_score(freq, trig) =
		( strike_score(trig, freq*major_second/2)
		+ strike_score(trig, freq/perfect_fourth)
		+ strike_score(trig, freq*perfect_fourth)
		) / 3;

notify_steal(arpeggio_var) =
		1 : ba.impulsify :
		arpeggio_var(b_b) :
		*(1.35) <:
		freeverb_demo(0.5, 0.8, 0.8, 0.5);

positive_steal(freq, trig) =
		( 0.80 * strike_steal(delayInt(0, trig), freq)
		+ 0.80 * strike_steal(delayInt(1, trig), freq*major_second)
		+ 1.00 * strike_steal(delayInt(2, trig), freq*perfect_fifth)
		+ 1.00 * strike_steal(delayInt(3, trig), freq*major_sixth)
		) / 3
		with {
			delaySecs = 0.09;
			delayInt(n) = delay(n*delaySecs);
		};

negative_steal(freq, trig) =
		( 1.00 * strike_steal(delayInt(0, trig), freq*perfect_fifth)
		+ 1.00 * strike_steal(delayInt(1, trig), freq*major_second)
		+ 0.80 * strike_steal(delayInt(2, trig), freq*minor_seventh)
		+ 0.80 * strike_steal(delayInt(3, trig), freq*perfect_fourth)
		) / 3
		with {
			delaySecs = 0.09;
			delayInt(n) = delay(n*delaySecs);
		};

notify_recover(arpeggio_var) =
		1 : ba.impulsify :
		arpeggio_var(b_b) :
		*(1.00) <:
		freeverb_demo(0.5, 0.8, 0.8, 0.5);

positive_recover(freq, trig) =
		( 0.75 * strike_recover(delayInt(0, trig), freq*perfect_fifth)
		+ 0.90 * strike_recover(delayInt(1, trig), freq*major_second*2)
		+ 1.00 * strike_recover(delayInt(2, trig), freq*2)
		) / 3
		with {
			delaySecs = 0.12;
			delayInt(n) = delay(n*delaySecs);
		};

negative_recover(freq, trig) =
		( 0.75 * strike_recover(delayInt(0, trig), freq*perfect_fourth)
		+ 0.90 * strike_recover(delayInt(1, trig), freq*perfect_fifth)
		+ 1.00 * strike_recover(delayInt(2, trig), freq*major_second)
		) / 3
		with {
			delaySecs = 0.12;
			delayInt(n) = delay(n*delaySecs);
		};

b_b = 420/2;

major_second = 1.122462048309373;
perfect_fourth = 1.3348398541700344;
perfect_fifth = 1.4983070768766815;
major_sixth = 1.681792830507429;
minor_seventh = 1.7817974362806785;

strike_score = strike_proto(1.0);
strike_steal = strike_proto(0.6);
strike_recover = strike_proto(1.0);

strike_proto(dilation, trig, f) =
			0.80*base_strike(f, 0.250)
		+ 0.10*base_strike(f*2, 0.185)
		+ 0.10*base_strike(f*12, 0.120)
		+ 0.10*base_strike(f*14, 0.120)
		with {
				base_strike(freq, release) =
					os.pulsetrain(freq, 0.5)
					: fi.lowpass(1, 2000)
					: *(en.ar(0.060*sqrt(dilation), release*dilation, trig));
		};

// Helper
delay(seconds) =
	de.delay(samples, samples)
	with { samples = ba.sec2samp(seconds); };

// Copy of dm.freeverb_demo with knobs converted to arguments
freeverb_demo(knob_damping, knob_combfeed, knob_spatSpread, knob_g) =
		_,_ <: (*(g)*fixedgain,*(g)*fixedgain :
    re.stereo_freeverb(combfeed, allpassfeed, damping, spatSpread)),
    *(1-g), *(1-g) :> _,_
with{
    scaleroom   = 0.28;
    offsetroom  = 0.7;
    allpassfeed = 0.5;
    scaledamp   = 0.4;
    fixedgain   = 0.1;
    origSR = 44100;

		damping = knob_damping*scaledamp*origSR/ma.SR;
    combfeed = knob_combfeed*scaleroom*origSR/ma.SR + offsetroom;
    spatSpread = knob_spatSpread*46*ma.SR/origSR : int;
    g = knob_g;
};
